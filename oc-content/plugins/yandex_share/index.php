<?php
/*
Plugin Name: Yandex share
Plugin URI: https://osclass.pro
Description: Блок кнопок поделиться в соц.сетях от Яндекс
Version: 1.1.2
Author: Osclass
Author URI: https://osclass.pro
Short Name: social-share
Plugin update URI: yandex-share
*/

    function yandex_share($content) {
        $content .= '<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="https://yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,skype"></div>' ;

        return $content ;
    }



    /**
     *  HOOKS
     */
    osc_register_plugin(osc_plugin_path(__FILE__), '');
    osc_add_hook(osc_plugin_path(__FILE__) . '_uninstall', '');

    osc_add_filter('item_description', 'yandex_share');


?>
