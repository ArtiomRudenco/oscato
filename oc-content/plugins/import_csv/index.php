<?php
/*
Plugin Name: Import CSV plugin
Plugin URI: mailto:saboteurdevelop@gmail.com
Description: Custom plugin which make possible import from CSV file.
Version: 1.0.1
Author: Custom plugin
Author URI: mailto:saboteurdevelop@gmail.com
Plugin update URI:
*/


function import_csv_upload() {
    osc_admin_render_plugin('import_csv/admin.php');
}

function import_csv_admin_menu() {
    osc_admin_menu_plugins('Import CSV plugin', osc_admin_render_plugin_url('import_csv/admin.php'), 'import_csv_submenu');
}

osc_register_plugin(osc_plugin_path(__FILE__), 'import_csv_call_after_install') ;
osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'import_csv_upload');
osc_add_hook('admin_menu_init', 'import_csv_admin_menu');
