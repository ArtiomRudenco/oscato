<?php

/**  */
class ImportCSV
{

    /**
     * ImportCSV constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->process($file);
    }


    /**
     * @param $file
     */
    public function process($file)
    {
        $content = [];
        if (!empty($file)) {
            $content = $this->getContent();
        }

        if (!empty($content)) {
            $this->import($content);
        }
    }


    /**
     * @return array
     */
    public function getContent()
    {
        $content = [];
        if (($h = fopen($_FILES["importfile"]["tmp_name"], "r")) !== FALSE) {
            while (($csvData = fgetcsv($h, 0, ";")) !== FALSE) {
                $content[] = $csvData;
            }
            fclose($h);
        }
        return $content;
    }


    /**
     * @param $content
     */
    public function import($content)
    {
        // Create db connection
        $conn = new DBConnectionClass(osc_db_host(), osc_db_user(), osc_db_password(), osc_db_name());
        $c_db = $conn->getOsclassDb();
        $comm = new DBCommandClass($c_db);

        $header = [];

        $contentData = [];
        foreach ($content as $key => $value) {
            foreach ($value as $k => $val) {
                if ($key === 0) {
                    $header[$k] = $val;
                }
                if ($key !== 0) {
                    if (!empty($contentData[$key][$header[$k]])) {
                        $contentData[$key][$header[$k]] .= "," . $value[$k];
                    } else {
                        $contentData[$key][$header[$k]] = $value[$k];
                    }
                }
            }
        }

//        Item should has stats, otherwise it will not be rendered Item addresses on search page.
//            Check if item has no stats - create it with 0 reviews
//        $items = $comm->query("SELECT `pk_i_id` FROM `oc_t_item`")->result();
//        $itemStats = $comm->query("SELECT `fk_i_item_id` FROM `oc_t_item_stats`")->result();
//
//        foreach($items as $item){
//            if(!in_array($item['pk_i_id'], array_column($itemStats, 'fk_i_item_id'))){
//                $comm->query("INSERT INTO `oc_t_item_stats` (`fk_i_item_id`, `i_num_views`, `i_num_spam`, `i_num_repeated`,
//                                                `i_num_bad_classified`, `i_num_offensive`, `i_num_expired`, `i_num_premium_views`, `dt_date`)
//                                    VALUES ('{$item['pk_i_id']}', 0, 0, 0, 0, 0, 0, 0, '2020-01-01');");
//            }
//
//        }




        $uploadedItems = [];
        $notUploadedItems = [];
        foreach ($contentData as $column => $row) {
            $categoryId = $this->getMainCategory($row['Категории'], $row['Главная категория']);
            $additionalCategoryIds = $this->getAdditionalCategories($row['Дополнительные категории']);

            // Start Importing if main category exist
            if (!empty($categoryId[0]['fk_i_category_id'])) {
                $itemId = $row['﻿ID'];
                $isItemExist = $comm->query("SELECT `pk_i_id` FROM `oc_t_item` WHERE `pk_i_id` = '{$itemId}'")->result();
                if (empty($isItemExist[0]) && !empty($itemId)) {
                    $secret = osc_genRandomPassword();
                    $comm->query("INSERT INTO `oc_t_item` (`pk_i_id`, `fk_i_category_id`, `dt_pub_date`, `dt_mod_date`, `i_price`, `fk_c_currency_code`, 
                                `s_contact_name`, `s_contact_email`, `s_ip`, `b_premium`, `b_enabled`, `b_active`, `b_spam`, `s_secret`, `b_show_email`, 
                                `dt_expiration`, `i_categories`)
                                VALUES ({$row['﻿ID']},'{$categoryId[0]['fk_i_category_id']}', NOW(), NOW(), '{$row['Средний чек(для организаций)/Стоимость(для мероприятий)']}', 'RUB','{$row['Тип: Организация/Событие']}','{$row['Емайл']}',  'import_csv_plugin', '0', '1', '1', '0', '{$secret}', '0', '9999-12-31 23:59:59', '{$additionalCategoryIds}')");

                    // Show error if item was not created
                    if(!empty($comm->errorDesc)){
                        $row['errorMessage'] = "Объявление не было загружено. ID: " . $row['﻿ID'];
                        $notUploadedItems[] = $row;
                        continue;
                    }

//                    $itemId = $comm->query("SELECT `pk_i_id` FROM `oc_t_item` WHERE `s_contact_name` = '{$row['Тип: Организация/Событие']}'
//                                                             AND `s_contact_email` = '{$row['Емайл']}'
//                                                             AND `s_ip` = 'import_csv_plugin'
//                                                             AND `i_price` = '{$row['Средний чек(для организаций)/Стоимость(для мероприятий)']}'
//                                                             AND `i_categories` = '{$additionalCategoryIds}'
//                                                             AND `dt_pub_date` >= NOW() - INTERVAL 3 MINUTE")->result();

                    // Filter special characters before execution
                    $row['Название'] = str_ireplace("'", '"', $row['Название']);
                    $row['Описание'] = str_ireplace("'", '"', $row['Описание']);

                    $comm->query("INSERT INTO `oc_t_item_description` (`fk_i_item_id`, `fk_c_locale_code`, `s_title`, `s_description`) 
                                    VALUES ('{$itemId}', 'ru_RU', '{$row['Название']}', '{$row['Описание']}');");

                    $comm->query("INSERT INTO `oc_t_item_stats` (`fk_i_item_id`, `i_num_views`, `i_num_spam`, `i_num_repeated`, 
                                                `i_num_bad_classified`, `i_num_offensive`, `i_num_expired`, `i_num_premium_views`, `dt_date`) 
                                    VALUES ('{$itemId}', 0, 0, 0, 0, 0, 0, 0, '2020-01-01');");

                    $getCountryCode = $comm->query("SELECT `pk_c_code` FROM `oc_t_country` WHERE `s_name` = 'Россия' LIMIT 1")->result();
                    $getCityId = $comm->query("SELECT `pk_i_id` FROM `oc_t_city` WHERE `s_name` = '{$row['Город']}' LIMIT 1")->result();

                    $comm->query("INSERT INTO `oc_t_item_location` (`fk_i_item_id`, `fk_c_country_code`, `s_country`, `s_address`,
                                               `fk_i_city_id`,`s_city`, `s_city_area`) 
                                    VALUES ('{$itemId}','{$getCountryCode[0]['pk_c_code']}','Россия',
                                            '{$row['Адрес']}', '{$getCityId[0]['pk_i_id']}','{$row['Город']}','{$row['Район']}');");

                    $dateEvent = strtotime($row['Дата проведения(для Мероприятий)']) ? strtotime($row['Дата проведения(для Мероприятий)']) : null;
                    $comm->query("INSERT INTO `oc_t_item_meta` (`fk_i_item_id`, `fk_i_field_id`, `s_value`)
                                    VALUES
                                    ('{$itemId}', '2','номер1'),
                                    ('{$itemId}', '3','{$row['Возраст']}'),
                                    ('{$itemId}', '6','{$row['Метро']}'),
                                    ('{$itemId}', '8','{$row['Телефоны']}'),
                                    ('{$itemId}', '9','{$dateEvent}'),
                                    ('{$itemId}', '10','{$row['Емайл']}'),
                                    ('{$itemId}', '13','{$row['Пол']}'),
                                    ('{$itemId}', '15','{$row['Рейтинг']}'),
                                    ('{$itemId}', '16','{$row['Режим работы']}'),
                                    ('{$itemId}', '17','{$row['Средний чек(для организаций)/Стоимость(для мероприятий)']}'),
                                    ('{$itemId}', '20','{$row['Сайт']}'),
                                    ('{$itemId}', '21','{$row['Соцсети']}');");


                    if (!empty($row['Фото(основное)'])) {
                        $addImage = $this->addImage($row['Фото(основное)'], $itemId);
                        if ($addImage === 0) {
                            $row['uploadedPhoto'][] = $row['Фото(основное)'];
                        } else if ($addImage == false && !file_exists(osc_content_path() . 'uploads/temp/' . $row['Фото(основное)'])) {
                            $row['notUploadedPhoto'][] = $row['Фото(основное)'] . " - файл не найден";
                        }
                    } else {
                        $row['imageNotIndicated'][] = "Основное фото не указано.";
                    }

                    if (!empty($row['Фото дополнительное'])) {
                        $images = explode(',', $row['Фото дополнительное']);
                        foreach ($images as $image) {
                            $addImage = $this->addImage($image, $itemId);
                            if ($addImage === 0) {
                                $row['uploadedPhoto'][] = $image;
                            } else if ($addImage == false && !file_exists(osc_content_path() . 'uploads/temp/' . $image)) {
                                $row['notUploadedPhoto'][] = $image . " - файл не найден";
                            }
                        }
                    } else {
                        $row['imageNotIndicated'][] = "Дополнительное фото не указано.";
                    }

                    $row['uploadedPhoto'] = implode(", ", $row['uploadedPhoto']);
                    $row['notUploadedPhoto'] = implode(",<br>", $row['notUploadedPhoto']);
                    $row['imageNotIndicated'] = implode(", ", $row['imageNotIndicated']);

                    $uploadedItems[] = $row;
                } else {
                    // Update existing item

                    $secret = osc_genRandomPassword();
                    $comm->query("UPDATE `oc_t_item` SET 
                                            `fk_i_category_id` = '{$categoryId[0]['fk_i_category_id']}',
                                            `dt_mod_date` = NOW(),
                                            `i_price` = '{$row['Средний чек(для организаций)/Стоимость(для мероприятий)']}',
                                            `fk_c_currency_code` = 'RUB',
                                            `s_contact_name` = '{$row['Тип: Организация/Событие']}',
                                            `s_contact_email` = '{$row['Емайл']}',
                                            `s_ip` = 'import_csv_plugin',
                                            `b_premium` = '0',
                                            `b_enabled` = '1',
                                            `b_active` = '1',
                                            `b_spam` = '0',
                                            `s_secret` = '{$secret}',
                                            `b_show_email` = '0', 
                                            `dt_expiration` = '9999-12-31 23:59:59',
                                            `i_categories` = '{$additionalCategoryIds}'
                                        WHERE `oc_t_item`.`pk_i_id` = '{$itemId}'");

                    // Show error if item was not updated
                    if(!empty($comm->errorDesc)){
                        $row['errorMessage'] = "Не удалось обновить объявление.";
                        $notUploadedItems[] = $row;
                        continue;
                    }

                    // Filter special characters before execution
                    $row['Название'] = str_ireplace("'", '"', $row['Название']);
                    $row['Описание'] = str_ireplace("'", '"', $row['Описание']);

                    $comm->query("UPDATE `oc_t_item_description` SET 
                                            `fk_c_locale_code` = 'ru_RU', 
                                            `s_title` = '{$row['Название']}', 
                                            `s_description` = '{$row['Описание']}'
                                        WHERE `fk_i_item_id` = {$itemId};");


                    $getCountryCode = $comm->query("SELECT `pk_c_code` FROM `oc_t_country` WHERE `s_name` = 'Россия' LIMIT 1")->result();
                    $getCityId = $comm->query("SELECT `pk_i_id` FROM `oc_t_city` WHERE `s_name` = '{$row['Город']}' LIMIT 1")->result();

                    // Show error if item was not updated
                    if(empty($getCityId)){
                        $row['errorMessage'] = "Не удалось обновить адресс - указанный город не найден. ID: " . $itemId;
                        $notUploadedItems[] = $row;
                        continue;
                    }

                    $comm->query("UPDATE `oc_t_item_location` SET 
                                            `fk_c_country_code` = '{$getCountryCode[0]['pk_c_code']}',
                                            `s_country` = 'Россия',
                                            `s_address` = '{$row['Адрес']}',
                                            `fk_i_city_id` ='{$getCityId[0]['pk_i_id']}',
                                            `s_city` ='{$row['Город']}',
                                            `s_city_area` ='{$row['Район']}'
                                        WHERE `fk_i_item_id` = '{$itemId}'");

                    $dateEvent = strtotime($row['Дата проведения(для Мероприятий)']) ? strtotime($row['Дата проведения(для Мероприятий)']) : null;
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = 'номер1' 
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '2'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Возраст']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '3'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Метро']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '6'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Телефоны']}' 
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '8'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = {$dateEvent}
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '9'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Емайл']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '10'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Пол']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '13'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Рейтинг']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '15'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Режим работы']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '16'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Средний чек(для организаций)/Стоимость(для мероприятий)']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '17'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Сайт']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '20'");
                    $comm->query("UPDATE `oc_t_item_meta` SET `s_value` = '{$row['Соцсети']}'
                                        WHERE `oc_t_item_meta` = '{$itemId}' AND `fk_i_field_id` = '21'");

                    $row['errorMessage'] = "Объявление успешно обновлено";
                    $updatedItems[] = $row;
                }
            } else {
                $row['errorMessage'] = "Категории / подкатегории не существует: " . $row['Категории'] . "," . $row['Главная категория'];
                $notUploadedItems[] = $row;
            }
        }


        if (!empty($uploadedItems)) {
            $uploadedItemsCount = count($uploadedItems);
            echo "
                <table class=\"table\" id=\"userTable\">
                <a href='index.php?page=plugins&action=renderplugin&file=import_csv/admin.php'>Вернуться на страницу Импорта</a>
                <h4 style='color: green'>Объявления были успешно загружены (" . $uploadedItemsCount . "):</h4>
                    <td>ID</td>
                    <td>Название</td>
                    <td>Город</td>
                    <td>Адрес</td>
                    <td>Емайл</td>
                    <td>Тип: Организация/Событие</td>
                    <td style='color: green'>Загруженные фото</td>
                    <td style='color: #880000'>Не загруженные фото</td>
                </tr></div>";

            foreach ($uploadedItems as $uploadedItem) {
                if (!empty($uploadedItem)) {
                    echo "
                        <tr>
                            <td>" . $uploadedItem['﻿ID'] . "</td>
                            <td>" . $uploadedItem['Название'] . "</td>
                            <td>" . $uploadedItem['Город'] . "</td>
                            <td>" . $uploadedItem['Адрес'] . "</td>
                            <td>" . $uploadedItem['Емайл'] . "</td>
                            <td>" . $uploadedItem['Тип: Организация/Событие'] . "</td>
                            <td style='color: green'>" . $uploadedItem['uploadedPhoto'] . "</td>
                            <td style='color: #880000'>" . $uploadedItem['notUploadedPhoto'] . "<br>" . $uploadedItem['imageNotIndicated'] . "</td>

                        </tr>";
                }
            }

        }


        if (!empty($updatedItems)) {
            $updatedItemsCount = count($updatedItems);
            echo "
                <table class=\"table content\" id=\"userTable\">
                <h4 style='color: green'>Объявления были успешно обновлены (" . $updatedItemsCount . "):</h4>
                <a href='index.php?page=plugins&action=renderplugin&file=import_csv/admin.php'>Вернуться на страницу Импорта</a>
                <tr>
                    <td>ID</td>
                    <td>Название</td>
                    <td>Город</td>
                    <td>Адрес</td>
                    <td>Емайл</td>
                    <td>Тип: Организация/Событие</td>
                </tr>";

            foreach ($updatedItems as $updatedItem) {
                if (!empty($updatedItem)) {
                    echo "
                        <tr>
                            <td>" . $updatedItem['﻿ID'] . "</td>
                            <td>" . $updatedItem['Название'] . "</td>
                            <td>" . $updatedItem['Город'] . "</td>
                            <td>" . $updatedItem['Адрес'] . "</td>
                            <td>" . $updatedItem['Емайл'] . "</td>
                            <td>" . $updatedItem['Тип: Организация/Событие'] . "</td>
                        </tr>";
                }
            }

        }

        if (!empty($notUploadedItems)) {
            $notUploadedItemsCount = count($notUploadedItems);
            echo "
                <table class=\"table\" id=\"userTable\">
                <h4 style='color: #880000'>Объявления не были загружены (" . $notUploadedItemsCount . "):</h4>
                <a href='index.php?page=plugins&action=renderplugin&file=import_csv/admin.php'>Вернуться на страницу Импорта</a>
                <tr>
                    <td>ID</td>
                    <td>Название</td>
                    <td>Город</td>
                    <td>Адрес</td>
                    <td>Емайл</td>
                    <td>Тип: Организация/Событие</td>
                    <td>Причина</td>
                </tr>";

            foreach ($notUploadedItems as $notUploadedItem) {
                if (!empty($notUploadedItem)) {
                    echo "
                        <tr>
                            <td>" . $notUploadedItem['﻿ID'] . "</td>
                            <td>" . $notUploadedItem['Название'] . "</td>
                            <td>" . $notUploadedItem['Город'] . "</td>
                            <td>" . $notUploadedItem['Адрес'] . "</td>
                            <td>" . $notUploadedItem['Емайл'] . "</td>
                            <td>" . $notUploadedItem['Тип: Организация/Событие'] . "</td>
                            <td>" . $notUploadedItem['errorMessage'] . "</td>
                        </tr>";
                }
            }


        }

    }


    /**
     * @param $subCategory
     * @param $category
     * @return array|mixed|object
     */
    public function getMainCategory($subCategory, $category)
    {
        // Create db connection
        $conn = new DBConnectionClass(osc_db_host(), osc_db_user(), osc_db_password(), osc_db_name());
        $c_db = $conn->getOsclassDb();
        $comm = new DBCommandClass($c_db);

        $categoryNames = implode('","', explode(',', $subCategory));

        //Check subcategories ID, if not exist get subcategories of Main category
        $categoryId = $comm->query("SELECT cd.`fk_i_category_id` FROM `oc_t_category`AS c 
                                        INNER JOIN `oc_t_category_description` AS cd ON c.pk_i_id = cd.fk_i_category_id 
                                        WHERE `cd`.`s_name` = '{$categoryNames}' LIMIT 1;")->result();

        if (empty($categoryId[0]['fk_i_category_id'])) {
            $categoryId = $comm->query("SELECT cd.`fk_i_category_id` FROM `oc_t_category`AS c 
                                        INNER JOIN `oc_t_category_description` AS cd ON c.pk_i_id = cd.fk_i_category_id 
                                        WHERE `cd`.`s_name` = '{$category}'")->result();

        }

        return $categoryId;
    }


    /**
     * @param $categories
     * @return string
     */
    public function getAdditionalCategories($categories)
    {
        // Create db connection
        $conn = new DBConnectionClass(osc_db_host(), osc_db_user(), osc_db_password(), osc_db_name());
        $c_db = $conn->getOsclassDb();
        $comm = new DBCommandClass($c_db);

        $additionalCatNames = $categories ? $categories : "('')";
        $additionalCategoriesNames = str_replace('," ', ',"', '("' . implode('","', explode(',', $categories)) . '")');
        $additionalCategs = $comm->query("SELECT cd.`fk_i_category_id` FROM `oc_t_category`AS c 
                                        INNER JOIN `oc_t_category_description` AS cd ON c.pk_i_id = cd.fk_i_category_id 
                                        WHERE `cd`.`s_name` IN {$additionalCategoriesNames}")->result();

        $additionalCategoryIds = "";
        foreach ($additionalCategs as $category) {
            $additionalCategoryIds .= '\"' . $category['fk_i_category_id'] . '\",';
        }
        if (!empty($additionalCategoryIds)) {
            $additionalCategoryIds = "[" . rtrim($additionalCategoryIds, "\, ") . "]";
        } else {
            $additionalCategoryIds = "";
        }

        return $additionalCategoryIds;
    }


    /**
     * @param $image
     * @param $itemId
     * @return bool
     */
    public function addImage($image, $itemId)
    {
        $result = false;
        $itemActions = new ItemActions;
        $image = str_replace(" ", "", $image);
        // If there is no image extension then look for the image by name and set it
        if (!file_exists(osc_content_path() . 'uploads/temp/' . $image)) {
            $filepath = glob(osc_content_path() . "uploads/temp/" . $image . ".*", GLOB_BRACE);
            if (!empty($filepath)) {
                $ext = substr($filepath[0], strrpos($filepath[0], '.') + 1);
                if (!empty($ext)) {
                    $image = $image . "." . $ext;
                }
            }
        }

        if (file_exists(osc_content_path() . 'uploads/temp/' . $image)) {
            @chmod(osc_content_path() . 'uploads/temp/' . $image, 755);

            // Validate
            if (!$itemActions->checkAllowedExt($image)) {
                return false;
            }
            if (!$itemActions->checkSize($image)) {
                return false;
            }
            $ext = substr($image, strrpos($image, '.') + 1);
            $aItem['photos']['name'][] = $image;
            $aItem['photos']['type'][] = 'image/' . $ext;
            $aItem['photos']['tmp_name'][] = osc_content_path() . 'uploads/temp/' . $image;
            $aItem['photos']['error'][] = UPLOAD_ERR_OK;
            $aItem['photos']['size'][] = 0;

            if (!empty($aItem['photos']['name']) && !empty($aItem['photos']['type']) && !empty($aItem['photos']['tmp_name'])) {
                // Upload item resources
                $result = $itemActions->uploadItemResources($aItem['photos'], $itemId);
            }
        }

        return $result;
    }


}
?>


