<?php
header('Content-Type: text/html; charset=utf-8');
?>
<?php _e('Импорт объявлений CSV', 'import_csv'); ?>
<h2 class="render-title">
    <div class="popup_import">
        <form class="form-horizontal" action="<?php echo osc_admin_render_plugin_url('import_csv/import.php'); ?>"
              method="post" name="csvimport" id="csvimport"
              enctype="multipart/form-data">
            <table class="table">
                <tr>
                    <td>
                        <input type='file' class="btn btn-submit" name="importfile" id="importfile"
                               accept=".csv">
                        <input type="submit" class="btn btn-submit" id="importcsv" name="importcsv" value="Import">
                    </td>
                </tr>
                <tr>
                    <td><h2>Памятка : </h2><br/>
                        <ul>
                            <li>Поле <b>Категории</b> и подкатегории могут содержать по одному значению.</li>
                            <li>Поле <b>Фото</b> должны находится на сервере<b>до</b> импорта в папке <b>oc-content/uploads/temp</b></li>
                            <li>Поле <b>Фото</b> может содержать название фото без расширения</li>
                            <li>Поле <b>Фото(основное)</b> может содержать лишь одно значение</li>
                            <li>Поле <b>Фото дополнительное</b> может содержать одно или несколько значений,
                                разделенных через запятую
                            </li>
                            <li>Поле <b>Фото дополнительное</b> может может повторяться</li>
                            <li><a href="../oc-content/plugins/import_csv/import_example.csv"
                                   download="import_example">Скачать шаблон файла загрузки (CSV)</a></li>
                        </ul>
                    </td>
                </tr>
            </table>
    </div>
    </h2>
    </div>
    <!-- Import form (end) -->
