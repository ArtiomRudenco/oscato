<?php
require_once '../oc-content/plugins/import_csv/import_csv.php';

if (isset($_POST['importcsv']) && !empty($_FILES["importfile"]['size']) && !empty($_FILES["importfile"]['name'])) {
    $importCSV = new ImportCSV($_FILES["importfile"]);
}else if (empty($_FILES["importfile"]['size']) && empty($_FILES['name'])){
    header("Location:" . "index.php?page=plugins&action=renderplugin&file=import_csv/admin.php");
}
