This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

GNU GENERAL PUBLIC LICENSE v3

*********************************************************************
Brought to you by SmaRTeY
*FREE* Osclass "Cookie Consent" Plugin

Cookie Consent on Github:
https://github.com/silktide/cookieconsent2

NOTE: (thanks teseo & dev101 for review)
- Works with both *root* installs as with *sub-folder* installs
- Prefixed functions for best compatability
- index.php in *all* folders to prevend possible directory listing
*********************************************************************
