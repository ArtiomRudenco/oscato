<?php defined('ABS_PATH') or die('Access denied');

class IPAAdmin {
    
    function __construct() {
        osc_add_hook('admin_menu_init', array(&$this, 'adminMenu'));
        osc_add_hook('admin_header', array(&$this, 'adminHeader'));
                
        osc_add_route('ipa-settings', 'ipa-settings', 'ipa-settings', IPA_ADM_PATH . 'settings.php');
    }
    
    function adminMenu() {
            osc_admin_menu_plugins('iPhone Auth', osc_route_admin_url('ipa-settings'), 'ipa-settings-menu');
        }
        
    function adminHeader() {
        $_route = Params::getParam('route');
        
        switch ($_route) {
            case 'ipa-settings':
                osc_enqueue_style('open-sans-font', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700');
                osc_enqueue_style('icons-css', 'https://cdn.iconmonstr.com/1.3.0/css/iconmonstr-iconic-font.min.css');
                osc_enqueue_style('normalize-css', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css');     
                osc_enqueue_style('bootstrap-grid', IPA_ADM_STYLE . 'bootstrap-grid.min.css');
                osc_enqueue_style('select-css', IPA_ADM_STYLE . 'select.css');
                osc_enqueue_style('labelauty-css', IPA_ADM_STYLE . 'jquery-labelauty.css');
                osc_enqueue_style('adaptive-modal-css', IPA_ADM_STYLE . 'adaptive-modal.css');
                osc_enqueue_style('main-style', IPA_ADM_STYLE . 'style.css?v=' . time());
                  
                osc_register_script('chckbox-js', IPA_ADM_JS . 'chckbox-radio.js');
                osc_enqueue_script('chckbox-js'); 
                osc_register_script('label-js', IPA_ADM_JS . 'jquery.label_better.min.js?v=' . time());
                osc_enqueue_script('label-js');
                osc_register_script('classie-js', IPA_ADM_JS . 'classie.js?v=' . time());
                osc_enqueue_script('classie-js');
                osc_register_script('select-js', IPA_ADM_JS . 'selectFx.js?v=' . time());
                osc_enqueue_script('select-js');
                osc_register_script('labelauty-js', IPA_ADM_JS . 'jquery-labelauty.js?v=' . time());
                osc_enqueue_script('labelauty-js');
                osc_register_script('adaptive-modal-js', IPA_ADM_JS . 'jquery.adaptive-modal.min.js?v=' . time());
                osc_enqueue_script('adaptive-modal-js');
                osc_register_script('main-js', IPA_ADM_JS . 'scripts.js?v=' . time());
                osc_enqueue_script('main-js'); 
                
                osc_remove_hook('admin_page_header', 'customPageHeader');
                osc_add_hook('admin_page_header', array(&$this, 'adminTitle'));
            break;
            
            default: break;
        }
    }
    
    function adminTitle() {
        echo '<h1>Innova Phone Auth PRO</h1>';
    }
}