<?php defined('ABS_PATH') or die('Access denied');

class IPAUser {
    
    private static $instance;
    
    function __construct() {
        osc_add_hook('header', array(&$this, 'initPageHeader'));

        if(osc_get_preference('ipa_enable', 'innova_phone_auth')) {
            osc_add_hook('before_validating_login', array(&$this, 'initUserLogin'));
        }

        if(osc_get_preference('ipa_signup_page', 'innova_phone_auth')) {
            osc_add_hook('before_user_register', array(&$this, 'initUserRegister'));
        }
    }
    
    public static function newInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function initPageHeader() {
        osc_register_script('placeholder-js', IPA_USR_JS . 'imask.min.js');

        if(osc_is_login_page() && osc_get_preference('ipa_enable', 'innova_phone_auth')) {
            osc_enqueue_script('placeholder-js');

            osc_add_hook('footer', array(&$this, 'initUserLoginFooter'));
        }

        if(osc_is_register_page() && osc_get_preference('ipa_signup_page', 'innova_phone_auth')) {
            osc_enqueue_script('placeholder-js');

            osc_add_hook('footer', array(&$this, 'initUserRegisterFooter'));
        }

        if(osc_is_recover_page() && (!osc_get_preference('ipa_email_register_enable', 'innova_phone_auth') || osc_get_preference('ipa_type', 'innova_phone_auth') == "phone")) {
            header('Location:' . osc_base_url());
        }
    }

    public function initUserLogin() {
        IPAModel::newInstance()->userLogin();
    }

    public function initUserRegister() {
        IPAModel::newInstance()->userRegister();
    }

    public function initUserLoginFooter() {
        require_once IPA_USR_PATH . 'login.php';
    }

    public function initUserRegisterFooter() {
        require_once IPA_USR_PATH . 'register.php';
    }
}