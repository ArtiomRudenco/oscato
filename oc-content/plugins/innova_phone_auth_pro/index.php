<?php defined('ABS_PATH') or die('Access denied');
/*
  Plugin Name: Innova Phone Auth PRO
  Plugin URI: https://osclass-nero.com/
  Plugin update URI: iphone-auth
  Short Name: ipa
  Description: OsClass Professional Phone Auth Plugin
  Version: 1.0.0
  Author: osNero
  Author URI: https://osclass-nero.com/
  Author Email: support@osclass-nero.com
*/

define('IPA_PLUGIN_FOLDER', 'innova_phone_auth_pro');
define('IPA_PATH', osc_plugins_path() . IPA_PLUGIN_FOLDER . '/');

require_once IPA_PATH . 'consts.php';
require_once IPA_PATH . 'load.php';

if(OC_ADMIN) {
    $admin = new IPAAdmin();
}

$user = new IPAUser();

function ipa_install() {
    IPAModel::newInstance()->install();
}

function ipa_unistall() {
    IPAModel::newInstance()->uninstall();
}

function ipa_settings() {
    osc_redirect_to(osc_route_admin_url('ipa-settings'));
}

osc_register_plugin(osc_plugin_path(__FILE__), 'ipa_install');
osc_add_hook(osc_plugin_path(__FILE__) . "_uninstall", 'ipa_unistall');
osc_add_hook(osc_plugin_path(__FILE__) . "_configure", 'ipa_settings');