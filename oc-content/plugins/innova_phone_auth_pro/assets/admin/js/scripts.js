$(document).ready(function(){ 
    $('.chkbox:checkbox').chkbox();
    $('.rdo:radio').rdo();
    $(".leability:checkbox").labelauty();

    [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
		new SelectFx(el);
	});
    
    $("input.label_better").label_better({
        position: "top",
        animationTime: 500,
        easing: "ease-in-out",
        offset: -26
    });
    
    $('input[data-filter="only-float"]').keyup(function() {
        var preg = $(this).val().replace(/[^.\d]+/g,"").replace( /^([^\.]*\.)|\./g, '$1' );
        $(this).val(preg);
    });
    
    $('input[data-filter="only-num"]').keyup(function() {
        $(this).val(this.value.replace(/[^\d]/gi, ''));
    });
});