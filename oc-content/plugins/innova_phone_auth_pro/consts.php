<?php defined('ABS_PATH') or die('Access denied');

define('IPA_PLUGIN_URL', osc_plugin_url(IPA_PLUGIN_FOLDER . '/ '));

define('IPA_ADM_STYLE', IPA_PLUGIN_URL . 'assets/admin/css/');
define('IPA_ADM_JS', IPA_PLUGIN_URL . 'assets/admin/js/');
define('IPA_ADM_IMG', IPA_PLUGIN_URL . 'assets/admin/images/');
define('IPA_USR_JS', IPA_PLUGIN_URL . 'assets/js/');
define('IPA_USR_PATH', IPA_PATH . '/user/');
define('IPA_ADM_PATH', IPA_PLUGIN_FOLDER . '/admin/');