<?php defined('ABS_PATH') or die('Access denied'); ?>

<script type="text/javascript">
    var emailLabel = $('label[for="email"]');
    var emailInput = $('#s_email');
    var phoneLabel = '<label class="control-label" for="s_phone_mobile"><?php _e('Phone:', 'innova_phone_auth_pro'); ?></label>';
    var phoneInput  = '<input id="s_phone_mobile" type="text" name="s_phone_mobile">';
    var phoneHtml  = '<div class="control-group"><label class="control-label" for="s_phone_mobile"><?php _e('Phone:', 'innova_phone_auth_pro'); ?></label><div class="controls"><input id="s_phone_mobile" type="text" name="s_phone_mobile" placeholder="<?php _e('Phone:', 'innova_phone_auth_pro'); ?>" value="<?php echo Params::getParam('s_phone_mobile'); ?>" autocomplete="off"></div></div>';
</script>

<?php if(osc_get_preference('ipa_signup_page', 'innova_phone_auth')): ?>
    <?php if(osc_get_preference('ipa_email_register_enable', 'innova_phone_auth')): ?>
        <script type="text/javascript">
            emailLabel.parent('div').after(phoneHtml);
        </script>
    <?php else: ?>
        <script type="text/javascript">
            emailLabel.replaceWith(phoneLabel);
            emailInput.replaceWith(phoneInput);
        </script>
    <?php endif; ?>

    <?php if(!osc_get_preference('ipa_email_register_enable', 'innova_phone_auth') || osc_get_preference('ipa_signup_phone_required', 'innova_phone_auth')): ?>
        <script type="text/javascript">
            $("form[name=register]").validate({
                rules: {
                    s_name: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    <?php if(osc_get_preference('ipa_mask_enable', 'innova_phone_auth')): ?>
                    phone_unmask: {
                        required: true
                    },
                    <?php else: ?>
                    s_phone_mobile: {
                        required: true
                    },
                    <?php endif; ?>
                    s_password: {
                        required: true,
                        minlength: 5
                    },
                    s_password2: {
                        required: true,
                        minlength: 5,
                        equalTo: "#s_password"
                    }
                },
                messages: {
                    s_name: {
                        required: '<?php _e('Name: this field is required', 'innova_phone_auth_pro'); ?>.'
                    },
                    s_email: {
                        required: '<?php _e('Email: this field is required', 'innova_phone_auth_pro'); ?>.',
                        email: '<?php _e('Invalid email address', 'innova_phone_auth_pro'); ?>.'
                    },
                    <?php if(osc_get_preference('ipa_mask_enable', 'innova_phone_auth')): ?>
                    phone_unmask: {
                        required: '<?php _e('Phone: this field is required', 'innova_phone_auth_pro'); ?>.'
                    },
                    <?php else: ?>
                    s_phone_mobile: {
                        required: '<?php _e('Phone: this field is required', 'innova_phone_auth_pro'); ?>.'
                    },
                    <?php endif; ?>
                    s_password: {
                        required: '<?php _e('Password: this field is required', 'innova_phone_auth_pro'); ?>.',
                        minlength: '<?php _e('Password: enter at least 5 characters', 'innova_phone_auth_pro'); ?>.'
                    },
                    s_password2: {
                        required: '<?php _e('Second password: this field is required', 'innova_phone_auth_pro'); ?>.',
                        minlength: '<?php _e('Second password: enter at least 5 characters', 'innova_phone_auth_pro'); ?>.',
                        equalTo: "<?php _e('Passwords don\'t match', 'innova_phone_auth_pro'); ?>."
                    }
                },
                errorLabelContainer: "#error_list",
                wrapper: "li",
                invalidHandler: function(form, validator) {
                    $('html,body').animate({ scrollTop: $('h1').offset().top }, { duration: 250, easing: 'swing'});
                },
                submitHandler: function(form){
                    $('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
                    form.submit();
                }
            });
        </script>
    <?php endif; ?>

    <?php if(osc_get_preference('ipa_mask_enable', 'innova_phone_auth')): ?>
        <script type="text/javascript">
            var phoneInput = document.getElementById('s_phone_mobile');

            var maskOptions = {
                mask: '<?php echo osc_get_preference('ipa_mask', 'innova_phone_auth'); ?>',
                <?php if(osc_get_preference('ipa_display_mask_enable', 'innova_phone_auth')): ?>
                lazy: false,
                placeholderChar: '<?php echo osc_get_preference('ipa_placeholder', 'innova_phone_auth'); ?>'
                <?php endif; ?>
            };

            var phoneMask = IMask(phoneInput, maskOptions).on('accept', function() {
                if(document.getElementById('phone_unmask') == null) {
                    $('form').prepend('<input id="phone_unmask" type="hidden" name="phone_unmask" value="' + phoneMask.unmaskedValue + '">');
                } else {
                    $('#phone_unmask').val(phoneMask.unmaskedValue);
                }
            });

            $('form').prepend('<input id="phone_unmask" type="hidden" name="phone_unmask" value="">');
            $('form').prepend('<input type="hidden" name="phone_placeholder" value="' + $('#s_phone_mobile').val() + '">');
        </script>
    <?php endif; ?>
<?php endif; ?>