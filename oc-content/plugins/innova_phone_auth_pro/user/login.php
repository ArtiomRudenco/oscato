<?php defined('ABS_PATH') or die('Access denied'); ?>

<script type="text/javascript">
    var emailLabel = $('label[for="email"]');
    var emailInput = $('#email');
    var phoneLabel = '<label class="control-label" for="s_phone_mobile"><?php _e('Phone:', 'innova_phone_auth_pro'); ?></label>';
    var phoneEmailLabel = '<label class="control-label" for="email"><?php _e('E-mail/Phone:', 'innova_phone_auth_pro'); ?></label>';
    var phoneHtml  = '<input id="s_phone_mobile" type="text" name="s_phone_mobile" placeholder="<?php _e('Phone:', 'innova_phone_auth_pro'); ?>">';
</script>
<?php if(osc_get_preference('ipa_type', 'innova_phone_auth') == "phone"): ?>
    <script type="text/javascript">
        emailLabel.replaceWith(phoneLabel);
        emailInput.replaceWith(phoneHtml);
    </script>
<?php else: ?>
    <script type="text/javascript">
        emailLabel.replaceWith(phoneEmailLabel);
        emailInput.attr('placeholder', '<?php _e('E-mail/Phone:', 'innova_phone_auth_pro'); ?>')
    </script>
<?php endif; ?>

<?php if(osc_get_preference('ipa_mask_enable', 'innova_phone_auth')): ?>
    <script type="text/javascript">
        var phoneInput = document.getElementById('s_phone_mobile');

        if(phoneInput == null) {
            phoneInput = document.getElementById('email');
        }

        var maskOptions = {
            <?php if(osc_get_preference('ipa_type', 'innova_phone_auth') == "phone"): ?>
                mask: '<?php echo osc_get_preference('ipa_mask', 'innova_phone_auth'); ?>',
            <?php else: ?>
                mask: [
                    {
                        mask: '<?php echo osc_get_preference('ipa_mask', 'innova_phone_auth'); ?>'
                    },
                    {
                        mask: /^\S*@?\S*$/
                    }
                ],
            <?php endif; ?>
            <?php if(osc_get_preference('ipa_display_mask_enable', 'innova_phone_auth')): ?>
               lazy: false,
               placeholderChar: '<?php echo osc_get_preference('ipa_placeholder', 'innova_phone_auth'); ?>'
            <?php endif; ?>
        };

        var phoneMask = IMask(phoneInput, maskOptions).on('accept', function() {
            if(document.getElementById('phone_unmask') == null) {
                $('form').prepend('<input id="phone_unmask" type="hidden" name="phone_unmask" value="' + phoneMask.unmaskedValue + '">');
            } else {
                $('#phone_unmask').val(phoneMask.unmaskedValue);
            }
        });

        $('form').prepend('<input type="hidden" name="phone_placeholder" value="' + $('#s_phone_mobile').val() + '">');
    </script>
<?php endif; ?>

<?php if(!osc_get_preference('ipa_email_register_enable', 'innova_phone_auth') || osc_get_preference('ipa_type', 'innova_phone_auth') == "phone"): ?>
    <script type="text/javascript">
        $('a[href="<?php echo osc_recover_user_password_url(); ?>"]').remove();
    </script>
<?php endif; ?>
