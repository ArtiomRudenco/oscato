<?php defined('ABS_PATH') or die('Access denied');
    if(Params::getParam('plugin_action') =='done') {
        osc_set_preference('ipa_enable', Params::getParam("ipa_enable") ? Params::getParam("ipa_enable") : '0', 'innova_phone_auth', 'BOOLEAN');
        osc_set_preference('ipa_email_register_enable', Params::getParam("ipa_email_register_enable") ? Params::getParam("ipa_email_register_enable") : '0', 'innova_phone_auth', 'BOOLEAN');
        osc_set_preference('ipa_signup_page', Params::getParam("ipa_signup_page") ? Params::getParam("ipa_signup_page") : '0', 'innova_phone_auth', 'BOOLEAN');
        osc_set_preference('ipa_signup_phone_required', Params::getParam("ipa_signup_phone_required") ? Params::getParam("ipa_signup_phone_required") : '0', 'innova_phone_auth', 'BOOLEAN');
        osc_set_preference('ipa_mask_enable', Params::getParam("ipa_mask_enable") ? Params::getParam("ipa_mask_enable") : '0', 'innova_phone_auth', 'BOOLEAN');
        osc_set_preference('ipa_display_mask_enable', Params::getParam("ipa_display_mask_enable") ? Params::getParam("ipa_display_mask_enable") : '0', 'innova_phone_auth', 'BOOLEAN');
        osc_set_preference('ipa_mask', Params::getParam("ipa_mask"), 'innova_phone_auth', 'STRING');
        osc_set_preference('ipa_placeholder', Params::getParam("ipa_placeholder"), 'innova_phone_auth', 'STRING');
        osc_set_preference('ipa_type', Params::getParam("ipa_type"), 'innova_phone_auth', 'STRING');
        
        ob_get_clean();
        osc_add_flash_ok_message(__('Congratulations, the plugin is now configured', 'innova_phone_auth_pro'), 'admin');
        osc_redirect_to(osc_route_admin_url('ipa-settings'));
    }
?>

<div class="ipp-main">
    <div class='wrapper'>
        <div class="row">
            <div class="col-md-6 logo-block">
                <a href="https://osclass-nero.com" target="_blank"><img class="logo" src="<?php echo IPA_ADM_IMG ?>logo.png" alt="Osclass Nero"></a>
            </div>
        </div>

        <?php require_once 'top-menu.php'; ?>
        
        <div class='content'>
            <h1 class='content-title'><?php _e('Settings', 'innova_phone_auth_pro'); ?></h1>
           
            <div class='content-body'>
                <form action="<?php echo osc_route_admin_url('ipa-settings'); ?>" method="post">
                    <input type="hidden" name="page" value="plugins" />
                    <input type="hidden" name="action" value="renderplugin" />
                    <input type="hidden" name="route" value="ipa-settings" />
                    <input type="hidden" name="plugin_action" value="done" />
                    
                    <div class="form-wrapper">
                        <div class="row">
                            <div class="col-md-5">
                                <label for="ipa-enable"><?php _e('Phone Auth Enable', 'innova_phone_auth_pro') ?></label>
                                <input id="ipa-enable" class="chkbox" type="checkbox" name="ipa_enable" <?php if(osc_get_preference('ipa_enable', 'innova_phone_auth')): ?>checked<?php endif; ?> >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <label for="ipa-email-register-enable"><?php _e('E-mail Field on the Register Page Enable', 'innova_phone_auth_pro') ?></label>
                                <input id="ipa-email-register-enable" class="chkbox" type="checkbox" name="ipa_email_register_enable" <?php if(osc_get_preference('ipa_email_register_enable', 'innova_phone_auth')): ?>checked<?php endif; ?> >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <label for="ipa-signup-page"><?php _e('Add Phone to Register Page', 'innova_phone_auth_pro') ?></label>
                                <input id="ipa-signup-page" class="chkbox" type="checkbox" name="ipa_signup_page" <?php if(osc_get_preference('ipa_signup_page', 'innova_phone_auth')): ?>checked<?php endif; ?> >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <label for="ipa-signup-phone-required"><?php _e('Phone field on the registration page is required', 'innova_phone_auth_pro') ?></label>
                                <input id="ipa-signup-phone-required" class="chkbox" type="checkbox" name="ipa_signup_phone_required" <?php if(osc_get_preference('ipa_signup_phone_required', 'innova_phone_auth')): ?>checked<?php endif; ?> >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <label for="ipa-mask-enable"><?php _e('Phone Mask Input Enable', 'innova_phone_auth_pro') ?></label>
                                <input id="ipa-mask-enable" class="chkbox" type="checkbox" name="ipa_mask_enable" <?php if(osc_get_preference('ipa_mask_enable', 'innova_phone_auth')): ?>checked<?php endif; ?>>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <label for="ipa-display-mask-enable"><?php _e('Display Mask in Placeholder Enable', 'innova_phone_auth_pro') ?></label>
                                <input id="ipa-display-mask-enable" class="chkbox" type="checkbox" name="ipa_display_mask_enable" <?php if(osc_get_preference('ipa_display_mask_enable', 'innova_phone_auth')): ?>checked<?php endif; ?>>
                            </div>

                            <div class="col-md-2">
                                <a href="javascript:;" data-toggle="adaptive-modal" data-title="<p><?php _e('Works best when you have set the Phone Auth Type as \'Phone only\'', 'innova_phone_auth_pro') ?></p>" class="ipp-btn ipp-btn-outline"><i class="im im-info"></i> <?php _e('Important', 'innova_phone_auth_pro') ?></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <input class="label_better" type="text"  name="ipa_mask" value="<?php echo osc_get_preference('ipa_mask', 'innova_phone_auth'); ?>" placeholder="<?php _e('Phone Input Mask', 'innova_phone_auth_pro') ?>">
                            </div>

                            <div class="col-md-2">
                                <a href="javascript:;" data-toggle="adaptive-modal" data-title="<p><?php _e('In order for users to enter data according to your template, you can set a mask for the input field.', 'innova_phone_auth_pro') ?></p><p><?php _e('The plugin uses a universal mask, i.e. it will work both with the option \'Phone only\' and \'Phone + E-mail\'.', 'innova_phone_auth_pro') ?></p><p><?php _e('You can set any template for entering a phone number. To do this, use the following parameters:', 'innova_phone_auth_pro') ?></p><p><?php _e('<strong>+{Any digits}</strong> - these digits will be automatically displayed before entering the phone number. This will be useful if you need a strictly defined country prefix.', 'innova_phone_auth_pro') ?></p><p><?php _e('<strong>0</strong> - Any digit.', 'innova_phone_auth_pro') ?></p><p><?php _e('<strong>1-9</strong> - fixed digit.', 'innova_phone_auth_pro') ?></p><p><?php _e('<strong>[ ]</strong> - make input optional. Usually used at the end of an input field.', 'innova_phone_auth_pro') ?></p><p><?php _e('Example, if we want to set a mask for entering a US phone number, we must write: +{1}(000) 000-0000', 'innova_phone_auth_pro') ?></p>" class="ipp-btn ipp-input-text ipp-btn-outline"><i class="im im-info"></i> <?php _e('Help', 'innova_phone_auth_pro') ?></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <input class="label_better" type="text"  name="ipa_placeholder" value="<?php echo osc_get_preference('ipa_placeholder', 'innova_phone_auth'); ?>" placeholder="<?php _e('Phone Input Placeholder Symbol', 'innova_phone_auth_pro') ?>">
                            </div>

                            <div class="col-md-2">
                                <a href="javascript:;" data-toggle="adaptive-modal" data-title="<p><?php _e('If you enable display mask in placeholder, this symbol will be displayed as a tooltip.', 'innova_phone_auth_pro') ?></p>" class="ipp-btn ipp-input-text ipp-btn-outline"><i class="im im-info"></i> <?php _e('Help', 'innova_phone_auth_pro') ?></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <select class="cs-select cs-skin-underline" name="ipa_type">
                                    <option>
                                        <?php _e('Phone Auth Type', 'innova_phone_auth_pro') ?>
                                    </option>

                                    <option value="ephone" <?php if(osc_get_preference('ipa_type', 'innova_phone_auth') == "ephone"): ?>selected<?php endif; ?>>
                                        <?php _e('Phone + E-mail', 'innova_phone_auth_pro') ?>
                                    </option>

                                    <option value="phone" <?php if(osc_get_preference('ipa_type', 'innova_phone_auth') == "phone"): ?>selected<?php endif; ?>>
                                        <?php _e('Phone Only', 'innova_phone_auth_pro') ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="nr-btn"><i class="im im-floppy-disk"></i> <?php echo osc_esc_html(__("Save changes", 'innova_phone_auth_pro')); ?></button>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-6 advanced-info-block">
                        <h3><?php _e('Advanced Information', 'innova_phone_auth_pro') ?></h3>
                        <p><?php _e('If the E-mail field is disabled on the registration page or the option "Authorization type" is set to "Phone only", password recovery will not be available.', 'innova_phone_auth_pro') ?></p>
                        <p><?php _e('For it to work, you need to integrate the plugin to the SMS sending service.', 'innova_phone_auth_pro') ?></p>
                        <p><?php _e('For an additional fee, we will connect any SMS sending service as you wish!', 'innova_phone_auth_pro') ?></p>
                        <p><?php _e('For any questions and improvements, please contact us by E-mail:', 'innova_phone_auth_pro') ?> <a href="mailto:support@osclass-nero.com" target="_blank" title="<?php _e('Write to Us', 'innova_phone_auth_pro') ?>">support@osclass-nero.com</a></p>
                        <p><?php _e('Best Regards, Osclass Nero Team!', 'innova_phone_auth_pro') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>