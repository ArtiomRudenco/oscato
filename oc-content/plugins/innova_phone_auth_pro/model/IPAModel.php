<?php defined('ABS_PATH') or die('Access denied');

class IPAModel extends DAO {
    
    private static $instance;
    
    function __construct() {
        parent::__construct();
    }
    
    public static function newInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    public function install($version = '') {
        if($version == '') {
            osc_set_preference('version', 100, 'innova_phone_auth', 'INTEGER');
        } else {
            osc_set_preference('version', $version, 'innova_phone_auth', 'INTEGER');
        }
    }
    
    public function uninstall() {
        Preference::newInstance()->delete(array('s_section' => 'innova_phone_auth'));
    }

    public function getTable_user() {
        return DB_TABLE_PREFIX . 't_user';
    }

    public function findUserByEmailPhone($login) {
        if(!$login) {
            return false;
        }

        $this->dao->select();
        $this->dao->from($this->getTable_user());
        $this->dao->where('s_email = "' . $login . '" OR s_phone_mobile = "' . $login . '"');
        $this->dao->limit(1);

        $result = $this->dao->get();

        if($result->numRows()) {
            return $result->row();
        }

        return false;
    }

    public function findUserByPhone($phone) {
        if(!$phone) {
            return false;
        }

        $this->dao->select();
        $this->dao->from($this->getTable_user());
        $this->dao->where('s_phone_mobile', $phone);
        $this->dao->limit(1);

        $result = $this->dao->get();

        if($result->numRows()) {
            return $result->row();
        }

        return false;
    }

    public function userLogin() {
        $user = array();
        $password = Params::getParam('password', false, false);

        $wrongCredentials = false;

        if(osc_get_preference('ipa_type', 'innova_phone_auth') == "phone") {
            $login = trim(Params::getParam('s_phone_mobile'));

            if(osc_get_preference('ipa_mask_enable', 'innova_phone_auth')) {
                if ($login == Params::getParam('phone_placeholder')) {
                    osc_add_flash_error_message(__('Please provide a phone', 'innova_phone_auth_pro'));
                    $wrongCredentials = true;
                }
            } else {
                if (!$login) {
                    osc_add_flash_error_message(__('Please provide a phone', 'innova_phone_auth_pro'));
                    $wrongCredentials = true;
                }
            }

            if ($password == '') {
                osc_add_flash_error_message(__('Empty passwords are not allowed. Please provide a password', 'innova_phone_auth_pro') );
                $wrongCredentials = true;
            }

            if ($wrongCredentials) {
                header('Location:' . osc_user_login_url());
                exit;
            }

            $user = $this->findUserByPhone($login);

            if (empty($user)) {
                osc_add_flash_error_message(__('The user doesn\'t exist', 'innova_phone_auth_pro'));
                header('Location:' . osc_user_login_url());
                exit;
            }

            if (!osc_verify_password($password, (isset($user['s_password']) ? $user['s_password'] : ''))) {
                osc_add_flash_error_message(__('The password is incorrect', 'innova_phone_auth_pro'));
                header('Location:' . osc_user_login_url());
                exit;
            }

            $banned = osc_is_banned($user['s_email']);

            if($banned == 1) {
                osc_add_flash_error_message(__('Your current email is not allowed', 'innova_phone_auth_pro'));
            }

            if($banned == 2) {
                osc_add_flash_error_message(__('Your current IP is not allowed', 'innova_phone_auth_pro'));
            }

            if($banned == 3) {
                osc_add_flash_error_message(__('Your current IP and email is not allowed', 'innova_phone_auth_pro'));
            }

            if($banned) {
                header('Location:' . osc_user_login_url());
                exit;
            }
        } else {
            $login = trim(Params::getParam('email'));

            if (!$login) {
                osc_add_flash_error_message(__('Please provide an email address', 'innova_phone_auth_pro'));
                $wrongCredentials = true;
            }

            if ($password == '') {
                osc_add_flash_error_message(__('Empty passwords are not allowed. Please provide a password', 'innova_phone_auth_pro') );
                $wrongCredentials = true;
            }

            if ($wrongCredentials) {
                header('Location:' . osc_user_login_url());
                exit;
            }

            $user = $this->findUserByEmailPhone($login);

            if (empty($user)) {
                osc_add_flash_error_message(__('The user doesn\'t exist', 'innova_phone_auth_pro'));
                header('Location:' . osc_user_login_url());
                exit;
            }

            if (!osc_verify_password($password, (isset($user['s_password']) ? $user['s_password'] : ''))) {
                osc_add_flash_error_message(__('The password is incorrect', 'innova_phone_auth_pro'));
                header('Location:' . osc_user_login_url());
                exit;
            }

            $banned = osc_is_banned($user['s_email']);

            if($banned == 1) {
                osc_add_flash_error_message(__('Your current email is not allowed', 'innova_phone_auth_pro'));
            }

            if($banned == 2) {
                osc_add_flash_error_message(__('Your current IP is not allowed', 'innova_phone_auth_pro'));
            }

            if($banned == 3) {
                osc_add_flash_error_message(__('Your current IP and email is not allowed', 'innova_phone_auth_pro'));
            }

            if($banned) {
                header('Location:' . osc_user_login_url());
                exit;
            }
        }

        $urlRedirect = osc_get_http_referer();

        if(osc_rewrite_enabled() && $urlRedirect) {
            if(strpos($urlRedirect,'oc-admin') !== false) {
                $urlRedirect = osc_user_dashboard_url();
            } else {
                $requestUri = urldecode(preg_replace('@^' . osc_base_url() . '@', "", $urlRedirect));
                $tmpArr = explode("?", $requestUri);
                $requestUri = $tmpArr[0];
                $rules = Rewrite::newInstance()->listRules();

                foreach($rules as $match => $uri) {
                    if(preg_match('#' . $match . '#', $requestUri, $m)) {
                        $requestUri = preg_replace('#' . $match . '#', $uri, $requestUri);

                        if(preg_match('|([&?]{1})page=([^&]*)|', '&' . $requestUri . '&', $match)) {
                            $pageRedirect = $match[2];

                            if($pageRedirect == '' || $pageRedirect == 'login') {
                                $urlRedirect = osc_user_dashboard_url();
                            }
                        }
                        break;
                    }
                }
            }
        }

        require_once LIB_PATH . 'osclass/UserActions.php';

        $userActions = new UserActions(false);
        $logged = $userActions->bootstrap_login($user['pk_i_id']);

        if(!$logged) {
            osc_add_flash_error_message(__('The user doesn\'t exist', 'innova_phone_auth_pro'));
        } else if($logged == 1) {
            if(osc_get_preference('ipa_type', 'innova_phone_auth') == "phone") {
                if (Params::getParam('remember') == 1) {
                    require_once osc_lib_path() . 'osclass/helpers/hSecurity.php';

                    $secret = osc_genRandomPassword();

                    User::newInstance()->update(
                        array('s_secret' => $secret),
                        array('pk_i_id' => $user['pk_i_id'])
                    );

                    Cookie::newInstance()->set_expires(osc_time_cookie());
                    Cookie::newInstance()->push('oc_userId', $user['pk_i_id']);
                    Cookie::newInstance()->push('oc_userSecret', $secret);
                    Cookie::newInstance()->set();
                }

                if(!$urlRedirect) {
                    $urlRedirect = osc_user_dashboard_url();
                }

                osc_run_hook('after_login', $user, $urlRedirect);

                header('Location:' . osc_apply_filter('correct_login_url_redirect', $urlRedirect));
                exit;
            } else {
                if((time() - strtotime($user['dt_access_date'])) > 1200) {
                    osc_add_flash_error_message(sprintf(__('The user has not been validated yet. Would you like to re-send your <a href="%s">activation?</a>', 'innova_phone_auth_pro'), osc_user_resend_activation_link($user['pk_i_id'], $user['s_email'])));
                } else {
                    osc_add_flash_error_message(__('The user has not been validated yet', 'innova_phone_auth_pro'));
                }
            }
        } else if($logged == 2) {
            osc_add_flash_error_message(__('The user has been suspended', 'innova_phone_auth_pro'));
        } else if($logged == 3) {
            if (Params::getParam('remember') == 1) {
                require_once osc_lib_path() . 'osclass/helpers/hSecurity.php';

                $secret = osc_genRandomPassword();

                User::newInstance()->update(
                    array('s_secret' => $secret),
                    array('pk_i_id' => $user['pk_i_id'])
                );

                Cookie::newInstance()->set_expires(osc_time_cookie());
                Cookie::newInstance()->push('oc_userId', $user['pk_i_id']);
                Cookie::newInstance()->push('oc_userSecret', $secret);
                Cookie::newInstance()->set();
            }

            if(!$urlRedirect) {
                $urlRedirect = osc_user_dashboard_url();
            }

            osc_run_hook('after_login', $user, $urlRedirect);

            header('Location:' . osc_apply_filter('correct_login_url_redirect', $urlRedirect));
            exit;
        } else {
            osc_add_flash_error_message(__('Something went wrong', 'innova_phone_auth_pro'));
        }

        header('Location:' . osc_base_url());
        exit;
    }

    public function userRegister() {
        $name = Params::getParam('s_name');
        $phone = Params::getParam('s_phone_mobile');
        $password = Params::getParam('s_password', false, false);
        $rePassword = Params::getParam('s_password2', false, false);

        osc_get_preference('ipa_email_register_enable', 'innova_phone_auth') ? $email = Params::getParam('s_email') : $email = Params::getParam('phone_unmask') . '@' . Params::getServerParam('HTTP_HOST');

        $banned = osc_is_banned($email);

        if($banned == 1) {
            osc_add_flash_error_message(__('Your current email is not allowed', 'innova_phone_auth_pro'));
        } else if($banned == 2) {
            osc_add_flash_error_message(__('Your current IP is not allowed', 'innova_phone_auth_pro'));
        }

        if($banned) {
            header('Location:' . osc_register_account_url());
            exit;
        }

        $isPhoneExist = $this->findUserByPhone($phone);

        if($isPhoneExist) {
            osc_add_flash_error_message(__('The specified phone is already in use', 'innova_phone_auth_pro'));

            header('Location:' . osc_register_account_url());
            exit;
        }

        Params::setParam('s_email', $email);

        require_once LIB_PATH . 'osclass/UserActions.php';
        $userActions = new UserActions(false);
        $success = $userActions->add();

        if($success == 1) {
            if(osc_get_preference('ipa_email_register_enable', 'innova_phone_auth')) {
                osc_add_flash_ok_message(__('The user has been created. An activation email has been sent', 'innova_phone_auth_pro'));
            } else {
                osc_add_flash_ok_message(__('Your account has been created successfully', 'innova_phone_auth_pro'));

                $user = $this->findUserByPhone($phone);

                $this->dao->update($this->getTable_user(), array('b_active' => 1), array('pk_i_id' => $user['pk_i_id']));

                Params::setParam('action', 'login_post');
                Params::setParam('email', $email);
                Params::setParam('password', $password);

                require_once(osc_lib_path() . 'osclass/controller/login.php');
                $do = new CWebLogin();
                $do->doModel();
            }

            header('Location:' . osc_base_url());
            exit;
        } else if($success == 2) {
            osc_add_flash_ok_message( _m('Your account has been created successfully', 'innova_phone_auth_pro'));

            Params::setParam('action', 'login_post');
            Params::setParam('email', $email);
            Params::setParam('password', $password);

            require_once(osc_lib_path() . 'osclass/controller/login.php');
            $do = new CWebLogin();
            $do->doModel();
        } else {
            osc_add_flash_error_message($success);

            header('Location:' . osc_register_account_url());
            exit;
        }

        header('Location:' . osc_base_url());
        exit;
    }
}