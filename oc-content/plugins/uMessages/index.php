<?php defined('ABS_PATH') or die('Access denied');
/***************************************************************************
*                                                                          *
* Copyright 2018 osclass-pro.com                                           *
*                                                                          *
* You shall not distribute this plugin and any its files                   *
* (except third-party libraries) to third parties. Rental, leasing, sale   *
* and any other form of distribution are not allowed and are strictly      *
* forbidden.                                                               *    
*                                                                          *
****************************************************************************
    Plugin Name: uMessages
    Plugin URI: https://osclass-pro.com/
	Plugin update URI: ultimate-messaging
    Description: Ultimate Messages for osClass
    Version: 1.0.3
    Author: Dis
    Author URI: https://osclass-pro.com/
    Short Name: um                                                         
****************************************************************************/
define('UMS_PLUGIN_PATH', osc_plugins_path() . 'uMessages/');

require_once UMS_PLUGIN_PATH . 'consts.php';
require_once UMS_PLUGIN_PATH . 'load.php';

if(OC_ADMIN) $admin = new uMAdmin();
    else $user = new uMUser();

function um_install() {
    UMModel::newInstance()->install();
}

function um_unistall() {
    UMModel::newInstance()->uninstall();
}

function um_settings() {
    osc_redirect_to(osc_route_admin_url('um-dashboard'));
}

osc_register_plugin(osc_plugin_path(__FILE__), 'um_install');
osc_add_hook(osc_plugin_path(__FILE__) . "_uninstall", 'um_unistall');
osc_add_hook(osc_plugin_path(__FILE__) . "_configure", 'um_settings');