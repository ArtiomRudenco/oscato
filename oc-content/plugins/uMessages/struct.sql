CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_anti_spam` (
  `pk_i_spam_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_i_user_id` int(11) NOT NULL,
  `i_spam_count` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `i_admin_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `i_message_id` int(11) DEFAULT '0',
  `d_admin_blocked_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_i_spam_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_attachments` (
  `pk_i_attach_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_i_message_id` int(11) NOT NULL,
  `s_path` varchar(255) NOT NULL,
  `s_file` varchar(255) NOT NULL,
  PRIMARY KEY (`pk_i_attach_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_blocked` (
  `pk_i_block_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_i_user_id` int(11) NOT NULL,
  `i_user_block_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_i_block_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_boxes` (
  `pk_i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_user_id` int(11) unsigned NOT NULL,
  `s_box_type` enum('inbox','archive','trash','') NOT NULL,
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_messages` (
  `pk_i_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_i_topic_id` int(11) NOT NULL,
  `s_message` text NOT NULL,
  `fk_i_user_id` int(11) NOT NULL,
  `d_message_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_i_message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_petitions` (
  `pk_i_petition_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_i_user_id` int(11) NOT NULL,
  `s_message` text NOT NULL,
  `i_petition_status` tinyint(1) NOT NULL DEFAULT '2',
  `d_petition_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_i_petition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_topics` (
  `pk_i_topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_title` varchar(255) NOT NULL,
  `fk_i_item_id` int(11) NOT NULL,
  `s_item_name` varchar(255) NOT NULL,
  `i_spammed` tinyint(1) NOT NULL DEFAULT '0',
  `d_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_i_topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/t_um_user_box` (
  `pk_i_ubox_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_i_box_id` int(11) NOT NULL,
  `fk_i_topic_id` int(11) NOT NULL,
  `i_user_id` int(11) NOT NULL,
  `i_user_con_id` int(11) NOT NULL,
  `i_read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_i_ubox_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;