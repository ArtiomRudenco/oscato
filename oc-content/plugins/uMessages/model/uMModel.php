<?php defined('ABS_PATH') or die('Access denied');

class UMModel extends DAO {
    
    private static $instance;
    
    function __construct() {
        parent::__construct();
    }

    public static function newInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    
    public function import($file) {
        $sql = file_get_contents($file);

        if(!$this->dao->importSQL($sql) ) {
            throw new Exception($this->dao->getErrorLevel() . ' - ' . $this->dao->getErrorDesc()) ;
        }
    }
    
    public function install() {
        $this->import(UMS_PLUGIN_PATH . 'struct.sql');
        
		osc_set_preference('version', '103', 'umess', 'INTEGER');
        osc_set_preference('um_widget_position', 'sidebar', 'umess', 'STRING');
        osc_set_preference('um_fext', '.png,.gif,.jpg,.jpeg,.zip,.doc,.docx', 'umess', 'STRING');
        osc_set_preference('um_fmaxsize', 2048, 'umess', 'STRING');
        osc_set_preference('um_max_attach', 3, 'umess', 'STRING');
        osc_set_preference('um_thumb_size', '180', 'umess', 'INTEGER');
        osc_set_preference('um_usr_antispam_number', 3, 'umess', 'STRING');
        osc_set_preference('um_fcolor', '#ffffff', 'umess', 'STRING');
        osc_set_preference('um_lbcolor', '#ff6347', 'umess', 'STRING');
        osc_set_preference('um_bgcolor', '#35c3d9', 'umess', 'STRING');
        
        $sStart = '<div style="margin:15px;padding:15px 45px;border:5px solid #ebebeb;"><p style="font-size: 20px; color: #0c72b9; margin-top:10px;">Hello, {CONTACT_NAME}!</p>';
        
        $sEnd = '<hr style="margin:70px 0 20px;" /><p style="color: #888;">This is an automatic email, if you already did that, please ignore this email.<br />Thanks.</p></div>';
        
        $description[osc_language()]['s_title'] = '{WEB_TITLE} - Started a new conversation on the ad: {ITEM_TITLE}';
        $description[osc_language()]['s_text'] = $sStart . '<p style="color: #888;">User <span style="color:#0c72b9;">{SENDER_NAME}</span> started a conversation on your ad: <span style="color:#0c72b9;">{ITEM_LINK}</span></p><p style="color: #888;"><span style="color:#0c72b9;">{SENDER_NAME}</span> says:</p><p style="color: #888; background-color: #fff; margin: 15px; padding: 15px; border: 1px dashed #888;">{MESSAGE}</p>' . $sEnd;  
        
        $res = Page::newInstance()->insert(
            array('s_internal_name' => 'email_conversation_started', 'b_indelible' => '1'),
            $description
        );
        
        $description[osc_language()]['s_title'] = '{WEB_TITLE} - Received a new message on the conversation: {CONVERSATION_TITLE}';
        $description[osc_language()]['s_text'] = $sStart . '<p style="color: #888;">User <span style="color:#0c72b9;">{SENDER_NAME}</span> sent a new message on your conversation: <span style="color:#0c72b9;">{CONVERSATION_LINK}</span></p><p style="color: #888;"><span style="color:#0c72b9;">{SENDER_NAME}</span> says:</p><p style="color: #888; background-color: #fff; margin: 15px; padding: 15px; border: 1px dashed #888;">{MESSAGE}</p>' . $sEnd;  
        
        $res = Page::newInstance()->insert(
            array('s_internal_name' => 'email_message_sent', 'b_indelible' => '1'),
            $description
        );
        
        $description[osc_language()]['s_title'] = '{WEB_TITLE} - Received a new Petition';
        $description[osc_language()]['s_text'] = $sStart . '<p style="color: #888;">You received a new petition from User: <span style="color:#0c72b9;">{SENDER_NAME}</span></p><p style="color: #888;"><span style="color:#0c72b9;">{SENDER_NAME}</span> says:</p><p style="color: #888; background-color: #fff; margin: 15px; padding: 15px; border: 1px dashed #888;">{MESSAGE}</p>' . $sEnd;  
        
        $res = Page::newInstance()->insert(
            array('s_internal_name' => 'email_petition_sent', 'b_indelible' => '1'),
            $description
        );
        
        $description[osc_language()]['s_title'] = '{WEB_TITLE} - User is blocked by Antispam protection';
        $description[osc_language()]['s_text'] = $sStart . '<p style="color: #888;">User: <span style="color:#0c72b9;">{SENDER_NAME}</span> is blocked by Antispam protection</p>' . $sEnd;  
        
        $res = Page::newInstance()->insert(
            array('s_internal_name' => 'email_anti_spam_blocked', 'b_indelible' => '1'),
            $description
        );
    }
    
    public function uninstall() {
        $this->umAttachmentsAllRemove();
        
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_antispam()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_attachments()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_blocked()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_boxes()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_messages()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_petitions()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_topics()));
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_user_boxes()));
        
        Preference::newInstance()->delete(array('s_section' => 'umess'));
        
        Page::newInstance()->deleteByInternalName('email_conversation_started');
        Page::newInstance()->deleteByInternalName('email_message_sent');
        Page::newInstance()->deleteByInternalName('email_petition_sent');
        Page::newInstance()->deleteByInternalName('email_anti_spam_blocked');
    }
    
    public function getTable_attachments() {
        return DB_TABLE_PREFIX . 't_um_attachments';
    }
    
    public function getTable_antispam() {
        return DB_TABLE_PREFIX . 't_um_anti_spam';
    }
    
    public function getTable_blocked() {
        return DB_TABLE_PREFIX . 't_um_blocked';
    }
    
    public function getTable_boxes() {
        return DB_TABLE_PREFIX . 't_um_boxes';
    }
    
    public function getTable_messages() {
        return DB_TABLE_PREFIX . 't_um_messages';
    }
    
    public function getTable_petitions() {
        return DB_TABLE_PREFIX . 't_um_petitions';
    }
    
    public function getTable_topics() {
        return DB_TABLE_PREFIX . 't_um_topics';
    }
    
    public function getTable_user_boxes() {
        return DB_TABLE_PREFIX . 't_um_user_box';
    }
    
    public function getTable_items() {
        return DB_TABLE_PREFIX . 't_item';
    }
    
    public function getTable_users() {
        return DB_TABLE_PREFIX . 't_user';
    }
    
    public function mbUcFirst($string) {
        $string = preg_replace('/^\s*(\S)/eu',"mb_strtoupper('\\1', 'UTF-8')",$string);
        $string = mb_strtoupper(mb_substr($string, 0, 1, "UTF-8"), "UTF-8").mb_substr($string, 1, mb_strlen($string), "UTF-8" );  
        
        return $string;  
    }
    
    public function umTopMenu($page) {
        $currPage = Params::getParam('route');
        
        if($currPage == $page) return TRUE;
        
        return FALSE;
    }
    
    public function umUserPageHeader() {
        $_r = Params::getParam('route');
        
        switch($_r) {
            case 'umessages':
                $header = 'Messages';
            break;
            
            case 'umessages-archive':
                $header = 'Archive';
            break;
            
            case 'umessages-trash':
                $header = 'Trash';
            break;
            
            case 'umessages-blocked':
                $header = 'Black List';
            break;
            
            case 'umessages-view':
                $header = 'Conversation';
            break;
        }
        
        return $header;
    }
    
    public function umItemDetails($itemId) {
        $item = Item::newInstance()->findByPrimaryKey($itemId); 
        
        if($item) {
            $resource = ItemResource::newInstance()->getResource($itemId);
            $data = array();
            
            $data['location'] = implode(', ', array_filter(array($item['s_country'], $item['s_region'], $item['s_city'])));
            
            isset($resource['s_name']) ? $rUrl = osc_base_url() . $resource['s_path'] . $resource['pk_i_id'] . '_thumbnail.' . $resource['s_extension'] : $rUrl = osc_base_url() . 'oc-content/plugins/uMessages/assets/images/no_photo.gif';
            
            $data['r_path'] = $rUrl;
            $data['price'] = osc_format_price($item['i_price'], $item['fk_c_currency_code']);
            
            return $data;
        }
        
        return FALSE;
    }
    
    public function umSendNotify($topicId, $pattern, $itemId = false, $userId = false, $message = '', $toAdmin = false) {
        $page = new Page();
        $page = $page->findByInternalName($pattern);
        
        $locale = osc_current_user_locale();
        
        $content = array();
        
        if(isset($page['locale'][$locale]['s_title'])) $content = $page['locale'][$locale];
            else $content = current($page['locale']);
            
        $words   = array();
        $words[] = array('{WEB_TITLE}', '{ITEM_TITLE}', '{CONVERSATION_TITLE}', '{CONTACT_NAME}', '{SENDER_NAME}', '{ITEM_LINK}', '{CONVERSATION_LINK}', '{MESSAGE}');
        
        if($itemId) {
            $item = Item::newInstance()->findByPrimaryKey($itemId);
            
            $itemName = $item['s_title'];
            $itemUrl = '<a style="color: #0c72b9;text-decoration: none;border-bottom: 1px dashed #0c72b9;" href="' . osc_item_url_ns($itemId) . '">' . $itemName . '</a>';
        }
        else {$itemName = ''; $itemUrl = '';}
        
        if($userId) {
            $conInfo = $this->umTopicGet($topicId);
            
            $sender = User::newInstance()->findByPrimaryKey($userId);
            $senderName = $sender['s_name'];

            $contact = User::newInstance()->findByPrimaryKey($this->umUserConversationInterlocutor($topicId, $userId));
            
            if(!$toAdmin) list($contactName, $email, $conTitle) = array($contact['s_name'], $contact['s_email'], $conInfo['s_title']);
                else list($contactName, $email, $conTitle) = array('Administrator', osc_get_preference('contactEmail', 'osclass'), '');
            
            $conUrl = '<a style="color: #0c72b9;text-decoration: none;border-bottom: 1px dashed #0c72b9;" href="' . osc_route_url('umessages-view', array('cid' => $topicId)) . '">' . $conTitle . '</a>';
        }
        
        $words[] = array(osc_page_title(), $itemName, $conTitle, $contactName, $senderName, $itemUrl, $conUrl, $message);
        
        $title = osc_mailBeauty($content['s_title'], $words);
        $body  = osc_mailBeauty(($content['s_text']), $words);
        
        $emailParams = array('subject'   => $title,
                             'to'        => $email,
                             'to_name'   => $contactName,
                             'body'      => $body
                            );
        
        osc_sendMail($emailParams);
    }
    
    public function umUserDataClear($userId) {
        $getUserCons = $this->umUserConversationsGet($userId);
        
        if($getUserCons) {
            $getUserMessages = $this->umMessagesUserGet($userId);
            
            foreach($getUserCons as $con) {
                $this->umTopicDelete($con['fk_i_topic_id']);
            }
            
            foreach($getUserMessages as $message) {
                $this->umMessageDelete($message['pk_i_message_id'], $userId, true);
            }
            
            $this->umUserConversationsDelete($userId);
            $this->umUserBoxesDelete($userId);
        }
        
        return FALSE;
    }
    
    public function umLimitsCheck($conId, $itemId, $userId, $conUserId) {
        $isConExist = true; $isAntiSpamBlocked = false; $isItemExist = true; $isUserBlocked = false;
        
        $antiSpamCount = $this->umAntiSpamGet($userId);
        $checkBlocked = $this->umAntiSpamGet($userId, false);
        
        $isItemExist = Item::newInstance()->findByPrimaryKey($itemId);
        $isUserBlocked = $this->umBlockedUserCheck($userId, $conUserId);
        
        if(!is_null($conId)) $isConExist = $this->umUserBelongConversation($conId, $userId);
        
        if(!$isConExist) {
            osc_add_flash_error_message(__('Error! You can not send a message, this conversation does not exist!', 'uMessages'));
            
            return TRUE;
        }
        
        if($checkBlocked['i_admin_blocked']) {
            osc_add_flash_error_message(__('Error! You can not send a message, you have been Blocked by the Administrator!', 'uMessages'));
            
            return TRUE;
        }
            
        
        if(osc_get_preference('um_usr_antispam_enable','umess') && $antiSpamCount >= osc_get_preference('um_usr_antispam_number','umess')) $isAntiSpamBlocked = true;
        
        if(osc_get_preference('um_usr_antispam_enable','umess') && $isAntiSpamBlocked) {
            osc_add_flash_error_message(__('Error! You can not send a message, you have been Blocked by the Antispam protection!', 'uMessages'));
            
            return TRUE;
        }
        
        if(!$isItemExist) {
            osc_add_flash_error_message(__('Error! You can not send a message, Item does not Exist!', 'uMessages'));
            
            return TRUE;
        }
        
        if(osc_get_preference('um_usr_blist_enable','umess') && $isUserBlocked) {
            osc_add_flash_error_message(__('Error! You can not send a message, you have been Blacklisted!', 'uMessages'));
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    public function umUserBoxesIfExist($userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_boxes());
        $this->dao->where('i_user_id', $userId);
        $this->dao->where('s_box_type', 'inbox');
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            $inboxId = $result[0]['pk_i_id'];
        }
        else {
            $inboxId = $this->umUserBoxesCreate($userId);
        }
        
        return $inboxId;
    }
    
    public function umUserBoxGet($userId, $boxType) {
        $this->dao->select('pk_i_id');
        $this->dao->from($this->getTable_boxes());
        $this->dao->where('i_user_id', $userId);
        $this->dao->where('s_box_type', $boxType);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result[0]['pk_i_id'];
        }
        
        return FALSE;
    }
    
    public function umUserBoxesCreate($userId) {
        for($i = 0; $i < 3; $i++) {
            switch($i) {
                case 1: $boxType = 'archive'; break;
                case 2: $boxType = 'trash'; break;
                default: $boxType = 'inbox'; break;
            }
            
            $result = $this->dao->insert(
                            $this->getTable_boxes(),
                            array(
                                'i_user_id' => $userId,
                                's_box_type' => $boxType
                            )
                      );
                      
            if(!$i) $inboxId = $this->dao->insertedId();
        }
        
        return $inboxId;
    }
    
    public function umUserBoxesDelete($userId) {
        $this->dao->delete($this->getTable_boxes(), array('i_user_id' => $userId));
    }
    
    public function umConversationStart($itemId, $subject, $message, $files = false) {
        if($subject && $message) {
            $item = Item::newInstance()->findByPrimaryKey($itemId);
        
            if($this->umLimitsCheck(null, $itemId, osc_logged_user_id(), $item['fk_i_user_id'])) return FALSE;
            
            $curDate = date('Y-m-d H:i:s');
            
            $sId = osc_logged_user_id();
            $rId = $item['fk_i_user_id'];
            
            $sBoxId = $this->umUserBoxesIfExist($sId);
            $rBoxId = $this->umUserBoxesIfExist($rId);
            
            $topic = $this->dao->insert(
                            $this->getTable_topics(),
                            array(
                                's_title' => $subject,
                                'fk_i_item_id' => $itemId,
                                's_item_name' => $item['s_title'],
                                'd_date' => $curDate
                            )
                      );
                          
            $topicId = $this->dao->insertedId();
    
            $this->umMessageSend($topicId, $message, $sId, $curDate, $files);
            $this->umUserConversationCreate($sBoxId, $topicId, $sId, $rId, 2);
            $this->umUserConversationCreate($rBoxId, $topicId, $rId, $sId);
            
            if(osc_get_preference('um_notifi_enable','umess') && osc_get_preference('um_notifi_nt_enable','umess')) {
                $this->umSendNotify($topicId, 'email_conversation_started', $itemId, $sId, $message);
            }
            
            return $topicId;
        }
        
        osc_add_flash_error_message(__('Error! Input your Thread and Message', 'uMessages'));
        
        return FALSE;
    }
    
    public function umUserConversationCreate($boxId, $topicId, $userId, $cUserId, $read = 0) {
        $this->dao->insert(
                $this->getTable_user_boxes(),
                array(
                    'fk_i_box_id' => $boxId,
                    'fk_i_topic_id' => $topicId,
                    'i_user_id' => $userId,
                    'i_user_con_id' => $cUserId,
                    'i_read' => $read
                )
        );
    }
    
    public function umUserConversationGet($topicId, $userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where(sprintf('fk_i_topic_id = %s AND i_user_id = %s', $topicId, $userId));
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result[0];
        }
        
        return FALSE;
    }
    
    public function umUserConversationsGet($userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where(sprintf('i_user_id = %s OR i_user_con_id = %s', $userId, $userId));
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result;
        }
        
        return FALSE;
    }
    
    public function umUserConversationsDelete($userId) {
        $this->dao->delete($this->getTable_user_boxes(), sprintf('i_user_id = %s OR i_user_con_id = %s', $userId, $userId));
    }
    
    public function umUserBelongConversation($topicId, $userId) {
        $getTopic = $this->umTopicGet($topicId);
        
        $this->dao->select('*');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where(sprintf('fk_i_topic_id = %s AND (i_user_id = %s OR i_user_con_id = %s)', $topicId, $userId, $userId));
        
        $result = $this->dao->get();
        
        if($result->numRows()) return TRUE;
        
        return FALSE;
    }
    
    public function umUserConversationsUnreadCount($userId) {
        $this->dao->select('COUNT(pk_i_ubox_id) AS i_count');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where('i_user_id', $userId);
        $this->dao->where('i_read', 0);
        $this->dao->where('fk_i_box_id != 0');
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result[0]['i_count'];
        }
        
        return 0;
    }
    
    public function umUserConversationReadStatus($topicId, $userId) {
        $this->dao->select('i_read');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where('fk_i_topic_id', $topicId);
        $this->dao->where('i_user_id', $userId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result[0]['i_read'];
        }
        
        return FALSE;
    }
    
    public function umUserConversationReadStatusChange($topicId, $userId, $status = 1, $a = false) {
        $this->dao->select('i_read');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where('fk_i_topic_id', $topicId);
        $this->dao->where('i_user_id', $userId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            $readStatus = $result[0]['i_read'];

            if(!$readStatus && $status == 1) $this->dao->update($this->getTable_user_boxes(), array('i_read' => 1), array('fk_i_topic_id' => $topicId, 'i_user_id' => $userId));
            else if($a) $this->dao->update($this->getTable_user_boxes(), array('i_read' => $status), array('fk_i_topic_id' => $topicId, 'i_user_id' => $userId));
        }
    }
    
    public function umUserConversationInterlocutor($topicId, $userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_user_boxes());
        $this->dao->where(sprintf("fk_i_topic_id = %d AND i_user_id = %d", $topicId, $userId));
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            $conUserId = $result[0]['i_user_con_id'];
            
            return $conUserId;
        }
        
        return FALSE;
    }
    
    public function umUserConversationActions($userId, $action, $id) {
        switch($action) {
            case 'munread':
                $this->dao->update($this->getTable_user_boxes(), array('i_read' => 0), array('fk_i_topic_id' => $id, 'i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! Conversation successfully marked as Unread', 'uMessages'));
            break;
            
            case 'mspam':
                $spammerId = $this->umUserConversationInterlocutor($id, $userId);
                
                $this->umAntiSpamSet($spammerId, $id);
                $this->dao->update($this->getTable_user_boxes(), array('fk_i_box_id' => 0), array('fk_i_topic_id' => $id, 'i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! Conversation successfully marked as Spam', 'uMessages'));
            break;
            
            case 'ublock':
                $blockedUserId = $this->umUserConversationInterlocutor($id, $userId);
                $status = $this->umBlockedUserAdd($blockedUserId, $userId);
                
                if($status) osc_add_flash_ok_message(__('Congratulations! User successfully moved to Black List', 'uMessages'));
                    else osc_add_flash_error_message(__('Error! User is already blacklisted', 'uMessages'));
            break;
            
            case 'unblock':
                $status = $this->dao->delete($this->getTable_blocked(), array('pk_i_block_id' => $id, 'fk_i_user_id' => $userId));
                
                if($status) osc_add_flash_ok_message(__('Congratulations! User successfully removed from Black List', 'uMessages'));
                    else osc_add_flash_error_message(__('Error! You can not remove the User from Black List', 'uMessages'));
            break;
            
            case 'minbox':
                $boxId = $this->umUserBoxGet($userId, 'inbox');

                $this->dao->update($this->getTable_user_boxes(), array('fk_i_box_id' => $boxId), array('fk_i_topic_id' => $id, 'i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! Conversation successfully moved to Inbox', 'uMessages'));
            break;
            
            case 'marchive':
                $boxId = $this->umUserBoxGet($userId, 'archive');

                $this->dao->update($this->getTable_user_boxes(), array('fk_i_box_id' => $boxId), array('fk_i_topic_id' => $id, 'i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! Conversation successfully moved to Archive', 'uMessages'));
            break;
            
            case 'mtrash':
                $boxId = $this->umUserBoxGet($userId, 'trash');
                
                $this->dao->update($this->getTable_user_boxes(), array('fk_i_box_id' => $boxId), array('fk_i_topic_id' => $id, 'i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! Conversation successfully moved to Trash', 'uMessages'));
            break;
            
            case 'mdelete':
                $this->dao->delete($this->getTable_user_boxes(), array('fk_i_topic_id' => $id, 'i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! Conversation successfully Deleted', 'uMessages'));
            break;
        }
        
        return TRUE;
    }
    
    public function umTopicGet($topicId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_topics());
        $this->dao->where('pk_i_topic_id', $topicId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            $data = $result[0];
            
            return $data;
        }
        
        return FALSE;
    }
    
    public function umTopicsGet($userId, $boxType) {
        $boxId = $this->umUserBoxGet($userId, $boxType);
        
        if($boxId) {
            $this->dao->select('ub.*, t.*, u.s_name, i.fk_i_user_id');
            $this->dao->from($this->getTable_user_boxes() . ' ub');
            $this->dao->join($this->getTable_topics() . ' t', 'ub.fk_i_topic_id = t.pk_i_topic_id ', 'LEFT');
            $this->dao->join($this->getTable_users() . ' u', 'ub.i_user_con_id = u.pk_i_id ', 'LEFT');
            $this->dao->join($this->getTable_items() . ' i', 't.fk_i_item_id = i.pk_i_id ', 'LEFT');
            $this->dao->where('ub.fk_i_box_id', $boxId);
            $this->dao->orderBy('t.d_date', 'DESC');
            
            $result = $this->dao->get();
            
            if($result->numRows()) {
                $result = $result->result();
                
                return $result;
            }
            
            return FALSE;
        }
        return FALSE;
    }
    
    public function umTopicDateChange($topicId) {
        $this->dao->update($this->getTable_topics(), array('d_date' => date('Y-m-d H:i:s')), array('pk_i_topic_id' => $topicId));
    }
    
    public function umTopicDelete($topicId) {
        $this->dao->delete($this->getTable_topics(), array('pk_i_topic_id' => $topicId));
    }
    
    public function umBlockedUserCheck($blockedUserId, $userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_blocked());
        $this->dao->where('fk_i_user_id', $userId);
        $this->dao->where('i_user_block_id', $blockedUserId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) return TRUE;
        
        return FALSE;
    }
    
    public function umBlockedUsersGet($userId) {
        $this->dao->select('ub.*, u.s_name');
        $this->dao->from($this->getTable_blocked() . ' ub');
        $this->dao->join($this->getTable_users() . ' u', 'ub.i_user_block_id = u.pk_i_id ', 'LEFT');
        $this->dao->where('ub.fk_i_user_id', $userId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result;
        }
        
        return FALSE;
    }
    
    public function umBlockedUserAdd($blockedUserId, $userId) {
        $checkUserBlocked = $this->umBlockedUserCheck($blockedUserId, $userId);
        
        if(!$checkUserBlocked) {
            $this->dao->insert(
                    $this->getTable_blocked(),
                    array(
                        'fk_i_user_id' => $userId,
                        'i_user_block_id' => $blockedUserId
                    )
            );
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    public function umAntiSpamGet($userId, $spamCount = true) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_antispam());
        $this->dao->where('fk_i_user_id', $userId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            if($spamCount) $data = $result[0]['i_spam_count'];
                else $data = $result[0];
            
            
            return $data;
        }
        
        return FALSE;
    }
    
    public function umAntiSpamSet($userId, $topicId) {
        $spamCount = $this->umAntiSpamGet($userId);
        $updSpamCount = $spamCount + 1;

        if(!$spamCount) {
            $this->dao->insert(
                    $this->getTable_antispam(),
                    array(
                        'fk_i_user_id' => $userId,
                        'i_spam_count' => 1
                    )
            );
        }
        else {
            $this->dao->update($this->getTable_antispam(), array('i_spam_count' => $updSpamCount), array('fk_i_user_id' => $userId));
        }
        
        if(osc_get_preference('um_notifi_enable','umess') && osc_get_preference('um_aspam_notifi_enable','umess') && $updSpamCount >= osc_get_preference('um_usr_antispam_number','umess')) {
            $this->umSendNotify(false, 'email_anti_spam_blocked', false, $userId, false, true);
        }
        
        $this->dao->update($this->getTable_topics(), array('i_spammed' => 1), array('pk_i_topic_id' => $topicId));
    }
    
    public function umMessageGet($messageId) {      
        $this->dao->select('*');
        $this->dao->from($this->getTable_messages());
        $this->dao->where('pk_i_message_id', $messageId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            $data = $result[0];
            
            return $data;
        }
        
        return FALSE;
    }
    
    public function umMessagesGet($topicId, $userId) {
        $checkUserBelongs = $this->umUserBelongConversation($topicId, $userId);
        
        if(!$checkUserBelongs) return FALSE;
        
        $this->umUserConversationReadStatusChange($topicId, $userId);
        
        $this->dao->select('m.*, u.s_name');
        $this->dao->from($this->getTable_messages() . ' m');
        $this->dao->join($this->getTable_users() . ' u', 'm.fk_i_user_id = u.pk_i_id ', 'LEFT');
        $this->dao->where('fk_i_topic_id', $topicId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $messages = array();
            $result = $result->result();
            
            foreach($result as $i => $message) {
                $messages[$i] = $message;
                
                $getAttacments = $this->umAttachmentsGet($message['pk_i_message_id']);
                if($getAttacments) $messages[$i]['attachments'] = $getAttacments;
            }
            
            return $messages;
        }
        
        return FALSE;
    }
    
    public function umMessagesUserGet($userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_messages());
        $this->dao->where('fk_i_user_id', $userId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result;
        }
        
        return FALSE;
    }
    
    public function umMessageSend($topicId, $message, $userId, $date, $files = false) {
        if($message) {
            $this->dao->insert(
                    $this->getTable_messages(),
                    array(
                        'fk_i_topic_id' => $topicId,
                        's_message' => $message,
                        'fk_i_user_id' => $userId,
                        'd_message_date' => $date
                    )
            );
            
            $messageId = $this->dao->insertedId();
            
            if(!empty($files) && $messageId) {
                foreach($files['name'] as $key => $value) {
                    $fileName = uniqid() . '_' . $value;
                    $filePath = sprintf($_SERVER['DOCUMENT_ROOT'] . '/oc-content/plugins/uMessages/uploads/%s', $fileName);
                    $tempFile = $files['tmp_name'][$key];
                    
                    if(move_uploaded_file($tempFile, $filePath)) $this->umAttachmentsPost($fileName, $messageId);
                }
            }
            
            osc_add_flash_ok_message(__('Congratulations! Message sent successfully', 'uMessages'));
            
            return $messageId;
        }
        
        return FALSE;
    }
    
    public function umMessageReply($topicId, $message, $userId, $files = false) {
        $topicInfo = $this->umTopicGet($topicId);
        $cUserId = $this->umUserConversationInterlocutor($topicId, $userId);
        
        if($this->umLimitsCheck($topicId, $topicInfo['fk_i_item_id'], $userId, $cUserId)) return FALSE;
        
        if(osc_get_preference('um_notifi_enable','umess') && osc_get_preference('um_notifi_nm_enable','umess')) {
            if(osc_get_preference('um_nm_only_once_enable','umess')) {
                $isUnreadExist = $this->umUserConversationReadStatus($topicId, $cUserId);
                if($isUnreadExist) $this->umSendNotify($topicId, 'email_message_sent', false, $userId, $message);
            }
            else $this->umSendNotify($topicId, 'email_message_sent', false, $userId, $message);
        }
        
        $this->umUserConversationReadStatusChange($topicId, $userId, 2, true);
        $this->umUserConversationReadStatusChange($topicId, $cUserId, 0, true);
        $messageId = $this->umMessageSend($topicId, $message, $userId, date('Y-m-d H:i:s'));
        $this->umTopicDateChange($topicId);
        
        if(!empty($files) && $messageId) {
            foreach($files['name'] as $key => $value) {
                $fileName = uniqid() . '_' . $value;
                $filePath = sprintf($_SERVER['DOCUMENT_ROOT'] . '/oc-content/plugins/uMessages/uploads/%s', $fileName);
                $tempFile = $files['tmp_name'][$key];
                
                if(move_uploaded_file($tempFile, $filePath)) $this->umAttachmentsPost($fileName, $messageId);
            }
        }
        
        $isInterConversationExist = $this->umUserConversationGet($topicId, $cUserId);
        
        if(!$isInterConversationExist) {
            $getInterBoxId = $this->umUserBoxGet($cUserId, 'inbox');
            $this->umUserConversationCreate($getInterBoxId, $topicId, $cUserId, $userId);
        }
        
        if(!$messageId) osc_add_flash_error_message(__('Error! Input your message', 'uMessages'));
    }
    
    public function umMessageDelete($messageId, $userId, $userDeleted = false) {
        $messData = $this->umMessageGet($messageId);
        $checkBlocked = $this->umAntiSpamGet($userId, false);
        
        if($checkBlocked['i_admin_blocked'] || $checkBlocked['i_spam_count'] > osc_get_preference('um_usr_antispam_number','umess')) {
            osc_add_flash_error_message(__('Error! You can not delete a message, because you have been blocked', 'uMessages'));
            
            return FALSE;
        }
        
        if(isset($messData) && $messData['fk_i_user_id'] == $userId) {
            $this->umAttachmentsDelete($messageId);
            
            if(!$userDeleted) osc_add_flash_ok_message(__('Congratulations! Message successfully deleted', 'uMessages'));
            
            return $this->dao->delete($this->getTable_messages(), array('pk_i_message_id' => $messageId, 'fk_i_user_id' => $userId));
        }
        
        return FALSE;
    }
    
    public function umAttachmentsAllGet() {
        $this->dao->select('s_path, s_file');
        $this->dao->from($this->getTable_attachments());
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result;
        }
        
        return FALSE;
    }
    
    public function umAttachmentsGet($messageId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_attachments());
        $this->dao->where('fk_i_message_id', $messageId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $attachments = array();
            $result = $result->result();
            
            foreach($result as $i => $attachment) {
                $attachments[$i]['attachment'] = $this->umAttachmentThumbnailsGet($attachment['s_file'], $attachment['s_path']);
            }
            
            return $attachments;
        }
        
        return FALSE;
    }
    
    public function umAttachmentThumbnailsGet($file, $path) {
        $zoomId = ''; 
        $link = '<a href="' . UMS_USR_UPLOADS . $file . '" target="_blank">' . osc_highlight($file, 20) . '</a>';
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        
        if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'bmp' || $ext == 'tiff') {
            $zoomId = 'id="img-' . $file . '" style="width:' . osc_get_preference('um_thumb_size','umess') . 'px !important;" class="zoomTarget" data-targetsize="0.75" data-closeclick="true"';
            $link = '';
            $thumb = $path . $file;
        } 
        elseif($ext == 'zip' || $ext == 'rar' || $ext == 'tar' || $ext == '7z') {
            $thumb = UMS_USR_IMG . 'zip.png';
        }
        elseif($ext == 'txt') {
            $thumb = UMS_USR_IMG . 'txt.png';
        }
        elseif($ext == 'doc' || $ext == 'docx') {
            $thumb = UMS_USR_IMG . 'doc.png';
        }
        elseif($ext == 'xls' || $ext == 'xlsx') {
            $thumb = UMS_USR_IMG. 'xls.png';
        } 
        elseif($ext == 'ppt' || $ext == 'pptx') {
            $thumb = UMS_USR_IMG. 'ppt.png';
        }
        elseif($ext == 'pdf') {
            $thumb = UMS_USR_IMG . 'pdf.png';
        }
        else{
            $thumb = UMS_USR_IMG. 'folder.png';
        }
        
        return '<img ' . $zoomId . ' src="' . $thumb . '" alt="' . $file . '" /> ' . $link;
    }
    
    public function umAttachmentsPost($file, $messageId) {
        $this->dao->insert(
                $this->getTable_attachments(),
                array(
                    'fk_i_message_id' => $messageId,
                    's_path' => '/oc-content/plugins/uMessages/uploads/',
                    's_file' => $file
                )
        );
    }
    
    public function umAttachmentsDelete($messageId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_attachments());
        $this->dao->where('fk_i_message_id', $messageId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            foreach($result as $attachment) {
                @unlink($_SERVER['DOCUMENT_ROOT'] . $attachment['s_path'] . $attachment['s_file']);
            }
            
            $this->dao->delete($this->getTable_attachments(), array('fk_i_message_id' => $messageId));
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    public function umAttachmentsAllRemove() {
        $getAttachments = $this->umAttachmentsAllGet();
        
        if($getAttachments) {
            foreach($getAttachments as $attachment) {
                @unlink($_SERVER['DOCUMENT_ROOT'] . $attachment['s_path'] . $attachment['s_file']);
            }
        }
        
        return FALSE;
    }
    
    public function umPetitionExist($userId) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_petitions());
        $this->dao->where('fk_i_user_id', $userId);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            return TRUE;
        }
        
        return FALSE;
    }
    
    public function umPetitionSend($userId, $message) {
        $checkAlerts = $this->umPetitionExist($userId);
        
        if(!$checkAlerts) {
            $result = $this->dao->insert(
                            $this->getTable_petitions(),
                            array(
                                'fk_i_user_id' => $userId,
                                's_message' => $message,
                                'd_petition_date' => date('Y-m-d H:i:s')
                            )
                    );
                    
            if(osc_get_preference('um_notifi_enable','umess') && osc_get_preference('um_na_notifi_enable','umess')) {
                $this->umSendNotify(false, 'email_petition_sent', false, $userId, $message, true);
            }
                    
            return $result;
        }
        
        return FALSE;
    }
    
    public function umAlertsOutputs($id, $userId, $new = false) {
        $isConExist = true; $isAntiSpamBlocked = false; $isItemExist = true; $isUserBlocked = false; $isAdminBlocked = false;
        
        $checkBlocked = $this->umAntiSpamGet($userId, false);
        
        $antiSpamCount = $this->umAntiSpamGet($userId);
        if(osc_get_preference('um_usr_antispam_enable','umess') && $antiSpamCount >= osc_get_preference('um_usr_antispam_number','umess')) $isAntiSpamBlocked = true;
          
        if($new) {
            $isItemExist = Item::newInstance()->findByPrimaryKey($id);
            
            if(!$isItemExist) return __('You can not send a message, Item does not Exist!', 'uMessages');
            if(is_null($isItemExist['fk_i_user_id'])) return __('You can not send a message, the User does not Exist or is Blocked!', 'uMessages');

            $isUserBlocked = $this->umBlockedUserCheck($userId, $isItemExist['fk_i_user_id']);
        }
        else {
            $isConExist = $this->umUserBelongConversation($id, $userId);
            
            $cUserId = $this->umUserConversationInterlocutor($id, $userId);
            
            $topicInfo = $this->umTopicGet($id);
            $isItemExist = Item::newInstance()->findByPrimaryKey($topicInfo['fk_i_item_id']);
            
            $isUserBlocked = $this->umBlockedUserCheck($userId, $cUserId);
        }
        
        if(!$isConExist)
            return __('You can not send a message, this conversation does not exist!', 'uMessages');
        
        if($checkBlocked['i_admin_blocked'])
            return __('You can not send a message, you have been Blocked by the Administrator!', 'uMessages');
            
        if(osc_get_preference('um_usr_antispam_enable','umess') && osc_get_preference('um_amess_as_enable','umess') && $isAntiSpamBlocked) {
            $checkAlerts = $this->umPetitionExist($userId);
            
            if(!$checkAlerts) $reqLink = __('If you believe that you were blocked by mistake, you can', 'uMessages') . ' <a href="' . osc_route_url('umessages-petition') . '">' . __('Send a Petition', 'uMessages') . '</a>';
            
            return __('You can not send a message, you have been Blocked by the Antispam protection! ', 'uMessages') . $reqLink;
        }
        
        if(osc_get_preference('um_amess_id_enable','umess') && !$isItemExist) 
            return __('You can not send a message, Item Removed!', 'uMessages');
        
        if(osc_get_preference('um_usr_blist_enable','umess') && osc_get_preference('um_amess_bl_enable','umess') && $isUserBlocked) 
            return __('You can not send a message, you have been Blacklisted!', 'uMessages');
        
        return FALSE;
    }
    
    /* ADMIN PANEL */
    public function umAdminPageHeader() {
        $_r = Params::getParam('route');
        
        switch($_r) {
            case 'um-dashboard' :
                $header = 'Dashboard';
            break;
            
            case 'um-petitions' :
                $header = 'Petitions';
            break;
            
            case 'um-messages' :
                $header = 'Messages';
            break;
            
            case 'um-blocked' :
                $header = 'Blocked';
            break;
            
            case 'um-settings' :
                $header = 'Settings';
            break;
            
            case 'um-help' :
                $header = 'Help';
            break;
        }
        
        return $header;
    }
    
    public function umAdminStatisticsGet($type, $today = false, $where = '') {
        switch($type) {
            case 'messages':
                $table = $this->getTable_messages();
                
                if($today) $where = $this->dao->where('d_message_date >= CURDATE()');
            break;
            
            case 'petitions':
                $table = $this->getTable_petitions();
                if($today) $where = $this->dao->where('d_petition_date >= CURDATE()');
            break;
            
            case 'user-blocks':
                $table = $this->getTable_antispam();
                $where = $this->dao->where('i_admin_blocked', 1);
            break;
        }
        
        $this->dao->select('*');
        $this->dao->from($table);
        $where;
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return count($result);
        }
        
        return 0;
    }
    
    public function umAdminChartsData($type, $period) {
        $countData = '';
        $date = time('Y-m-d');
        $getPeriods = $this->umAdminChartsTimePeriods($period);
        
        if($type == 'sent') $table = $this->getTable_messages();
            else $table = $this->getTable_antispam();
        
        foreach($getPeriods as $date) {
            $this->dao->select('*');
            $this->dao->from($table);
            if($type == 'sent') $where = $this->dao->where("DATE_FORMAT(d_message_date, '%Y-%m-%d') = '" . $date . "'");
                else $where = $this->dao->where("i_admin_blocked = 1 AND DATE_FORMAT(d_admin_blocked_date, '%Y-%m-%d') = '" . $date . "'");
            
            $result = $this->dao->get();
            
            if($result->numRows()) {
                $result = $result->result();
                
                $count = count($result);
            }
            else $count = 0;
            
            $countData .= $count . ',';
        }
        
        $countData = explode(',', $countData);
        
        return $countData;
    }

    public function umAdminChartsTimePeriods($period, $revDate = true) {
        $arrDates = array(); $strData = '';
        
        $recurr = ceil($period / 7);

        $previousDayDate = new DateTime();
        
        for($dayInterval = 0; $dayInterval < 7; $dayInterval++) {
            if($revDate) array_unshift($arrDates, $previousDayDate->format('Y-m-d'));
                else array_unshift($arrDates, $previousDayDate->format('d-m-Y'));
                
            $previousDayDate->sub(new DateInterval('P' . $recurr . 'D'));
        }
        
        return $arrDates;
    }
    
    public function umAdminPetitionGet($id) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_petitions());
        $this->dao->where('pk_i_petition_id', $id);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $result = $result->result();
            
            return $result[0];
        }
        
        return FALSE;
    }
    
    public function umAdminPetitionsGet($total = false, $start = 1, $limit = 20) {
        $this->dao->select('*');
        $this->dao->from($this->getTable_petitions());
        $this->dao->orderBy('d_petition_date', 'DESC');
        
        if($total) $this->dao->limit ($start, $limit);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $petitions = array();
            $result = $result->result();
            
            foreach($result as $i => $petition) {
                $petitions[$i] = $petition;
                
                $user = User::newInstance()->findByPrimaryKey($petition['fk_i_user_id']);
                $petitions[$i]['s_name'] = $user['s_name'];
                
                switch($petition['i_petition_status']) {
                    case 0: $status = '<span class="alert alert-danger">' . __('Rejected', 'uMessages') . '</span>'; break; 
                    case 1: $status = '<span class="alert alert-success">' . __('Accepted', 'uMessages') . '</span>'; break;
                    default: $status = '<span class="alert alert-warning">' . __('New', 'uMessages') . '</span>'; break;
                }
                
                $petitions[$i]['status'] = $status;
            }
            
            return $petitions;
        }
        
        return FALSE;
    }
    
    public function umAdminPetitionActions($action, $petitionId) {
        if($action == 'petition-accepted') {
            $getPetition = $this->umAdminPetitionGet($petitionId);
            
            $checkBlocked = $this->umAntiSpamGet($getPetition['fk_i_user_id'], false);

            if(!$checkBlocked['i_admin_blocked']) {
                $this->dao->delete($this->getTable_antispam(), array('pk_i_spam_id' => $checkBlocked['pk_i_spam_id']));
                $this->dao->update($this->getTable_petitions(), array('i_petition_status' => 1), array('pk_i_petition_id' => $petitionId));
                
                osc_add_flash_ok_message(__('Congratulations! Petition Accepted successfully', 'uMessages'), 'admin');
            }
            else {
                $this->dao->update($this->getTable_petitions(), array('i_petition_status' => 0), array('pk_i_petition_id' => $petitionId));
                
                osc_add_flash_error_message(__('Error! You can not accept petition, because user was blocked by Administrator', 'uMessages'), 'admin');
            }
        }
        elseif($action == 'petition-rejected') {
            $this->dao->update($this->getTable_petitions(), array('i_petition_status' => 0), array('pk_i_petition_id' => $petitionId));
            
            osc_add_flash_ok_message(__('Congratulations! Petition Rejected successfully', 'uMessages'), 'admin');
        }
    }
    
    public function umAdminMessagesGet($total = false, $start = 1, $limit = 20) {        
        $this->dao->select('m.*, t.fk_i_item_id, t.s_item_name, u.s_name');
        $this->dao->from($this->getTable_messages() . ' m');
        $this->dao->join($this->getTable_topics() . ' t', 'm.fk_i_topic_id = t.pk_i_topic_id ', 'LEFT');
        $this->dao->join($this->getTable_users() . ' u', 'm.fk_i_user_id = u.pk_i_id ', 'LEFT');
        $this->dao->orderBy('m.d_message_date', 'DESC');
        
        if($total) $this->dao->limit ($start, $limit);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $messages = array();
            $result = $result->result();
            
            foreach($result as $i => $message) {
                $messages[$i] = $message;
                
                $checkBlocked = $this->umAntiSpamGet($message['fk_i_user_id'], false);
                
                if($checkBlocked['i_admin_blocked']) {
                    $status = '<span class="alert alert-danger">' . __('Admin Blocked', 'uMessages') . '</span>';
                }
                elseif($checkBlocked['i_spam_count'] > osc_get_preference('um_usr_antispam_number', 'umess')) {
                    $status = '<span class="alert alert-danger">' . __('Spam Blocked', 'uMessages') . '</span>';
                }
                else {
                    $status = '<span class="alert alert-success">' . __('Active', 'uMessages') . '</span>';
                }
                
                $messages[$i]['status'] = $status;
                $messages[$i]['admin_blocked'] = $checkBlocked['i_admin_blocked'];
                $messages[$i]['ads'] = '<a href="' . osc_item_url_ns($message['fk_i_item_id'], osc_locale_code()) . '" target="_blank">' . osc_highlight($message['s_item_name'], 100) . '</a>';
            }
            
            return $messages;
        }
        
        return FALSE;
    }
    
    public function umAdminBlockedUsersGet($total = false, $start = 1, $limit = 20) {
        $this->dao->select('aspm.*, m.s_message, u.s_name');
        $this->dao->from($this->getTable_antispam() . ' aspm');
        $this->dao->join($this->getTable_messages() . ' m', 'aspm.i_message_id = m.pk_i_message_id ', 'LEFT');
        $this->dao->join($this->getTable_users() . ' u', 'aspm.fk_i_user_id = u.pk_i_id ', 'LEFT');
        //$this->dao->where('aspm.i_admin_blocked', 1);
        $this->dao->orderBy('aspm.pk_i_spam_id', 'DESC');
        
        if($total) $this->dao->limit ($start, $limit);
        
        $result = $this->dao->get();
        
        if($result->numRows()) {
            $users = array();
            $result = $result->result();
            
            foreach($result as $i => $user) {
                $users[$i] = $user;
                
                if($user['i_admin_blocked']) {
                    $status = '<span class="alert alert-danger">' . __('Admin Blocked', 'uMessages') . '</span>';
                }
                elseif(!$user['i_admin_blocked'] && $user['i_spam_count'] >= osc_get_preference('um_usr_antispam_number', 'umess')) {
                    $status = '<span class="alert alert-danger">' . __('Anti-Spam Blocked', 'uMessages') . '</span>';
                }
                
                $users[$i]['status'] = $status;
            }
            
            return $users;
        }
        
        return FALSE;
    }
    
    public function umAdminBlockedUserActions($action, $messageId, $userId) {
        $checkBlocked = $this->umAntiSpamGet($userId, false);
        
        if($action == 'user-block') {
            if(is_array($checkBlocked) && !$checkBlocked['i_admin_blocked']) {
                $this->dao->update($this->getTable_antispam(), array('i_admin_blocked' => 1, 'i_message_id' => $messageId, 'd_admin_blocked_date' => date('Y-m-d H:i:s')), array('fk_i_user_id' => $userId));
            }
            else {
                $this->dao->insert(
                        $this->getTable_antispam(),
                        array(
                            'fk_i_user_id' => $userId,
                            'i_spam_count' => 0,
                            'i_admin_blocked' => 1,
                            'i_message_id' => $messageId,
                            'd_admin_blocked_date' => date('Y-m-d H:i:s')
                        )
                );
            }
            
            osc_add_flash_ok_message(__('Congratulations! User Blocked successfully', 'uMessages'), 'admin');
        }
        elseif($action == 'user-unblock') {
            if(is_array($checkBlocked) && $checkBlocked['i_admin_blocked']) {
                $this->dao->update($this->getTable_antispam(), array('i_admin_blocked' => 0, 'i_message_id' => 0, 'd_admin_blocked_date' => '0000-00-00 00:00:00'), array('fk_i_user_id' => $userId));
            
                osc_add_flash_ok_message(__('Congratulations! User Unblocked successfully', 'uMessages'), 'admin');
            }
            elseif(!$checkBlocked['i_admin_blocked'] && $checkBlocked['i_spam_count'] >= osc_get_preference('um_usr_antispam_number', 'umess')) {
                $this->dao->delete($this->getTable_antispam(), array('fk_i_user_id' => $userId));
                
                osc_add_flash_ok_message(__('Congratulations! User Unblocked successfully', 'uMessages'), 'admin');
            }
            else {
                osc_add_flash_error_message(__('Error! You can not Unblocked User, because User was not blocked by Administrator', 'uMessages'), 'admin');
            }
        }
    }
    
    public function umAdminPagination($currPage, $totalData, $route, $perPage = 20, $showPage = 5) {
        $countPages = floor($totalData / $perPage);
        if($countPages == 1) $countPages = 2;
        
        if($currPage < 1) {
            ob_get_clean();
            osc_redirect_to(osc_route_admin_url($route));
        }
        
        if($currPage > max(1, $countPages)) {
            ob_get_clean(); 
            osc_redirect_to(osc_route_admin_url($route, array('upage' => max(1, $countPages))));
        }
        
        if($countPages) {
            $pagination = '<ul class="um-pagination">';
            
            if($currPage - $showPage > 1) $pagination .= '<li onclick="javascript:location.href=\''. osc_route_admin_url($route, array('upage' => 1)) . '\';"><a href="' . osc_route_admin_url($route, array('upage' => 1)) . '">' . __('First') . '</a></li>';
            
            if($currPage - 1 >= 1) $pagination .= '<li onclick="javascript:location.href=\''. osc_route_admin_url($route, array('upage' => $currPage - 1)) . '\';"><a href="' . osc_route_admin_url($route, array('upage' => $currPage - 1)) . '">' . __('Back') . '</a></li>';
            
            $start = max($currPage - $showPage, 1);
    		$finish = min($currPage + $showPage, $countPages);
            
    		if($finish < $showPage * 2) $finish = min($showPage * 2, $countPages);
    		if($finish == $countPages - 1 && $start > 3) $start = max(3, min($countPages - $showPage * 2 + 1, $start));

            for($i = $start; $i <= $finish; $i++) {
                if($i == $currPage) {
                    $currPageClass = 'class="active-page"';
                    $url = 'javascript:void(0);';
                    $listScript = '';
                }
                else {
                    $currPageClass = '';
                    $url = osc_route_admin_url($route, array('upage' => $i));
                    $listScript = 'onclick="javascript:location.href=\''. $url . '\';"';
                }
                
                $pagination .= '<li ' . $currPageClass .' ' . $listScript . '><a href="' . $url . '">' . $i . '</a></li>';
            }
            
            if($currPage + 1 <= $countPages) $pagination .= '<li onclick="javascript:location.href=\''. osc_route_admin_url($route, array('upage' => $currPage + 1)) . '\';"><a href="' . osc_route_admin_url($route, array('upage' => $currPage + 1)) . '">' . __('Next') . '</a></li>';
            if($currPage + $showPage < $countPages) $pagination .= '<li onclick="javascript:location.href=\''. osc_route_admin_url($route, array('upage' => $countPages)) . '\';"><a href="' . osc_route_admin_url($route, array('upage' => $countPages)) . '">' . __('Last') . '</a></li>';
            
            $pagination .= '</ul>';
            
            return $pagination;
        }
        
        return FALSE;
    }
}