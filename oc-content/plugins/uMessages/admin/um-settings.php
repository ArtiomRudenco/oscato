<?php defined('ABS_PATH') or die('Access denied'); 
    if(Params::getParam('plugin_action')=='done') {
        /* General Settings */
        osc_set_preference('um_messages_enable', Params::getParam("um_messages_enable") ? Params::getParam("um_messages_enable") : '0', 'umess', 'BOOLEAN'); 
        osc_set_preference('um_btn_enable', Params::getParam("um_btn_enable") ? Params::getParam("um_btn_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_cs_replace_enable', Params::getParam("um_cs_replace_enable") ? Params::getParam("um_cs_replace_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_umenu_link_enable', Params::getParam("um_umenu_link_enable") ? Params::getParam("um_umenu_link_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_ur_mark_enable', Params::getParam("um_ur_mark_enable") ? Params::getParam("um_ur_mark_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_rm_msg_enable', Params::getParam("um_rm_msg_enable") ? Params::getParam("um_rm_msg_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_widget_enabled', Params::getParam("um_widget_enabled") ? Params::getParam("um_widget_enabled") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_widget_position', Params::getParam("um_widget_position"), 'umess', 'STRING');
        osc_set_preference('um_widget_style', Params::getParam("um_widget_style"), 'umess', 'STRING');
        osc_set_preference('um_fcolor', Params::getParam("um_fcolor"), 'umess', 'STRING');
        osc_set_preference('um_lbcolor', Params::getParam("um_lbcolor"), 'umess', 'STRING');
        osc_set_preference('um_bgcolor', Params::getParam("um_bgcolor"), 'umess', 'STRING');
        
        
        
        /* Attachments */
        osc_set_preference('um_attach_enable', Params::getParam("um_attach_enable") ? Params::getParam("um_attach_enable") : '0', 'umess', 'BOOLEAN'); 
        osc_set_preference('um_fext', Params::getParam("um_fext"), 'umess', 'STRING');
        osc_set_preference('um_fmaxsize', Params::getParam("um_fmaxsize"), 'umess', 'STRING');
        osc_set_preference('um_max_attach', Params::getParam("um_max_attach"), 'umess', 'STRING');
        osc_set_preference('um_thumb_size', Params::getParam("um_thumb_size"), 'umess', 'INTEGER');
        
        /* Notifications */
        osc_set_preference('um_notifi_enable', Params::getParam("um_notifi_enable") ? Params::getParam("um_notifi_enable") : '0', 'umess', 'BOOLEAN'); 
        osc_set_preference('um_notifi_nt_enable', Params::getParam("um_notifi_nt_enable") ? Params::getParam("um_notifi_nt_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_notifi_nm_enable', Params::getParam("um_notifi_nm_enable") ? Params::getParam("um_notifi_nm_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_nm_only_once_enable', Params::getParam("um_nm_only_once_enable") ? Params::getParam("um_nm_only_once_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_na_notifi_enable', Params::getParam("um_na_notifi_enable") ? Params::getParam("um_na_notifi_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_aspam_notifi_enable', Params::getParam("um_aspam_notifi_enable") ? Params::getParam("um_aspam_notifi_enable") : '0', 'umess', 'BOOLEAN');
        
        /* Auto Messages */ 
        osc_set_preference('um_amess_enable', Params::getParam("um_amess_enable") ? Params::getParam("um_amess_enable") : '0', 'umess', 'BOOLEAN'); 
        osc_set_preference('um_amess_as_enable', Params::getParam("um_amess_as_enable") ? Params::getParam("um_amess_as_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_amess_bl_enable', Params::getParam("um_amess_bl_enable") ? Params::getParam("um_amess_bl_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_amess_id_enable', Params::getParam("um_amess_id_enable") ? Params::getParam("um_amess_id_enable") : '0', 'umess', 'BOOLEAN');
        
        /* User Block */
        osc_set_preference('um_usr_blist_enable', Params::getParam("um_usr_blist_enable") ? Params::getParam("um_usr_blist_enable") : '0', 'umess', 'BOOLEAN'); 
        osc_set_preference('um_usr_antispam_enable', Params::getParam("um_usr_antispam_enable") ? Params::getParam("um_usr_antispam_enable") : '0', 'umess', 'BOOLEAN');
        osc_set_preference('um_usr_antispam_number', Params::getParam("um_usr_antispam_number"), 'umess', 'STRING');
        
        ob_get_clean();
        osc_add_flash_ok_message(__('Congratulations, the plugin is now configured', 'uMessages'), 'admin');
        osc_redirect_to(osc_route_admin_url('um-settings'));
    }
?>

<?php require_once 'top-menu.php'; ?>
<div class="dis-manage-wrapper">
    <form action="<?php echo osc_admin_base_url(true); ?>" method="post">
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="route" value="um-settings" />
        <input type="hidden" name="plugin_action" value="done" />
            
        <div class="cd-tabs">
            <nav>
                <ul class="cd-tabs-navigation">
                    <li>
                        <a data-content="general" class="selected" href="javascript:void(0);">
                            <i class="fas fa-sliders-h"></i> <?php _e('General', 'uMessages'); ?>
                        </a>
                    </li>
                    
                    <li>
                        <a data-content="attachments" href="javascript:void(0);">
                            <i class="fas fa-paperclip"></i> <?php _e('Attachments', 'uMessages'); ?>
                        </a>
                    </li>
                    
                    <li>
                        <a data-content="notifications" href="javascript:void(0);">
                            <i class="fas fa-bullhorn"></i> <?php _e('Notifications', 'uMessages'); ?>
                        </a>
                    </li>
                    
                    <li>
                        <a data-content="amessages" href="javascript:void(0);">
                            <i class="fas fa-retweet"></i> <?php _e('Auto-Messages', 'uMessages'); ?>
                        </a>
                    </li>
                    
                    <li>
                        <a data-content="ublock" href="javascript:void(0);">
                            <i class="fas fa-comment-slash"></i> <?php _e('User Block', 'uMessages'); ?>
                        </a>
                    </li>
                </ul>
            </nav>
            
            <ul class="cd-tabs-content">
                <li data-content="general" class="selected">
                    <table class="table table-no-border">
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Messages Enable', 'uMessages'); ?>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_messages_enable', 'umess') ? 'checked' : ''); ?> name="um_messages_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Replace Contact Seller Feature', 'uMessages'); ?> 
                                <i id="tips" data-tipso="<?php echo osc_esc_html(__('Contact seller form will be replaced with uMessages plugin', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_cs_replace_enable', 'umess') ? 'checked' : ''); ?> name="um_cs_replace_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Button on Item page', 'uMessages'); ?> 
                                <i id="tips" data-tipso="<?php echo osc_esc_html(__('Show button to send a message on the Item page', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_btn_enable', 'umess') ? 'checked' : ''); ?> name="um_btn_enable" value="1" /> 
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('User Menu Link', 'uMessages'); ?> 
                                <i id="tips" data-tipso="<?php echo osc_esc_html(__('Enable if you want to display uMessages link in User Menu', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_umenu_link_enable', 'umess') ? 'checked' : ''); ?> name="um_umenu_link_enable" value="1" /> 
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Mark as Unread Enable', 'uMessages'); ?> 
                                <i id="tips" data-tipso="<?php echo osc_esc_html(__('Users are able to mark a conversation as unread when somebody send them a message', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_ur_mark_enable', 'umess') ? 'checked' : ''); ?> name="um_ur_mark_enable" value="1" /> 
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Allow Users to Remove Messages', 'uMessages'); ?> 
                                <i id="tips" data-tipso="<?php echo osc_esc_html(__('Enable if you want to allow users to remove their messages in threads', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_rm_msg_enable', 'umess') ? 'checked' : ''); ?> name="um_rm_msg_enable" value="1" /> 
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Widget Enable', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Show widget to display a link to the inbox and the number of unread messages', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input id="widget-enable" type="checkbox" <?php echo (osc_get_preference('um_widget_enabled', 'umess') ? 'checked' : ''); ?> name="um_widget_enabled" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Widget Position', 'uMessages'); ?>
                            </td>
                            <td>
                                <select id="widget-position" name="um_widget_position" <?php echo (!osc_get_preference('um_widget_enabled', 'umess') ? 'disabled="disabled"' : ''); ?>>
                                    <option value="sidebar" <?php if(osc_get_preference('um_widget_position', 'umess') == 'sidebar') { echo 'selected';} ?>><?php _e('Sidebar', 'uMessages') ?></option>
    								<option value="header" <?php if(osc_get_preference('um_widget_position', 'umess') == 'header') { echo 'selected';} ?> ><?php _e('Header Menu', 'uMessages') ?></option>                             
                                </select>
                                
                                <span id="pos-help" class="help-block-form <?php if(osc_get_preference('um_widget_position', 'umess') != 'header'): ?>hide<?php endif; ?>"><?php _e('For the feature to work, go to the ', 'uMessages'); ?> <a href="<?php echo osc_route_admin_url('um-help'); ?>"><?php _e('Help', 'uMessages'); ?></a></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Widget Style', 'uMessages'); ?>
                            </td>
                            <td>
                                <select id="widget-style" name="um_widget_style" <?php echo (osc_get_preference('um_widget_position', 'umess') == 'sidebar' ? 'disabled="disabled"' : ''); ?>>
                                    <option value="text" <?php if(osc_get_preference('um_widget_style', 'umess') == 'text') { echo 'selected';} ?>><?php _e('Text', 'uMessages') ?></option>
    								<option value="ico" <?php if(osc_get_preference('um_widget_style', 'umess') == 'ico') { echo 'selected';} ?> ><?php _e('Icon', 'uMessages') ?></option>                             
                                </select>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Font Color', 'uMessages'); ?> 
                            </td>
                            <td>
                                <input type="color" name="um_fcolor" value="<?php echo osc_get_preference('um_fcolor', 'umess'); ?>" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Label Color', 'uMessages'); ?> 
                            </td>
                            <td>
                                <input type="color" name="um_lbcolor" value="<?php echo osc_get_preference('um_lbcolor', 'umess'); ?>" />
								<span id="pos-help-s" class="help-block-form <?php if(osc_get_preference('um_widget_position', 'umess') != 'header'): ?>hide<?php endif; ?> next"><?php _e('For the Header widget Label Color not work', 'uMessages'); ?></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Background Color', 'uMessages'); ?> 
                            </td>
                            <td>
                                <input type="color" name="um_bgcolor" value="<?php echo osc_get_preference('um_bgcolor', 'umess'); ?>" />
								<span id="pos-help-d" class="help-block-form <?php if(osc_get_preference('um_widget_position', 'umess') != 'header'): ?>hide<?php endif; ?> next"><?php _e('For the Header widget Background Color not work', 'uMessages'); ?></span>
                            </td>
                        </tr>
                    </table>
                </li>
                
                <li data-content="attachments">
                    <table class="table table-no-border">
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Attachments Enable', 'uMessages'); ?>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_attach_enable', 'umess') ? 'checked' : ''); ?> name="um_attach_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Allowed File Extensions', 'uMessages'); ?> 
                                <i id="info" data-tipso="<?php echo osc_esc_html(__('Comma-separated list of file extensions that are allowed, for example: \'.png,.gif,.jpg,.jpeg,.zip,.doc\'', 'uMessages')); ?>" class="fa fa-exclamation-circle fa-alert"></i>
                            </td>
                            <td>
                                <input type="text" name="um_fext" value="<?php echo osc_get_preference('um_fext', 'umess'); ?>" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Maximum File Size', 'uMessages'); ?> 
                                <i id="info" data-tipso="<?php echo osc_esc_html(__('Max file size allowed in Kb', 'uMessages')); ?>" class="fa fa-exclamation-circle fa-alert"></i>
                            </td>
                            <td>
                                <input type="text" name="um_fmaxsize" value="<?php echo osc_get_preference('um_fmaxsize', 'umess'); ?>" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Maximum Attachments', 'uMessages'); ?> 
                                <i id="info" data-tipso="<?php echo osc_esc_html(__('Maximum number of files that can be attached to a single message', 'uMessages')); ?>" class="fa fa-exclamation-circle fa-alert"></i>
                            </td>
                            <td>
                                <input type="text" name="um_max_attach" value="<?php echo osc_get_preference('um_max_attach', 'umess'); ?>" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Thumbnail width', 'uMessages'); ?> 
                                <i id="info" data-tipso="<?php echo osc_esc_html(__('For images only. Size must be specified as \'{width}\', eg. \'180\'', 'uMessages')); ?>" class="fa fa-exclamation-circle fa-alert"></i>
                            </td>
                            <td>
                                <input type="text" name="um_thumb_size" value="<?php echo osc_get_preference('um_thumb_size', 'umess'); ?>" />
                            </td>
                        </tr>
                        
                    </table>
                </li>
                
                <li data-content="notifications">
                    <table class="table table-no-border">
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Notifications Enable', 'uMessages'); ?>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_notifi_enable', 'umess') ? 'checked' : ''); ?> name="um_notifi_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('New Thread Notification', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Send a notification when a conversation is started', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_notifi_nt_enable', 'umess') ? 'checked' : ''); ?> name="um_notifi_nt_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('New Message Notification', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Send a notification when a new message sended', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_notifi_nm_enable', 'umess') ? 'checked' : ''); ?> name="um_notifi_nm_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Only Once Notification', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('User will be notified about new messages only if previous messages in same conversation has been read', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_nm_only_once_enable', 'umess') ? 'checked' : ''); ?> name="um_nm_only_once_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('New Petition Notification', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Admin will be notified about new Petition if the feature is enabled', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_na_notifi_enable', 'umess') ? 'checked' : ''); ?> name="um_na_notifi_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Anti-Spam Blocked Notification', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Admin will be notified when users block messages collective complaints on spam', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_aspam_notifi_enable', 'umess') ? 'checked' : ''); ?> name="um_aspam_notifi_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                    </table>
                </li>
                
                <li data-content="amessages">
                    <table class="table table-no-border">
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Auto Messages Enable', 'uMessages'); ?>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_amess_enable', 'umess') ? 'checked' : ''); ?> name="um_amess_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Anti-Spam Blocked', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Send an auto message when the user has blocked messages by collective complaints on spam', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_amess_as_enable', 'umess') ? 'checked' : ''); ?> name="um_amess_as_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Black List', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Send an auto message if the user has been added to the recipient\'s black list', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_amess_bl_enable', 'umess') ? 'checked' : ''); ?> name="um_amess_bl_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Item Deleted', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('Send an auto message if the Item has been deleted', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_amess_id_enable', 'umess') ? 'checked' : ''); ?> name="um_amess_id_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </li>
                
                <li data-content="ublock">
                    <table class="table table-no-border">
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Users Black List Enable', 'uMessages'); ?>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_usr_blist_enable', 'umess') ? 'checked' : ''); ?> name="um_usr_blist_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 350px;">
                                <?php _e('Anti-Spam Blocked Enable', 'uMessages'); ?> <i id="tips" data-tipso="<?php echo osc_esc_html(__('If \'X\' people complain about receiving spam from the sender, the user will be blocked from sending messages!', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <div data-toggle="switch">
                                    <input type="checkbox" <?php echo (osc_get_preference('um_usr_antispam_enable', 'umess') ? 'checked' : ''); ?> name="um_usr_antispam_enable" value="1" />
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <?php _e('Anti-Spam Complaints Number', 'uMessages'); ?> 
                                <i id="tips" data-tipso="<?php echo osc_esc_html(__('Number of complaints from users for blocking messages', 'uMessages')); ?>" class="fa fa-question-circle fa-help"></i>
                            </td>
                            <td>
                                <input type="text" name="um_usr_antispam_number" value="<?php echo osc_get_preference('um_usr_antispam_number', 'umess'); ?>" />
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>
        </div>
        
        <div class="form-actions">
            <input type="submit" id="save_changes" value="<?php echo osc_esc_html(__("Save changes", 'uMessages')); ?>" class="btn btn-submit">
        </div>
    </form>
</div>

<script type="text/javascript">
    $('#widget-enable').change(function(){
        var wPos = $('#widget-position').val();
        
        if($(this).is(':checked')) {
            $('#widget-position').removeAttr('disabled').css('opacity', 0);
            
            if(wPos == 'header') {
                $('#widget-style').removeAttr('disabled').css('opacity', 0);
            }
            else {
                $('#widget-style').attr('disabled', true).css('opacity', 0.7);
            }
        }
        else {
            $('#widget-position').attr('disabled', true).css('opacity', 0.7);
            $('#widget-style').attr('disabled', true).css('opacity', 0.7);
        }
    });
    
    $('#widget-position').change(function() {
        var pos = $(this).val();
        
        if(pos == 'header') {
            $('#pos-help').removeClass('hide');
			$('#pos-help-s').removeClass('hide');
			$('#pos-help-d').removeClass('hide');
            $('#widget-style').removeAttr('disabled').css('opacity', 0);
        }
        else {
            $('#pos-help').addClass('hide');
			$('#pos-help-s').addClass('hide');
			$('#pos-help-d').addClass('hide');
            $('#widget-style').attr('disabled', true).css('opacity', 0.7);
        }
    });
    
    $('i#tips').tipso({
        position: 'top',
        background: '#018be3',
		color: '#eee',
        width: '',
		maxWidth: 500,
        tooltipHover: true,
        animationIn: 'flipInX',
		animationOut: 'flipOutX'
    });
    
    $('i#info').tipso({
        position: 'top',
        background: '#d90909',
		color: '#eee',
        width: '',
		maxWidth: 500,
        tooltipHover: true,
        animationIn: 'flipInX',
		animationOut: 'flipOutX'
    });
</script>