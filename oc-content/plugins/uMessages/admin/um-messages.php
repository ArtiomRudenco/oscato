<?php defined('ABS_PATH') or die('Access denied'); 
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
    use Carbon\Carbon; 
    
    Params::getParam('upage') ? $page = Params::getParam('upage') : $page = 1;
    $b_actions = Params::getParam('um-action');
    $mid = Params::getParam('mid');
    $uid = Params::getParam('uid');
    
    $start = ($page - 1) * 20;
    
    $messages = UMModel::newInstance()->umAdminMessagesGet(true, $start);
    $total_messages = count(UMModel::newInstance()->umAdminMessagesGet(0));

    if($b_actions) {
        ob_get_clean();
        $result = UMModel::newInstance()->umAdminBlockedUserActions($b_actions, $mid, $uid);
        
        osc_redirect_to(osc_route_admin_url('um-messages'));
    }
?>

<?php require_once 'top-menu.php';?>
<div class="dis-manage-wrapper">
    <div style="margin:15px;">
        <?php if($messages): ?>
            <table class="table-striped">
                <tr>
                    <th style="width: 25px;"><?php _e('ID', 'uMessages'); ?></th>
                    <th><?php _e('Message', 'uMessages'); ?></th>
        			<th style="min-width: 250px;"><?php _e('Ads', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Sender', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Date', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Status', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Action', 'uMessages'); ?></th>  
                </tr>
                <?php foreach($messages as $i => $message): ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo osc_highlight($message['s_message'], 50); ?></td>
                    <td><?php echo $message['ads']; ?></td>
                    <td><?php echo $message['s_name']; ?></td>
                    <td><?php echo Carbon::createFromFormat('Y-m-d H:i:s', $message['d_message_date'])->format($dateFormat); ?></td>
                    <td><?php echo $message['status']; ?></td>
                    <td>
                        <a href="javascript: messageView(<?php echo $message['pk_i_message_id']; ?>);" title="<?php echo osc_esc_html(__('View Message', 'uMessages')); ?>"><i class="far fa-eye"></i></i></a>
                        <?php if($message['admin_blocked']): ?>
                            <a id="user-unblock" href="<?php echo osc_route_admin_url('um-messages', array('um-action' => 'user-unblock', 'mid' => $message['pk_i_message_id'], 'uid' => $message['fk_i_user_id'])); ?>" title="<?php echo osc_esc_html(__('Unblock User', 'uMessages')); ?>"><i class="fas fa-check"></i></a>
                        <?php else: ?>
                            <a id="user-block" href="<?php echo osc_route_admin_url('um-messages', array('um-action' => 'user-block', 'mid' => $message['pk_i_message_id'], 'uid' => $message['fk_i_user_id'])); ?>" title="<?php echo osc_esc_html(__('Block User', 'uMessages')); ?>"><i class="fas fa-ban"></i></a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
           </table>
       <?php else: ?>
            <div class="alert-block alert-warning"><?php  _e("You don't have any Messages", "uMessages") ?></div> 
       <?php endif; ?>
       
       <?php echo UMModel::newInstance()->umAdminPagination($page, $total_messages, 'um-messages'); ?>
   </div>
</div>

<script type="text/javascript">
    $('a#user-block').click(function() {
        if(confirm('<?php _e("Are you sure you want to Block the User?", "uMessages") ?>')) return true;
        
        return false;
    });
    
    $('a#user-unblock').click(function() {
        if(confirm('<?php _e("Are you sure you want to Unblock the User?", "uMessages") ?>')) return true;
        
        return false;
    });
    
    function messageView(message_id){
        var url = '/index.php?page=ajax&action=custom&ajaxfile=uMessages/ajax.php'; 
        
        $.ajax({
            url: url,
            type: 'POST',
            data: {"um-action" : "view-message", "mid" : message_id},
            error: function(){},
            success: function(data){
                var res = JSON.parse(data),
                    btns, focus;

                if(res.status == 0 || !res.status) {
                    btns = {'<?php _e('Block User', 'uMessages') ?>': 0};
                    focus = 1;
                }
                else {
                    btns = {'<?php _e('Unblock User', 'uMessages') ?>': 1};
                    focus = 0;
                }
                
                var temp = {
                		state0: {
                			title: '<?php _e('Message Details', 'uMessages') ?>',
                			html:'<p>' + res.message + '</p>',
                			buttons: btns,
                			focus: focus,
                			submit:function(e,v,m,f){ 
                                if(v == 0 || v == 1) return true;								
                                
                				return false; 
                			}
                		}
                	}
                	
            	$.prompt(temp, {
            		close: function(e,v,m,f){
            			if(v !== undefined){
        					$.ajax({
                                url: url,
                                type: 'POST',
                                data: {"um-action" : "user-change-status", "mid" : message_id, "status" : v},
                                error: function(){},
                                success: function(data){
                                    location.href = data;
                                }
                            }); 
            			}
            		},
            		classes: {
            			box: '',
        				fade: '',
        				prompt: '',
        				close: '',
        				title: 'lead',
        				message: 'pure-form',
        				buttons: '',
        				button: 'pure-button',
        				defaultButton: 'pure-button-primary'
            		}
            	});
            }
        });
    }
</script>