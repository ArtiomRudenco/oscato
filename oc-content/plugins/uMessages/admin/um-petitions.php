<?php defined('ABS_PATH') or die('Access denied'); 
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
    use Carbon\Carbon; 
 
    Params::getParam('upage') ? $page = Params::getParam('upage') : $page = 1;
    $p_actions = Params::getParam('um-action');
    $pid = Params::getParam('pid');
    
    $start = ($page - 1) * 20;
    
    $petitons = UMModel::newInstance()->umAdminPetitionsGet(true, $start);
    $totalPetition = count(UMModel::newInstance()->umAdminPetitionsGet(0));

    if($p_actions) {
        ob_get_clean();
        $result = UMModel::newInstance()->umAdminPetitionActions($p_actions, $pid);
        
        osc_redirect_to(osc_route_admin_url('um-petitions'));
    }
?>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.1.0/pure-min.css" />
<?php require_once 'top-menu.php'; ?>
<div class="dis-manage-wrapper">
    <div style="margin:15px;">
            <?php if($petitons): ?>
            <table class="table-striped">
                <tr>
                    <th style="width: 25px;"><?php _e('ID', 'uMessages'); ?></th>
                    <th><?php _e('Message', 'uMessages'); ?></th>
            		<th style="width: 150px;"><?php _e('User', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Date', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Status', 'uMessages'); ?></th>
                    <th style="width: 80px;"><?php _e('Action', 'uMessages'); ?></th>  
                </tr>
                <?php foreach($petitons as $i => $petiton): ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo osc_highlight($petiton['s_message'], 100); ?></td>
                    <td><?php echo $petiton['s_name']; ?></td>
                    <td><?php echo Carbon::createFromFormat('Y-m-d H:i:s', $petiton['d_petition_date'])->format($dateFormat); ?></td>
                    <td><?php echo $petiton['status']; ?></span></td>
                    <td>
                        <a id="alert-view" href="javascript: alertView(<?php echo $petiton['pk_i_petition_id']; ?>);" title="<?php echo osc_esc_html(__('View Details', 'uMessages')); ?>"><i class="far fa-eye"></i></i></a>
                        <?php if($petiton['i_petition_status'] == 2): ?>
                            <a id="alert-accept" href="<?php echo osc_route_admin_url('um-petitions', array('um-action' => 'petition-accepted', 'pid' => $petiton['pk_i_petition_id'])); ?>" title="<?php echo osc_esc_html(__('Accept &amp; Unblock', 'uMessages')); ?>"><i class="fas fa-check"></i></a>
                            <a id="alert-reject" href="<?php echo osc_route_admin_url('um-petitions', array('um-action' => 'petition-rejected', 'pid' => $petiton['pk_i_petition_id'])); ?>" title="<?php echo osc_esc_html(__('Reject', 'uMessages')); ?>"><i class="fas fa-times"></i></i></a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        <?php else: ?>
            <div class="alert-block alert-warning"><?php  _e("You don't have any Petitions", "uMessages") ?></div>    
        <?php endif; ?>
        
        <?php echo UMModel::newInstance()->umAdminPagination($page, $totalPetition, 'um-petitions'); ?>
    </div>
</div>

<script type="text/javascript">
    $('a#alert-accept').click(function() {
        if(confirm('<?php _e("Are you sure you want to Accept the Petition and unblocked?", "uMessages") ?>')) return true;
        
        return false;
    });

    $('a#alert-reject').click(function() {
        if(confirm('<?php _e('Are you sure you want to Reject the Petition?', 'uMessages') ?>')) return true;
        
        return false;
    });

    function alertView(petition_id){
        
        var url = '/index.php?page=ajax&action=custom&ajaxfile=uMessages/ajax.php'; 
        
        $.ajax({
            url: url,
            type: 'POST',
            data: {"um-action" : "view-petition", "pid" : petition_id},
            error: function(){},
            success: function(data){
                var res = JSON.parse(data),
                    btns;

                if(res.status == 2) btns = {'<?php _e('Reject', 'uMessages') ?>': 2, '<?php _e('Accept', 'uMessages') ?>': 1};
                    else btns = {};
                
                var temp = {
                		state0: {
                			title: '<?php _e('Peition Details', 'uMessages') ?>',
                			html:'<p>' + res.message + '</p>',
                			buttons: btns,
                			focus: 2,
                			submit:function(e,v,m,f){ 
                				if(v==0) 
                					$.prompt.close()
                				else if(v == 1 || v == 2)								
                					return true;
                				return false; 
                			}
                		}
                	}
                	
            	$.prompt(temp, {
            		close: function(e,v,m,f){
            			if(v !== undefined){
        					$.ajax({
                                url: url,
                                type: 'POST',
                                data: {"um-action" : "petition-change-status", "pid" : petition_id, "status" : v},
                                error: function(){},
                                success: function(data){
                                    location.href = data;
                                }
                            }); 
            			}
            		},
            		classes: {
            			box: '',
        				fade: '',
        				prompt: '',
        				close: '',
        				title: 'lead',
        				message: 'pure-form',
        				buttons: '',
        				button: 'pure-button',
        				defaultButton: 'pure-button-primary'
            		}
            	});
            }
        }); 
    }
</script>