<?php defined('ABS_PATH') or die('Access denied'); 
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
?>

<?php require_once 'top-menu.php'; ?>
<div class="dis-manage-wrapper">
    <div class="dis-stats-block">
        <div class="dis-stat dis-block-green">
            <h2><?php _e('New Messages', 'uMessages') ?></h2>
            
            <p class="dis-stat-row"><span class="dis-stat-count"><?php echo number_format(UMModel::newInstance()->umAdminStatisticsGet('messages', true), 0, '', '\'') ?></span> <i class="fas fa-comments fa-2x"></i></p>
        </div>
        
        <div class="dis-stat dis-block-blue">
            <h2><?php _e('Total Messages', 'uMessages') ?></h2>
            
            <p class="dis-stat-row"><span class="dis-stat-count"><?php echo number_format(UMModel::newInstance()->umAdminStatisticsGet('messages'), 0, '', '\'') ?></span> <i class="fa fa-comment-dots fa-2x"></i></p>
        </div>
        
        <div class="dis-stat dis-block-orange">
            <h2><?php _e('New Petitions', 'uMessages') ?></h2>
            
            <p class="dis-stat-row"><span class="dis-stat-count"><?php echo number_format(UMModel::newInstance()->umAdminStatisticsGet('petitions', true), 0, '', '\'') ?></span> <i class="fas fa-bell fa-2x"></i></p>
        </div>
        
        <div class="dis-stat">
            <h2><?php _e('Total Blocked', 'uMessages') ?></h2>
            
            <p class="dis-stat-row"><span class="dis-stat-count"><?php echo number_format(UMModel::newInstance()->umAdminStatisticsGet('user-blocks'), 0, '', '\'') ?></span> <i class="fa fa-comment-slash fa-2x"></i></p>
        </div>
    </div>       
    <div class="clear"></div>
    
    <form action="<?php echo osc_admin_base_url(true); ?>" method="post">   
        <table class="table table-no-border" style="margin: 10px 50px; background-color: #018be3; width: 550px; padding: 5px 25px;">
            <tr>
                <td style="font-weight: bold; font-size: 18px; color: #fff;">
                    <?php _e('Period: ', 'uMessages'); ?>
                </td>
                <td style="text-align: center;">
                    <select id="dashboard-filter">
                        <option value="7" selected><?php _e('7 Days', 'uMessages') ?></option>
						<option value="15"><?php _e('15 Days', 'uMessages') ?></option>
						<option value="30"><?php _e('1 Month', 'uMessages') ?></option>   
                        <option value="60"><?php _e('2 Months', 'uMessages') ?></option> 
                        <option value="90"><?php _e('3 Months', 'uMessages') ?></option>                          
                    </select>
                </td>
            </tr>
        </table>
    </form>
    
    <div id="main" style="height:400px;"></div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var url = '/index.php?page=ajax&action=custom&ajaxfile=uMessages/ajax.php',
            myChart = echarts.init(document.getElementById('main'));
            
        $.ajax({
            url: url,
            type: 'POST',
            data: {"um-action" : "charts-data", "period" : 7},
            error: function(){},
            success: function(data){
                var res = JSON.parse(data);
                
                myChart.setOption({
                    title : {
                        text: '<?php _e('Statistics Sent and Blocked Messages', 'uMessages') ?>',
                        subtext: '<?php _e('in the last ', 'uMessages') . _e('7 Days', 'uMessages'); ?>',
                        x: 50
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['<?php _e('New Messages', 'uMessages') ?>', '<?php _e('Blocked Messages', 'uMessages') ?>'],
                        y: 'bottom'
                    },
                    
                    grid: {
                        x: 60,
                        x2: 50    
                    },
                    
                    color : ['#4caf50', '#f44336'],
                    
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                            restore : {show: true},
                            saveAsImage : {show: false}
                        }
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : res.period
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                                {
                                    name: '<?php _e('New Messages', 'uMessages') ?>',
                                    type:'line',
                                    smooth:true,
                                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                                    data: res.sent
                                },
                                {
                                    name:'<?php _e('Blocked Messages', 'uMessages') ?>',
                                    type:'line',
                                    smooth:true,
                                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                                    data: res.blocked
                                }
                            ]
                });
            }
        });
        
        $('#dashboard-filter').change(function() {
            myChart.showLoading();
            
            var period = $(this).val(),
                text = $('option:selected', this).text(); 
                
            $.ajax({
                url: url,
                type: 'POST',
                data: {"um-action" : "charts-data", "period" : period},
                error: function(){},
                success: function(data){
                    var res = JSON.parse(data);
    
                    setTimeout(function() {
                        myChart.dispose();
                        myChart = echarts.init(document.getElementById('main'));  
            
                        myChart.setOption({
                            title : {
                                text: '<?php _e('Statistics Sent and Blocked Messages', 'uMessages') ?>',
                                subtext: '<?php _e('in the last ', 'uMessages'); ?>' + text + '',
                                x: 50
                            },
                            tooltip : {
                                trigger: 'axis'
                            },
                            legend: {
                                data:['<?php _e('New Messages', 'uMessages') ?>', '<?php _e('Blocked Messages', 'uMessages') ?>'],
                                y: 'bottom'
                            },
                            
                            grid: {
                                x: 60,
                                x2: 50    
                            },
                            
                            color : ['#4caf50', '#f44336'],
                            
                            toolbox: {
                                show : true,
                                feature : {
                                    mark : {show: true},
                                    dataView : {show: true, readOnly: false},
                                    magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                                    restore : {show: true},
                                    saveAsImage : {show: false}
                                }
                            },
                            calculable : true,
                            xAxis : [
                                {
                                    type : 'category',
                                    boundaryGap : false,
                                    data : res.period
                                }
                            ],
                            yAxis : [
                                {
                                    type : 'value'
                                }
                            ],
                            series : [
                                        {
                                            name: '<?php _e('New Messages', 'uMessages') ?>',
                                            type:'line',
                                            smooth:true,
                                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                                            data: res.sent
                                        },
                                        {
                                            name:'<?php _e('Blocked Messages', 'uMessages') ?>',
                                            type:'line',
                                            smooth:true,
                                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                                            data: res.blocked
                                        }
                                    ]
                        });
                    }, 1000);
                }
            });       
        });
    });
</script>