<?php defined('ABS_PATH') or die('Access denied'); 
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
 
    Params::getParam('upage') ? $page = Params::getParam('upage') : $page = 1;
    $b_actions = Params::getParam('um-action');
    $mid = Params::getParam('mid');
    $uid = Params::getParam('uid');
    
    $start = ($page - 1) * 20;
    
    $blocked_users = UMModel::newInstance()->umAdminBlockedUsersGet(true, $start);
    $total_blocked_users = count(UMModel::newInstance()->umAdminBlockedUsersGet(0));

    if($b_actions) {
        ob_get_clean();
        $result = UMModel::newInstance()->umAdminBlockedUserActions($b_actions, $mid, $uid);
        
        osc_redirect_to(osc_route_admin_url('um-blocked'));
    }
?>

<?php require_once 'top-menu.php'; ?>
<div class="dis-manage-wrapper"> 
    <div style="margin:15px;">
        <?php if($blocked_users): ?>
            <table class="table-striped">
                <tr>
                    <th style="width: 25px;"><?php _e('ID', 'uMessages'); ?></th>
                    <th style="min-width: 400px;"><?php _e('Message', 'uMessages'); ?></th>
            		<th style="width: 80px;"><?php _e('User', 'uMessages'); ?></th>
                    <th style="width: 110px;"><?php _e('Status', 'uMessages'); ?></th>
                    <th style="width: 50px;"><?php _e('Action', 'uMessages'); ?></th>  
                </tr>
                <?php foreach($blocked_users as $i => $user): ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php if($user['s_message']): ?><?php echo osc_highlight($user['s_message'], 200); ?><?php else: ?>-<?php endif; ?></td>
                    <td><?php echo $user['s_name']; ?></td>
                    <td><?php echo $user['status']; ?></td>
                    <td>
                        <a id="user-unblock" href="<?php echo osc_route_admin_url('um-blocked', array('um-action' => 'user-unblock', 'mid' => $user['i_message_id'], 'uid' => $user['fk_i_user_id'])); ?>" title="<?php echo osc_esc_html(__('Unblock User', 'uMessages')); ?>"><i class="fas fa-check"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        <?php else: ?>
            <div class="alert-block alert-warning"><?php  _e("You don't have any Blocked Users", "uMessages") ?></div> 
        <?php endif; ?>
        
        <?php echo UMModel::newInstance()->umAdminPagination($page, $total_blocked_users, 'um-blocked'); ?>
    </div>
</div>

<script type="text/javascript">
    $('a#user-unblock').click(function() {
        if(confirm('<?php _e("Are you sure you want to Unblock the User?", "uMessages") ?>')) return true;
        
        return false;
    });
</script>