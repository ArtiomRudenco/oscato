<?php defined('ABS_PATH') or die('Access denied'); ?>
<?php require_once 'top-menu.php'; ?>
<div class="ua-manage-wrapper">
    <div style="margin: 15px 15px 0 15px;">
        <div class="accordion">
            <section class="accordion_item">
                <h3 class="title_block"><?php _e('How to embed a uMessages Widget in the Site Header', 'uMessages'); ?></h3>
                
                <div class="info">
                    <p class="info_item">
                        1. <?php _e('Set \'Widget Position\' to \'Header Menu\' in the plugin settings and open these file in your template:', 'uMessages') ?> <strong>header.php</strong>
                    </p>
                    
                    <p class="info_item">
                        2. <?php _e('And add the following line of code to the place where you want to display the widget:', 'uMessages') ?> 
                        
                        <span class="code-block"> 
                            <span class="php-tag">&lt;?php</span>
                                <span class="php-function"> osc_run_hook(<span class="php-function-data">'um_btn'</span>); </span>
                            <span class="php-tag">?&gt;</span>
                        </span>
                    </p>
                </div>
            </section>
        </div>
    </div>
		<div style="margin: 0 15px 0 15px;">
        <div class="accordion">
            <section class="accordion_item">
                <h3 class="title_block"><?php _e('Antispam - User block', 'uMessages'); ?></h3> 
                <div class="info">
                   <p class="info_item">
                     <?php _e('You can configure the blocking of spam messages in Settings - User Block.', 'uMessages') ?>
                    </p>
                    <p class="info_item">
                     <?php _e('If users mark messages as spam - plugin can block sender', 'uMessages') ?>
                    </p>
                    <p class="info_item">
                     <?php _e('The blocked user will no longer be able to write new messages to other users', 'uMessages') ?>
                    </p>    					
                </div>
            </section>
        </div>
    </div>
	<div style="margin: 0 15px 0 15px;">
        <div class="accordion">
            <section class="accordion_item">
                <h3 class="title_block"><?php _e('Petitions', 'uMessages'); ?></h3>
                
                <div class="info">
                    <p class="info_item">
                     <?php _e('Petitions what is it?', 'uMessages') ?>
                    </p>
                   <p class="info_item">
                     <?php _e('A user who has been blocked by antispam can send request for the unblock once. Administrator can unblock this user.', 'uMessages') ?>
                    </p> 					
                </div>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript">
! function(i) {
  var o, n;
  i(".title_block").on("click", function() {
    o = i(this).parents(".accordion_item"), n = o.find(".info"),
      o.hasClass("active_block") ? (o.removeClass("active_block"),
        n.slideUp()) : (o.addClass("active_block"), n.stop(!0, !0).slideDown(),
        o.siblings(".active_block").removeClass("active_block").children(
          ".info").stop(!0, !0).slideUp())
  })
}(jQuery);
</script>