$(document).ready(function(){ 
    $('[data-toggle="switch"]').hurkanSwitch({
        'onTitle' : 'On',
        'offTitle' : 'Off',
        'animate' : true,
        'offColor' : 'danger',
        'onColor' : 'success',
        'width': 40
    });
    
    $('i#tips').tipso({
        position: 'top',
        background: '#018be3',
		color: '#eee',
        width: '',
		maxWidth: 500,
        tooltipHover: true,
        animationIn: 'flipInX',
		animationOut: 'flipOutX'
    });
    
    $('i#info').tipso({
        position: 'top',
        background: '#d90909',
		color: '#eee',
        width: '',
		maxWidth: 500,
        tooltipHover: true,
        animationIn: 'flipInX',
		animationOut: 'flipOutX'
    });
});