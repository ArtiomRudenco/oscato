<?php defined('ABS_PATH') or die('Access denied');

class uMUser {
    
    private static $instance;
    public $fColor, $bgColor, $lbColor;    
    
    function __construct() {
        $this->fColor = osc_get_preference('um_fcolor', 'umess');
        $this->bgColor = osc_get_preference('um_bgcolor', 'umess');
        $this->lbColor = osc_get_preference('um_lbcolor', 'umess');
        
        /**
         * Init hooks
         */
        osc_add_hook('header', array(&$this, 'umHeader'));
        osc_add_hook('footer', array(&$this, 'umSidebar'));
        osc_add_hook('user_menu', array(&$this, 'umMenu'));
        osc_add_hook('um_btn', array(&$this, 'umBtn'));
        osc_add_hook('item_detail', array(&$this, 'umItemBtn'));
        osc_add_hook('delete_user', array(&$this, 'umUserDeleted'));
        
        if(osc_get_preference('um_cs_replace_enable','umess')) {
            osc_add_hook('hook_email_item_inquiry', array(&$this, 'umContactSeller'));
        }
        
        
        /**
         * Routes
         */ 
        osc_add_route('umessages', 'umessages/inbox', 'umessages/inbox', UMS_USR_FOLDER . 'messages-inbox.php');
        osc_add_route('umessages-view', 'umessages/view/([0-9]+)', 'umessages/view/{cid}', UMS_USR_FOLDER . 'messages-view.php');
        osc_add_route('umessages-archive', 'umessages/archive', 'umessages/archive', UMS_USR_FOLDER . 'messages-archive.php');
        osc_add_route('umessages-trash', 'umessages/trash', 'umessages/trash', UMS_USR_FOLDER . 'messages-trash.php');
        osc_add_route('umessages-blocked', 'umessages/blocked', 'umessages/blocked', UMS_USR_FOLDER . 'messages-blocked.php');
        osc_add_route('umessages-send', 'umessages/send/([0-9]+)', 'umessages/send/{itemId}', UMS_USR_FOLDER . 'messages-send.php');
        osc_add_route('umessages-petition', 'umessages/petition', 'umessages/petition', UMS_USR_FOLDER . 'petition-send.php');
    }
    
    public static function newInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    public function umHeader() {
        $_r = Params::getParam('route');
        
        switch ($_r) {
            case 'umessages':
            case 'umessages-archive':
            case 'umessages-trash':
            case 'umessages-blocked':
                //osc_enqueue_style('bootstrap-css', UMS_USR_STYLE . 'bootstrap.min.css');
				osc_enqueue_style('bootstrap-css', UMS_USR_STYLE . 'bootstrap-grid.css');
                osc_enqueue_style('data-tables-css', UMS_USR_STYLE . 'dataTables.bootstrap4.min.css');
                
                //osc_register_script('jquery-js', UMS_USR_JS . 'jquery-1.12.4.min.js');
                //osc_enqueue_script('jquery-js');
                osc_register_script('data-tables-js', UMS_USR_JS . 'jquery.dataTables.min.js', 'jquery');
                osc_enqueue_script('data-tables-js');
                osc_register_script('bootstrap-js', UMS_USR_JS . 'dataTables.bootstrap4.min.js', 'jquery');
                osc_enqueue_script('bootstrap-js');
            
            case 'umessages-view':
            case 'umessages-send':
            case 'umessages-petition':
                osc_enqueue_style('messages-css', UMS_USR_STYLE . 'messages.css');      
                osc_enqueue_style('message-view-css', UMS_USR_STYLE . 'message-view.css');
                osc_enqueue_style('dropzone-css', UMS_USR_STYLE . 'drop-zone.css');
                osc_register_script('dropzone-js', UMS_USR_JS . 'dropzone.js?v=1.0.1');
                osc_enqueue_script('dropzone-js');
                osc_register_script('zoomooz-js', UMS_USR_JS . 'jquery.zoomooz.min.js', 'jquery');
                osc_enqueue_script('zoomooz-js');
                
            default:
                //osc_enqueue_style('fawesome-svg-css', UMS_USR_STYLE . 'svg-with-js.min.css');
                //osc_enqueue_style('fawesome-solid-css', UMS_USR_STYLE . 'solid.min.css');
                //osc_enqueue_style('fawesome-css', UMS_USR_STYLE . 'fontawesome.min.css');
				osc_enqueue_style('materialicons-css', UMS_USR_STYLE . 'materialdesignicons.min.css');
                osc_enqueue_style('widgets-css', UMS_USR_STYLE . 'widgets.css');
                osc_register_script('widgets-js', UMS_USR_JS . 'widgets.js', 'jquery');
                osc_enqueue_script('widgets-js');
            break;
        }
    }
    
    public function umContactSeller($aItem) {
        
        if(osc_logged_user_id()) {
            $itemId = $aItem['id'];
            
            $item = Item::newInstance()->findByPrimaryKey($itemId);
            
            $title = __('Conversation on the ad: ', 'uMessages') . $item['s_title'];
            $message = nl2br(htmlspecialchars($aItem['message']));
            
            $conId = UMModel::newInstance()->umConversationStart($itemId, $title, $message);
            
            osc_add_flash_ok_message(__('Congratulations! Message sent successfully', 'uMessages'));
            osc_redirect_to(osc_route_url('umessages-view', array('cid' => $conId)));
        }
    }
    
    public function umUserDeleted($userId) {
        UMModel::newInstance()->umUserDataClear($userId);
    }
    
    public function umBtn() {
        if(osc_get_preference('um_messages_enable', 'umess') && osc_get_preference('um_widget_enabled', 'umess') && osc_get_preference('um_widget_position', 'umess') == 'header') {
            osc_get_preference('um_widget_style', 'umess') == 'text' ? $style = '<a style="color: ' . $this->fColor . ';" href="' . osc_route_url('umessages') .'">' . __('Messages', 'uMessages') . ' (' . UMModel::newInstance()->umUserConversationsUnreadCount(osc_logged_user_id()) . ')' . '</a>' : $style = '<a class="um-btn-ico" href="' . osc_route_url('umessages') .'"><div class="um-2x"><span style="color: ' . $this->fColor . ';" class="um-layers"><i class="mdi mdi-email mdi-36px"></i><span class="um-circle" style="background: ' . $this->lbColor . '; color: ' . $this->fColor . ';">' . UMModel::newInstance()->umUserConversationsUnreadCount(osc_logged_user_id()) . '</span></span></div></a>';
            
            echo $style;
        }
    }
    
    public function umSidebar() {
        if(osc_get_preference('um_messages_enable', 'umess') && osc_get_preference('um_widget_enabled', 'umess') && osc_get_preference('um_widget_position', 'umess') == 'sidebar' && osc_logged_user_id()) {
            $style = '<div class="um-sidebar hide" style="background: ' . $this->bgColor . ';"><a style="color: ' . $this->fColor . '!important;" class="um-sidebar-btn" href="' . osc_route_url('umessages') .'"><div class="um-2x"><span class="um-layers"><i class="mdi mdi-email mdi-36px"></i><span class="um-circle" style="background: ' . $this->lbColor . '; color: ' . $this->fColor . ';">' . UMModel::newInstance()->umUserConversationsUnreadCount(osc_logged_user_id()) . '</span></span></div></a></div>';
            
            echo $style;
        }
    }
    
    public function umMenu() {      
        if(osc_get_preference('um_messages_enable', 'umess') && osc_get_preference('um_umenu_link_enable', 'umess')) {
             echo '<li class="opt_umessages" ><a href="' . osc_route_url('umessages') . '" >' . __("Messages", "uMessages") . '</a></li>' ;
        }
    }
    
    public function umItemBtn($item = NULL) {
        if(osc_get_preference('um_btn_enable', 'umess')) {
            $item ? $itemId = $item['pk_i_id'] : $itemId = osc_item_id();
            
            if($itemId && osc_logged_user_id() != $item['fk_i_user_id'] && osc_item_user_id() != null) {
                osc_logged_user_id() ? $url = osc_route_url('umessages-send', array('itemId' => $itemId)) : $url = osc_user_login_url();
                
                $itemBtn = '<a class="um-item-btn" href="' . $url . '"><i class="mdi mdi-email mdi-24px"></i><span class="um-but-item">' . __("Send Message", "uMessages") . '</span></a>';
                
                echo $itemBtn;
            }
        }
    }
}