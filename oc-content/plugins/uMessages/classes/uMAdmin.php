<?php defined('ABS_PATH') or die('Access denied');

class uMAdmin {
    
    function __construct() {
        /**
         * Init hooks
         */
        osc_add_hook('admin_menu_init', array(&$this, 'um_admin_menu'));
        osc_add_hook('admin_header', array(&$this, 'um_init_admin_page_header'));

        /**
         * Routes
         */   
        osc_add_route('um-dashboard', 'um-dashboard', 'um-dashboard', UMS_ADM_FOLDER . 'um-dashboard.php');
        osc_add_route('um-petitions', 'um-petitions', 'um-petitions', UMS_ADM_FOLDER . 'um-petitions.php'); 
        osc_add_route('um-messages', 'um-messages', 'um-messages', UMS_ADM_FOLDER . 'um-messages.php');
        osc_add_route('um-blocked', 'um-blocked', 'um-blocked', UMS_ADM_FOLDER . 'um-blocked.php'); 
        osc_add_route('um-settings', 'um-settings', 'um-settings', UMS_ADM_FOLDER . 'um-settings.php');
        osc_add_route('um-help', 'um-help', 'um-help', UMS_ADM_FOLDER . 'help.php');
    }
    
    function um_admin_menu() {
        osc_admin_menu_plugins('uMessages', osc_route_admin_url('um-dashboard'), 'um-dashboard-menu');
    }
        
    function um_init_admin_page_header() {
        $_r = Params::getParam('route');
        
        switch ($_r) {
            case 'um-dashboard':
                osc_register_script('charts-js', UMS_ADM_JS . 'echarts.js');
                osc_enqueue_script('charts-js');
            case 'um-petitions':
            case 'um-messages':
            case 'um-blocked':
                osc_enqueue_style('pure-css', UMS_ADM_STYLE . 'pure-min.css');
                osc_enqueue_style('prompt-style', UMS_ADM_STYLE . 'impromptu.css');
                osc_register_script('prompt-js', UMS_ADM_JS . 'impromptu.js', 'jquery');
                osc_enqueue_script('prompt-js');
            case 'um-settings':
            case 'um-help':
                osc_register_script('font-awesome-4.7', 'https://use.fontawesome.com/af830f475b.js');
                osc_enqueue_script('font-awesome-4.7');
                osc_enqueue_style('font-awesome-all', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css');
                osc_enqueue_style('font-awesome-shim', 'https://use.fontawesome.com/releases/v5.1.0/css/v4-shims.css');
                
                osc_register_script('tabs-js', UMS_ADM_JS . 'tabs.js', 'jquery');
                osc_enqueue_script('tabs-js');
                osc_enqueue_style('tabs-style', UMS_ADM_STYLE . 'tabs.css');
                osc_enqueue_style('accordion', UMS_ADM_STYLE . 'accordion.css');
                osc_register_script('switch-js', UMS_ADM_JS . 'hurkanSwitch.js', 'jquery');
                osc_enqueue_script('switch-js');
                osc_enqueue_style('switch-style', UMS_ADM_STYLE . 'hurkanSwitch.css');
                osc_enqueue_style('tipso-css', UMS_ADM_STYLE . 'tipso.css');
                osc_enqueue_style('animate-css', UMS_ADM_STYLE . 'animate.css');
                osc_register_script('tipso-js', UMS_ADM_JS . 'tipso.js', 'jquery');
                osc_enqueue_script('tipso-js');
                
                osc_enqueue_style('um-css', UMS_ADM_STYLE . 'style.css?v=1.0');
                osc_register_script('um-admin-js', UMS_ADM_JS . 'script.js', 'jquery');
                osc_enqueue_script('um-admin-js');
                
                osc_remove_hook('admin_page_header', 'customPageHeader');
                osc_add_hook('admin_page_header', array(&$this, 'um_admin_page_header'));
                break;
            default:
                break;
        }
    }
    
    function um_admin_page_header() {
        echo '<h1>uMessages</h1>';
    }
}