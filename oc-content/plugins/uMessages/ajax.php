<?php
    $action = Params::getParam('um-action');

    switch($action) {
        case 'view-petition':
            $id = Params::getParam('pid');
            
            $result = UMModel::newInstance()->umAdminPetitionGet($id);
            
            $arrJSON = array('message' => $result['s_message'], 'status' => $result['i_petition_status']);
            
            echo json_encode($arrJSON);
        break;
        
        case 'petition-change-status':
            $admin = new uMAdmin();
            
            $id = Params::getParam('pid');
            $status = Params::getParam('status');
            
            if($status == 1) $strStatus = 'petition-accepted';
                elseif($status == 2) $strStatus = 'petition-rejected';
            
            UMModel::newInstance()->umAdminPetitionActions($strStatus, $id);
            
            echo osc_route_admin_url('um-petitions');
        break;
        
        case 'view-message':
            $id = Params::getParam('mid');
            
            $getMessage = UMModel::newInstance()->umMessageGet($id);
            $getBlocks = UMModel::newInstance()->umAntiSpamGet($getMessage['fk_i_user_id'], false);
            
            $arrJSON = array('message' => $getMessage['s_message'], 'status' => $getBlocks['i_admin_blocked']);
            
            echo json_encode($arrJSON);
        break;
        
        case 'user-change-status':
            $admin = new uMAdmin();
            
            $id = Params::getParam('mid');
            $status = Params::getParam('status');
            
            $getMessage = UMModel::newInstance()->umMessageGet($id);
            
            if(!$status) $strStatus = 'user-block';
                else $strStatus = 'user-unblock';
            
            UMModel::newInstance()->umAdminBlockedUserActions($strStatus, $id, $getMessage['fk_i_user_id']);
            
            echo osc_route_admin_url('um-messages');
        break;
        
        case 'charts-data':
            $period = Params::getParam('period');
            
            $arrJSON = array('period' => UMModel::newInstance()->umAdminChartsTimePeriods($period, false), 'sent' => UMModel::newInstance()->umAdminChartsData('sent', $period), 'blocked' => UMModel::newInstance()->umAdminChartsData('blocked', $period));
            
            echo json_encode($arrJSON);
        break;
    }
?>