<?php
    $action = Params::getParam('msg-type');

    switch($action) {
        case 'conversation':
            $itemId = Params::getParam('item_id');
            $thread = Params::getParam('thread');
            $message = Params::getParam('message');
            $files = Params::getFiles('attached');
            
            $result = UMModel::newInstance()->umConversationStart($itemId, $thread, $message, $files);
            
            echo osc_route_url('umessages-view', array('cid' => $result));
        break;
        
        case 'message':
            $cid = Params::getParam('cid');
            $message = Params::getParam('message');
            $files = Params::getFiles('attached');
            
            UMModel::newInstance()->umMessageReply($cid, $message, osc_logged_user_id(), $files);
        break;
    }
?>