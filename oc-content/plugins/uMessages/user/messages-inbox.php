  <div class="col-left">  
    <div class="left-menu">
                                <div class="profile-demo">
                                    <img src="<?php echo osc_current_web_theme_url('img/profile.jpg'); ?>" alt="img">
                                    <strong class="profile-demo__title"><?php _e('User account manager', 'eva'); ?></strong>
                                    <a href="<?php echo osc_user_profile_url(); ?>"><strong><?php echo osc_logged_user_name(); ?></strong></a>
                                </div>
                                <ul>
    <?php echo osc_private_user_menu(get_user_menu()); ?>
                                </ul>
                            </div>
                         </div>
  <div class="messages-wrapper">
    <?php defined('ABS_PATH') or die('Access denied');
    /*
     * Copyright 2017 osclass-pro.com
     *
     * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
     * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
     */ 
        use Carbon\Carbon;
        
        if ( !osc_logged_user_id() ) {  
            return false;
        }
     
        $topics = UMModel::newInstance()->umTopicsGet(osc_logged_user_id(), 'inbox');
        
        $c_actions = Params::getParam('um-action');
        $aid = Params::getParam('aid');
        
        if($c_actions) {
            ob_get_clean();
            $result = UMModel::newInstance()->umUserConversationActions(osc_logged_user_id(), $c_actions, $aid);
            
            osc_redirect_to(osc_route_url('umessages'));
        }

        require_once 'top-menu.php';
    ?>

    <?php if($topics): ?>
        <table id="msg-inbox" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php _e('Subject', 'uMessages'); ?></th>
                    <th><?php _e('Ads', 'uMessages'); ?></th>
                    <th><?php _e('User', 'uMessages'); ?></th>
                    <th><?php _e('Date', 'uMessages'); ?></th>
                    <th><?php _e('Actions', 'uMessages'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($topics as $i => $topic): ?>
                <tr <?php if(!$topic['i_read']): ?>class="um-unreaded-msg"<?php endif; ?>>
                    <td><?php if(!$topic['i_read']): ?><i class="mdi mdi-bell mdi-18px"></i><?php endif; ?> <?php echo $i + 1; ?></td>
                    <td><a href="<?php echo osc_route_url('umessages-view', array('cid' => $topic['pk_i_topic_id'])); ?>"><?php echo $topic['s_title'] ?></a></td>
                    <td><a target="_blank" href="<?php echo osc_item_url_ns($topic['fk_i_item_id'], osc_locale_code()); ?>"><?php echo osc_highlight($topic['s_item_name'], 50); ?></a></td>
                    <td><?php echo $topic['s_name']; ?></td>
                    <td style="width: 200px;"><?php echo Carbon::createFromFormat('Y-m-d H:i:s', $topic['d_date'])->format($dateFormat); ?></td>
                    <td class="um-table-actions">
                        <?php if($topic['i_read'] == 1 && osc_get_preference('um_ur_mark_enable', 'umess')): ?>
                        <a href="<?php echo osc_route_url('umessages', array('um-action' => 'munread','aid' => $topic['pk_i_topic_id'])); ?>" title="<?php echo osc_esc_html(__('Mark as Unread', 'uMessages')); ?>"><i class="mdi mdi-eye-off mdi-24px"></i></a>
                        <?php endif; ?>
                        
                        <?php if(osc_get_preference('um_usr_antispam_enable', 'umess') && $topic['fk_i_user_id'] == osc_logged_user_id() && !$topic['i_spammed']): ?>
                        <a id="action-spam" href="<?php echo osc_route_url('umessages', array('um-action' => 'mspam','aid' => $topic['pk_i_topic_id'])); ?>" title="<?php echo osc_esc_html(__('Mark as Spam', 'uMessages')); ?>"><i class="mdi mdi-alert mdi-24px"></i></a>
                        <?php endif; ?>
                        
                        <?php if(osc_get_preference('um_usr_blist_enable', 'umess') && !UMModel::newInstance()->umBlockedUserCheck(UMModel::newInstance()->umUserConversationInterlocutor($topic['pk_i_topic_id'], osc_logged_user_id()), osc_logged_user_id())): ?>
                        <a id="action-blist" href="<?php echo osc_route_url('umessages', array('um-action' => 'ublock','aid' => $topic['pk_i_topic_id'])); ?>" title="<?php echo osc_esc_html(__('Block User', 'uMessages')); ?>"><i class="mdi mdi-account-off mdi-24px"></i></a>
                        <?php endif; ?>
                        
                        <a id="action-archive" href="<?php echo osc_route_url('umessages', array('um-action' => 'marchive','aid' => $topic['pk_i_topic_id'])); ?>" title="<?php echo osc_esc_html(__('Move to Archive', 'uMessages')) ?>"><i class="mdi mdi-inbox-arrow-down mdi-24px"></i></a>
                        <a id="action-trash" href="<?php echo osc_route_url('umessages', array('um-action' => 'mtrash','aid' => $topic['pk_i_topic_id'])); ?>" title="<?php echo osc_esc_html(__('Delete Conversation', 'uMessages')); ?>"><i class="mdi mdi-delete mdi-24px"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <div class="alert alert-warning"><?php  _e("You don't have any conversations", "uMessages"); ?></div>
    <?php endif; ?>
  </div>
  <style>
      .messages-wrapper {
        display: inline-block;
        max-width: 70%;
        width: 55vw;
      }
      .col-left {
        display: inline-block;
      }
      .notmain {
        margin-bottom: 53px;
      } 
      .um-page-header {
            font-size: 30px;
            text-align: left;
            margin: 0 0 21px;
        }
      .box {
        border: 1px solid #E9E9E9;
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.16);
      }
      .box .header {
        background: #fff;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      .box .content {
        background: #fff;
      .box .header h4 {
        border-bottom: none;
      }
      .box .header h4 {
        border-bottom: none;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      #sendMessage {
        background-color: #6BE400!important;
        transition: all .2s ease;
        font-size: 14px;
        border-radius: 2px;
        color: #fff;
        padding: 14px 10px;
        min-width: 200px;
        text-align: center;
        cursor: pointer;
        height: auto;
        margin-bottom: 20px;
      }
      #sendMessage:hover {
        background-color: #1531AE!important;
      }
      .ibtn .ibtn-primary {
        background-color: #6BE400!important;
        transition: all .2s ease;
      }
      .ibtn .ibtn-primary:hover {
        background-color: #1531AE!important;
      }
      .page-item.active .page-link {
        background-color: #1531AE!important;
      }
      a:hover {
        color: #fff!important;
      }
      @media screen and (max-width: 1000px) {
        .messages-wrapper {
            max-width: 100%;
            width: 100%;
          }
      }
  </style>
<script type="text/javascript">
$(document).ready(function() {
    $('a#action-spam').click(function() {
        if(confirm('<?php echo osc_esc_js(__('Are you sure you want to Mark the Conversation as Spam?', 'uMessages')); ?>')) return true;
        
        return false;
    });
    
    $('a#action-blist').click(function() {
        if(confirm('<?php echo osc_esc_js(__('Are you sure you want to Add the user to the Black List?', 'uMessages')); ?>')) return true;
        
        return false;
    });
    
    $('a#action-archive').click(function() {
        if(confirm('<?php echo osc_esc_js(__('Are you sure you want to Move the Conversation to the Archive?', 'uMessages')); ?>')) return true;
        
        return false;
    });
    
    $('a#action-trash').click(function() {
        if(confirm('<?php echo osc_esc_js(__('Are you sure you want to Delete the Conversation?', 'uMessages')); ?>')) return true;
        
        return false;
    });
    
    var columnsCount = $('#msg-inbox th').length;

    $('#msg-inbox').DataTable({
        "language": {
            "lengthMenu": '<?php echo osc_esc_js(__('Display _MENU_ records per page', 'uMessages')); ?>',
            "zeroRecords": '<?php echo osc_esc_js(__('Nothing found - sorry', 'uMessages')); ?>',
            "info": '<?php echo osc_esc_js(__('Showing page _PAGE_ of _PAGES_', 'uMessages')); ?>',
            "infoEmpty": '<?php echo osc_esc_js(__('No records available', 'uMessages')); ?>',
            "infoFiltered": '<?php echo osc_esc_js(__('(filtered from _MAX_ total records)', 'uMessages')); ?>',
            "search": '<?php echo osc_esc_js(__('Search: ', 'uMessages')); ?>',
            
            paginate: {
                first:    '<?php echo osc_esc_js(__('First', 'uMessages')); ?>',
                previous: '<?php echo osc_esc_js(__('Previous', 'uMessages')); ?>',
                next:     '<?php echo osc_esc_js(__('Next', 'uMessages')); ?>',
                last:     '<?php echo osc_esc_js(__('Last', 'uMessages')); ?>'
            }
        },
        "columns": [ null, null, null, null, null, {"orderable": false} ]
    });
    $('.sorting, .sorting, .sorting_disabled').css('color', '#000');
    $(' .page-item.active').children().css('background-color', '#1531AE');
});
</script>