<div class="col-left">  
    <div class="left-menu">
                                <div class="profile-demo">
                                    <img src="<?php echo osc_current_web_theme_url('img/profile.jpg'); ?>" alt="img">
                                    <strong class="profile-demo__title"><?php _e('User account manager', 'eva'); ?></strong>
                                    <a href="<?php echo osc_user_profile_url(); ?>"><strong><?php echo osc_logged_user_name(); ?></strong></a>
                                </div>
                                <ul>
    <?php echo osc_private_user_menu(get_user_menu()); ?>
                                </ul>
                            </div>
                         </div>
  <div class="messages-wrapper">
<?php defined('ABS_PATH') or die('Access denied');
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
    if ( !osc_logged_user_id() ) {  
	ob_get_clean();
        osc_redirect_to(osc_user_login_url());
    }
    
    $cid = Params::getParam('cid');
 
    $messages = UMModel::newInstance()->umMessagesGet($cid, osc_logged_user_id());
    $checkBlocked = UMModel::newInstance()->umAntiSpamGet(osc_logged_user_id(), false);
    
    osc_current_user_locale() == 'ru_RU' ? $lang = '\Russian' : $lang = '\English';
    
    $config = array(
        'language' => '\RelativeTime\Languages' . $lang,
        'separator' => ', ',
        'suffix' => true,
        'truncate' => true
    );
    
    $relativeTime = new \RelativeTime\RelativeTime($config);
    
    $m_action = Params::getParam('um-action');
    $mid = Params::getParam('mid');
    
    if($m_action) {
        ob_get_clean();
        
        if($m_action == 'message-send') {
            UMModel::newInstance()->umMessageReply($cid, Params::getParam('message'), osc_logged_user_id());
        }
        elseif($m_action == 'dmessage') {
            UMModel::newInstance()->umMessageDelete($mid, osc_logged_user_id());
        }
        
        osc_redirect_to(osc_route_url('umessages-view', array('cid' => $cid)));
    }

    require_once 'top-menu.php';
?>
<div class="box">
    <div class="header">
    	<h4><?php _e('Conversation', 'uMessages') ?></h4>
    </div>
    
    <div class="content">                
        <?php if($messages): ?>
            <div class="messages-layout">
                <ul id="messages-list" class="zoomContainer">
                    <?php foreach($messages as $message): ?>
                	<li class="<?php if($message['fk_i_user_id'] == osc_logged_user_id()): ?>server<?php else: ?>client<?php endif; ?>">
                		<a href="<?php echo osc_user_public_profile_url($message['fk_i_user_id']); ?>" title=""><img src="<?php echo osc_base_url() . 'oc-content/plugins/uMessages/assets/images/user_default.gif'; ?>" alt="<?php echo osc_esc_html($message['s_name']); ?>"></a>
                		<div class="message-area">
                			<span class="pointer"></span>
                			<div class="info-row">
                				<span class="user-name"><a href="<?php echo osc_user_public_profile_url($message['fk_i_user_id']); ?>"><strong><?php echo $message['s_name'] ?></strong></a> <?php _e('says', 'uMessages'); ?>:</span>
                                <?php if(osc_get_preference('um_rm_msg_enable','umess') && $message['fk_i_user_id'] == osc_logged_user_id() && !$checkBlocked['i_admin_blocked'] && $checkBlocked['i_spam_count'] <= osc_get_preference('um_usr_antispam_number','umess')): ?>
                				<a id="delete-message" href="<?php echo osc_route_url('umessages-view', array('um-action' => 'dmessage', 'cid' => $cid, 'mid' => $message['pk_i_message_id'])); ?>"><span class="delete">x</span></a>
                                <?php endif; ?>
                				<span class="time"><?php echo $relativeTime->timeAgo($message['d_message_date']); ?></span>
                				<div class="clear"></div>
                			</div>
                			<p>
                				<?php echo $message['s_message'] ?>
                			</p>
                            <?php if(isset($message['attachments'])): ?>
                                <div class="message-attachments">
                                    <p><?php _e('Attachmets:', 'uMessages'); ?></p>
                                    <?php foreach($message['attachments'] as $attachment): ?>
                                        <span><?php echo $attachment['attachment'] ?></span>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                		</div>
                		<div class="clear"></div>
                	</li>
                    <?php endforeach; ?>
            	</ul>
            </div>
        <?php else: ?>
            <div class="um-no-messages"><?php  _e("You don't have any Messages", "uMessages") ?></div>
        <?php endif; ?>
        <?php if(osc_get_preference('um_amess_enable','umess') && UMModel::newInstance()->umAlertsOutputs($cid, osc_logged_user_id())): ?>
        <div class="alert alert-warning alert-view-message"><i class="fas fa-exclamation-circle"></i> <?php echo UMModel::newInstance()->umAlertsOutputs($cid, osc_logged_user_id()); ?></div>
        <?php else: ?>
        <div class="message-entry"> 
            <form id="um-msg-send" action="<?php echo osc_route_url('umessages-view', array('cid' => $cid) ); ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="um-action" value="message-send" />
                <input id="cid" type="hidden" name="cid" value="<?php echo $cid; ?>" />                
                
                <textarea id="message" name="message" rows="1" data-min-rows="1" data-rowheight="17" required></textarea>
                <div class="send-group">
                    <?php if(osc_get_preference('um_attach_enable','umess')): ?>
                    <a id="attach-file" href="javascript:void(0);" class="attachPhoto"></a>
                    <?php endif; ?>
                    <button type="submit" name="send-message" id="sendMessage" class="ibtn ibtn-primary"> <?php _e('Send', 'uMessages') ?></button>
                </div>
                
                <div id="attached-files" class="attached-files"></div>
            </form>
        </div> 
        <?php endif; ?>      
    </div>  
</div>
</div>
<style>
      .messages-wrapper {
        display: inline-block;
        max-width: 70%;
        width: 55vw;
      }
      .col-left {
        display: inline-block;
      }
      .notmain {
        margin-bottom: 53px;
      } 
      .um-page-header {
            font-size: 30px;
            text-align: left;
            margin: 0 0 21px;
        }
        .um-page-header {
            font-size: 30px;
            text-align: left;
            margin: 0 0 21px;
            text-transform: uppercase;
            font-weight: 400;
            margin: 0 0 25px;
            letter-spacing: 1px;
            color: #444343 !important;
      }
      .box {
        border: 1px solid #E9E9E9;
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.16);
      }
      .box .header {
        background: #fff;
      }
      .box .content {
        background: #fff;
      }
      .box .header h4 {
        border-bottom: none;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      #sendMessage {
        background-color: #6BE400!important;
        transition: all .2s ease;
        font-size: 14px;
        border-radius: 2px;
        color: #fff;
        padding: 14px 10px;
        min-width: 200px;
        text-align: center;
        cursor: pointer;
        height: auto;
        margin-bottom: 20px;
      }
      #sendMessage:hover {
        background-color: #1531AE!important;
      }
      .ibtn .ibtn-primary {
        background-color: #6BE400!important;
        transition: all .2s ease;
      }
      .ibtn .ibtn-primary:hover {
        background-color: #1531AE!important;
      }
      @media screen and (max-width: 1000px) {
        .messages-wrapper {
            max-width: 100%;
            width: 100%;
          }
      }
  </style>
<script type="text/javascript">

$('a#delete-message').click(function() {
    if(confirm('<?php echo osc_esc_js(__('Are you sure you want to Delete the Message?', 'uMessages')); ?>')) return true;
    
    return false;
});
    
window.onload = function(){
    var url = '/index.php?page=ajax&action=custom&ajaxfile=uMessages/upload.php';
    
    $('html, body').animate({scrollTop:$(document).height()}, 1000);
    $('.messages-layout').animate({scrollTop:$('#messages-list').height()}, 1000);
    
    $("#attach-file").dropzone({ 
                        url: url,
                        uploadMultiple: true,
                        maxFilesize: <?php echo round(osc_get_preference('um_fmaxsize','umess') / 1024, 2, PHP_ROUND_HALF_UP); ?>,
                        filesizeBase: 1024,
                        maxFiles: <?php echo osc_get_preference('um_max_attach','umess'); ?>,
                        parallelUploads: <?php echo osc_get_preference('um_max_attach','umess'); ?>,
                        acceptedFiles: '<?php echo osc_get_preference('um_fext','umess'); ?>',
                        autoProcessQueue: false,
                        addRemoveLinks: true,
                        previewsContainer: '#attached-files',
                        paramName: 'attached',
                        hiddenInputContainer: '#um-msg-send',
                        dictRemoveFile: '<?php  echo osc_esc_js(__('Remove File', 'uMessages')); ?>',
                        dictMaxFilesExceeded: '<?php  echo osc_esc_js(__('You can not upload any more files', 'uMessages')); ?>',
                        dictFileTooBig: '<?php  echo osc_esc_js(__('File is too big ({{filesize}}Mb). Max filesize: {{maxFilesize}}Mb', 'uMessages')); ?>',
                        dictInvalidFileType: '<?php  echo osc_esc_js(__('You can not upload files of this type', 'uMessages')); ?>',
                        init: function() {
                            var myDropzone = this;
                            
                            myDropzone.on('addedfile', function(file) {
                                var thumb = getImgThumbnail(file.name);
                
                                myDropzone.createThumbnailFromUrl(file, thumb);
                            });
                            
                            $('#sendMessage').click(function(e) {
                                if (myDropzone.files != "") {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    myDropzone.processQueue();
                                }
                            });
                            
                            myDropzone.on('sendingmultiple', function(data, xhr, formData) {
                                console.log(formData);
                                formData.append('message', $('#message').val());
                                formData.append('cid', $('#cid').val());
                                formData.append('msg-type', 'message');
                            });
                            
                            myDropzone.on('successmultiple', function() {
                                location.href = location.href;
                            }); 
                        }
    });
                    
    $('#message').on('keyup',function(e){
        var maxHeight = 200; 
        var f = document.getElementById('message'); 
        
        if (f.clientHeight < f.scrollHeight && f.scrollHeight < maxHeight ) { 
            f.style.height = f.scrollHeight + 1 + 'px'; 
            
            if(f.scrollHeight > 34) {
                $('.send-group').css('top', f.scrollHeight - 37 + 'px');
            }
            
        }
        else {
            $('.send-group').css('right', '45px');
            $('#message').css('overflow-y', 'scroll');
        }
    }); 
    
    function getFileExt(file){                       
        file = file.toLowerCase();
        
        return file.split('.').pop();
    }
    
    function getImgThumbnail(file) {
        var ext, newThumbnail, imgFolder = '<?php echo UMS_USR_IMG; ?>';
        
        ext = getFileExt(file);
        
        if(ext == 'zip' || ext == 'rar' || ext == 'tar' || ext == '7z') {
            newThumbnail = imgFolder + 'zip.png';
        }
        else if(ext == 'txt') {
            newThumbnail = imgFolder + 'txt.png';
        }
        else if ( ext == 'doc' || ext == 'docx') {
            newThumbnail = imgFolder + 'doc.png';
        }
        else if ( ext == 'xls' || ext == 'xlsx') {
            newThumbnail = imgFolder + 'xls.png';
        }
        else if ( ext == 'ppt' || ext == 'pptx') {
            newThumbnail = imgFolder + 'ppt.png';
        }
        else if ( ext == 'pdf') {
            newThumbnail = imgFolder + 'pdf.png';
        }
        else if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff') {
            newThumbnail = '';
        }
        else {
            newThumbnail = imgFolder + 'folder.png';
        }
        
        return newThumbnail;
    }
}
</script>