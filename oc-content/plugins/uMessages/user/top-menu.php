<?php defined('ABS_PATH') or die('Access denied'); 
    if(osc_current_user_locale() == 'ru_RU') $dateFormat = 'd-m-Y H:i:s';
        else $dateFormat = 'm-d-Y H:i:s';
?>
<div class="um-top-menu-block">
    <div class="um-page-header"><?php _e(UMModel::newInstance()->umUserPageHeader(), 'uMessages'); ?></div>
    <ul class="um-top-menu">       
        <li>
            <a <?php if(UMModel::newInstance()->umTopMenu('umessages')): ?>class="active"<?php endif; ?> href="<?php echo osc_route_url('umessages'); ?>">
                <i class="mdi mdi-email mdi-24px"></i><br />
                <span><?php _e('Messages', 'uMessages'); ?></span>
            </a>
        </li>
        
        <li>
            <a <?php if(UMModel::newInstance()->umTopMenu('umessages-archive')): ?>class="active"<?php endif; ?> href="<?php echo osc_route_url('umessages-archive'); ?>">
                <i class="mdi mdi-archive mdi-24px"></i><br />
                <span><?php _e('Archive', 'uMessages'); ?></span>
            </a>
        </li>
        
        <li>
            <a <?php if(UMModel::newInstance()->umTopMenu('umessages-trash')): ?>class="active"<?php endif; ?> href="<?php echo osc_route_url('umessages-trash'); ?>">
                <i class="mdi mdi-delete mdi-24px"></i><br />
                <span><?php _e('Trash', 'uMessages'); ?></span>
            </a>
        </li>
        
        <li>
            <a <?php if(UMModel::newInstance()->umTopMenu('umessages-blocked')): ?>class="active"<?php endif; ?> href="<?php echo osc_route_url('umessages-blocked'); ?>">
                <i class="mdi mdi-account-off mdi-24px"></i><br />
                <span><?php _e('Black List', 'uMessages'); ?></span>
            </a>
        </li>
    </ul>
    <div class="clear"></div>
</div>