<div class="col-left">  
    <div class="left-menu">
                                <div class="profile-demo">
                                    <img src="<?php echo osc_current_web_theme_url('img/profile.jpg'); ?>" alt="img">
                                    <strong class="profile-demo__title"><?php _e('User account manager', 'eva'); ?></strong>
                                    <a href="<?php echo osc_user_profile_url(); ?>"><strong><?php echo osc_logged_user_name(); ?></strong></a>
                                </div>
                                <ul>
    <?php echo osc_private_user_menu(get_user_menu()); ?>
                                </ul>
                            </div>
                         </div>
  <div class="messages-wrapper">
<?php defined('ABS_PATH') or die('Access denied');
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
    if ( !osc_logged_user_id() ) {  
        return false;
    }
 
    $users = UMModel::newInstance()->umBlockedUsersGet(osc_logged_user_id());
    
    $c_actions = Params::getParam('um-action');
    $aid = Params::getParam('aid');
    
    if($c_actions) {
        ob_get_clean();
        $result = UMModel::newInstance()->umUserConversationActions(osc_logged_user_id(), $c_actions, $aid);
        
        osc_redirect_to(osc_route_url('umessages-blocked'));
    }

    require_once 'top-menu.php';
?>
<?php if($users): ?>
    <table id="msg-blocked" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th style="width: 20px;">#</th>
                <th><?php _e('User', 'uMessages'); ?></th>
                <th style="width: 50px;"><?php _e('Actions', 'uMessages'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $i => $user): ?>
            <tr>
                <td><?php echo $i + 1; ?></td>
                <td><?php echo $user['s_name']; ?></td>
                <td class="um-table-actions">
                    <a href="<?php echo osc_route_url('umessages-blocked', array('um-action' => 'unblock','aid' => $user['pk_i_block_id'])); ?>" title="<?php echo osc_esc_html(__('Unblock', 'uMessages')); ?>"><i class="mdi mdi-undo-variant mdi-24px"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="alert alert-warning"><?php  _e("You don't have any Blocked Users", "uMessages"); ?></div>
<?php endif; ?>
</div>
  <style>
      .messages-wrapper {
        display: inline-block;
        max-width: 70%;
        width: 55vw;
      }
      .col-left {
        display: inline-block;
      }
      .notmain {
        margin-bottom: 53px;
      } 
      .um-page-header {
            font-size: 30px;
            text-align: left;
            margin: 0 0 21px;
        }
       .box {
        border: 1px solid #E9E9E9;
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.16);
      }
      .box .header {
        background: #fff;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      .box .content {
        background: #fff;
      .box .header h4 {
        border-bottom: none;
      }
      .box .header h4 {
        border-bottom: none;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      #sendMessage {
        background-color: #6BE400!important;
        transition: all .2s ease;
        font-size: 14px;
        border-radius: 2px;
        color: #fff;
        padding: 14px 10px;
        min-width: 200px;
        text-align: center;
        cursor: pointer;
        height: auto;
        margin-bottom: 20px;
      }
      #sendMessage:hover {
        background-color: #1531AE!important;
      }
      .ibtn .ibtn-primary {
        background-color: #6BE400!important;
        transition: all .2s ease;
      }
      .ibtn .ibtn-primary:hover {
        background-color: #1531AE!important;
      }
      .page-item.active .page-link {
        background-color: #1531AE!important;
      }
      a:hover {
        color: #fff!important;
      } 
      @media screen and (max-width: 1000px) {
        .messages-wrapper {
            max-width: 100%;
            width: 100%;
          }
      }
  </style>
<script type="text/javascript">
$(document).ready(function() {   
    $('#msg-blocked').DataTable({
        "language": {
            "lengthMenu": '<?php echo osc_esc_js(__('Display _MENU_ records per page', 'uMessages')); ?>',
            "zeroRecords": '<?php echo osc_esc_js(__('Nothing found - sorry', 'uMessages')); ?>',
            "info": '<?php echo osc_esc_js(__('Showing page _PAGE_ of _PAGES_', 'uMessages')); ?>',
            "infoEmpty": '<?php echo osc_esc_js(__('No records available', 'uMessages')); ?>',
            "infoFiltered": '<?php echo osc_esc_js(__('(filtered from _MAX_ total records)', 'uMessages')); ?>',
            "search": '<?php echo osc_esc_js(__('Search: ', 'uMessages')); ?>',
            paginate: {
                first:    '<?php echo osc_esc_js(__('First', 'uMessages')); ?>',
                previous: '<?php echo osc_esc_js(__('Previous', 'uMessages')); ?>',
                next:     '<?php echo osc_esc_js(__('Next', 'uMessages')); ?>',
                last:     '<?php echo osc_esc_js(__('Last', 'uMessages')); ?>'
            }
        },
        "columns": [ null, null, {"orderable": false} ]
    });
    $('.sorting, .sorting, .sorting_disabled').css('color', '#000');
    $(' .page-item.active').children().css('background-color', '#1531AE');
});

</script>