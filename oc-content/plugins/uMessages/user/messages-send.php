<div class="col-left">  
    <div class="left-menu">
                                <div class="profile-demo">
                                    <img src="<?php echo osc_current_web_theme_url('img/profile.jpg'); ?>" alt="img">
                                    <strong class="profile-demo__title"><?php _e('User account manager', 'eva'); ?></strong>
                                    <a href="<?php echo osc_user_profile_url(); ?>"><strong><?php echo osc_logged_user_name(); ?></strong></a>
                                </div>
                                <ul>
    <?php echo osc_private_user_menu(get_user_menu()); ?>
                                </ul>
                            </div>
                         </div>
  <div class="messages-wrapper">
<?php defined('ABS_PATH') or die('Access denied');
/*
 * Copyright 2017 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
    if ( !osc_logged_user_id() ) {  
        return false;
    }
    
    $item_id = Params::getParam('itemId'); 
    
    if($item_id) {
        $item = Item::newInstance()->findByPrimaryKey($item_id);
        $item_details = UMModel::newInstance()->umItemDetails($item_id);
    }

    if(Params::getParam('um_action') == 'message_send') {
        $conversation_id = UMModel::newInstance()->umConversationStart($item_id, Params::getParam('um_titile'), Params::getParam('um_message'));
        ob_get_clean();
            
        if($conversation_id) {
            osc_redirect_to(osc_route_url('umessages-view', array('cid' => $conversation_id)));
        }
        else {
            osc_redirect_to(osc_route_url('umessages-send', array('itemId' => $item_id)));
        }
    }
?>

<?php if($item_id && isset($item) && $item): ?>
    <?php if(osc_is_web_user_logged_in() && $item['fk_i_user_id'] != osc_logged_user_id()): ?>
        <h2 class="um-message-send-h"><?php  _e("Start Conversation", "uMessages") ?></h2>
        
        <div class="um-item-block">
            <div class="um-item-resource"><img src="<?php echo $item_details['r_path']; ?>" /></div>
            <div class="um-item-details">
                <h2>
                    <a target="_blank" href="<?php echo osc_item_url_from_item( $item ); ?>"><?php echo $item['s_title']; ?></a>
                </h2>
                
                <p class="um-item-price"><?php echo $item_details['price']; ?></p>
                <p class="um-item-location"><?php echo $item_details['location']; ?></p>
            </div>
        </div>
    
        <div class="um-message-send-block">
            <?php if(osc_get_preference('um_amess_enable','umess') && UMModel::newInstance()->umAlertsOutputs($item_id, osc_logged_user_id(), true)): ?>
                <div class="alert alert-warning alert-start-con"><i class="fas fa-exclamation-circle"></i> <?php echo UMModel::newInstance()->umAlertsOutputs($item_id, osc_logged_user_id(), true); ?></div>
            <?php else: ?>
                <form id="um-conversation-start" action="<?php echo osc_route_url('umessages-send', array('itemId' => $item_id) ); ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="um_action" value="message_send">
                    <input id="item-id" type="hidden" name="um_item_id" value="<?php echo $item_id; ?>">
                    
                    <div class="um-message">
                        <input id="thread" class="um-thread-name" type="text" name="um_titile" placeholder="<?php echo osc_esc_html(__('Enter your thread...', 'uMessages')); ?>" required />
                        <textarea id="message" name="um_message" cols="40" rows="8" placeholder="<?php echo osc_esc_html(__('Write your message...', 'uMessages')); ?>" required></textarea>
                        
                        <button id="conversation-start-btn" type="submit"><i class="mdi mdi-send mdi-18px"></i> <?php _e('Send', 'uMessages'); ?></button>
                        <style>
                            #conversation-start-btn {
                                background-color: #6BE400!important;
                                transition: all .2s ease;
                                font-size: 14px;
                                border-radius: 2px;
                                color: #fff;
                                padding: 14px 10px;
                                min-width: 200px;
                                text-align: center;
                                cursor: pointer;
                                height: auto;
                              }
                              #conversation-start-btn:hover {
                                background-color: #1531AE!important;
                              }
                        </style>
                        
                        <?php if(osc_get_preference('um_attach_enable','umess')): ?>
                            <a id="attach-file" class="um-attach-file" href="javascript: void(0);"><i class="mdi mdi-attachment mdi-18px"></i><?php _e('Upload File', 'uMessages'); ?></a>
                        <?php endif; ?>
                        
                        <div id="attached-files" class="attached-files" style="margin-left: 0;"></div>
                    </div>
                </form>
            <?php endif; ?>
        </div>
    <?php else: ?>
        <div class="alert alert-warning"><?php  _e("You can't contact yourself", "uMessages"); ?></div>
    <?php endif; ?>
<?php else: ?>

    <div class="alert alert-warning"><?php  _e("You can not send a message, Item does not Exist!", "uMessages"); ?></div>
<?php endif; ?>
</div>
  <style>
      .messages-wrapper {
        display: inline-block;
        max-width: 70%;
        width: 55vw;
      }
      .col-left {
        display: inline-block;
      }
      .notmain {
        margin-bottom: 53px;
      } 
      .um-page-header {
            font-size: 30px;
            text-align: left;
            margin: 0 0 21px;
        }
      .box {
        border: 1px solid #E9E9E9;
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.16);
      }
      .box .header {
        background: #fff;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      .box .content {
        background: #fff;
      .box .header h4 {
        border-bottom: none;
      }
      .box .header h4 {
        border-bottom: none;
        color: #444343;
        font: 16px 'lato', sans-serif;
        font-weight: bold;
      }
      #sendMessage {
        background-color: #6BE400!important;
        transition: all .2s ease;
        font-size: 14px;
        border-radius: 2px;
        color: #fff;
        padding: 14px 10px;
        min-width: 200px;
        text-align: center;
        cursor: pointer;
        height: auto;
        margin-bottom: 20px;
      }
      #sendMessage:hover, #conversation-start-btn:hover {
        background-color: #1531AE!important;
      }
      .ibtn .ibtn-primary {
        background-color: #6BE400!important;
        transition: all .2s ease;
      }
      .ibtn .ibtn-primary:hover {
        background-color: #1531AE!important;
      }
      .page-item.active .page-link {
        background-color: #1531AE!important;
      }
      a:hover {
        color: #fff!important;
      }
      @media screen and (max-width: 1000px) {
        .messages-wrapper {
            max-width: 100%;
            width: 100%;
          }
      }
      @media screen and (max-width: 1000px) {
        .messages-wrapper {
            max-width: 100%;
            width: 100%;
          }
      }
  </style>
<script type="text/javascript">
window.onload = function(){
    var url = '/index.php?page=ajax&action=custom&ajaxfile=uMessages/upload.php';
    
    $("#attach-file").dropzone({ 
                        url: url,
                        uploadMultiple: true,
                        maxFilesize: <?php echo round(osc_get_preference('um_fmaxsize','umess') / 1024, 2, PHP_ROUND_HALF_UP); ?>,
                        filesizeBase: 1024,
                        maxFiles: <?php echo osc_get_preference('um_max_attach','umess'); ?>,
                        parallelUploads: <?php echo osc_get_preference('um_max_attach','umess'); ?>,
                        acceptedFiles: '<?php echo osc_get_preference('um_fext','umess'); ?>',
                        autoProcessQueue: false,
                        addRemoveLinks: true,
                        previewsContainer: '#attached-files',
                        paramName: 'attached',
                        hiddenInputContainer: '#um-conversation-start',
                        dictRemoveFile: '<?php  echo osc_esc_js(__('Remove File', 'uMessages')); ?>',
                        dictMaxFilesExceeded: '<?php  echo osc_esc_js(__('You can not upload any more files', 'uMessages')); ?>',
                        dictFileTooBig: '<?php  echo osc_esc_js(__('File is too big ({{filesize}}Mb). Max filesize: {{maxFilesize}}Mb', 'uMessages')); ?>',
                        dictInvalidFileType: '<?php  echo osc_esc_js(__('You can not upload files of this type', 'uMessages')); ?>',
                        init: function() {
                            var myDropzone = this;
                            
                            myDropzone.on('addedfile', function(file) {
                                var thumb = getImgThumbnail(file.name);
                
                                myDropzone.createThumbnailFromUrl(file, thumb);
                            });
                            
                            $('#conversation-start-btn').click(function(e) {
                                if (myDropzone.files != "") {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    myDropzone.processQueue();
                                }
                            });
                            
                            myDropzone.on('sendingmultiple', function(data, xhr, formData) {
                                formData.append('thread', $('#thread').val());
                                formData.append('message', $('#message').val());
                                formData.append('item_id', $('#item-id').val());
                                formData.append('msg-type', 'conversation');
                            });
                            
                            myDropzone.on('successmultiple', function(files, response) {                            
                                location.href = response;
                            }); 
                        }
    });
    
    function getFileExt(file){                       
        file = file.toLowerCase();
        
        return file.split('.').pop();
    }
    
    function getImgThumbnail(file) {
        var ext, newThumbnail, imgFolder = '<?php echo UMS_USR_IMG; ?>';
        
        ext = getFileExt(file);
        
        if(ext == 'zip' || ext == 'rar' || ext == 'tar' || ext == '7z') {
            newThumbnail = imgFolder + 'zip.png';
        }
        else if(ext == 'txt') {
            newThumbnail = imgFolder + 'txt.png';
        }
        else if ( ext == 'doc' || ext == 'docx') {
            newThumbnail = imgFolder + 'doc.png';
        }
        else if ( ext == 'xls' || ext == 'xlsx') {
            newThumbnail = imgFolder + 'xls.png';
        }
        else if ( ext == 'ppt' || ext == 'pptx') {
            newThumbnail = imgFolder + 'ppt.png';
        }
        else if ( ext == 'pdf') {
            newThumbnail = imgFolder + 'pdf.png';
        }
        else if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff') {
            newThumbnail = '';
        }
        else {
            newThumbnail = imgFolder + 'folder.png';
        }
        
        return newThumbnail;
    }
}
</script>