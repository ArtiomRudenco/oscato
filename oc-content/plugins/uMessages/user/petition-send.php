<?php defined('ABS_PATH') or die('Access denied');
/*
 * Copyright 2018 osclass-pro.com
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
    if ( !osc_logged_user_id() ) {  
        return false;
    }
    
    $checkPetition = UMModel::newInstance()->umPetitionExist(osc_logged_user_id()); 

    if(Params::getParam('um_action') == 'petiton_send') {
        $result = UMModel::newInstance()->umPetitionSend(osc_logged_user_id(), Params::getParam('um_message'));
        
        ob_get_clean();
            
        if($result) {
            osc_add_flash_ok_message(__('Congratulations! Petition sent successfully', 'uMessages'));
        }
        osc_redirect_to(osc_route_url('umessages-petition'));
    }
?>

<?php if(osc_is_web_user_logged_in() && !$checkPetition): ?>
    <h2 class="um-petition-h"><?php  _e("Start Petition", "uMessages") ?></h2>
    
    <div class="um-petition-send-block">
        <div class="um-petition-help-block">
            <p><?php _e('Remember! You can send the petition only once on the account. Therefore, send the petition only if you are sure that you were blocked by mistake!', 'uMessages'); ?></p>
        </div>
        <form action="<?php echo osc_route_url('umessages-petition'); ?>" method="post">
            <input type="hidden" name="um_action" value="petiton_send">
            
            <div class="um-message">
                <textarea name="um_message" cols="40" rows="8" placeholder="<?php echo osc_esc_html(__('Write your message...', 'uMessages')); ?>" required></textarea>
                
                <button type="submit"> <?php _e('Send Petition', 'uMessages'); ?></button>
            </div>
        </form>
    </div>
<?php else: ?>
    <div class="alert alert-warning"><?php  _e("You have already Sent a Petition and can no longer send again!", "uMessages") ?></div>
<?php endif; ?>