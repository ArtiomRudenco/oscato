<?php
/**
 * Russian.php
 *
 * @author  Vitaly Antonov <fenix.krd@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace RelativeTime\Languages;

/**
 * Russian Translation
 */
class Russian extends \RelativeTime\Adapters\Language
{
    protected $strings = array(
        'now' => 'только что',
        'ago' => '%s назад',
        'left' => '%s осталось',
        'seconds' => array(
            'plural' => '%d секунд',
            'singular' => '%d секунды',
        ),
        'minutes' => array(
            'plural' => '%d минут',
            'singular' => '%d минуты',
        ),
        'hours' => array(
            'plural' => '%d часов',
            'singular' => '%d час(а)',
        ),
        'days' => array(
            'plural' => '%d дней',
            'singular' => '%d дня',
        ),
        'months' => array(
            'plural' => '%d месяцев',
            'singular' => '%d месяц(а)',
        ),
        'years' => array(
            'plural' => '%d лет',
            'singular' => '%d год(а)',
        ),
    );
}

?>
