<?php defined('ABS_PATH') or die('Access denied');
require_once 'model/uMModel.php';
require_once 'classes/uMUser.php';
require_once 'classes/uMAdmin.php';
require_once 'libs/RelativeTime/Autoload.php';
require_once 'libs/Carbon/Carbon.php';