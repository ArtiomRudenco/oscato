��          �      �       H     I     W  1   e  1   �     �  *   �          #     ;  "   O     r     w  :  �    �     �     �  �     V   �  +   �  H   &  .   o  '   �  *   �  I   �     ;  4   N  s  �         
          	                                              Add No Follow Add No Opener Add a rel="nofollow" attribute to outbound links. Add a rel="noopener" attribute to outbound links. Auto Convert Emails Automatically converts emails into mailto. Frequently Asked Questions How to use this plugin? Open in New Windows Open outbound links in new windows Save Settings saved successfully Wrap our helper function for outputting a string or any content using <code>external_links_make_clickable()</code> function, for an example look for <code>osc_item_description()</code> inside item.php of the theme and apply the function helpers: <code>external_links_make_clickable( osc_item_description() )</code> Project-Id-Version: External Links for Osclass
POT-Creation-Date: 2019-09-18 03:30+0300
PO-Revision-Date: 2019-09-18 03:38+0300
Last-Translator: 
Language-Team: osclass.pro
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Добавлять No Follow Добавлять No Opener Добавлять атрибут rel="nofollow" к исходящим ссылкам. Закрывает от индексации Google Добавлять атрибут rel = "noopener" в исходящие ссылки. Авто преобразование email Автоматически конвертировать email в mailto. Часто задаваемые вопросы Как настроить плагин? Открывать в новом окне. Открывать исходящие ссылки в новом окне Сохранить Настройки успешно сохранены Оберните в нашу функцию  <code>external_links_make_clickable()</code> текст объявления, для примера найдите код <code>osc_item_description()</code> внутри файла item.php Вашего шаблона и примените функцию : <code>external_links_make_clickable( osc_item_description() )</code> 