<?php 
require_once 'ModelBlog.php';

$tbl_name = ModelBlog::newInstance()->getTable_Blog();
$dbLink = new mysqli(osc_db_host(), osc_db_user(), osc_db_password(), osc_db_name());
$sql = "SELECT id, b_title, b_date FROM $tbl_name WHERE b_status = 1 ORDER BY b_date DESC";
$result = $dbLink->query($sql);
$total = $result->num_rows;
if($result && $result->num_rows > 0)
{
	// An array to store the data in a more managable order.
	$data = array();
	?>
    <ul class="blog-archives">
	<?php
    // Add each entry to the $data array, sorted by Year and Month
	while($row = $result->fetch_assoc())
	{
	//$mysqldate = $blog['b_date'];
	//$phpdate = strtotime( $mysqldate );
	//echo date( 'd M Y',  $phpdate );
		$year = date('Y', strtotime($row['b_date']));
		$month = date('F', strtotime($row['b_date']));
		
		$data[$year][$month][] = $row;
	}
	$result->free();
	
	// Go through each Year and Month and print a list of entries, sorted by month.
	foreach($data as $_year => $_months) { ?>
	<li><a href='<?php echo osc_route_url('sc_blog_archive', array('year' => $_year, 'month'=> '')) ;?>'><?php echo $_year;?></a>
    	<ul>
	  	<?php foreach($_months as $_month => $_entries) { ?>
		<li><a href='<?php echo osc_route_url('sc_blog_archive', array('year' => $_year, 'month'=> date('m', strtotime($_month)))) ;?>'><?php echo __($_month, 'blog');?> (<?php echo count($_entries);?>)</a></li>
	   	<?php } ?>
        </ul>
	</li>
    <?php } ?>
    </ul>
<?php 
}
$dbLink->close();
?>