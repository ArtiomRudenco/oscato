CREATE TABLE IF NOT EXISTS /*TABLE_PREFIX*/t_blog (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `b_status` enum('1', '0') NOT NULL,
  `b_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `b_slug` varchar(150) NOT NULL,
  `b_category` varchar(250) NOT NULL DEFAULT 'Uncategory',
  `b_content` text CHARACTER SET utf8 NOT NULL,
  `b_meta_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `b_meta_content` text CHARACTER SET utf8 NOT NULL,
  `b_meta_keyword` text CHARACTER SET utf8 NOT NULL,
  `b_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';



CREATE TABLE IF NOT EXISTS /*TABLE_PREFIX*/t_blog_categories (
  `bc_id` int(11) NOT NULL AUTO_INCREMENT,
  `bc_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `bc_slug` varchar(250) NOT NULL,
  `bc_description` text NOT NULL,
  PRIMARY KEY (`bc_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';



INSERT INTO /*TABLE_PREFIX*/t_blog_categories (bc_id, bc_title, bc_slug, bc_description) VALUES (1,'Uncategory', 'uncategory', 'somthing about the category');
INSERT INTO /*TABLE_PREFIX*/t_blog (id, b_title, b_slug, b_category, b_content, b_date) VALUES (1,'Hello World','hello-world', '1','This is an example blog post, the osclass blog plugin developed by DrizzleThemes. You can edit this blog post or delete from Blog settings through osclass admin panel.','2016/01/12');