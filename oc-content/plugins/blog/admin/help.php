<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>css/plugin.css" type="text/css" rel="stylesheet" />

<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('Help', 'blog'); ?></h1>
    <a class="float_right" href="https://market.osclass.org/user/profile/15" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php');?>"><?php _e('All Posts', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/post.php');?>"><?php _e('Add New', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/categories.php');?>"><?php _e('Categories', 'blog'); ?></a></li>
              <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'blog'); ?></a></li>
        <li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'blog'); ?></a></li>
    </ul>
</div>

<div class="plugin-content">
	<h3><?php _e('What is Blog?','blog');?></h3>
    <p><?php _e('Blog Osclass plugin to create a beautiful responsive Blog or News section for osclass classified website. Blog plugin has admin dashboard to manage blog, category and settings of blog. Its also have Social share integration with Facebook, Twitter, Google and Recent post widget to use anywhere within osclass website.','blog');?></p>
    
    <h3><?php _e('How to use?','blog');?></h3>
    <p><strong><?php _e('Blog page link','blog');?></strong><br />
    <?php _e('Use following code to display Blog page link on your website front-end, Copy and paste the code into any theme files such as header.php, sidebar.php and footer.php:','blog');?><br /><br />
    <code>&lt;?php blog_url(); ?&gt;</code>
    </p>
    <br />
    <p><strong><?php _e('Recent post widget','blog');?></strong><br />
    <?php _e('Use following code to display Blog recent posts on your website wherever you need to show Blog recent posts.','blog');?>
    <br /><br />
	<code>&lt;?php recent_blog_post(); ?&gt;</code></p>
    <br />
    <?php osc_run_hook('blog_help_content'); ?>
    <br />
    <p><strong><?php _e('About','blog');?></strong><br />
    	<?php _e('Version:','blog');?> <?php echo VERSION; ?><br />
        <?php _e('Author:','blog');?> DrizzleThemes
     </p>
     <p><strong><?php _e('Support','blog');?></strong><br />
     	<?php _e('Email','blog');?>:<a href="mailto:support@drizzlethemes.com" target="_blank">support@drizzlethemes.com</a>
     </p>
</div>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Blog Osclass Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>
    
    