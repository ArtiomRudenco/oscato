<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>css/plugin.css" type="text/css" rel="stylesheet" />
<?php $pluginInfo = osc_plugin_get_info('blog/index.php');  ?>
<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('Settings', 'blog'); ?></h1>
    <a class="float_right" href="https://market.osclass.org/user/profile/15" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php');?>"><?php _e('All Posts', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/post.php');?>"><?php _e('Add New', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/categories.php');?>"><?php _e('Categories', 'blog'); ?></a></li>
         <li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'blog'); ?></a></li>
    </ul>
</div>

<div class="plugin-content">
	<div class="form-horizontal">
    <form action="<?php echo osc_admin_render_plugin_url('blog/admin/settings.php'); ?>" method="post">
      <input type="hidden" name="blogoption" value="blogsettings" />
     
      <div class="form-row">
        <div class="form-label"><?php _e('Blog Path', 'blog'); ?></div>
        <div class="form-controls">
            <input type="text" name="blog_path" value="<?php echo osc_esc_html(blog_path()); ?>"><br />
            <small><b><?php echo osc_base_url().''.osc_esc_html(blog_path()); ?></b> (<?php _e('It\'s work with permalink enabled', 'blog'); ?>)</small>
        </div>
      </div>
      <div class="form-row">
        <div class="form-label"><?php _e('Blog Editor', 'blog'); ?></div>
        <div class="form-controls">
            <select name="blog_editor">
            <option <?php if(blog_editor()=='tinymce'){echo 'selected';} ?> value="tinymce"><?php _e('Tinymce', 'blog'); ?></option>
            <option <?php if(blog_editor()=='niceedit'){echo 'selected';} ?> value="niceedit"><?php _e('Niceedit', 'blog'); ?></option>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-label"><?php _e('Blog posts count', 'blog'); ?></div>
        <div class="form-controls">
            <input type="text" class="swidth" name="blog_list_count" value="<?php echo osc_esc_html(blog_list_count()); ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-label"><?php _e('Recent posts count', 'blog'); ?></div>
        <div class="form-controls">
            <input type="text" class="swidth" name="blog_latest_count" value="<?php echo osc_esc_html(blog_latest_count()); ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-controls">
            <input type="checkbox" <?php echo (blog_show_thumb() ? 'checked="true"' : ''); ?> name="blog_show_thumb" id="blog_show_thumb" value="true" />
            <label><?php _e('Show thumbnail on blog list page', 'blog'); ?></label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-controls">
            <input type="checkbox" <?php echo (blog_show_archives() ? 'checked="true"' : ''); ?> name="blog_show_archives" id="blog_show_archives" value="true" />
            <label><?php _e('Show Archives', 'blog'); ?></label>
        </div>
      </div>
      
      
      <div class="form-row">
        <div class="form-controls">
            <input type="checkbox" <?php echo (blog_show_categories() ? 'checked="false"' : ''); ?> name="blog_show_categories" id="blog_show_categories" value="false" />
            <label><?php _e('Show Categories', 'blog'); ?></label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-controls">
            <input type="checkbox" <?php echo (blog_show_search() ? 'checked="true"' : ''); ?> name="blog_show_search" id="blog_show_search" value="true" />
            <label><?php _e('Show Blog search', 'blog'); ?></label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-controls">
            <input type="checkbox" <?php echo (blog_show_latest() ? 'checked="true"' : ''); ?> name="blog_show_latest" id="blog_show_latest" value="true" />
            <label><?php _e('Show Recent posts', 'blog'); ?></label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-controls">
            <input type="checkbox" <?php echo (blog_responsive_mode() ? 'checked="true"' : ''); ?> name="blog_responsive_mode" id="blog_responsive_mode" value="true" />
            <label><?php _e('Check this if you\'re using non-bootstrap theme', 'blog'); ?></label>
        </div>
      </div>
      
      <?php osc_run_hook('blog_plugin_setting');?>
    
      <div class="form-actions">
        <input type="submit" value="<?php _e('Save changes', 'blog'); ?>" class="btn btn-submit">
      </div>
      
    </form>
    </div>
</div>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Blog Osclass Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>