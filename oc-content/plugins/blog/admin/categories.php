<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>css/plugin.css" type="text/css" rel="stylesheet" />

<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('Categories', 'blog'); ?></h1>
    <a class="float_right" href="https://market.osclass.org/user/profile/15" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php');?>"><?php _e('All Posts', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/post.php');?>"><?php _e('Add New', 'blog'); ?></a></li>
        <li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/categories.php');?>"><?php _e('Categories', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'blog'); ?></a></li>
    </ul>
</div>

<?php 
if(Params::getParam("edit")) {
	$edit = ModelBlog::newInstance()->getCategoryById(Params::getParam("edit"));
		foreach($edit as $edit) { 
		$id = $edit['bc_id'];
		$title = $edit['bc_title'];
		$slug = $edit['bc_slug'];
		$description = $edit['bc_description'];
	}
} else {

		$id = "";
		$title = "";
		$slug = "";
		$description = "";

}
?>
<div class="plugin-content clearfix">
	<div class="float_left col5">
    	<?php if(Params::getParam("edit")) {?>
        	<h3><?php _e('Edit Category', 'blog'); ?></h3>
        <?php }else {?>
        	<h3><?php _e('Add New Category', 'blog'); ?></h3>
        <?php } ?>
        <form name="blog_category" id="blog_category" action="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/index.php');?>" method="post" >
			<input type="hidden" name="section" value="category" />
			<?php if(Params::getParam("edit")) {?>
            <input type="hidden" name="plugin_action" value="category_edit" />
            <input type="hidden" name="c_slug" value="<?php echo osc_esc_html($slug); ?>" />
            <input type="hidden" id="cId" name="cId" value="<?php echo osc_esc_html($id); ?>"/>
            <?php } else { ?>
            <input type="hidden" name="plugin_action" value="category_add" />
            <?php } ?>
			<div class="control-group">
                    <label><?php _e('Name', 'blog'); ?> :</label>
                    <div class="form-controls">
                        <input type="text" id="c_title" name="c_title" value="<?php echo osc_esc_html($title); ?>"/>
                    </div>
			</div>
			<?php if(!Params::getParam("edit")) {?> 
			<div class="control-group">
                    <label><?php _e('Slug', 'blog'); ?> :</label>
                    <div class="form-controls">
                        <input type="text" id="c_slug" name="c_slug" value="<?php echo osc_esc_html($slug); ?>"/>
                    </div>
			</div>
			<?php } ?>
			<div class="control-group">
                    <label><?php _e('Description', 'blog'); ?> :</label>
                    <div class="form-controls">
                        <textarea rows="10" cols="70" id="c_description" name="c_description"><?php echo $description; ?></textarea>
                    </div>
			</div>
			<div class="control-group">
                    <div class="form-controls">
                        <?php if(isset($_GET['edit'])) {?>
                            <input class="btn btn-blue" id="update" name="update" type="submit" value="<?php echo osc_esc_html(__('Update', 'blog'));?>">
                        <?php } else { ?>
                            <button class="btn btn-blue" type="submit" ><?php echo osc_esc_html(__('Save', 'blog')); ?></button>
                        <?php } ?>
                    </div>
			</div>
        </form>
    </div>
    <div class="float_right col5">
    	<h3><?php _e('Categories', 'blog'); ?></h3>
        <table class="table" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th><?php _e('Name', 'blog'); ?></th>
                    <th><?php _e('Action', 'blog'); ?></th>
                </tr>
            </thead>
            <tbody> 
            
            <?php $category = ModelBlog::newInstance()->getCategories();
        	foreach($category as $cat) { ?>
            <tr>
                <td><?php echo $cat['bc_title'];?></td>
                <td width="100">
                    <a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/categories.php');?>&edit=<?php echo $cat['bc_id'];?>"><?php _e('Edit', 'blog'); ?></a> | 
        <a onclick="return confirm('<?php echo osc_esc_js(__('Are you sure you want to continue?','blog')) ?>');" href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/index.php');?>&plugin_action=category_delete&cId=<?php echo $cat['bc_id']?>"><?php _e('Delete', 'blog'); ?></a>
                </td>
            </tr>
            <?php }?>
           </tbody>
        </table>
    </div>
</div>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Blog Osclass Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>