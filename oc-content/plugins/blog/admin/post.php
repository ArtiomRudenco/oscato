<?php if(blog_editor()=='tinymce'){ ?>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/blog/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
            tinyMCE.init({
                mode : "specific_textareas",
				editor_selector : "b_description",
                width: "100%",
                height: "400px",
                language: 'en',
                theme_advanced_toolbar_align : "left",
                theme_advanced_toolbar_location : "top",
                plugins : [
                    "advlist autolink lists link image charmap preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
				
				//toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
				
                entity_encoding : "raw",
                theme_advanced_buttons1_add : "forecolorpicker,fontsizeselect",
                theme_advanced_buttons2_add: "media, jbimages",
                theme_advanced_buttons3: "",
                theme_advanced_disable : "styleselect,anchor",
                relative_urls : false,
                remove_script_host : false,
                convert_urls : false
				
            });

        </script>
<?php } else { ?>
<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">
//bkLib.onDomLoaded(nicEditors.allTextAreas);
bkLib.onDomLoaded(
	function() {
		new nicEditor({
			fullPanel : true,
			height : 800
		}
		).panelInstance("b_description");
	}
	
); </script>
<style>
.form-controls.desc > div {
  width: 570px !important;
}
</style>
<?php } ?>
<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>css/plugin.css" type="text/css" rel="stylesheet" />

<?php 
if(Params::getParam("edit")) {
	$edit = ModelBlog::newInstance()->getBlogById(Params::getParam("edit"));
	foreach($edit as $edit) { 
		$id       = $edit['id'];
		$status   = $edit['b_status'];
		$title    = $edit['b_title'];
		$slug     = $edit['b_slug'];
		$ecategory = $edit['bc_id'];
		$description = $edit['b_content'];
		$metaTitle = $edit['b_meta_title'];
		$metaContent = $edit['b_meta_content'];
		$metaKeyword = $edit['b_meta_keyword'];
	}

    if ($status == '0'){ 
    	$private = 'checked="checked"';
		$public = "";
    } else if ($status == '' || $status == '1') {
    	$public = 'checked="checked"';
		$private = "";
    } 
		
	
} else {
		$id       = "";
		$status   = "";
		$title    = "";
		$slug     = "";
		$ecategory = "";
		$description = "";
		$metaTitle = "";
		$metaContent = "";
		$metaKeyword = "";
		$private = "";
		$public = "1";
}

?>

<div class="plugin-header clearfix">
    <?php if(Params::getParam("edit")) {?>
        <h1 class="float_left"><?php _e('Edit Post', 'blog'); ?></h1>
    <?php }else { $public = 'checked="checked"'; ?>
        <h1 class="float_left"><?php _e('Add Post', 'blog'); ?></h1>
    <?php } ?>
    <a class="float_right" href="https://market.osclass.org/user/profile/15" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php');?>"><?php _e('All Posts', 'blog'); ?></a></li>
        <li <?php if(!Params::getParam("edit")) {?> class="active"<?php } ?>><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/post.php');?>"><?php _e('Add New', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/categories.php');?>"><?php _e('Categories', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'blog'); ?></a></li>
    </ul>
</div>

<div class="plugin-content">
	<form name="blog_post" id="blog_cpost" action="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/index.php');?>" method="post" >
    <input type="hidden" name="section" value="blog" />
       <?php if(Params::getParam("edit")) {?>
        <input type="hidden" name="plugin_action" value="post_edit" />
        <input type="hidden" id="id" name="id" value="<?php echo osc_esc_html($id); ?>"/>
        <input type="hidden"  value="<?php echo osc_esc_html($slug) ;?>" name="b_slug" id="b_slug">
        <?php } else { ?>
        <input type="hidden" id="id" name="id" value="<?php //echo osc_esc_html($id); ?>"/>
        <input type="hidden" name="plugin_action" value="post_add" />
        <?php } ?>   
       
    <div class="form-horizontal">
    <div class="leftSide">
        <div class="form-row">
            <div class="form-label"><?php _e('Title', 'blog'); ?></div>
            <div class="form-controls">
                <input type="text" placeholder="<?php echo osc_esc_html(__('Enter title here', 'blog'));?>" value="<?php echo osc_esc_html($title);?>" name="b_title" id="b_title">
            </div>
         </div>
        <?php if(!Params::getParam("edit")) {?>
        <div class="form-row">
            <div class="form-label"><?php _e('Slug', 'blog'); ?></div>
            <div class="form-controls">
                <input type="text"  value="<?php  echo osc_esc_html($slug) ; ?>" name="b_slug" id="b_slug">
                <br /><small><?php _e("You can't edit this slug after posting", "blog"); ?></small>
            </div>
        </div>
        <?php } ?>
    
        <div class="form-row">
            <div class="form-label"><?php _e('Category', 'blog'); ?></div>
            <div class="form-controls">
                <div class="select-box undefined">
                    <select id="b_category" name="b_category">
                        <?php $category = ModelBlog::newInstance()->getCategories();
                        foreach($category as $cat) { ?>
                        <option <?php  if($ecategory == $cat['id']){ echo'selected="selected"';} ?> value="<?php echo osc_esc_html($cat['bc_id']); ?>"><?php echo $cat['bc_title']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
         </div>
         
          
        <div class="form-row">
            <div class="form-label"><?php _e('Description', 'blog'); ?></div>
            <div class="form-controls desc">
                <textarea rows="10" class="b_description" id="b_description" name="b_description"><?php  echo $description; ?></textarea>
           
            </div>
        </div>
        
      
       </div><!--leftSide-->
        <div class="rightSide">
        <div class="form-row">
            <div class=""><strong><?php _e('Status', 'blog'); ?></strong></div>
            <div class="">
                <div class="select-box undefined">
                    <?php _e('Publish Now','blog'); ?> <input <?php  echo $public; ?> type="radio" name="b_status" value="1" />
                    <?php _e('Keep Private','blog'); ?> <input <?php  echo $private;?> type="radio" name="b_status" value="0" />
                       
                </div>
            </div>
         </div>
                 
        <div class="form-row">
            <div class=""><?php _e('Meta Title', 'blog'); ?></div>
            <div class="">
                <input type="text" value="<?php  echo $metaTitle; ?>" name="b_meta_title" id="b_meta_title">
            </div>
         </div>
         
         <div class="form-row">
            <div class=""><?php _e('Meta Description', 'blog'); ?></div>
            <div class="">
            <textarea id="b_meta_content" name="b_meta_content"><?php  echo $metaContent; ?></textarea>
               
            </div>
         </div>
         
         <div class="form-row">
            <div class=""><?php _e('Meta Keyword', 'blog'); ?></div>
            <div class="">
               <textarea name="b_meta_keyword" id="b_meta_keyword"><?php  echo $metaKeyword;?></textarea>
            </div>
         </div>
         
               
        </div>
    	<div class="clear"></div>
        
      
        
        
        <div class="form-row">
            <div class="form-controls">
                <button class="btn btn-blue" type="submit"><?php _e('Publish', 'blog'); ?></button>
            </div>
        </div>
     </div>
    </form>
</div>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Blog Osclass Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>