<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>css/plugin.css" type="text/css" rel="stylesheet" />

<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('All Posts', 'blog'); ?></h1>
    <!-- <a class="float_right" href="https://market.osclass.org/user/profile/15" target="_blank"><img src="?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a> -->
</div>

<div class="sub-header clearfix">
    <ul>
        <li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php');?>"><?php _e('All Posts', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/post.php');?>"><?php _e('Add New', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/categories.php');?>"><?php _e('Categories', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'blog'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'blog'); ?></a></li>
    </ul>
</div>
    
<?php
	$page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
	$cat = Params::getParam('catId');
	$query = Params::getParam('query');
	$year = Params::getParam('year');
	$month = Params::getParam('month');
	$blogs = ModelBlog::newInstance()->getBlogsbyArg($page, $cat, osc_esc_html($query), $year, $month, 0);
	
$del = Params::getParam("deleteItem");
if(Params::getParam("deleteItem") != "") {
	ModelBlog::newInstance()->deleteBlog($del);
	osc_add_flash_ok_message(__('Post deleted', 'blog'), 'admin');
    osc_redirect_to(osc_admin_render_plugin_url('osclass_blog/admin/blog.php'));
}
?>
<div class="plugin-content">
	<div class="post-head clearfix">
    	<div class="post-search">
            <form action="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php');?>" method="post">
                <input name="query" type="text" class="input-text input-actions input-has-select " placeholder="<?php echo osc_esc_html(__('Search', 'blog')); ?>"  value="<?php echo osc_esc_html($query);?>"/>
                <button type="submit" class="btn"><?php _e('Search', 'blog'); ?></button>
            </form>
        </div>
    </div>

<table class="table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th><?php _e('Title', 'blog'); ?></th>
            <th><?php _e('Action', 'blog'); ?></th>
            <th><?php _e('Category', 'blog'); ?></th>
            <th><?php _e('Date', 'blog'); ?></th>
            <th><?php _e('Status', 'blog'); ?></th>
           
		</tr>
	</thead>
	<tbody>
 	<?php 

	if( count($blogs) > 0 ) { 
		foreach($blogs as $blog) { ?>
			<tr class="<?php echo ($blog['b_status'] =="0" ? "private": "public");?>">
			<td class="title">
			<p><?php echo $blog['b_title'];?></p>
            </td>
            <td>
            <p class="option"><a target="_blank" href="<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>"><?php _e('View', 'blog'); ?></a> | <a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/post.php');?>&edit=<?php echo $blog['id'];?>"><?php _e('Edit', 'blog'); ?></a> | 
	<a onclick="return confirm('<?php echo osc_esc_js(__('Are you sure you want to continue?', 'blog')); ?>');" href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/index.php');?>&plugin_action=post_delete&bId=<?php echo $blog['id']?>"><?php _e('Delete', 'blog'); ?></a></p>
            </td>
			<td><?php echo $blog['b_category'];?></td>
			<td>
             <?php $mysqldate = $blog['b_date'];
                    $phpdate = strtotime( $mysqldate );
                    echo date('d M Y', $phpdate); ?>
            </td>
            <td>
            	<?php echo ($blog['b_status'] =="0" ? __("Private", "blog"): __("Public", "blog"));?>
            </td>
			</tr>
		<?php } 
		} else {?>
		<tr><td><?php _e('Sorry, no post found!', 'blog'); ?></td></tr>
		<?php } ?>
   
	</tbody>
  </table>
  <br />
  <div class="has-pagination">
    <?php 
    $tbl_name = ModelBlog::newInstance()->getTable_Blog();
    $count = new DAO();
    $count->dao->select('COUNT(*) AS count');
    $count->dao->from($tbl_name);
    
    $result = $count->dao->get()->row();
    $total_records = $result['count'];
    $total_pages = ceil($total_records / TOTALITEMS); 
          
    for ($i=1; $i<=$total_pages; $i++) { 
                echo "<a href='". osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/blog.php')."&iPage=".$i."'>".$i."</a> "; 
    }; 
    ?>
    
    </div>
</div>
<style>
    .plugin-header {
        background: #f1f1f1;
    }
</style>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Blog Osclass Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>