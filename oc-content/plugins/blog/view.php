<script src="https://unpkg.com/scrollreveal"></script>
<?php require_once 'ModelBlog.php';?>
<div <?php if (blog_responsive_mode() != '0'){ ?> class="row"<?php } ?> id="blog">
	<div class="col-md-9 col-xs-12 ">
		<h1 class="title-category h2-bottom-line">Категория</h1>
    	<div class="main-entry">
        	<?php  if (!Params::getParam('blogId')) {
			$page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
			$query = Params::getParam('query');
			$year = Params::getParam('year');
			$month = Params::getParam('month');
			$cat = Params::getParam('catId');
			$cat = ModelBlog::newInstance()->getCategoryBySlug($cat);
			$blogs = ModelBlog::newInstance()->getBlogsbyArg($page, $cat['bc_id'], osc_esc_html($query), $year, $month, '1');
			if( count($blogs) > 0 ) { 
				foreach($blogs as $blog) { ?>
                
					<div id="post-<?php echo $blog['id'];?>" class="post">
						<h3 class="entry-title"><a href="<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>"><?php echo $blog['b_title'];?></a></h3>
						<div class="entry-meta">
                            <span class="entry-date"><i class="fa fa-clock-o fa-lg"></i> 
                            <?php  $mysqldate = $blog['b_date'];
                    		$phpdate = strtotime( $mysqldate );
                    		$date = date('d', $phpdate);
                    		$month = date('F', $phpdate);
                    		$year =  date('Y', $phpdate); 
                    		echo $date.' '.__($month, 'blog').' '.$year;
                    		?>
                            </span>
                             
                            <span><i class="fa fa-folder-o fa-lg"></i> <a href="<?php echo osc_route_url('sc_blog_cat', array('catId' => $blog['bc_slug'])) ;?>"><?php echo $blog['bc_title'];?></a></span>
                        </div>
						<div id="main-entry" class="entry-content">
                        <?php if (blog_show_thumb()){?>
						<?php $texthtml = $blog['b_content'];
						 preg_match('/<img.+src=[\'"](?P<src>.+)[\'"].*>/i', $texthtml, $image);
						  if (isset($image['src'])){?>
                         <div class="entry-thumb">
							<img src="<?php echo $image['src'];?>" alt="<?php echo osc_esc_html($blog['b_title']); ?>" />
                         </div>
                        <?php } ?>
                        <?php } ?>
						<?php echo osc_highlight( $blog['b_content'] ,250) ; ?></div>
						<div class="entry-utility">
							<a  class="read_more" href="<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>"><?php _e('ПОДРОБНЕЕ', 'blog'); ?> &rarr;</a>
						</div>
					</div>
			<?php } ?>  
			<?php 
				$tbl_name = ModelBlog::newInstance()->getTable_Blog();
				$count = new DAO();
				$count->dao->select('COUNT(*) AS count');
				$count->dao->from($tbl_name);
				if(Params::getParam('catId')){
					$count->dao->where('b_category', Params::getParam('catId') );
				}
				if(Params::getParam('query')){
					$count->dao->where('b_content', Params::getParam('query') );
				}
				$result = $count->dao->get()->row();
				$total_records = $result['count'];
				$page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
				$pages_count = ceil($total_records / blog_list_count());
				$is_first = $page == 1;
				$is_last  = $page == $pages_count;
				// Prev cannot be less than one
				$prev = max(1, $page - 1);
				// Next cannot be larger than $pages_count
				$next = min($pages_count , $page + 1); ?>
				<div id="nav-below" class="navigation clearfix">
					<?php if($pages_count > 0) {
						if(Params::getParam('catId')){
							if(!$is_last) { ?>
								<div class="nav-next float_left">
									<a href="<?php echo osc_route_url('sc_blog_page_multi', array('iPage' => $next, 'catId' => Params::getParam('catId')));?>">&larr; <?php _e('Older posts', 'blog');?></a>
								</div>
							<?php } ?>
							<?php if(!$is_first) {?>
								<div class="nav-previous float_right">
									<a href="<?php echo osc_route_url('sc_blog_page_multi', array('iPage' => $prev, 'catId' => Params::getParam('catId')));?>"><?php _e('Newer posts', 'blog');?> &rarr;</a>
								</div>';
							<?php } ?>
						<?php } else {?>
							<?php if(!$is_last) { ?>
								<div class="nav-next float_left">
									<a  href="<?php echo osc_route_url('sc_blog_page', array('iPage' => $next));?>">&larr; <?php _e('Older posts', 'blog');?></a>
								 </div>
							<?php }?>
							<?php if(!$is_first) { ?>
								<div class="nav-previous float_right">
									<a href="<?php echo osc_route_url('sc_blog_page', array('iPage' => $prev));?>"><?php _e('Newer posts', 'blog');?> &rarr;</a>
								</div>
							<?php }?>
						<?php }?>
					<?php }?>
				</div>
			<?php } else { ?>
				<div  class="post">
					<?php _e('Sorry, no post found!', 'blog'); ?>
				</div>
			<?php } ?>
		<?php } else {?>
        	<div class="bckblog nav-previous clearfix">
				<a href="<?php echo osc_route_url('sc_blog', array('blogId' => "")) ;?>">&larr;  <?php _e('Back to blog', 'blog'); ?></a>
                
            </div>
			<div class="clear"><br /></div>
			<?php $blogs = ModelBlog::newInstance()->getBlogBySlug(Params::getParam('blogId'));
				if( count($blogs) > 0 ) { 
					foreach($blogs as $blog) { ?>

						<div id="post-<?php echo $blog['id'];?>" class="post">
							<h3 class="entry-title"><?php echo $blog['b_title'];?></h3>
							<div class="entry-meta">
								<span class="entry-date"><i class="fa fa-clock-o fa-lg"></i> 
                                <?php  $mysqldate = $blog['b_date'];
	                    		$phpdate = strtotime( $mysqldate );
	                    		$date = date('d', $phpdate);
	                    		$month = date('F', $phpdate);
	                    		$year =  date('Y', $phpdate); 
	                    		echo $date.' '.__($month, 'blog').' '.$year;
	                    		?>
                                </span>
                                 
                                <span><i class="fa fa-folder-o fa-lg"></i> <a href="<?php echo osc_route_url('sc_blog_cat', array('catId' => $blog['bc_slug'])) ;?>"><?php echo $blog['bc_title'];?></a></span>
                            </div>
							<div class="entry-content"><?php echo $blog['b_content'];?></div>
							<div class="entry-utility">
                            	<!-- <div class="social-share">
                            	<?php _e('Share', 'blog'); ?>:
                                <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>'), 'facebook-share-dialog', 'height=279, width=575'); return false;"><i class="fa fa-facebook"></i> Facebook</a>
                                <a href="https://twitter.com/intent/tweet?text=<?php echo $blog['b_title'];?>&url=<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>"><i class="fa fa-twitter"></i> Twitter</a>
                                <a onclick="javascript:window.open(this.href, '','menubar=no,toolbar=no,height=600,width=600');return false;" href="https://plus.google.com/share?url=<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>"><i class="fa fa-google-plus"></i> Google+</a>
                                </div> -->
							</div>
                            
                            <div class="comments">
                            	<?php osc_run_hook("blog_comment"); ?>
                            </div>
                            
						</div>
					<?php } ?>
				<?php } else { ?>
				<div  class="post">
					<?php _e('Sorry, no post found!', 'blog'); ?>
				</div>
			<?php } ?>
		<?php } ?>
        </div>
    </div><!-- / Main Entry -->
    <div class="col-md-3 col-xs-12">
    	<div id="sidebar">
			<?php if (blog_show_search()){?>
         	<div class="widget-box">
                <div class="box blog-search">
                    <h3><?php _e('Искать', 'blog'); ?></h3>
                    <form action="<?php echo osc_route_url('sc_blog', array('blogId' => '')) ;?>" method="post" >
                    <input type="text" name="query" value="<?php echo osc_esc_html(Params::getParam('query')); ?>" />
                    <button class='go' type="submit"><?php _e('ПОИСК', 'blog'); ?></button>
                    </form>
                </div>
            </div>
           	<?php } ?>
			<?php if (blog_show_archives()){?>
                <div class="widget-box">
                    <div class="box blog-archive">
                        <h3><?php _e('Archives', 'blog'); ?></h3>
                        <?php include 'archives.php'; ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (blog_show_categories()){?>
                <div class="widget-box">
                    <div class="box">
                        <h3><?php _e('Categories', 'blog'); ?></h3>
                        <ul>
                        <?php $category = ModelBlog::newInstance()->getCategories();
                            foreach($category as $cat) { ?>
                                <li><a href="<?php echo osc_route_url('sc_blog_cat', array('catId' => $cat['bc_slug'])) ;?>"><?php echo $cat['bc_title'];?></a></li>
                        <?php } ?>
                        </ul>
                    </div>
            	</div>
            <?php } ?>
            
            <?php if (blog_show_latest()){?>
            	<?php echo recent_blog_post() ; ?>
            <?php } ?>
        </div>
        <style>
        	.go	{
        		margin-top: 25px;
        		margin-left: 8.5%;
        	}
        	.post, .widget-box {
        		box-shadow: 0 0 16px rgba(109, 109, 109, 0.25);
        		border: none!important; 
        		border-radius: 0px!important;
        	}
        	.read_more {
        		min-width: 200px;
			    height: 45px;
			    border-radius: 4px;
			    border: none;
			    color: #fff;
			    font-weight: 700;
			    font-size: 14px;
			    transition: all 250ms;
			    background-color: #1531AE!important;
			    padding-top: 12px;
			    text-align: center;
        	}
        	.read_more:hover { 
        		background-color: #6BE400!important;
        		color: #fff!important;
        	}
        	.read_more:hover a {
        		color: #fff!important;
        	}
        	.title-category {
				font-weight: 400;
				color: #444343;
				text-transform: uppercase;
        	}
        	@media screen and (max-width: 1200px) {
        		.go	{
	        		min-width: 100px;
	        		margin-left: 24.5%;
	        	}
        	}
        	@media screen and (max-width: 1000px) {
        		.go	{
	        		margin-left: 11.5%;
	        	}
        	}
        	@media screen and (max-width: 767px) {
        		.go	{
	        		min-width: 200px;
	        		margin-left: 0%;
	        	}
        	}
        </style>
        <script>
        	ScrollReveal().reveal(".post", { distance: '50px', duration: 1000 } );
        </script>
    </div><!-- / Sidebar -->
</div>
