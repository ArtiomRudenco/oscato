��    T      �  q   \            !     '     .     6     G  	   P     Z  "   c     �     �     �     �  =  �  	   �     �     	     	  
   3	     >	     G	     e	     �	  .   �	  	   �	     �	     �	     �	     �	     �	  	   
     
     
     &
     )
     .
      :
     [
     h
     p
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
          -     5     <     D     P     Y     l     y     �     �     �     �     �     �     �     �     �      �               /     E     L     T     \     b     s  �   z  l        �     �     �  &   �  "  �     �     �     �          0     ?     U     ^     ~     �     �     �  �  �     8     D     ^  .   v     �  
   �     �  "   �  $   �  2   $     W     g  	   l     v     �     �     �     �     �     �     �     �  )   �          "     *     ;  
   K     V     Z     q     z     �     �     �     �      �                     (     ;     I     _     n     �     �  	   �     �     �     �     �     �     �  5        H     M      `     �     �     �     �     �     �  �   �  �   �                 6   /        9      "      D   :   >   %   R   N   P   1                  -             !              S   F      5   /                         6   (       G   <          '   B   *   Q           L   )       .   O                                 2       0          +   @   M   7   &      =   C   H   	   $   K          4   A               I   3         J   E   ?           8   ,       ;   
             T   #                        About Action Add New Add New Category Add Post All Posts Archives Are you sure you want to continue? Author: Back to blog Blog Blog Editor Blog Osclass plugin to create a beautiful responsive Blog or News section for osclass classified website. Blog plugin has admin dashboard to manage blog, category and settings of blog. Its also have Social share integration with Facebook, Twitter, Google and Recent post widget to use anywhere within osclass website. Blog Path Blog page link Blog posts count Blog settings has been updated Categories Category Category created successfully Category deleted successfully Category updated successfully Check this if you're using non-bootstrap theme Dashboard Date Delete Description Edit Edit Category Edit Post Email Enter title here Go Help How to use? It's work with permalink enabled Keep Private Listing Meta Description Meta Keyword Meta Title Name Newer posts Niceedit No posts available Older posts Post created successfully Post deleted Post deleted successfully Post updated successfully Private Public Publish Publish Now Readmore Recent post widget Recent posts Recent posts count Save Save changes Search Settings Share Show Archives Show Blog search Show Categories Show Recent posts Show thumbnail on blog list page Slug Slug already exists Sorry, no post found! Status Support Tinymce Title Title is missing Update Use following code to display Blog page link on your website front-end, Copy and paste the code into any theme files such as header.php, sidebar.php and footer.php: Use following code to display Blog recent posts on your website wherever you need to show Blog recent posts. Version: View What is Blog? You can't edit this slug after posting Project-Id-Version: Blog Osclass Plugin
POT-Creation-Date: 2017-02-23 17:29+0100
PO-Revision-Date: 2017-02-23 17:37+0100
Last-Translator: 
Language-Team: DrizzleThemes <support@drizzlethemes.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SearchPath-0: archives.php
X-Poedit-SearchPath-1: index.php
X-Poedit-SearchPath-2: ModelBlog.php
X-Poedit-SearchPath-3: view.php
X-Poedit-SearchPath-4: admin/blog.php
X-Poedit-SearchPath-5: admin/categories.php
X-Poedit-SearchPath-6: admin/help.php
X-Poedit-SearchPath-7: admin/index.php
X-Poedit-SearchPath-8: admin/post.php
X-Poedit-SearchPath-9: admin/settings.php
 Sur Action Ajouter un nouveau Ajouter une nouvelle catégorie Ajouter Poster Tous les publications Archives Es-tu sur de vouloir continuer? Auteur: Retour au blog Blog Blog éditeur Blog Osclass plugin pour créer un beau Blog réactif ou la section Nouvelles pour le site classé osclass. Blog plugin dispose d'un tableau de bord d'administration pour gérer le blog, la catégorie et les paramètres du blog. Il a également une intégration de partage social avec Facebook, Twitter, Google et un widget de publication récente à utiliser n'importe où dans le site Web osclass. Blog Chemin Lien vers la page du blog Nombre de liste de Blog Les paramètres du blog ont été mis à jour. Catégories Catégorie Catégorie créée avec succès Catégorie supprimée avec succès Catégorie mise à jour avec succès Vérifiez ce thème non-bootstrap si vous utilisez Tableau de bord Date Supprimer Description Modifier Modifier une catégorie Modifier poste E-mail Entrez le titre ici Aller Aide Comment utiliser? Il est un travail avec permaliens activé Garder Privé Annonce Meta Description Meta Mots-clés Meta Titre Nom Articles plus récents Niceedit Pas de postes disponibles Articles plus anciens Poste créé avec succès Poste supprimé Poste supprimé avec succès Message mis à jour avec succès Privé Public Publier Publier maintenant Lire la suite Derniers posts widget Posts récents Posts récents comptent Sauvegarder Enregistrer Recherche Paramètres Partager Voir Archives Voir le blog Recherche Afficher catégories Voir les derniers articles Afficher la vignette sur la page de la liste de blogs Slug Slug existe déjà Désolé, aucun article trouvé! Statut Soutien Tinymce Titre Titre manquant Mettre à jour Utilisez le code suivant pour afficher le lien de la page du blog sur votre site Web, Copiez et collez le code dans n'importe quel fichier de thème tel que header.php, sidebar.php et footer.php: Utilisez le code suivant pour afficher les articles récents sur votre site Web où que vous ayez besoin pour afficher les articles récents. Version: Vue Qu'est-ce que Blog? Vous ne pouvez pas modifier ce slug après l'affichage 