��    T      �  q   \            !     '     .     6     G  	   P     Z  "   c     �     �     �     �  =  �  	   �     �     	     	  
   3	     >	     G	     e	     �	  .   �	  	   �	     �	     �	     �	     �	     �	  	   
     
     
     &
     )
     .
      :
     [
     h
     p
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
          -     5     <     D     P     Y     l     y     �     �     �     �     �     �     �     �     �      �               /     E     L     T     \     b     s  �   z  l        �     �     �  &   �  #  �  	   �     �               +     @     S  '   [     �     �     �     �  �  �     ?     O     l  ,   �     �  
   �     �  "   �  !     ?   1     q     y          �     �     �     �     �     �     �     �     �  $   �     !     2     :     L     `     m     t     �      �     �  !   �     �       !   .     P     X     a     j  	   x     �     �     �     �     �     �     �  	         
          9     M  4   m     �     �  +   �     �     �     �     �       
     �     �   �  	   �     �     �  1   �        9      "      D   :   >   %   R   N   P   1                  -             !              S   F      5   /                         6   (       G   <          '   B   *   Q           L   )       .   O                                 2       0          +   @   M   7   &      =   C   H   	   $   K          4   A               I   3         J   E   ?           8   ,       ;   
             T   #                        About Action Add New Add New Category Add Post All Posts Archives Are you sure you want to continue? Author: Back to blog Blog Blog Editor Blog Osclass plugin to create a beautiful responsive Blog or News section for osclass classified website. Blog plugin has admin dashboard to manage blog, category and settings of blog. Its also have Social share integration with Facebook, Twitter, Google and Recent post widget to use anywhere within osclass website. Blog Path Blog page link Blog posts count Blog settings has been updated Categories Category Category created successfully Category deleted successfully Category updated successfully Check this if you're using non-bootstrap theme Dashboard Date Delete Description Edit Edit Category Edit Post Email Enter title here Go Help How to use? It's work with permalink enabled Keep Private Listing Meta Description Meta Keyword Meta Title Name Newer posts Niceedit No posts available Older posts Post created successfully Post deleted Post deleted successfully Post updated successfully Private Public Publish Publish Now Readmore Recent post widget Recent posts Recent posts count Save Save changes Search Settings Share Show Archives Show Blog search Show Categories Show Recent posts Show thumbnail on blog list page Slug Slug already exists Sorry, no post found! Status Support Tinymce Title Title is missing Update Use following code to display Blog page link on your website front-end, Copy and paste the code into any theme files such as header.php, sidebar.php and footer.php: Use following code to display Blog recent posts on your website wherever you need to show Blog recent posts. Version: View What is Blog? You can't edit this slug after posting Project-Id-Version: Blog Osclass Plugin
POT-Creation-Date: 2017-02-23 17:26+0100
PO-Revision-Date: 2017-02-23 17:29+0100
Last-Translator: 
Language-Team: DrizzleThemes <support@drizzlethemes.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SearchPath-0: archives.php
X-Poedit-SearchPath-1: index.php
X-Poedit-SearchPath-2: ModelBlog.php
X-Poedit-SearchPath-3: view.php
X-Poedit-SearchPath-4: admin/blog.php
X-Poedit-SearchPath-5: admin/categories.php
X-Poedit-SearchPath-6: admin/help.php
X-Poedit-SearchPath-7: admin/index.php
X-Poedit-SearchPath-8: admin/post.php
X-Poedit-SearchPath-9: admin/settings.php
 Acerca de Acción Añadir nuevo Añadir nueva categoria Añadir publicación Todas las entradas Archivo Estás seguro de que quieres continuar? Autor Volver al blog Blog Editor de blogs Blog Osclass plugin para crear una hermosa blog sensible o sección de noticias para osclass clasificado sitio web. Plugin de blog tiene un panel de administración para administrar el blog, la categoría y la configuración del blog. Su también tienen integración social de la parte con Facebook, gorjeo, Google y widget reciente del poste para utilizar dondequiera dentro del Web site del osclass. Camino del blog Enlace a la página del blog Cuota de artículos de blog Se ha actualizado la configuración del blog Categorías Categoría Categoría creada correctamente Categoría eliminada correctamente Categoría actualizada con éxito Compruebe esto si está utilizando un tema que no sea de inicio Tablero Fecha Eliminar Descripción Editar Editar categoría Editar publicación Email Ingrese el título aquí Ir Ayuda ¿Cómo utilizar? Su trabajo con permalinks habilitado Mantener privado Listado Meta descripción Meta palabras clave Meta título Nombre Publicaciones mas recientes Niceedit No hay publicaciones disponibles Las entradas más antiguas Publicación creada correctamente Publicación eliminada Mensaje eliminado correctamente Publicación creada correctamente Privado Público Publicar Publica ahora Leer más Widget de publicación reciente Publicaciones mas recientes Cuota de mensajes recientes Salvar Guardar cambios Buscar Ajustes Compartir Mostrar Archivos Mostrar la búsqueda de blogs Mostrar categorías Mostrar publicaciones recientes Mostrar miniatura en la página de la lista de blogs Babosa Slug ya existe Lo sentimos, no se han encontrado entradas! Estado Apoyo Tinymce Título Falta el título Actualizar Utilice el siguiente código para mostrar el enlace de la página del blog en su front-end del sitio web. Copie y pegue el código en cualquier archivo de tema, como header.php, sidebar.php y footer.php: Utilice el siguiente código para mostrar las publicaciones recientes de Blog en su sitio web donde quiera que necesite para mostrar las publicaciones recientes de Blog. Versión: Ver ¿Qué es Blog? No se puede editar este slug después de publicar 