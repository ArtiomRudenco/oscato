<?php
class ModelBlog extends DAO
    {
        private static $instance ;
		public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

        /**
         * Construct
         */
        function __construct()
        {
            parent::__construct();
        }
        
        
        public function getTable_Blog()
        {
            return DB_TABLE_PREFIX.'t_blog';
        }
        
        
        public function getTable_BlogCategory()
        {
            return DB_TABLE_PREFIX.'t_blog_categories';
        }
        
      
        public function import($file)
        {
            $path = osc_plugin_resource($file) ;
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( "Error importSQL::ModelBlog<br>".$file ) ;
            }
        }
                
        /**
         * Remove data and tables related to the plugin.
         */
        public function uninstall()
        {
            $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_Blog()) ) ;
            $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_BlogCategory()) ) ;
      
        }
        
       /**
         * Get blogs by page for admin.
         */
        public function recentBlogs()
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_Blog() ) ;
			$this->dao->where('b_status', 1 );
			$this->dao->limit(blog_latest_count() ) ;
			$this->dao->orderBy('id', 'DESC') ;
			

            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		/**
         * Checking slug if already exist.
         */
		public function checkSlug($slug)
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_Blog() ) ;
			$this->dao->where('b_slug', $slug );
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		
		public function checkCatSlug($slug)
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_BlogCategory() ) ;
			$this->dao->where('bc_slug', $slug );
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}

		
		
		/**
         * Get blogs by page and category.
         */
		public function getBlogsbyArg($page, $cat, $query, $year, $month, $status)
        {
			
			$items_per_page = blog_list_count();
			$offset = ($page - 1) * $items_per_page;

            $this->dao->select();
            $this->dao->from( $this->getTable_Blog() ) ;
			$this->dao->join($this->getTable_BlogCategory().' bc', 'b_category = bc.bc_id', 'INNER');
			$this->dao->limit($offset, $items_per_page );
			if($status =="1"){
			$this->dao->where('b_status', $status );
			}
			if(Params::getParam('catId')){
			$this->dao->where('bc.bc_id', $cat );
			}
			if(Params::getParam('year')){
				$this->dao->where('b_date >=', $year.'-01-01');
				$this->dao->where('b_date <=', $year.'-12-31');
				if(Params::getParam('month')){
					$this->dao->where('b_date >=', $year.'-'.$month.'-01');
					$this->dao->where('b_date <=', $year.'-'.$month.'-31');
				}
			}
			if(Params::getParam('query')){
				$keys = explode(" ",$query);
                foreach($keys as $k){
				$this->dao->like('b_content', $k);
				$this->dao->orLike('b_title', $k);
			
				}
				
            }
            $this->dao->orderBy('id', 'DESC') ;
			

            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
			
		
        }
		
		
		
		public function getBlogById( $id )
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_Blog());
			$this->dao->join($this->getTable_BlogCategory().' bc', 'b_category = bc.bc_id', 'INNER');
            $this->dao->where('id', $id );
            
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }

            return $result->result();
        }
		
		 public function getBlogBySlug( $slug )
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_Blog());
			$this->dao->join($this->getTable_BlogCategory().' bc', 'b_category = bc.bc_id', 'INNER');
            $this->dao->where('b_slug', $slug );
            
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }

            return $result->result();
        }
		
		
		
		
		public function getCategories()
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_BlogCategory() ) ;
            $this->dao->orderBy('bc_id', 'ASC') ;

            $results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
        }
		
		
		public function getCategoryById( $id )
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_Blogcategory());
            $this->dao->where('bc_id', $id );
            
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }

            return $result->result();
        }
		public function getCategoryBySlug( $slug )
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_Blogcategory());
            $this->dao->where('bc_slug', $slug );
            
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }

            return $result->row();
        }
        
       
        
        /**
         * Insert a Blog
         *
         * @param string $name 
         */
        public function insertBlog( $status, $title, $slug, $category, $description, $metaTitle, $metaContent, $metaKeyword )
        {
            return $this->dao->insert($this->getTable_Blog(), array('b_status' => $status, 'b_title' => $title, 'b_slug' => $slug, 'b_category' => $category, 'b_content' => $description, 'b_meta_title' => $metaTitle, 'b_meta_content' => $metaContent, 'b_meta_keyword' => $metaKeyword, 'b_date' => date("Y-m-d"))) ;
        }
        
		 public function insertCategory( $title, $slug, $description )
        {
            return $this->dao->insert($this->getTable_Blogcategory(), array('bc_title' => $title, 'bc_slug' => $slug, 'bc_description' => $description)) ;
        }
       
       
        
        /**
         * Update Blog
         *
         */
        
		public function updateBlog( $blogId, $status, $title, $slug, $category, $description, $metaTitle, $metaContent, $metaKeyword  ) {
			$aSet = array('b_status' => $status, 'b_title' => $title, 'b_slug' => $slug, 'b_category' => $category, 'b_content' => $description, 'b_meta_title' => $metaTitle, 'b_meta_content' => $metaContent, 'b_meta_keyword' => $metaKeyword);
			$aWhere = array('id' => $blogId);
		
			return $this->_update($this->getTable_Blog(), $aSet, $aWhere);
		  }
		
		public function updateCategory( $categoryId, $title, $slug, $description ) {
			$aSet = array('bc_title' => $title, 'bc_slug' => $slug, 'bc_description' => $description);
			$aWhere = array('bc_id' => $categoryId);
		
			return $this->_update($this->getTable_Blogcategory(), $aSet, $aWhere);
		  }
	
		/**
         * Delete blog
         * 
         */
        public function deleteBlog( $blogId )
        {
            return $this->dao->delete( $this->getTable_Blog(), array('id' => $blogId) ) ;
        }
		
		public function deleteCategory( $categoryId )
        {
            return $this->dao->delete( $this->getTable_Blogcategory(), array('bc_id' => $categoryId) ) ;
        }
        
       
        // update
        function _update($table, $values, $where)
        {
            $this->dao->from($table) ;
            $this->dao->set($values) ;
            $this->dao->where($where) ;
            return $this->dao->update() ;
        }
		
		
    }
	


?>