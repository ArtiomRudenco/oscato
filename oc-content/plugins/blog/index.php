<?php
/*
Plugin Name: Blog for Osclass
Plugin URI: http://www.drizzlethemes.com/
Description: Blog Osclass plugin to create a beautiful Blog or News section for your classified website.
Version: 1.0.7
Author: DrizzleThemes
Author URI: http://www.drizzlethemes.com/
Short name: blog
Plugin update URI: blog-for-osclass_2
*/

require_once 'ModelBlog.php';

define('TOTALITEMS', '12');
define('VERSION', '1.0.7');
/* Install Plugin */
function oscBlog_install() {
	osc_set_preference('blog_path', 'blog', 'blog', 'STRING');
	osc_set_preference('blog_editor', 'tinymce', 'blog', 'INTEGER');
  	osc_set_preference('blog_latest_count', '5', 'blog', 'INTEGER');
  	osc_set_preference('blog_list_count', '10', 'blog', 'INTEGER');
	osc_set_preference('blog_show_thumb', 'true', 'blog', 'STRING');
  	osc_set_preference('blog_show_archives', 'true', 'blog', 'STRING');
 	osc_set_preference('blog_show_categories', 'false', 'blog', 'STRING');
  	osc_set_preference('blog_show_search', 'true', 'blog', 'STRING');
  	osc_set_preference('blog_show_latest', 'true', 'blog', 'STRING');
  	osc_set_preference('blog_responsive_mode', 'true', 'blog', 'STRING');
	ModelBlog::newInstance()->import('blog/struct.sql');
}

/* Uninstall Plugin */
function oscBlog_uninstall() {
	osc_delete_preference('blog_path', 'blog');
	osc_delete_preference('blog_editor', 'blog');
  	osc_delete_preference('blog_latest_count', 'blog');
  	osc_delete_preference('blog_list_count', 'blog');
	osc_delete_preference('blog_show_thumb', 'blog');
  	osc_delete_preference('blog_show_archives', 'blog');
  	osc_delete_preference('blog_show_categories', 'blog');
  	osc_delete_preference('blog_show_search', 'blog');
  	osc_delete_preference('blog_show_latest', 'blog');
  	osc_delete_preference('blog_responsive_mode', 'blog');
	ModelBlog::newInstance()->uninstall();
}

function blog_actions() {
  $dao_preference = new Preference();
  $option = Params::getParam('blogoption');

  if (Params::getParam('file') != 'blog/admin/settings.php') {
    return '';
  }

  if ($option == 'blogsettings') {
    osc_set_preference('blog_path', Params::getParam("blog_path") ? Params::getParam("blog_path") : '0', 'blog', 'STRING');
	osc_set_preference('blog_editor', Params::getParam("blog_editor") ? Params::getParam("blog_editor") : '0', 'blog', 'STRING');
    osc_set_preference('blog_latest_count', Params::getParam("blog_latest_count") ? Params::getParam("blog_latest_count") : '0', 'blog', 'STRING');
    osc_set_preference('blog_list_count', Params::getParam("blog_list_count") ? Params::getParam("blog_list_count") : '0', 'blog', 'STRING');
    osc_set_preference('blog_show_archives', Params::getParam("blog_show_archives") ? Params::getParam("blog_show_archives") : '0', 'blog', 'STRING');
	osc_set_preference('blog_show_thumb', Params::getParam("blog_show_thumb") ? Params::getParam("blog_show_thumb") : '0', 'blog', 'STRING');
    osc_set_preference('blog_show_categories', Params::getParam("blog_show_categories") ? Params::getParam("blog_show_categories") : '0', 'blog', 'STRING');
    osc_set_preference('blog_show_search', Params::getParam("blog_show_search") ? Params::getParam("blog_show_search") : '0', 'blog', 'STRING');
	osc_set_preference('blog_show_latest', Params::getParam("blog_show_latest") ? Params::getParam("blog_show_latest") : '0', 'blog', 'STRING');
    osc_set_preference('blog_responsive_mode', Params::getParam("blog_responsive_mode") ? Params::getParam("blog_responsive_mode") : '0', 'blog', 'STRING');
	osc_run_hook('blog_settings_action');
    osc_add_flash_ok_message(__('Blog settings has been updated', 'blog'), 'admin');
    osc_redirect_to(osc_admin_render_plugin_url('blog/admin/settings.php'));
  }
}

// HELPER
function blog_path() {
  return(osc_get_preference('blog_path', 'blog'));
}
function blog_editor() {
  return(osc_get_preference('blog_editor', 'blog'));
}

function blog_latest_count() {
  return(osc_get_preference('blog_latest_count', 'blog'));
}

function blog_list_count() {
  return(osc_get_preference('blog_list_count', 'blog'));
}

function blog_show_archives() {
  return(osc_get_preference('blog_show_archives', 'blog'));
}
function blog_show_thumb() {
  return(osc_get_preference('blog_show_thumb', 'blog'));
}

function blog_show_categories() {
  return(osc_get_preference('blog_show_categories', 'blog'));
}

function blog_show_search() {
  return(osc_get_preference('blog_show_search', 'blog'));
}

function blog_show_latest() {
  return(osc_get_preference('blog_show_latest', 'blog'));
}

function blog_responsive_mode() {
  return(osc_get_preference('blog_responsive_mode', 'blog'));
}

function blog_get_url() {
	if ( osc_rewrite_enabled() ) {
 		return osc_base_url().''.blog_path();
	} else {
  		return osc_route_url('sc_blog');
	}
}
function blog_url() {
	echo '<a href="'.blog_get_url().'">'. __('Blog','blog'). '</a>';
}


function recent_blog_post() { ?>
<div class="widget-box">
    <div class="box">
    	<h3><?php _e('Recent posts', 'blog'); ?></h3>
    	<?php $blogs = ModelBlog::newInstance()->recentBlogs() ;
    	if( count($blogs) > 0 ) {  ?>
        <ul>
        <?php foreach($blogs as $blog) { ?>
        	<li><a href="<?php echo osc_route_url('sc_blog', array('blogId' => $blog['b_slug'])) ;?>"><?php echo $blog['b_title'];?></a></li>
        <?php } ?>
        </ul>
        <?php } else { _e('No posts available', 'blog'); } ?>
    </div>
</div>
<?php }

function blogslug($string){
	$table = array(
		'�'=>'S', '�'=>'s', '�'=>'Dj', 'd'=>'d', '�'=>'Z', '�'=>'z', 'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c',
		'�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E',
		'�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O',
		'�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss',
		'�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c', '�'=>'e', '�'=>'e',
		'�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o',
		'�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'y', '�'=>'b',
		'�'=>'y', 'R'=>'R', 'r'=>'r', '/'=> '-', ' ' => '-'
	);
	$blogslug = strtolower(strtr($string, $table));
	$blogslug = preg_replace('#[^0-9a-z]+#i', " ", $blogslug);
	$blogslug = str_replace(' ','-',trim($blogslug));
	return $blogslug;
}

function blog_meta_title_filter(){
		
		$blogs = ModelBlog::newInstance()->getBlogBySlug(Params::getParam('blogId'));
		foreach($blogs as $blog) { 
			$text = $blog['b_meta_title'];
			return $text;
			
		}
}

function blog_meta_content_filter(){
	
	$blogs = ModelBlog::newInstance()->getBlogBySlug(Params::getParam('blogId'));
	foreach($blogs as $blog) { 
		$text = $blog['b_meta_content'];
		return $text; 
	}
	
}
function blog_meta_keyword_filter(){
	$blogs = ModelBlog::newInstance()->getBlogBySlug(Params::getParam('blogId'));
	foreach($blogs as $blog) { 
		$text = $blog['b_meta_keyword'];
		return $text; 
	}
	
}

function style_admin_view() { 
	echo '<link type="text/css" rel="stylesheet" href="'.osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__).'assets/css/admin.css">';
}
function style_web_view() { 
	echo '<link type="text/css" rel="stylesheet" href="'.osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__).'assets/css/style.css">';
} 


osc_add_hook('init_admin', 'blog_actions');
osc_add_hook('admin_header','style_admin_view');
osc_add_hook('header','style_web_view');

include_once 'filters.php';

osc_add_hook(osc_plugin_path(__FILE__) . '_uninstall', 'oscBlog_uninstall') ;
osc_register_plugin(osc_plugin_path(__FILE__), 'oscBlog_install') ;

function blog_comment_menu(){
	osc_add_admin_submenu_page('osc_blog', __('Comments', 'blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/help.php'), 'Comments');
}
osc_add_hook('blog_main_menu', 'blog_comment_menu');

/* Blog Route */
osc_add_route('sc_blog_archive',blog_path().'/archive/?(.*)/(.*)?',blog_path().'/archive/{year}/{month}',osc_plugin_folder(__FILE__).'view.php','','custom','',blog_path());
osc_add_route('sc_blog_cat',blog_path().'/category/?(.*)/?',blog_path().'/category/{catId}',osc_plugin_folder(__FILE__).'view.php','','custom','',blog_path());
osc_add_route('sc_blog_page',blog_path().'/page/?(.*)/?',blog_path().'/page/{iPage}',osc_plugin_folder(__FILE__).'view.php','','custom','',blog_path());
osc_add_route('sc_blog',blog_path().'/?(.*)/?',blog_path().'/{blogId}',osc_plugin_folder(__FILE__).'view.php','','blog','blog-view',blog_path());
osc_add_route('sc_blog_page_multi',blog_path().'/page/?(.*)/(.*)?',blog_path().'/page/{iPage}/{catId}',osc_plugin_folder(__FILE__).'view.php','','custom','',blog_path());

/* Admin Menu */
osc_add_admin_menu_page(__('Dashboard', 'blog'),osc_admin_base_url(),'dash','moderator');
osc_add_admin_menu_page(__('Listing', 'blog'),osc_admin_base_url(true).'?page=items','items','moderator'); 
osc_add_admin_menu_page(__('Blog', 'blog'),osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/blog.php'),'osc_blog','moderator');
osc_add_admin_submenu_page('osc_blog', __('All Posts', 'blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/blog.php'), 'Blogs');
osc_add_admin_submenu_page('osc_blog', __('Add New', 'blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/post.php'), 'Blog_New');
osc_add_admin_submenu_page('osc_blog', __('Categories', 'blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/categories.php'), 'Blog_Categories');
osc_run_hook('blog_main_menu');
osc_add_admin_submenu_page('osc_blog', __('Settings', 'blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/settings.php'), 'Settings');
osc_add_admin_submenu_page('osc_blog', __('Help', 'blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/help.php'), 'Help');
?>