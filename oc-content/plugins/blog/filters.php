<?php
$location = Rewrite::newInstance()->get_location();
if ( osc_rewrite_enabled() ) {
	osc_add_filter('meta_title_filter', 'modified_meta_title_filter');
	osc_add_filter('meta_description_filter', 'modified_meta_description');
	osc_add_filter('meta_keywords_filter', 'modified_meta_keywords');
} else {
	if(Params::getParam('blogId')){
		osc_add_filter('meta_title_filter', 'blog_meta_title_filter');
		osc_add_filter('meta_description_filter', 'blog_meta_content_filter');
		osc_add_filter('meta_keywords_filter', 'blog_meta_keyword_filter');
	}
}

function modified_meta_keywords( ) {
	$location = Rewrite::newInstance()->get_location();
	$text = '';
	
	if( $location =="blog" ) {
		$text = blog_meta_keyword_filter();
	}
    // search
    if( osc_is_search_page() ) {
        if( osc_has_items() ) {
            $keywords = array();
            $keywords[] = osc_item_category();
            if( osc_item_city() != '' ) {
                $keywords[] = osc_item_city();
                $keywords[] = sprintf('%s %s', osc_item_category(), osc_item_city());
            }
            if( osc_item_region() != '' ) {
                $keywords[] = osc_item_region();
                $keywords[] = sprintf('%s %s', osc_item_category(), osc_item_region());
            }
            if( (osc_item_city() != '') && (osc_item_region() != '') ) {
                $keywords[] = sprintf('%s %s %s', osc_item_category(), osc_item_region(), osc_item_city());
                $keywords[] = sprintf('%s %s', osc_item_region(), osc_item_city());
            }
            $text = implode(', ', $keywords);
        }
        osc_reset_items();
    }
    // listing
    if( osc_is_ad_page() ) {
        $keywords = array();
        $keywords[] = osc_item_category();
        if( osc_item_city() != '' ) {
            $keywords[] = osc_item_city();
            $keywords[] = sprintf('%s %s', osc_item_category(), osc_item_city());
        }
        if( osc_item_region() != '' ) {
            $keywords[] = osc_item_region();
            $keywords[] = sprintf('%s %s', osc_item_category(), osc_item_region());
        }
        if( (osc_item_city() != '') && (osc_item_region() != '') ) {
            $keywords[] = sprintf('%s %s %s', osc_item_category(), osc_item_region(), osc_item_city());
            $keywords[] = sprintf('%s %s', osc_item_region(), osc_item_city());
        }
        $text = implode(', ', $keywords);
    }

    return $text;
}

function modified_meta_description( ) {
$location = Rewrite::newInstance()->get_location();
    $text = '';
	//if(osc_is_custom_page()){
	if( $location == "blog" ) {
		$text = blog_meta_content_filter();
	}
    // home page
    if( osc_is_home_page() ) {
        $text = osc_page_description();
    }
    // static page
    if( osc_is_static_page() ) {
        $text = osc_highlight(osc_static_page_text(), 140, '', '');
    }
    // search
    if( osc_is_search_page() ) {
        if( osc_has_items() ) {
            $text = osc_item_category() . ' ' . osc_item_city() . ', ' . osc_highlight(osc_item_description(), 120);
        }
        osc_reset_items();
    }
    // listing
    if( osc_is_ad_page() ) {
        $text = osc_item_category() . ' ' . osc_item_city() . ', ' . osc_highlight(osc_item_description(), 120);
    }
	
    return  $text;
}

function modified_meta_title_filter(){
$location = Rewrite::newInstance()->get_location();
    $section  = Rewrite::newInstance()->get_section();
    $text = '';

    switch ($location) {
        case ('item'):
            switch ($section) {
                case 'item_add':    $text = __('Publish a listing', 'blog'); break;
                case 'item_edit':   $text = __('Edit your listing', 'blog'); break;
                case 'send_friend': $text = __('Send to a friend', 'blog') . ' - ' . osc_item_title(); break;
                case 'contact':     $text = __('Contact seller', 'blog') . ' - ' . osc_item_title(); break;
                default:            $text = osc_item_title() . ' ' . osc_item_city(); break;
            }
        break;
		 case('blog'):
            $text = blog_meta_title_filter();
        break;
        case('page'):
            $text = osc_static_page_title();
        break;
        case('error'):
            $text = __('Error', 'blog');
        break;
        case('search'):
            $region   = osc_search_region();
            $city     = osc_search_city();
            $pattern  = osc_search_pattern();
            $category = osc_search_category_id();
            $s_page   = '';
            $i_page   = Params::getParam('iPage');

            if($i_page != '' && $i_page > 1) {
                $s_page = ' - ' . __('page', 'blog') . ' ' . $i_page;
            }

            $b_show_all = ($region == '' && $city == '' && $pattern == '' && empty($category));
            $b_category = (!empty($category));
            $b_pattern  = ($pattern != '');
            $b_city     = ($city != '');
            $b_region   = ($region != '');

            if($b_show_all) {
                $text = __('Show all listings', 'blog') . ' - ' . $s_page . osc_page_title();
            }

            $result = '';
            if($b_pattern) {
                $result .= $pattern . ' &raquo; ';
            }

            if($b_category && is_array($category) && count($category) > 0) {
                $cat = Category::newInstance()->findByPrimaryKey($category[0]);
                if( $cat ) {
                    $result .= $cat['s_name'].' ';
                }
            }

            if($b_city) {
                $result .= $city . ' &raquo; ';
            } else if($b_region) {
                $result .= $region . ' &raquo; ';
            }

            $result = preg_replace('|\s?&raquo;\s$|', '', $result);

            if($result == '') {
                $result = __('Search results', 'blog');
            }

            $text = '';
            if( osc_get_preference('seo_title_keyword') != '' ) {
                $text .= osc_get_preference('seo_title_keyword') . ' ';
            }
            $text .= $result . $s_page;
        break;
        case('login'):
            switch ($section) {
                case('recover'): $text = __('Recover your password', 'blog');
                default:         $text = __('Login', 'blog');
            }
        break;
        case('register'):
            $text = __('Create a new account');
        break;
        case('user'):
            switch ($section) {
                case('dashboard'):       $text = __('Dashboard', 'blog'); break;
                case('items'):           $text = __('Manage my listings', 'blog'); break;
                case('alerts'):          $text = __('Manage my alerts', 'blog'); break;
                case('profile'):         $text = __('Update my profile', 'blog'); break;
                case('pub_profile'):     $text = __('Public profile', 'blog') . ' - ' . osc_user_name(); break;
                case('change_email'):    $text = __('Change my email', 'blog'); break;
                case('change_username'): $text = __('Change my username', 'blog'); break;
                case('change_password'): $text = __('Change my password', 'blog'); break;
                case('forgot'):          $text = __('Recover my password', 'blog'); break;
            }
        break;
        case('contact'):
            $text = __('Contact', 'blog');
        break;
        default:
            $text = osc_page_title();
        break;
    }

    if( !osc_is_home_page() ) {
        if($text!='') {
            $text .= ' - ' . osc_page_title();
        } else {
            $text = osc_page_title();
        }
    }

    return $text;

}

?>