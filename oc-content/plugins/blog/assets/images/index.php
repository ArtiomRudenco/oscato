<?php
/*
Plugin Name: sc_blog
Plugin URI: http://www.drizzlethemes.com
Description: Blog.
Version: 1.0.0
Author: DrizzleThemes
Author URI: http://www.drizzlethemes.com
Short name: Blogs
Plugin update URI: scBlog
*/


include_once 'ModelBlog.php';
/* Install Plugin */
function scBlog_install() {
	ModelBlog::newInstance()->import('sc_blog/struct.sql');
}

/* Uninstall Plugin */
function scBlog_uninstall() {
	ModelBlog::newInstance()->uninstall();
}

function blogslug($string){
	$table = array(
		'�'=>'S', '�'=>'s', '�'=>'Dj', 'd'=>'dj', '�'=>'Z', '�'=>'z', 'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c',
		'�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E',
		'�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O',
		'�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss',
		'�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c', '�'=>'e', '�'=>'e',
		'�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o',
		'�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'y', '�'=>'b',
		'�'=>'y', 'R'=>'R', 'r'=>'r', '/'=> '-', ' ' => '-'
	);
	$blogslug = strtolower(strtr($string, $table));
	$blogslug = preg_replace('#[^0-9a-z]+#i', " ", $blogslug);
	$blogslug = str_replace(' ','-',trim($blogslug));
	return $blogslug;
}

function style_admin_menu() { ?>
<style>
     .ico.ico-sc_blog {
        background-image: url('<?php echo osc_base_url();?>oc-content/plugins/<?php echo osc_plugin_folder(__FILE__);?>/assets/images/big.png') !important;
		background-position: 9px 4px;
    }
	
	li.current .ico.ico-sc_blog, li.hover .ico.ico-sc_blog {
        background-image: url('<?php echo osc_base_url();?>oc-content/plugins/<?php echo osc_plugin_folder(__FILE__);?>/assets/images/big.png') !important;
		background-position: 9px -39px;
    }
	
	body.compact .ico.ico-sc_blog {
        background-image: url('<?php echo osc_base_url();?>oc-content/plugins/<?php echo osc_plugin_folder(__FILE__);?>/assets/images/spilt.png') !important;
		background-position: -1px -48px;
    }
	
	body.compact li.current .ico.ico-sc_blog, li.hover .ico.ico-sc_blog {
        background-image: url('<?php echo osc_base_url();?>oc-content/plugins/<?php echo osc_plugin_folder(__FILE__);?>/assets/images/spilt.png') !important;
		background-position: -1px -5px;
    }
	

	
  
}
   
    }
  
</style>

    <?php
}

osc_add_hook('admin_header','style_admin_menu');

/* Configure */
function scBlog_Configure() {
	osc_admin_render_plugin('scBlog/admin/config.php');
}
osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'scBlog_Configure');
osc_add_hook(osc_plugin_path(__FILE__) . '_uninstall', 'scBlog_uninstall') ;
osc_register_plugin(osc_plugin_path(__FILE__), 'scBlog_install') ;
/* Blog Route */
osc_add_route('sc_blog_page','blog/page/?(.*)/(.*)?','blog/page/{iPage}/{catId}',osc_plugin_folder(__FILE__).'view.php','','custom','','blog');
osc_add_route('sc_blog_cat','blog/category/?(.*)/?','blog/category/{catId}',osc_plugin_folder(__FILE__).'view.php','','custom','','blog');
osc_add_route('sc_blog','blog/?(.*)/?','blog/{blogId}',osc_plugin_folder(__FILE__).'view.php','','custom','','blog');
/* Admin Menu */
osc_add_admin_menu_page(__('Dashboard'),osc_admin_base_url(),'dash','moderator'); 
osc_add_admin_menu_page(__('Listing'),osc_admin_base_url(true).'?page=items','items','moderator'); 
osc_add_admin_menu_page(__('Blog'),osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/blogs.php'),'sc_blog','moderator');
osc_add_admin_submenu_page('sc_blog', __('Posts','sc_blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/blogs.php'), 'Blogs');
osc_add_admin_submenu_page('sc_blog', __('Add Post','sc_blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/post.php'), 'Blog_New');
osc_add_admin_submenu_page('sc_blog', __('Categories','sc_blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/categories.php'), 'Blog_Categories');
osc_add_admin_submenu_page('sc_blog', __('Settings','sc_blog'), osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/admin/help.php'), 'Blog_Settings');
?>