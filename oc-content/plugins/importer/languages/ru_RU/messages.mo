��          �      l      �     �     �                  K  @     �     �     �     �  (   �     �       )     :   H     �  
   �     �  !   �      �    �     �  0     !   B  !   d  R   �    �  9   �     .     ;  #   Y  I   }  0   �  1   �  Y   *  x   �     �          '  Q   9  3   �                     
                                                              	               %s (Item %d) (Ads numbers: {LIST_ADS}) Ad Importer Ad importer All ads were imported correctly Be sure you have ALL curencies from xml added in your osclass site, if you plan to upload in parent categories make sure you have ticked the option "Allow users to select a parent category as a category when inserting or editing a listing" in settings->general. Make sure you have all countries from xml (if you add ads with diff countries).<br />
You have a test xml there as example (example: cityarea I use it as phone field, but you can use it as u want).<br />
If you want to add custom fields in xml see example <br />
Custom fields must exists and must be attached to category...  File uploaded was not valid Help Import completed Import in progress Importing ad {NUM_AD} out of {TOTAL_ADS} Invalid ad number No action defined No ads have been detected, nothing to do. This process could take a while, DO NOT CLOSE the browser. Upload Upload XML WARNING {FAILED_ADS} ads failed to import {IMPORTED_ADS} ads were imported Project-Id-Version: Renew Ads
POT-Creation-Date: 2018-09-26 00:52+0300
PO-Revision-Date: 
Last-Translator: cartagena68 <webmaster@swappingol.com>
Language-Team: osclass.pro
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: ../..
X-Poedit-SearchPath-0: .
 %s (объявление % d) (Номера объявлений : {LIST_ADS}) Импорт объявлений Импорт объявлений Все объявления были импортированы правильно Убедитесь в том, что Все валюты из Вашего xml файла есть на сайте. Если Вы хотите загружать объявления в родительские категории, убедитесь что в Настройки -Главные включена опция "Разрешить пользователям выбирать только родительскую категорию при публикации объявления". Убедитесь что все страны из файла, так же добавлены на Вашем сайте..<br />
В тестовом файле в поле район добавлен телефон. Вы можете сделать так же или удалить поле. <br />
Вы можете добавить любые дополнительные поля, но на Вашем сайте они так же уже должны быть созданы.<br />
Посмотреть пример файла для импорта:  Загруженный файл не корректный Помощь Импорт завершен Импорт выполняется Импортировано {NUM_AD} объявлений из {TOTAL_ADS} Неверный номер объявления Никаких действий не задано Никаких объявлений не обнаружено, нечего делать. Этот процесс может занять некоторое время, не закрывайте браузер. Загрузить Загрузить XML ВНИМАНИЕ! {FAILED_ADS}  не удалось импортировать объявления {IMPORTED_ADS}  были импортированы 