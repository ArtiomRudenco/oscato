<?php
 /*
 * Copyright 2017 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */

class ModelSeoplugin extends DAO {

private static $instance ;

public static function newInstance() {
  if( !self::$instance instanceof self ) {
    self::$instance = new self ;
  }
  return self::$instance ;
}

function __construct() {
 parent::__construct();
}
        


public function getTableSEO_category() {
  return DB_TABLE_PREFIX.'t_seopro_category' ;
    }
public function getTableSEO_category_simple() {
  return DB_TABLE_PREFIX.'t_seopro_category_simple' ;
    }
public function getTableSEO_region() {
        return DB_TABLE_PREFIX.'t_seopro_region' ;
    }
public function getTableSEO_region_simple() {
        return DB_TABLE_PREFIX.'t_seopro_region_simple' ;
    }
public function getTableSEO_country() {
        return DB_TABLE_PREFIX.'t_seopro_country' ;
    }
public function getTableSEO_country_simple() {
        return DB_TABLE_PREFIX.'t_seopro_country_simple' ;
    }
public function getTableSEO_city() {
        return DB_TABLE_PREFIX.'t_seopro_city' ;
}
public function getTableSEO_city_simple() {
        return DB_TABLE_PREFIX.'t_seopro_city_simple' ;
}
public function getTableSEO_home() {
        return DB_TABLE_PREFIX.'t_seopro_home' ;
}

public function getTableSEO_page() {
  return DB_TABLE_PREFIX.'t_seopro_pages' ;
}

public function getTableSEO_item() {
  return DB_TABLE_PREFIX.'t_seopro_item';
}
        
 public function import($file)
        {                                      
            $sql = file_get_contents($file);
            
            if(! $this->dao->importSQL($sql) ){
                throw new Exception( "Error import SQL::ModelSeoplugin<br>".$file ) ; 
            }
        }
		
function install() {
    
    $this->import ( SEO_PATH.'struct.sql' );
	osc_set_preference('version', '371', 'seo_plugin', 'INTEGER');
    osc_set_preference('sitemap_number','50000','seo_plugin','INTEGER');
    osc_set_preference('sitemap_pages','3','seo_plugin','INTEGER');
    osc_set_preference('sitemap_categories','1','seo_plugin','INTEGER');
    osc_set_preference('sitemap_country','1','seo_plugin','INTEGER');
    osc_set_preference('sitemap_countries','1','seo_plugin','INTEGER');
    osc_set_preference('sitemap_regions','1','seo_plugin','INTEGER');
    osc_set_preference('sitemap_cities','1','seo_plugin','INTEGER');
	osc_set_preference('seoplugin_sitemap_freq', 'daily', 'seo_plugin', 'STRING');

    osc_set_preference('seoplugin_description', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_title_first', '0', 'seo_plugin', 'INTEGER');
    osc_set_preference('delimiter', '|', 'seo_plugin', 'STRING');
    osc_set_preference('item_city_enable', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_city_number', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_region_enable', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_region_number', '2', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_country_enable', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_country_number', '3', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_category_enable', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_category_number', '4', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_title_enable', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_title_number', '5', 'seo_plugin', 'INTEGER');
    osc_set_preference('item_body_number', '6', 'seo_plugin', 'INTEGER');
	osc_set_preference('country_number', '1', 'seo_plugin', 'INTEGER');
	osc_set_preference('country_text_number', '2', 'seo_plugin', 'INTEGER');
	osc_set_preference('region_number', '1', 'seo_plugin', 'INTEGER');
	osc_set_preference('region_text_number', '2', 'seo_plugin', 'INTEGER');
	osc_set_preference('city_number', '1', 'seo_plugin', 'INTEGER');
	osc_set_preference('city_text_number', '2', 'seo_plugin', 'INTEGER');
	osc_set_preference('category_number', '1', 'seo_plugin', 'INTEGER');
	osc_set_preference('category_text_number', '2', 'seo_plugin', 'INTEGER');
    

    osc_set_preference('help_home', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_config', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_category', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_country', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_regions', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_city', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_pages', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_sitemap', '1', 'seo_plugin', 'INTEGER');
    osc_set_preference('help_robots', '1', 'seo_plugin', 'INTEGER');
	osc_set_preference('simple_country_enable', '0', 'seo_plugin', 'INTEGER');
	osc_set_preference('simple_region_enable', '0', 'seo_plugin', 'INTEGER');
	osc_set_preference('simple_city_enable', '0', 'seo_plugin', 'INTEGER');
	osc_set_preference('simple_category_enable', '0', 'seo_plugin', 'INTEGER');
	
	osc_set_preference('canonical_enable', '1', 'seo_plugin', 'INTEGER');
	osc_set_preference('canonical_add_to_theme', '1', 'seo_plugin', 'INTEGER');
	
	osc_set_preference('fb_enable', '0', 'seo_plugin', 'INTEGER');
	osc_set_preference('schema_enable', '0', 'seo_plugin', 'INTEGER');
	osc_set_preference('twitter_enable', '0', 'seo_plugin', 'INTEGER');
}

public function uninstall() {
    $this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_category()) ) ;
	$this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_category_simple()) ) ;
    $this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_region()) ) ;
	$this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_region_simple()) ) ;
    $this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_country()) ) ;
	$this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_country_simple()) ) ;
    $this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_city()) ) ;
	$this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_city_simple()) ) ;
    $this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_home()) ) ;
	$this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_page()) ) ;
	$this->dao->query(sprintf('DROP TABLE %s', $this->getTableSEO_item()) ) ;
			
	Preference::newInstance()->delete(array('s_section' => 'seo_plugin'));
        }
		
 public function versionUpdate() {
            $version = osc_get_preference('version', 'seo_plugin');
            if ( $version < 340 ) {
                osc_set_preference('version', 340, 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
			if ( $version < 350 ) {
				$this->import ( SEO_PATH.'struct_update.sql' );
                osc_set_preference('version', 350, 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
			if ( $version < 351 ) {
                osc_set_preference('version', 351, 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
			if ( $version < 352 ) {
                osc_set_preference('version', 352, 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
			if ( $version < 362 ) {
                osc_set_preference('version', 362, 'seo_plugin', 'INTEGER');
				osc_set_preference('canonical_enable', '1', 'seo_plugin', 'INTEGER');
	            osc_set_preference('canonical_add_to_theme', '1', 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
			if ( $version < 370 ) {
                osc_set_preference('version', 370, 'seo_plugin', 'INTEGER');
				osc_set_preference('fb_enable', '0', 'seo_plugin', 'INTEGER');
	            osc_set_preference('schema_enable', '0', 'seo_plugin', 'INTEGER');
				osc_set_preference('twitter_enable', '0', 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
			if ( $version < 371 ) {
                osc_set_preference('version', 371, 'seo_plugin', 'INTEGER');
                osc_reset_preferences();
            }
        }
//items
public function getSEO_item( $item_id ) {
  $this->dao->select('*');
  $this->dao->from( $this->getTableSEO_item() );
  $this->dao->where('id', $item_id );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
}

public function insertSEO_item( $item_id, $title, $desc, $keywords ) {
 $this->dao->insert($this->getTableSEO_item(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $item_id));
}
        
public function updateSEO_item( $item_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_item(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $item_id));
}        
        
//category
public function getSEOCategory($category_id) {
  $this->dao->select('*');
  $this->dao->from( $this->getTableSEO_category() );
  $this->dao->where('id', $category_id );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
}

public function getSEOCategory_simple() {
  $this->dao->select('*');
  $this->dao->from( $this->getTableSEO_category_simple());
  $this->dao->where('id', 1 );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }

public function getSEOCategoryId($category_id ) {
   $this->dao->select('*');
   $this->dao->from( $this->getTableSEO_category_simple());
   $this->dao->where('id', $category_id );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }
  
public function insertSEOCategory( $category_id, $title, $desc, $keywords ) {
$this->dao->insert($this->getTableSEO_category(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $category_id));
}
public function insertSEOCategory_simple( $category_id, $title, $desc, $keywords ) {
$this->dao->insert($this->getTableSEO_category_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $category_id));
}
public function updateSEOCategory( $category_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_category(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $category_id));
}
public function updateSEOCategory_simple( $category_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_category_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $category_id));
}
//country
public function getSEOcountry() {
  $this->dao->select('*');
  $this->dao->from( $this->getTableSEO_country());
  $this->dao->where('id', 1 );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }
  
public function getSEOcountry_simple() {
  $this->dao->select('*');
  $this->dao->from( $this->getTableSEO_country_simple());
  $this->dao->where('id', 1 );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }
  
public function getSEOcountryId($country_id) {
   $this->dao->select('*');
   $this->dao->from( $this->getTableSEO_country());
   $this->dao->where('id', $country_id );
  $result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }
  
public function insertSEOcountry( $country_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_country(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $country_id));
}

public function insertSEOcountry_simple( $country_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_country_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $country_id));
}
public function updateSEOcountry( $country_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_country(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $country_id));
}

public function updateSEOcountry_simple( $country_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_country_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $country_id));
}
//regions
public function getSEORegion() {
        $this->dao->select('*');
        $this->dao->from( $this->getTableSEO_region());
        $this->dao->where('id', 1 );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
    }

public function getSEORegion_simple() {
        $this->dao->select('*');
        $this->dao->from( $this->getTableSEO_region_simple());
        $this->dao->where('id', 1 );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
    }
	
public function getSEORegionId($region_id) {
        $this->dao->select('*');
        $this->dao->from( $this->getTableSEO_region());
        $this->dao->where('id', $region_id );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
    }


public function insertSEORegion_simple( $region_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_region_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $region_id));
    }
	
public function insertSEORegion( $region_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_region(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $region_id));
    }

public function updateSEORegion( $region_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_region(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $region_id));
}

public function updateSEORegion_simple( $region_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_region_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $region_id));
}

//city
public function getSEOcity() {
        $this->dao->select('*');
        $this->dao->from( $this->getTableSEO_city());
        $this->dao->where('id', 1 );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
    }
	
public function getSEOcity_simple() {
        $this->dao->select('*');
        $this->dao->from( $this->getTableSEO_city_simple());
        $this->dao->where('id', 1 );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
    }

public function getSEOcityId($city_id) {
        $this->dao->select('*');
        $this->dao->from( $this->getTableSEO_city());
        $this->dao->where('id', $city_id );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
    }
	
public function insertSEOcity( $city_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_city(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $city_id));
    }

public function insertSEOcity_simple( $city_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_city_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $city_id));
    }
	
public function updateSEOcity( $city_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_city(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $city_id));
}

public function updateSEOcity_simple( $city_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_city_simple(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $city_id));
}

//home
public function getSEOhome() {
$this->dao->select('*');
$this->dao->from( $this->getTableSEO_home());
$this->dao->where('id', 1 );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }
	
public function insertSEOhome( $home_id, $title, $desc, $keywords ) {
	$this->dao->insert($this->getTableSEO_home(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $home_id));
  }

public function updateSEOhome( $home_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_home(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $home_id));
}
 //pages
public function getSEOpage( $page_id ) {
  $this->dao->select('*');
  $this->dao->from( $this->getTableSEO_page() );
  $this->dao->where('id', $page_id );
$result = $this->dao->get();
  if($result) {
  return $result->row();
  }
  return array();
  }
public function inserSEOpage( $page_id, $title, $desc, $keywords ) {
  $this->dao->insert($this->getTableSEO_page(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords, 'id' => $page_id));
}
        
public function updateSEOpage( $page_id, $title, $desc, $keywords ) {
$this->dao->update($this->getTableSEO_page(), array('title'  => $title, 'description'  => $desc, 'keywords' => $keywords), array('id' => $page_id));
}
        
}
?>