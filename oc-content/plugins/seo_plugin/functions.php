<?php
 /*
 * Copyright 2017 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
include 'sitemap/vendor/autoload.php';

use samdark\sitemap\Sitemap;
use samdark\sitemap\Index;

function ping_engines(){
        osc_doRequest('http://www.google.com/webmasters/sitemaps/ping?sitemap='.urlencode(osc_base_url().'sitemap-index.xml'), array());
        osc_doRequest('http://www.bing.com/webmaster/ping.aspx?siteMap='.urlencode(osc_base_url().'sitemap-index.xml'), array());
        osc_doRequest('http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid='.osc_page_title().'&url='.urlencode(osc_base_url().'sitemap-index.xml'), array());
        osc_doRequest('http://webmaster.yandex.ru/wmconsole/sitemap_list.xml?host='.urlencode(osc_base_url() . 'sitemap-index.xml'), array());
    }

    function list_dir_xml($dir){
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    $path_part = pathinfo($file);
                    if (isset($path_part['extension']) && $path_part['extension'] == 'xml') {
                        $dirFiles[] = $file;
                    }
                }
                closedir($dh);
            }
        }
        return $dirFiles;
    }

    function unlink_old_xml($dir){
        $_dellfile = list_dir_xml($dir);
        foreach ($_dellfile as $_file){
           @unlink($dir.$_file);
        }
    }


function generate_sitemap()
{
$freq = osc_get_preference('seoplugin_sitemap_freq', 'seo_plugin');
$_number = osc_get_preference('sitemap_number', 'seo_plugin');
$_categories = osc_get_preference('sitemap_categories', 'seo_plugin');
$_countries = osc_get_preference('sitemap_countries', 'seo_plugin');
$_regions = osc_get_preference('sitemap_regions', 'seo_plugin');
$_cities = osc_get_preference('sitemap_cities', 'seo_plugin');
$_pages = osc_get_preference('sitemap_pages', 'seo_plugin');

$path_xml = osc_base_path();
    $site_name = osc_base_url();


    unlink_old_xml($path_xml);

    if($_countries == 1 || $_regions == 1 || $_cities == 1) {
        $sitemap_location = new Sitemap($path_xml . 'sitemap-location.xml');
        $sitemap_location->maxUrls = $_number;
        if ($_countries == 1) {
            if (osc_count_countries() > 0) {
                while (osc_has_countries()) {
                    $sitemap_location->addItem(osc_country_url(), time(), 'weekly');
                }
            }
        }
        if ($_regions == 1) {
            if (osc_count_regions() > 0) {
                while (osc_has_regions()) {
                    $sitemap_location->addItem(osc_region_url(), time(), 'weekly');
                }
            }
        }

        if ($_cities == 1) {
            if (osc_count_cities() > 0) {
                while (osc_has_cities()) {
                    $sitemap_location->addItem(osc_city_url(), time(), 'weekly');
                }
            }
        }
        $sitemap_location->write();
    }

    if($_categories == 1) {
        $sitemap_category = new Sitemap($path_xml . 'sitemap-category.xml');
        $sitemap_category->maxUrls = $_number;
        if (osc_count_categories() > 0) {
            while (osc_has_categories()) {
                $sitemap_category->addItem(osc_search_category_url(), time(), 'hourly');
                if (osc_count_subcategories() > 0) {
                    while (osc_has_subcategories()) {
                        $sitemap_category->addItem(osc_search_category_url(), time(), 'hourly');
                    }
                }
            }
        }
        $sitemap_category->write();
    }

    $sitemap_page = new Sitemap($path_xml.'sitemap-page.xml');
    $sitemap_page->maxUrls = $_number;
	$sitemap_page->addItem(osc_base_url(), time(), 'always');
    $sitemap_page->addItem(osc_search_url(), time(), 'always');
    if (osc_count_static_pages() > 0) {
        while (osc_has_static_pages()) {
            $sitemap_page->addItem(osc_static_page_url(), time(), 'monthly');
            
        }
    }
    $sitemap_page->write();

    $sitemap_item = new Sitemap($path_xml.'sitemap-item.xml');
    $sitemap_item->maxUrls = $_number;
	$sitemap_item->maxFiles = $_pages;
    function sitemap_items_url($url) {
        if( preg_match('|\?(.*)|', $url, $match) ) {
            $sub_url = $match[1];
            $param = explode('&', $sub_url);
            foreach($param as &$p) {
                list($key, $value) = explode('=', $p);
                $p = $key . '=' . urlencode($value);
            }
            $sub_url = implode('&', $param);
            $url = preg_replace('|\?.*|', '?' . $sub_url, $url);
        }
        return htmlentities($url, ENT_QUOTES, "UTF-8");
    }
    $mSearch = new Search() ;
	$mSearch->limit(0,500000) ;
    $aItems = $mSearch->doSearch();
    View::newInstance()->_exportVariableToView('items', $aItems);
    if(osc_count_items() > 0) {
        while(osc_has_items()) {
            $sitemap_item->addItem(osc_item_url(), time(), 'weekly');
        }
    }
    $sitemap_item->write();

    $index = new Index($path_xml.'/sitemap-index.xml');
    $dirFiles = list_dir_xml($path_xml);
    sort($dirFiles, SORT_NATURAL);
    foreach($dirFiles as $file){
        $sitemapurl = osc_base_url().''.$file;
        $filename   = osc_base_path().''.$file;
        $lastmod    = date("Y-m-d", filemtime($filename));

        $index->addSitemap($sitemapurl, $lastmod, 1);

    }
    $index->write();

    ping_engines();

}


//sitemap
function ActulatN() {
  $sort_n = array();
  $name_n = array();
  $i = 0;
  if(osc_get_preference('item_city_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('City', 'seo_plugin');
    $name_n[$i]['id'] = 'city';
    $sort_n[$i] = osc_get_preference('item_city_number','seo_plugin');
    $i++;
  }
  if(osc_get_preference('item_region_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Region', 'seo_plugin');
    $name_n[$i]['id'] = 'region';
    $sort_n[$i] = osc_get_preference('item_region_number','seo_plugin');
    $i++;
  }
  if(osc_get_preference('item_country_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Country', 'seo_plugin');
    $name_n[$i]['id'] = 'country';
    $sort_n[$i] = osc_get_preference('item_country_number','seo_plugin');
    $i++;
  }
  if(osc_get_preference('item_title_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Site Title', 'seo_plugin');
    $name_n[$i]['id'] = 'site_name';
    $sort_n[$i] = osc_get_preference('item_title_number','seo_plugin');
    $i++;
  }
  if(osc_get_preference('item_category_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Category', 'seo_plugin');
    $name_n[$i]['id'] = 'category';
    $sort_n[$i] = osc_get_preference('item_category_number','seo_plugin');
    $i++;
  }

  $name_n[$i]['name'] = __('Item Title', 'seo_plugin');
  $name_n[$i]['id'] = 'title';
  $sort_n[$i] = osc_get_preference('item_body_number','seo_plugin');

  array_multisort($sort_n, $name_n);
  $name_n = array_filter( $name_n );
  return $name_n;
}
function ActulatC() {
  $sort_n = array();
  $name_n = array();
  $i = 0;
  
  if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Country', 'seo_plugin');
    $name_n[$i]['id'] = 'country';
    $sort_n[$i] = osc_get_preference('country_number','seo_plugin');
    $i++;
  }
  
  if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
  $name_n[$i]['name'] = __('Text', 'seo_plugin');
  $name_n[$i]['id'] = 'country_text';
  $sort_n[$i] = osc_get_preference('country_text_number','seo_plugin');
     $i++;
  }

  array_multisort($sort_n, $name_n);
  $name_n = array_filter( $name_n );
  return $name_n;
}
function ActulatR() {
  $sort_n = array();
  $name_n = array();
  $i = 0;
  
  if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Region', 'seo_plugin');
    $name_n[$i]['id'] = 'region';
    $sort_n[$i] = osc_get_preference('region_number','seo_plugin');
    $i++;
  }
  
  if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
  $name_n[$i]['name'] = __('Text', 'seo_plugin');
  $name_n[$i]['id'] = 'region_text';
  $sort_n[$i] = osc_get_preference('region_text_number','seo_plugin');
     $i++;
  }

  array_multisort($sort_n, $name_n);
  $name_n = array_filter( $name_n );
  return $name_n;
}
function ActulatCity() {
  $sort_n = array();
  $name_n = array();
  $i = 0;
  
  if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('City', 'seo_plugin');
    $name_n[$i]['id'] = 'city';
    $sort_n[$i] = osc_get_preference('city_number','seo_plugin');
    $i++;
  }
  
  if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
  $name_n[$i]['name'] = __('Text', 'seo_plugin');
  $name_n[$i]['id'] = 'city_text';
  $sort_n[$i] = osc_get_preference('city_text_number','seo_plugin');
     $i++;
  }

  array_multisort($sort_n, $name_n);
  $name_n = array_filter( $name_n );
  return $name_n;
}
function ActulatCategory() {
  $sort_n = array();
  $name_n = array();
  $i = 0;
  
  if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
    $name_n[$i]['name'] = __('Category', 'seo_plugin');
    $name_n[$i]['id'] = 'category';
    $sort_n[$i] = osc_get_preference('category_number','seo_plugin');
    $i++;
  }
  
  if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
  $name_n[$i]['name'] = __('Text', 'seo_plugin');
  $name_n[$i]['id'] = 'category_text';
  $sort_n[$i] = osc_get_preference('category_text_number','seo_plugin');
     $i++;
  }

  array_multisort($sort_n, $name_n);
  $name_n = array_filter( $name_n );
  return $name_n;
}
function pages_exist() {
  if ( !View::newInstance()->_exists('pages') ) {
    View::newInstance()->_exportVariableToView('pages', Page::newInstance()->listAll(false, 0) );
  }

  $page = View::newInstance()->_next('pages');
  View::newInstance()->_exportVariableToView('page_meta', json_decode($page['s_meta'], true));
  return $page;
}


?>