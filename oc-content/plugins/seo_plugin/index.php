<?php
 /*
 * Copyright 2017 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
/*
  Plugin Name: SEO PRO Plugin
  Plugin URI: https://osclass-pro.com
  Plugin update URI: seo-pro
  Description: SEO PRO plugin for Osclass
  Version: 3.7.1
  Author: Dis
  Author URI: https://osclass-pro.com/
  Short Name: seo_plugin
*/
define('SEO_PATH', osc_plugins_path() . osc_plugin_folder(__FILE__));
define('SEO_RELATIVE_PATH', osc_plugin_folder(__FILE__));
define('SEO_ADM_STYLE', osc_plugin_url(__FILE__) . 'admin/css/');
define('SEO_ADM_SCRIPT', osc_plugin_url(__FILE__) . 'admin/js/');
require_once osc_plugins_path() . osc_plugin_folder(__FILE__) . 'functions.php';
require_once osc_plugins_path() . osc_plugin_folder(__FILE__) . 'ModelSeoplugin.php';
 
 function seoplugin_install() {
        ModelSeoplugin::newInstance()->install();
    }

    function seoplugin_uninstall() {
        ModelSeoplugin::newInstance()->uninstall();
    }

function seoplugin_admin_menu() {
        osc_add_admin_submenu_divider('plugins', 'SEO plugin', 'seoplugin_divider', 'administrator');
		osc_add_admin_submenu_page('plugins', __('Settings', 'seo_plugin'), osc_route_admin_url('seoplugin-admin-home'), 'seoplugin_home', 'administrator');
    }
	
function seoplugin_configure_link() {
        osc_redirect_to(osc_route_admin_url('seoplugin-admin-home'));
    }
function seoplugin_version_update() {
        ModelSeoplugin::newInstance()->versionUpdate();
    }

function seoplugin_init_admin_page_header() {
        $_r = Params::getParam('route');
        
        switch ($_r) {
            case 'seoplugin-admin-config' :
            case 'seoplugin-admin-categories' :
            case 'seoplugin-admin-country' :
            case 'seoplugin-admin-city' :
            case 'seoplugin-admin-home' :
            case 'seoplugin-admin-pages' :
            case 'seoplugin-admin-regions' :
			case 'seoplugin-admin-robots' :
			case 'seoplugin-admin-sitemap' :
			case 'seoplugin-admin-canonical' :
			case 'seoplugin-admin-tags' :
			case 'seoplugin-admin-hteditor' :
                osc_enqueue_style('admin-style', SEO_ADM_STYLE . 'admin.css');
                osc_enqueue_style('font-style', SEO_ADM_STYLE . 'font-awesome.min.css');
                osc_remove_hook('admin_page_header', 'customPageHeader');
                osc_add_hook('admin_page_header', 'seoplugin_admin_page_header');
            break;
 
            default :
            break;
        }
    }
	
	function seoplugin_admin_page_header() {
        echo '<h1>SEO PRO</h1>';
    }
	
function get_id_city($name_city){
	if(is_numeric($name_city)){
		return $name_city;
	}else{
		foreach(City::newInstance()->listAll() as $c){
			if($c['s_name'] == $name_city){
			return $c['pk_i_id'];
			exit;			
			}
		}
	}
}

function get_id_region($name_region){
	if(is_numeric($name_region)){
		return $name_region;
	}else{
		foreach(Region::newInstance()->listAll() as $r){
			if($r['s_name'] == $name_region){
			return $r['pk_i_id'];
			exit;			
			}
		}
	}
}

function seoplugin_keywords($text) {
    $seoTitleHome = "";
    $location = Rewrite::newInstance()->get_location();
    if($location == 'search') {
        if (isset(osc_search_category_id()[0])) {
			 	if(osc_get_preference('simple_category_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CATEGORY_KEY();
        }else{
            $CategoryId = osc_search_category_id()[0];
            $seoKeywordsInfo = ModelSeoplugin::newInstance()->getSEOCategory($CategoryId);
            if (isset($seoKeywordsInfo['keywords'])) {
                $seoTitleHome = $seoKeywordsInfo['keywords'];
            }
            if (strlen($seoTitleHome) > 0) {
                $text = $seoTitleHome;
            }
		}
			if(osc_search_country()){
                if($text != ''){
                    $text.= ', ';}
                $text.= osc_search_country();}
            if(osc_search_region()){
                if($text != ''){
                    $text.= ', ';}
                $text.= osc_search_region();}
            if(osc_search_city()){
                if($text != ''){
                    $text.= ', ';}
                $text.= osc_search_city();}


        } elseif(osc_search_country() != '' && osc_search_region() == '' && osc_search_city() == ''){
        	if(osc_get_preference('simple_country_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_COUNTRY_KEY();
        }else{
	    $cou_id = Params::getParam('sCountry');
            $detail = ModelSeoplugin::newInstance()->getSEOcountryId($cou_id);
            if (isset($detail['keywords']) && $detail['keywords'] !='') {
                if ($text != '') {
                    $text .= ', ';
                }
                $text = $detail['keywords'];
            }
            }
			}elseif(osc_search_region() != '' && osc_search_city() == ''){
		if(osc_get_preference('simple_region_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_REGION_KEY();
        }else{
            $reg_id = get_id_region(Params::getParam('sRegion'));
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($reg_id);
            if (isset($detail['keywords']) && $detail['keywords'] !='') {
                if ($text != '') {
                    $text .= ', ';
                }
                $text = $detail['keywords'];
            }
		}
		}elseif(osc_search_city() != ''){
			if(osc_get_preference('simple_city_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CITY_KEY();
        }else{
            $city_id = get_id_city(Params::getParam('sCity'));
            $detail = ModelSeoplugin::newInstance()->getSEOcityId($city_id);
            if (isset($detail['keywords']) && $detail['keywords'] !='') {
               // if ($text != '') {
                 //   $text .= ', ';}
                $text = $detail['keywords'];
            }
		}
        }
		}

    if($location == 'page'){
        $page_id = osc_static_page_id();
        $detail = ModelSeoplugin::newInstance()->getSEOpage($page_id);
        if(isset($detail['keywords']) && $detail['keywords'] !=''){
            $text = $detail['keywords'];
        }
    }

    if(osc_is_home_page()){
        if(Params::getParam('sCountry')){
		if(osc_get_preference('simple_country_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_COUNTRY_KEY();
        }else{
            $id_cou = Params::getParam('sCountry');
            $detail = ModelSeoplugin::newInstance()->getSEOcountryId($id_cou);
            if(isset($detail['keywords']) && $detail['keywords'] !=''){
                $text = $detail['keywords'];
            }
		}
        }elseif(Params::getParam('sRegion')){
		if(osc_get_preference('simple_region_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_REGION_KEY();
        }else{
            $id_reg = Params::getParam('sRegion');
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($id_reg);
            if(isset($detail['keywords']) && $detail['keywords'] !=''){
                $text = $detail['keywords'];
            }
		}
        }elseif(Params::getParam('sCity')){
		if(osc_get_preference('simple_city_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CITY_KEY();
        }else{
            $id_city = Params::getParam('sCity');
            $detail = ModelSeoplugin::newInstance()->getSEOcityId($id_city);
            if(isset($detail['keywords']) && $detail['keywords'] !=''){
                $text = $detail['keywords'];
            }
		}
        }elseif(Params::getParam('sCategory')){
			if(osc_get_preference('simple_category_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CATEGORY_KEY();
        }else{
            $id_cat = Params::getParam('sCategory');
            $detail = ModelSeoplugin::newInstance()->getSEOCategory($id_cat);
            if(isset($detail['keywords']) && $detail['keywords'] !=''){
                $text = $detail['keywords'];
            }
		}
        }else{
            $seoKeywordsHomeInfo = ModelSeoplugin::newInstance()->getSEOhome();
            if(isset($seoKeywordsHomeInfo['keywords'])) {
                $seoTitleHome = $seoKeywordsHomeInfo['keywords'];
            }
            if(strlen($seoTitleHome)>0){
                $text = $seoTitleHome;
            }
        }
    }

    return $text;
}

function seoplugin_description($text) {
	$seoTitleHome = "";

    $location = Rewrite::newInstance()->get_location();

    if($location == 'search') {
        if(isset(osc_search_category_id()[0])){
		if(osc_get_preference('simple_category_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CATEGORY_DESC();
        }else{
            $CategoryId = osc_search_category_id()[0];
            $seoDescriptionInfo = ModelSeoplugin::newInstance()->getSEOCategory($CategoryId);
            if (isset($seoDescriptionInfo['description'])) {
                $seoTitleHome = $seoDescriptionInfo['description'];
            }
            if (strlen($seoTitleHome) > 0) {
                $text = $seoTitleHome;
            }
		}
			if(osc_search_country()){
                if($text != ''){
                    $text.= ', ';}
                $text.= osc_search_country();}
            if(osc_search_region()){
                if($text != ''){
                    $text.= ', ';}
                $text.= osc_search_region();}
            if(osc_search_city()){
                if($text != ''){
                    $text.= ', ';}
                $text.= osc_search_city();}

        }elseif(osc_search_country() != '' && osc_search_region() == '' && osc_search_city() == ''){
			if(osc_get_preference('simple_country_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_COUNTRY_DESC();
        }else{       
	    $cou_id = Params::getParam('sCountry');
            $detail = ModelSeoplugin::newInstance()->getSEOcountryId($cou_id);
            if($detail['description'] && $detail['description'] !=''){
                if($text != ''){
                    $text.= ', ';}
                $text= $detail['description'];
            }
		}
       }elseif(osc_search_city() != ''){
		if(osc_get_preference('simple_city_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_CITY_DESC();
        }else{   
            $city_id = get_id_city(Params::getParam('sCity'));
            $detail = ModelSeoplugin::newInstance()->getSEOcityId($city_id);
            if(isset($detail['description']) && $detail['description'] != ''){
                //if($text != ''){
                 //   $text.= ', ';}
                $text = $detail['description'];
            }
		}
		}elseif(osc_search_region() != '' && osc_search_city() == ''){
						if(osc_get_preference('simple_region_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_REGION_DESC();
        }else{   
            $reg_id = get_id_region(Params::getParam('sRegion'));
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($reg_id);
            if(isset($detail['description']) && $detail['description'] != ''){
                if($text != ''){
                    $text.= ', ';}
                $text= $detail['description'];
            }
        }
		}

    }
   

    if($location == 'page'){
        $page_id = osc_static_page_id();
        $detail = ModelSeoplugin::newInstance()->getSEOpage($page_id);
        if(isset($detail['description']) && $detail['description'] != ''){
            $text = $detail['description'];
        }
    }

    if(osc_is_home_page()){
		
        if(Params::getParam('sCountry')){
		if(osc_get_preference('simple_country_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_COUNTRY_DESC();
        }else{ 
            $id_cou = Params::getParam('sCountry');
            $detail = ModelSeoplugin::newInstance()->getSEOcountryId($id_cou);
            if(isset($detail['description']) && $detail['description'] != ''){
                $text = $detail['description'];
            }
			}
        }elseif(Params::getParam('sRegion')){
				if(osc_get_preference('simple_region_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_REGION_DESC();
        }else{ 
            $id_reg = Params::getParam('sRegion');
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($id_reg);
            if(isset($detail['description']) && $detail['description'] != ''){
                $text = $detail['description'];
            }
		}
        }elseif(Params::getParam('sCity')){
		if(osc_get_preference('simple_city_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_CITY_DESC();
        }else{  
            $id_city = Params::getParam('sCity');
            $detail = ModelSeoplugin::newInstance()->getSEOcityId($id_city);
            if(isset($detail['description']) && $detail['description'] != ''){
                $text = $detail['description'];
            }
		}
        }elseif(Params::getParam('sCategory')){
					if(osc_get_preference('simple_category_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CATEGORY_DESC();
        }else{
            $id_cat = Params::getParam('sCategory');
            $detail = ModelSeoplugin::newInstance()->getSEOCategory($id_cat);
            if(isset($detail['description']) && $detail['description'] != ''){
                $text = $detail['description'];
            }
		}
        }else{
            $seoDescriptionHomeInfo = ModelSeoplugin::newInstance()->getSEOhome();
            if(isset($seoDescriptionHomeInfo['description'])) {
                $seoTitleHome = $seoDescriptionHomeInfo['description'];
            }
            if(strlen($seoTitleHome)>0){
                $text = $seoTitleHome;
            }
        }

    }
    return $text;
}

function seoplugin_title($text) {
    $seoTitleHome = "";
    $location = Rewrite::newInstance()->get_location();
    $section  = Rewrite::newInstance()->get_section();


    if($location == 'search'){
        if(isset(osc_search_category_id()[0])){
		if(osc_get_preference('simple_category_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CATEGORY_TITLE();
        }else{
            $cat_id = osc_search_category_id()[0];
            $detail = ModelSeoplugin::newInstance()->getSEOCategory($cat_id);
            if(isset($detail['title']) && $detail['title'] !=''){
                $text = $detail['title'];
            }
		}
			if(osc_search_country()){
                if($text != ''){
                    $text.= Delimiter();}
                $text.= osc_search_country();}
            if(osc_search_region()){
                if($text != ''){
                    $text.= Delimiter();}
                $text.= osc_search_region();}
            if(osc_search_city()){
                if($text != ''){
                    $text.= Delimiter();}
                $text.= osc_search_city();}
                
        }elseif(osc_search_country() != '' && osc_search_region() == '' && osc_search_city() == ''){
		if(osc_get_preference('simple_country_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_COUNTRY_TITLE();
        }else{
	    $cou_id = Params::getParam('sCountry');
            $detail = ModelSeoplugin::newInstance()->getSEOcountryId($cou_id);
            if($detail['title'] && $detail['title'] !=''){
                if($text != ''){
                    $text.= Delimiter();}
                $text= $detail['title'];
            }
		}
        }elseif(osc_search_city() != ''){
		if(osc_get_preference('simple_city_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_CITY_TITLE();
        }else{
         $city_id = get_id_city(Params::getParam('sCity'));
            $detail = ModelSeoplugin::newInstance()->getSEOcityId($city_id);
            if(isset($detail['title']) && isset($detail['title']) !=''){
                //if($text != ''){
                  //  $text.= ', ';}
                $text = $detail['title'];
            }
		}
        }elseif(osc_search_region() != '' && osc_search_city() == ''){
		if(osc_get_preference('simple_region_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_REGION_TITLE();
        }else{
            $reg_id = get_id_region(Params::getParam('sRegion'));
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($reg_id);
            if(isset($detail['title']) && isset($detail['title']) !=''){
                if($text != ''){
                    $text.= ', ';}
                $text= $detail['title'];
            }
		    }
        }
    }

    if($location == 'page'){
        $page_id = osc_static_page_id();
        $detail = ModelSeoplugin::newInstance()->getSEOpage($page_id);
        if(isset($detail['title']) && $detail['title'] !=''){
            $text = $detail['title'];
        }
    }
	
	if($location == 'item'){
        if($section == 'item_add') {
            $text = __('Publish a listing', 'seo_plugin');
        }elseif($section == 'item_edit'){
            $text = __('Edit your listing', 'seo_plugin') .Delimiter().osc_item_title();
        }elseif($section == 'send_friend'){
            $text = __('Send to a friend', 'seo_plugin');
        }elseif($section =='contact'){
            $text = __('Contact seller', 'seo_plugin');
        }else {
            $text = SEOHPLUGIN();
        }
    }

    if(osc_is_home_page()){
		if(Params::getParam('sCountry')){
		if(osc_get_preference('simple_country_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_COUNTRY_TITLE();
        }else{
            $id_cou = Params::getParam('sCountry');
            $detail = ModelSeoplugin::newInstance()->getSEOcountryId($id_cou);
            if(isset($detail['title']) && $detail['title'] !=''){
                $text = $detail['title'];
            }
		}
        }elseif(Params::getParam('sRegion')){
					if(osc_get_preference('simple_region_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_REGION_TITLE();
        }else{
            $id_reg = Params::getParam('sRegion');
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($id_reg);
            if(isset($detail['title']) && $detail['title'] !=''){
                $text = $detail['title'];
            }
		}
        }elseif(Params::getParam('sCity')){
		if(osc_get_preference('simple_city_enable','seo_plugin') == 1) {
            $text = SEOHPLUGIN_CITY_TITLE();
        }else{
            $id_city = Params::getParam('sCity');
            $detail = ModelSeoplugin::newInstance()->getSEOcityId($id_city);
            if(isset($detail['title']) && $detail['title'] !=''){
                $text = $detail['title'];
            }
		}
        }elseif(Params::getParam('sCategory')){
					if(osc_get_preference('simple_category_enable','seo_plugin') == 1) {
                $text = SEOHPLUGIN_CATEGORY_TITLE();
        }else{
            $id_cat = Params::getParam('sCategory');
            $detail = ModelSeoplugin::newInstance()->getSEOCategory($id_cat);
            if(isset($detail['title']) && $detail['title'] !=''){
                $text = $detail['title'];
            }
		}
        }else{
            $seoTitleHomeInfo = ModelSeoplugin::newInstance()->getSEOhome();
            if (isset($seoTitleHomeInfo['title'])) {
                $seoTitleHome = $seoTitleHomeInfo['title'];
            }
            if (strlen($seoTitleHome) > 0) {
                $text = $seoTitleHome;
            }
        }
    }
    return $text;
}
function Delimiter(){
    return ' ' . osc_get_preference('delimiter','seo_plugin') . ' ';
}

function SEOHPLUGIN(){
    $sort_order = array();
    $elements = array();
    $del = Delimiter();
    $i = 0;
    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEO_item(osc_item_id());
    if(isset($ItemTitleDetail['title'])) {
        $ItemTitle = $detail['title'];
    } else {
	$ItemTitle = '';
	}
    if(osc_get_preference('item_city_enable','seo_plugin') == 1){
        $elements[$i] = osc_item_city();
        $sort_order[$i] = osc_get_preference('item_city_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('item_region_enable','seo_plugin') == 1){
        $elements[$i] = osc_item_region();
        $sort_order[$i] = osc_get_preference('item_region_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('item_country_enable','seo_plugin') == 1){
        $elements[$i] = osc_item_country();
        $sort_order[$i] = osc_get_preference('item_country_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('item_title_enable','seo_plugin') == 1){
        if( $ItemTitle == '') {
            $elements[$i] = osc_page_title();
        } else {
            $elements[$i] = $ItemTitle;
        }
        $sort_order[$i] = osc_get_preference('item_title_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('item_category_enable','seo_plugin') == 1){
        $elements[$i] = osc_item_category();
        $sort_order[$i] = osc_get_preference('item_category_number','seo_plugin');
        $i = $i + 1;
    }

    $elements[$i] = osc_item_title();
    $sort_order[$i] = osc_get_preference('item_body_number','seo_plugin');
    $i = $i + 1;

    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_COUNTRY_TITLE(){
    $sort_order = array();
    $elements = array();
    $del = Delimiter();
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOcountry_simple();
    if(isset($ItemTitleDetail['title'])) {
        $ItemTitle = $ItemTitleDetail['title'];
    }
    if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_country();
        $sort_order[$i] = osc_get_preference('country_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('country_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_COUNTRY_DESC(){
    $sort_order = array();
    $elements = array();
    $del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOcountry_simple();
    if(isset($ItemTitleDetail['description'])) {
        $ItemTitle = $ItemTitleDetail['description'];
    }
    if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_country();
        $sort_order[$i] = osc_get_preference('country_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('country_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_COUNTRY_KEY(){
    $sort_order = array();
    $elements = array();
	$del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOcountry_simple();
    if(isset($ItemTitleDetail['keywords'])) {
        $ItemTitle = $ItemTitleDetail['keywords'];
    }
    if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_country();
        $sort_order[$i] = osc_get_preference('country_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_country_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('country_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_REGION_TITLE(){
    $sort_order = array();
    $elements = array();
    $del = Delimiter();
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOregion_simple();
    if(isset($ItemTitleDetail['title'])) {
        $ItemTitle = $ItemTitleDetail['title'];
    }
    if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_region();
        $sort_order[$i] = osc_get_preference('region_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('region_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_REGION_DESC(){
    $sort_order = array();
    $elements = array();
    $del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOregion_simple();
    if(isset($ItemTitleDetail['description'])) {
        $ItemTitle = $ItemTitleDetail['description'];
    }
    if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_region();
        $sort_order[$i] = osc_get_preference('region_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('region_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_REGION_KEY(){
    $sort_order = array();
    $elements = array();
	$del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOregion_simple();
    if(isset($ItemTitleDetail['keywords'])) {
        $ItemTitle = $ItemTitleDetail['keywords'];
    }
    if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_region();
        $sort_order[$i] = osc_get_preference('region_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_region_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('region_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_CITY_TITLE(){
    $sort_order = array();
    $elements = array();
    $del = Delimiter();
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOcity_simple();
    if(isset($ItemTitleDetail['title'])) {
        $ItemTitle = $ItemTitleDetail['title'];
    }
    if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_city();
        $sort_order[$i] = osc_get_preference('city_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('city_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_CITY_DESC(){
    $sort_order = array();
    $elements = array();
    $del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOcity_simple();
    if(isset($ItemTitleDetail['description'])) {
        $ItemTitle = $ItemTitleDetail['description'];
    }
    if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_city();
        $sort_order[$i] = osc_get_preference('city_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('city_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_CITY_KEY(){
    $sort_order = array();
    $elements = array();
	$del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOcity_simple();
    if(isset($ItemTitleDetail['keywords'])) {
        $ItemTitle = $ItemTitleDetail['keywords'];
    }
    if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
        $elements[$i] = osc_search_city();
        $sort_order[$i] = osc_get_preference('city_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_city_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('city_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_CATEGORY_TITLE(){
    $sort_order = array();
    $elements = array();
    $del = Delimiter();
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOCategory_simple();
    if(isset($ItemTitleDetail['title'])) {
        $ItemTitle = $ItemTitleDetail['title'];
    }
    if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
        $dCategory = osc_search_category_id()[0];
    $cat = Category::newInstance()->findByPrimaryKey($dCategory);
        $elements[$i] = $cat['s_name'];
        $sort_order[$i] = osc_get_preference('category_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('category_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_CATEGORY_DESC(){
    $sort_order = array();
    $elements = array();
    $del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOCategory_simple();
    if(isset($ItemTitleDetail['description'])) {
        $ItemTitle = $ItemTitleDetail['description'];
    }
    if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
        $dCategory = osc_search_category_id()[0];
    $cat = Category::newInstance()->findByPrimaryKey($dCategory);
        $elements[$i] = $cat['s_name'];
        $sort_order[$i] = osc_get_preference('category_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('category_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
function SEOHPLUGIN_CATEGORY_KEY(){
    $sort_order = array();
    $elements = array();
	$del = ', ';
    $i = 0;

    $ItemTitleDetail = ModelSeoplugin::newInstance()->getSEOCategory_simple();
    if(isset($ItemTitleDetail['keywords'])) {
        $ItemTitle = $ItemTitleDetail['keywords'];
    }
    if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
	$dCategory = osc_search_category_id()[0];
    $cat = Category::newInstance()->findByPrimaryKey($dCategory);
        $elements[$i] = $cat['s_name'];
        $sort_order[$i] = osc_get_preference('category_number','seo_plugin');
        $i = $i + 1;
    }
    if(osc_get_preference('simple_category_enable','seo_plugin') == 1){
    $elements[$i] = $ItemTitle;
    $sort_order[$i] = osc_get_preference('category_text_number','seo_plugin');
    $i = $i + 1;
    }
    array_multisort($sort_order, $elements);
    $elements = array_filter( $elements );
    return implode($del, $elements);
}
if(!function_exists('seo_message_ok')) {
  function seo_message_ok( $text ) {
    $textin  = '<div id="flashmessage" class="flashmessage flashmessage-ok" style="display: block;"><a class="btn ico btn-mini ico-close">x</a>';
    $textin .= $text;
    $textin .= '</div>';
    echo $textin;
  }
}
$freq = osc_get_preference('seoplugin_sitemap_freq', 'seo_plugin');
if( $freq == 'weekly' ){
    osc_add_hook('cron_weekly', 'generate_sitemap');
}elseif( $freq == 'daily' ){
    osc_add_hook('cron_daily', 'generate_sitemap');
}elseif( $freq == 'hourly' ){
    osc_add_hook('cron_hourly', 'generate_sitemap');
}
//Canonical
function seo_pro_canonical()
{
    $location = Rewrite::newInstance()->get_location();
    $section  = Rewrite::newInstance()->get_section();

    $canonical = "";

    switch ($location) {
        case ('item'):
            switch ($section) {
                case 'item_add':
                    $canonical = osc_item_post_url();
                    break;
                case 'item_edit':
                    $canonical = osc_item_edit_url();
                    break;
                case 'send_friend':
                    $canonical = osc_item_send_friend_url();
                    break;
                case 'contact':
                    $canonical = osc_item_contact_url();
                    break;
                default:
                    $canonical = osc_item_url();
                    break;
            }
            break;
        case ('page'):
            $canonical = osc_static_page_url();
            break;
        case ('search'):
            $params = array(
                'sShowAs'    => null,
                'sOrder'     => null,
                'iOrderType' => null,
                'sPriceMin'  => null,
                'sPriceMax'  => null,
				'sCompany'  => null
            );

            if (Params::getParam('sCity') != "") {
                $params['sRegion'] = null;
                $params['sCountry'] = null;
            } elseif (Params::getParam('sRegion') != "") {
                $params['sCountry'] = null;
            }

            if (Params::existParam('sCategory') && is_numeric(Params::getParam('sCategory'))) {
                $category = Category::newInstance()->findByPrimarykey(Params::getParam('sCategory'));
                if ($category  && osc_rewrite_enabled ()) {
                    $params['sCategory'] = $category['s_slug'];
                }
            }
            $canonical = osc_update_search_url($params);

            break;
			
        case ('login'):
            $canonical = osc_user_login_url();
            break;
			
        case ('register'):
            $canonical = osc_register_account_url();
            break;

        case ('contact'):
            $canonical = osc_contact_url();
            break;
        case ('custom'):
            break;
        case (''):
            switch ($section) {
                case (''):
                    
                    $canonical = osc_base_url();
                break;
            }
            break;
        default:
            break;
    }

    if (osc_get_preference('canonical_enable', 'seo_plugin')) {
       View::newInstance()->_exportVariableToView('canonical', $canonical);
    }

    if (osc_get_preference('canonical_add_to_theme', 'seo_plugin') && osc_get_canonical() != '' ) {
        echo '<link rel="canonical" href="' . osc_get_canonical() . '"/>';
    }
}

function seo_pro_item_tags() {
  $item_id = osc_item_id();

  if(isset($item_id) && $item_id != '' && $item_id > 0 && osc_is_ad_page()) {


    $text = '' . PHP_EOL . PHP_EOL;
    $item = Item::newInstance()->findByPrimaryKey( $item_id );
    $resource = ItemResource::newInstance()->getResource( $item_id );

    if(isset($resource['s_name']) && $resource['s_name'] != '') {
      $resource_url = osc_base_url() . $resource['s_path'] . $resource['pk_i_id'] . '.' . $resource['s_extension'];
    } else {
      $resource_url = osc_base_url() . 'oc-content/plugins/seo_plugin/img/no-photo.png';
    }

    $locale = osc_current_user_locale();
    $site_title = osc_page_title();
    $item_title = $item['s_title'];
    $item_desc = osc_highlight( $item['s_description'], 500 );
    $item_url = osc_item_url();
    $item_price = $item['i_price'];  
    $country = $item['s_country'];
    $region = $item['s_region'];
    $city = $item['s_city'];
    $address = $item['s_address'];
    $author = $item['s_contact_name'];
    $currency_code = $item['fk_c_currency_code'];
 
    $location = array($country, $region, $city, $address);
    $location = array_filter($location);
    $location = implode(', ', $location);


    $currency_full = Currency::newInstance()->findByPrimaryKey($currency_code);
    $currency_symbol = isset($currency_full['s_description']) ? $currency_full['s_description'] : '';


    if($item_price == '') {
      $price = __('Check with seller', 'seo_plugin');
    } else if($item_price == 0) {
      $price = __('Free', 'seo_plugin');
    } else {
      $price = round($item['i_price']/1000000, 2); 
    }
	
	if(osc_get_preference('schema_enable','seo_plugin') == 1) {
      $text .= '<!-- Schema Tags-->' . PHP_EOL;
	  $text .= '<script type="application/ld+json">' . PHP_EOL;
      $text .= '{' . PHP_EOL; 
      $text .= '"@context": "https://schema.org",' . PHP_EOL;
$text .= '"@type": "Product",' . PHP_EOL;
$text .= '"name": "' . osc_esc_html( $item_title ) . '",' . PHP_EOL;
$text .= '"image": "' . osc_esc_html( $resource_url ) . '",' . PHP_EOL;
$text .= '"description":"' . osc_esc_html( $item_desc ) . '",' . PHP_EOL;
$text .= '"offers": {' . PHP_EOL;
$text .= '"@type": "Offer",' . PHP_EOL;
$text .= '"availability": "http://schema.org/InStock",' . PHP_EOL;
$text .= '"price": "' . osc_esc_html( $price ) . '",' . PHP_EOL;
if($item_price != '' && $item_price > 0) { $text .= '"priceCurrency": "' . osc_esc_html( $currency_symbol ) . '"' . PHP_EOL; } 
$text .= '}' . PHP_EOL;
$text .= '}' . PHP_EOL;
$text .= '</script>' . PHP_EOL . PHP_EOL;
 }


    if(osc_get_preference('fb_enable','seo_plugin') == 1) {
      $text .= '<!-- Facebook Open Graph Tags-->' . PHP_EOL;
      $text .= '<meta property="og:title" content="' . osc_esc_html( $item_title ) . '" />' . PHP_EOL;
      $text .= '<meta property="og:site_name" content="' . osc_esc_html( $site_title ) . '"/>' . PHP_EOL;
      $text .= '<meta property="og:url" content="' . $item_url . '" />' . PHP_EOL;
      $text .= '<meta property="og:description" content="' . osc_esc_html( $item_desc ) . '" />' . PHP_EOL;
      $text .= '<meta property="og:locale" content="' . osc_esc_html( $locale ) . '" />' . PHP_EOL;
      $text .= '<meta property="og:place" content="' . osc_esc_html( $location ) . '" />' . PHP_EOL;
      $text .= '<meta property="og:type" content="product" />' . PHP_EOL;
      $text .= '<meta property="product:availability" content="' . __('Available', 'seo_plugin') . '" />' . PHP_EOL;
      $text .= '<meta property="product:retailer_item_id" content="' . osc_esc_html( $item_id ) . '" />' . PHP_EOL;
      $text .= '<meta property="product:price:amount" content="' . osc_esc_html( $price ) . '" />' . PHP_EOL;
      if($item_price != '' && $item_price > 0) { $text .= '<meta property="product:price:currency" content="' . osc_esc_html( $currency_symbol ) . '" />' . PHP_EOL; }
      $text .= '<meta property="og:image" content="' . $resource_url . '" />' . PHP_EOL . PHP_EOL;
    }


    if(osc_get_preference('twitter_enable','seo_plugin') == 1) {
      $text .= '<!-- Twitter Tags-->' . PHP_EOL;
      $text .= '<meta name="twitter:card" content="summary" />' . PHP_EOL;
      if($author != __('Anonymous', 'seo_plugin')) { $text .= '<meta name="twitter:site" content="@' . osc_esc_html( $author ) . '" />' . PHP_EOL; }
      $text .= '<meta name="twitter:title" content="' . osc_esc_html( $item_title ) . '" />' . PHP_EOL;
      $text .= '<meta name="twitter:description" content="' . osc_esc_html( $item_desc ) . '" />' . PHP_EOL;
      $text .= '<meta name="twitter:image" content="' . osc_esc_html( $resource_url ) . '" />' . PHP_EOL . PHP_EOL;
    }

    echo $text;
  }
}


osc_add_route('seoplugin-admin-config', 'seo_plugin/admin/config', 'seo_plugin/admin/config', osc_plugin_folder(__FILE__).'admin/config.php');
osc_add_route('seoplugin-admin-categories', 'seo_plugin/admin/categories', 'seo_plugin/admin/categories', osc_plugin_folder(__FILE__).'admin/categories.php');
osc_add_route('seoplugin-admin-country', 'seo_plugin/admin/country', 'seo_plugin/admin/country', osc_plugin_folder(__FILE__).'admin/country.php');
osc_add_route('seoplugin-admin-city', 'seo_plugin/admin/city', 'seo_plugin/admin/city', osc_plugin_folder(__FILE__).'admin/city.php');
osc_add_route('seoplugin-admin-home', 'seo_plugin/admin/home', 'seo_plugin/admin/home', osc_plugin_folder(__FILE__).'admin/home.php');
osc_add_route('seoplugin-admin-pages', 'seo_plugin/admin/pages', 'seo_plugin/admin/pages', osc_plugin_folder(__FILE__).'admin/pages.php');
osc_add_route('seoplugin-admin-regions', 'seo_plugin/admin/regions', 'seo_plugin/admin/regions', osc_plugin_folder(__FILE__).'admin/regions.php');
osc_add_route('seoplugin-admin-robots', 'seo_plugin/admin/robots', 'seo_plugin/admin/robots', osc_plugin_folder(__FILE__).'admin/robots.php');
osc_add_route('seoplugin-admin-sitemap', 'seo_plugin/admin/sitemap', 'seo_plugin/admin/sitemap', osc_plugin_folder(__FILE__).'admin/sitemap.php');
osc_add_route('seoplugin-admin-canonical', 'seo_plugin/admin/canonical', 'seo_plugin/admin/canonical', osc_plugin_folder(__FILE__).'admin/canonical.php');
osc_add_route('seoplugin-admin-tags', 'seo_plugin/admin/tags', 'seo_plugin/admin/tags', osc_plugin_folder(__FILE__).'admin/tags.php');
osc_add_route('seoplugin-admin-hteditor', 'seo_plugin/admin/hteditor', 'seo_plugin/admin/hteditor', osc_plugin_folder(__FILE__).'admin/hteditor.php');

osc_register_plugin(osc_plugin_path(__FILE__), 'seoplugin_install');
    osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'seoplugin_configure_link');
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'seoplugin_uninstall');
    osc_add_hook(osc_plugin_path(__FILE__)."_enable", 'seoplugin_version_update');

osc_add_hook('admin_menu_init', 'seoplugin_admin_menu');
osc_add_hook("header", "seo_pro_canonical");
osc_add_hook('header', 'seo_pro_item_tags');
osc_add_hook('admin_header', 'seoplugin_init_admin_page_header');


osc_add_filter('custom_plugin_title','seopluginTitle');
osc_add_filter('meta_title_filter', 'seoplugin_title');
osc_add_filter('meta_description_filter', 'seoplugin_description');
osc_add_filter('meta_keywords_filter', 'seoplugin_keywords');


?>