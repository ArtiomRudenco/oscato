<?php if ( (!defined('ABS_PATH')) ) exit('ABS_PATH is not loaded. Direct access is not allowed.'); ?>
<?php if ( !OC_ADMIN ) exit('User access is not allowed.'); ?>
<?php
 /*
 * Copyright 2016 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */

if(Params::getParam('plugin_action')=='done') {
osc_set_preference('item_city_enable', Params::getParam("item_city_enable") ? Params::getParam("item_city_enable") : '0', 'seo_plugin','INTEGER');
osc_set_preference('item_body_number', Params::getParam("item_body_number") ? Params::getParam("item_body_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_category_number', Params::getParam("item_category_number") ? Params::getParam("item_category_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_category_enable', Params::getParam("item_category_enable") ? Params::getParam("item_category_enable") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_country_number', Params::getParam("item_country_number") ? Params::getParam("item_country_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_country_enable', Params::getParam("item_country_enable") ? Params::getParam("item_country_enable") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_region_number', Params::getParam("item_region_number") ? Params::getParam("item_region_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_region_enable', Params::getParam("item_region_enable") ? Params::getParam("item_region_enable") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_city_number', Params::getParam("item_city_number") ? Params::getParam("item_city_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_title_number', Params::getParam("item_title_number") ? Params::getParam("item_title_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('item_title_enable', Params::getParam("item_title_enable") ? Params::getParam("item_title_enable") : '0', 'seo_plugin', 'INTEGER'); 
osc_set_preference('item_title_first', Params::getParam("item_title_first") ? Params::getParam("item_title_first") : '0', 'seo_plugin', 'INTEGER'); 
osc_set_preference('delimiter', Params::getParam("delimiter") ? Params::getParam("delimiter") : '|', 'seo_plugin', 'STRING'); 
osc_set_preference('help_config', Params::getParam("help_config") ? Params::getParam("help_config") : '1', 'seo_plugin', 'INTEGER');
  osc_reset_preferences();
  osc_add_flash_ok_message(__('Settings saved', 'seo_plugin'), 'admin');
  ob_get_clean();
  osc_redirect_to(osc_route_admin_url('seoplugin-admin-config'));
}
?>
<?php require_once 'top_menu.php'; ?>
<h2><i class="fa fa-cog"></i> <?php _e('Settings for Item page','seo_plugin'); ?></h2>
<div id="items">
<div class="well" >
  <div class="seo_help_button"><?php if(osc_get_preference('help_config', 'seo_plugin') == 0){echo 'open help';}else{echo 'close help';} ?></div>
  <div class="seo_help" <?php if(osc_get_preference('help_config', 'seo_plugin') == 0){echo 'style="display:none"';} ?>>
    <div class="seo_help_text">
      <?php _e('In this section you can configure the ad title. Selects whether to show it in the category, city, region, country and site name.', 'seo_plugin'); ?><br>
      <?php _e('You can choose one or more functions. It also set the sequence of their output.', 'seo_plugin'); ?><br>
      <?php _e('Delimiter is used in every meta title', 'seo_plugin'); ?>
    </div>
  </div>

  <form name="upayments_form" id="upayments_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
    <input type="hidden" name="page" value="plugins" />
    <input type="hidden" name="action" value="renderplugin" />
    <input type="hidden" name="route" value="seoplugin-admin-config" />
    <input type="hidden" name="plugin_action" value="done" />
    <input type="hidden" name="help_config" id="help_config" value="<?php echo osc_get_preference('help_config', 'seo_plugin'); ?>" />
    <input type="hidden" name="item_city_number" id="item_city_number" value="<?php echo osc_get_preference('item_city_number', 'seo_plugin'); ?>">
    <input type="hidden" name="item_region_number" id="item_region_number" value="<?php echo osc_get_preference('item_region_number', 'seo_plugin'); ?>">
    <input type="hidden" name="item_country_number" id="item_country_number" value="<?php echo osc_get_preference('item_country_number', 'seo_plugin'); ?>">
    <input type="hidden" name="item_category_number" id="item_category_number" value="<?php echo osc_get_preference('item_category_number', 'seo_plugin'); ?>">
    <input type="hidden" name="item_title_number" id="item_title_number" value="<?php echo osc_get_preference('item_title_number', 'seo_plugin'); ?>">
    <input type="hidden" name="item_body_number" id="item_body_number" value="<?php echo osc_get_preference('item_body_number', 'seo_plugin'); ?>">

<table>
  <tr>
    <td class="seo_info"><span>
    <?php _e("Show the city in the title", 'seo_plugin'); ?></span>
    </td><td>
    <label class="switch"><input type="checkbox" <?php echo (osc_get_preference('item_city_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="item_city_enable" id="item_city_enable" value="1" >
    <span class="slider round"></span>
</label> </td>
    </tr>
  <tr>
    <td class="seo_info"><span>
  <?php _e("Show the region in the title", 'seo_plugin'); ?></span>
    </td><td>
       <label class="switch"> <input type="checkbox" <?php echo (osc_get_preference('item_region_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="item_region_enable" id="item_region_enable" value="1" >
    <span class="slider round"></span>
</label> </td>
  </tr>
  <tr>
    <td class="seo_info"><span><?php _e("Show the country in the title", 'seo_plugin'); ?></span>
    </td><td>
        <label class="switch"><input type="checkbox" <?php echo (osc_get_preference('item_country_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="item_country_enable" id="item_country_enable" value="1" >
    <span class="slider round"></span>
</label> </td>
  </tr>
  <tr>
    <td class="seo_info"><span><?php _e("Show the category in the title", 'seo_plugin'); ?></span>
    </td><td>
        <label class="switch"><input type="checkbox" <?php echo (osc_get_preference('item_category_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="item_category_enable" id="item_category_enable" value="1" >
    <span class="slider round"></span>
</label> </td>
  </tr>
  <tr>
    <td class="seo_info"><span><?php _e("Show the site name in the title", 'seo_plugin'); ?></span>
    </td>
  <td>
        <label class="switch"><input type="checkbox" <?php echo (osc_get_preference('item_title_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="item_title_enable" id="item_title_enable" value="1" >
<span class="slider round"></span>
</label> </td></tr>
  <tr>
    <td><?php _e('Delimiter (used in title): ', 'seo_plugin'); ?></td>

    <td><div class="div_seo_meta"><input type="text" style="width:100px;" id="delimiter" name="delimiter" value="<?php echo osc_get_preference('delimiter', 'seo_plugin'); ?>" /></div></td>
  </tr>
</table>
<br>
  <br><br>
  <strong><?php _e('Current Order of Item Title','seo_plugin'); ?></strong><br><br>
  <div id="box">
    <?php
    foreach(ActulatN() as $name_n){
      echo '<div id="'.$name_n['id'].'" class="seo_title_el">
                <div class="seo_title_el_name">'.$name_n['name'].'</div>
                <div id="left" class="seo_title_button"> < </div>
                <div id="right" class="seo_title_button"> > </div>
                </div>';
    }
    ?>
  </div>

  <div class="clear"></div>
    <div class="seo_button">
      <button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
    </div>
    <div class="clear"></div>

  </form>


  <div class="seo_copyright">
	<span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>
<script type="text/javascript">
  $('#promo_form').ready(function(){

    function addTitleElement(id_el, name_el){
      $('<div id="'+id_el+'" class="seo_title_el">' +
          '<div class="seo_title_el_name">'+name_el+'</div>' +
          '<div id="left" class="seo_title_button"> < </div>' +
          '<div id="right" class="seo_title_button"> > </div>' +
          '</div>').appendTo('#box');
    }

    $(document).on("click", "input[name='item_city_enable']", function(){  // Add and delete elements
      if($(this).attr('checked') == 'checked'){
        addTitleElement('city', '<?php echo __('City', 'seo_plugin')?>');
      }else{
        $('#city').remove();
      }

    });
    $(document).on("click", "input[name='item_region_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('region', '<?php echo __('Region', 'seo_plugin')?>');
      }else{
        $('#region').remove();
      }
    });
    $(document).on("click", "input[name='item_country_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('country', '<?php echo __('Country', 'seo_plugin')?>');
      }else{
        $('[id="country"]').remove();
      }
    });
    $(document).on("click", "input[name='item_category_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('category', '<?php echo __('Category', 'seo_plugin')?>');
      }else{
        $('[id="category"]').remove();
      }
    });
    $(document).on("click", "input[name='item_title_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('site_name', '<?php echo __('Site Title', 'seo_plugin')?>');
      }else{
        $('[id="site_name"]').remove();
      }
    });

    $(document).on("click", ".seo_title_button", function(){
      var rout = $(this).attr('id');
      var el_out = $(this).parent();
      var id_out = $(this).parent().attr('id');
      if(rout == 'left'){
        var el_to = $(el_out).prev();
        var id_to = $(this).parent().prev().attr('id');
      }
      if(rout == 'right'){
        var el_to = $(el_out).next();
        var id_to = $(this).parent().next().attr('id');
      }
      var htm_out = $(el_out).html();
      var htm_to = $(el_to).html();
      $(el_out).html(htm_to);
      $(el_to).html(htm_out);
      $(el_out).attr('id', id_to);
      $(el_to).attr('id', id_out);
    });

    $(document).on("click", "#theButton", function(){ // Save Parameters
      var elems = $('.seo_title_el');
      for (var i = 0; i < elems.length; i++) {
        name_el = $(elems[i]).attr('id');
        num_id = $(elems[i]).index();
        if(name_el == 'city'){
          $("input[name='item_city_number']").val(num_id);
        }
        if(name_el == 'region'){
          $("input[name='item_region_number']").val(num_id);
        }
        if(name_el == 'country'){
          $("input[name='item_country_number']").val(num_id);
        }
        if(name_el == 'category'){
          $("input[name='item_category_number']").val(num_id);
        }
        if(name_el == 'site_name'){
          $("input[name='item_title_number']").val(num_id);
        }
        if(name_el == 'title'){
          $("input[name='item_body_number']").val(num_id);
        }
      }

    })
  });
</script>
</div>