$(document).ready(function() {
    var doc_w = $(document).width();
    var with_menu = Math.round((doc_w - 800) / 16);
    $('nav ul li a').css('padding-left', with_menu+'px');
    $('nav ul li a').css('padding-right', with_menu+'px');

    $('.seo_category').click(function(){
      var id = $(this).attr('id');
      //$('#'+id+'box').toggle();
      $('#'+id+'box').slideToggle("slow");
      if($('#'+id+'button').text() == 'open'){
        $('#'+id+'button').text('close');
     }else{
        $('#'+id+'button').text('open');
      }
    })

    $('.seo_help_button').click(function(){
      $('.seo_help').slideToggle("slow");
      if($('.seo_help_button').text() == 'close help'){
        $('.seo_help_button').text('open help');
        $("input[name='help']").val(0);
      }else{
        $('.seo_help_button').text('close help');
        $("input[name='help']").val(1);
      }
    });
            
  })

