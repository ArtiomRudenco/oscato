<?php
	if(Params::getParam('plugin_action')=='done') {
            $htaccess_file = osc_base_path() . '.htaccess' ;
            $edit_htaccess  = Params::getParam('edit_htaccess',false,false);

            $status = 0;
            if( file_exists($htaccess_file) ) {
                if( is_writable($htaccess_file) && file_put_contents($htaccess_file, $edit_htaccess) ) {
                    $status = 1;
                }
            } else {
                if( is_writable(osc_base_path()) && file_put_contents($htaccess_file, $edit_htaccess) ) {
                    $status = 1;
                }
            }

            if($status==1) {
                osc_add_flash_ok_message(__('File updated successfully', 'seo_plugin'), 'admin');
            } else {
                osc_add_flash_ok_message(__('There were a problem updating the file', 'seo_plugin'), 'admin');
            }
			ob_get_clean();
            osc_redirect_to(osc_route_admin_url('seoplugin-admin-hteditor'));

}
?>
<?php require_once 'top_menu.php'; ?>
<div id="regions">
<h2><i class="fa fa-file"></i> <?php _e('HTACCESS Editor','seo_plugin'); ?></h2>
<div class="well">
 <?php  $htaccessfile = osc_base_path() . '.htaccess';
    if(file_exists(osc_base_path() . '.htaccess')) {
        $htaccess = file_get_contents($htaccessfile);
?>
<form action="<?php echo osc_admin_base_url(true); ?>" method="post">
    <input type="hidden" name="page" value="plugins" />
    <input type="hidden" name="action" value="renderplugin" />
    <input type="hidden" name="route" value="seoplugin-admin-hteditor" />
    <input type="hidden" name="plugin_action" value="done" />
    <fieldset>
        <div class="form-horizontal">
 			<div class="form-row input-description-wide">
                <div class="form-label"><?php _e('Htaccess Editor', 'seo_plugin'); ?></div>
                <div class="form-controls"  ><textarea name="edit_htaccess" rows="10"><?php echo $htaccess; ?></textarea></div>
                <div class="help-box" style="color:red;"><?php _e('Make a backup before making any changes to your htaccess file', 'seo_plugin'); ?></div>
            </div>
                 
            
                <input type="submit" value="<?php _e('Save changes', 'seo_plugin'); ?>" class="btn btn-submit" />
            
        </div>
    </fieldset>
</form>
<style type="text/css">
.html4strict  {font-family:monospace;color: #006; border: 1px solid #d0d0d0; background-color: #f0f0f0;}
.html4strict a:link {color: #000060;}
.html4strict a:hover {background-color: #f0f000;}
.html4strict .imp {font-weight: bold; color: red;}
.html4strict .kw2 {color: #000000; font-weight: bold;}
.html4strict .kw3 {color: #000066;}
.html4strict .es0 {color: #000099; font-weight: bold;}
.html4strict .br0 {color: #66cc66;}
.html4strict .sy0 {color: #66cc66;}
.html4strict .st0 {color: #ff0000;}
.html4strict .nu0 {color: #cc66cc;}
.html4strict .sc-1 {color: #808080; font-style: italic;}
.html4strict .sc0 {color: #00bbdd;}
.html4strict .sc1 {color: #ddbb00;}
.html4strict .sc2 {color: #009900;}
.html4strict span.xtra { display:block; }
</style>
<h2><?php _e('Default Osclass rules:', 'seo_plugin'); ?></h2>
<div class="apache" style="font-family:monospace;color: #006; border: 1px solid #d0d0d0; background-color: #f0f0f0;">
<span class="sc2">&lt;IfModule mod_rewrite.c&gt;</span><br />
<span style="color: #00007f;">RewriteEngine</span> <span style="color: #0000ff;">On</span><br />
<span style="color: #00007f;">RewriteBase</span> <?php echo REL_WEB_URL ; ?><br />
<span style="color: #00007f;">RewriteRule</span> ^index\.php$ - <span style="color: #339933;">&#91;</span>L<span style="color: #339933;">&#93;</span><br />
<span style="color: #00007f;">RewriteCond</span> %<span style="color: #339933;">&#123;</span>REQUEST_FILENAME<span style="color: #339933;">&#125;</span> <span style="color: #008000;">!-</span>f<br />
<span style="color: #00007f;">RewriteCond</span> %<span style="color: #339933;">&#123;</span>REQUEST_FILENAME<span style="color: #339933;">&#125;</span> <span style="color: #008000;">!-</span>d<br />
<span style="color: #00007f;">RewriteRule</span> . <?php echo REL_WEB_URL ; ?>index.php <span style="color: #339933;">&#91;</span>L<span style="color: #339933;">&#93;</span><br />
<span class="sc2">&lt;<span class="sy0">/</span>IfModule&gt;</span><br /></div>
<?php } else { ?>
        <form action="#" method="post">
            <fieldset>
                <div class="form-horizontal">
                    <div class="form-row input-description-wide">
                        <div class="help-box" style="color:red;"><?php _e('.htaccess file does not exist or is not readable. Please enable Permalinks', 'seo_plugin'); ?></div>
                    </div>
                </div>
            </fieldset>
        </form>
<?php } ?>
        <div class="clear"></div>
<div class="seo_copyright">
    <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
</div>
<div class="clear"></div>
</div>
</div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>