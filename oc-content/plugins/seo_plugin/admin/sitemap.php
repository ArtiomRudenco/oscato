<?php

if(Params::getParam('plugin_action')=='done') {
	osc_set_preference('sitemap_number', Params::getParam("sitemap_number") ? Params::getParam("sitemap_number") : '50000', 'seo_plugin', 'INTEGER');
        osc_set_preference('sitemap_categories', Params::getParam("sitemap_categories") ? Params::getParam("sitemap_categories") : '0', 'seo_plugin','INTEGER');
        osc_set_preference('sitemap_country', Params::getParam("sitemap_country") ? Params::getParam("sitemap_country") : '0', 'seo_plugin', 'INTEGER');
		osc_set_preference('sitemap_countries', Params::getParam("sitemap_countries") ? Params::getParam("sitemap_countries") : '0', 'seo_plugin', 'INTEGER');
        osc_set_preference('sitemap_regions', Params::getParam("sitemap_regions") ? Params::getParam("sitemap_regions") : '0', 'seo_plugin', 'INTEGER');
		osc_set_preference('sitemap_cities', Params::getParam("sitemap_cities") ? Params::getParam("sitemap_cities") : '0', 'seo_plugin', 'INTEGER');
		osc_set_preference('help_sitemap', Params::getParam("help_sitemap") ? Params::getParam("help_sitemap") : '0', 'seo_plugin', 'INTEGER');
		osc_set_preference('seoplugin_sitemap_freq', Params::getParam("seoplugin_sitemap_freq") ? Params::getParam("seoplugin_sitemap_freq") : 'daily', 'seo_plugin', 'STRING');
        osc_set_preference('sitemap_pages', Params::getParam("sitemap_pages") ? Params::getParam("sitemap_pages") : '3', 'seo_plugin','INTEGER');
		osc_reset_preferences();
	seo_message_ok(__('Settings saved','seo_plugin'));
}

if(Params::getParam('plugin_action')=='generate_sitemap'){
	$start = microtime(true);
    generate_sitemap();
	$end = microtime(true);
    $execution = ($end - $start);
    $memory = (memory_get_peak_usage()/1024)/1024 ;
	seo_message_ok(__('Sitemap generated Memory Used: '.round($memory,2). 'MB, Total time '.round($execution,2).' sec','seo_plugin'));
    
}


?>
<?php require_once 'top_menu.php'; ?>
<div id="sitemap">
    <h2><i class="fa fa-cog"></i> <?php _e('Sitemap Generator', 'seo_plugin'); ?></h2>
    <div class="well" >
        <div class="seo_help_button"><?php if(osc_get_preference('help_sitemap', 'seo_plugin') == 0){echo 'open help';}else{echo 'close help';} ?></div>
        <div class="seo_help" <?php if(osc_get_preference('help_sitemap', 'seo_plugin') == 0){echo 'style="display:none"';} ?>>
            <div class="seo_help_text">
                <?php _e('You can customize the generation schedule: once a week, hour or day. For this job need use cron, read more: https://doc.osclass.org/Cron', 'seo_plugin'); ?><br>
                <?php _e('After each generation of sitemap, a request is sent to <strong>Google</strong>, <strong>Yandex</strong>, <strong>Bing</strong> and <strong>Yahoo</strong> search engines reported that sitemap changed.', 'seo_plugin'); ?><br>
                <?php _e('Max URL in one sitemap. Google allow MAX 50000 url in one sitemap.', 'seo_plugin'); ?><br>
                <?php _e('Max sitemaps with items. You can specify how much you want sitemaps with items.', 'seo_plugin'); ?><br>
			    <?php _e('But remember, the more items on the site, the more resources from the server need to generate a sitemap.', 'seo_plugin'); ?><br>
			    <?php _e('If you give errors when generating sitemap, try to reduce these values or go to a more powerful hosting.', 'seo_plugin'); ?><br>
			</div>
        </div>

        <form action="<?php echo osc_admin_base_url(true); ?>" method="post">
            <input type="hidden" name="page" value="plugins" />
            <input type="hidden" name="action" value="renderplugin" />
            <input type="hidden" name="route" value="seoplugin-admin-sitemap" />
            <input type="hidden" name="plugin_action" value="done" />
            <input type="hidden" name="help_sitemap" value="<?php echo osc_get_preference('help_sitemap', 'seo_plugin'); ?>" />

        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Max URL in one sitemap', 'seo_plugin'); ?></div>
                <div class="form-controls">
                   <input type="text" class="xlarge" name="sitemap_number" value="<?php echo osc_get_preference('sitemap_number', 'seo_plugin'); ?>">
                </div>
            </div>
			<div class="form-row">
                <div class="form-label"><?php _e('Max sitemaps with items', 'seo_plugin'); ?></div>
                <div class="form-controls">
                 <input type="text" class="xlarge" name="sitemap_pages" value="<?php echo osc_get_preference('sitemap_pages', 'seo_plugin'); ?>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Include Categories','seo_plugin'); ?></div>
                <div class="form-controls"><label class="switch">
<input type="checkbox" <?php echo (osc_get_preference('sitemap_categories', 'seo_plugin') ? 'checked="true"' : ''); ?> name="sitemap_categories" value="1" />
               <span class="slider round"></span>
</label> </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Include Countries','seo_plugin'); ?></div>
                <div class="form-controls"><label class="switch">
<input type="checkbox" <?php echo (osc_get_preference('sitemap_countries', 'seo_plugin') ? 'checked="true"' : ''); ?> name="sitemap_countries" value="1" />
                <span class="slider round"></span>
</label></div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Include Regions','seo_plugin'); ?></div>
                <div class="form-controls"><label class="switch">
<input type="checkbox" <?php echo (osc_get_preference('sitemap_regions', 'seo_plugin') ? 'checked="true"' : ''); ?> name="sitemap_regions" value="1" />
                <span class="slider round"></span>
</label></div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Include Cities','seo_plugin'); ?></div>
                <div class="form-controls"><label class="switch">
<input type="checkbox" <?php echo (osc_get_preference('sitemap_cities', 'seo_plugin') ? 'checked="true"' : ''); ?> name="sitemap_cities" value="1" />
               <span class="slider round"></span>
</label> </div>
            </div>
                <div class="form-row">
                <div class="form-label"><?php _e('Generation schedule','seo_plugin'); ?></div>
                <div class="form-controls">
                    <select name="seoplugin_sitemap_freq" id="seoplugin_sitemap_freq">
					<option value="weekly" <?php if(osc_get_preference('seoplugin_sitemap_freq', 'seo_plugin')=="weekly") { echo 'selected="selected"';}; ?> ><?php _e('Weekly','seo_plugin'); ?></option>
					<option value="daily" <?php if(osc_get_preference('seoplugin_sitemap_freq', 'seo_plugin')=="daily") { echo 'selected="selected"';}; ?> ><?php _e('Daily','seo_plugin'); ?></option>
                    <option value="hourly" <?php if(osc_get_preference('seoplugin_sitemap_freq', 'seo_plugin')=="hourly") { echo 'selected="selected"';}; ?> ><?php _e('Hourly','seo_plugin'); ?></option>
                    <option value="none" <?php if(osc_get_preference('seoplugin_sitemap_freq', 'seo_plugin')=="none") { echo 'selected="selected"';}; ?> ><?php _e('None','seo_plugin'); ?></option>   
                    </select>
                </div>
            </div>
                </diV>
            <div class="seo_button">
            <input type="submit" value="<?php _e('Save', 'seo_plugin'); ?>" class="btn btn-submit" />
        </div>

        </form>
		<br><br>
		    <div style="margin: 10px">

        <h4><?php _e('Generated Sitemaps:', 'seo_plugin'); ?></h4>
        
            <?php
			    $home = 'sitemap-index.xml' ;
                if (file_exists(ABS_PATH.'/sitemap-index.xml')){
                    clearstatcache();
                    $lastmod    = date("Y-m-d H:i", filemtime(ABS_PATH.'/'));
                    $arr_file = list_dir_xml(osc_base_path().'/');
                    sort($arr_file, SORT_NATURAL);
                    echo '<ul>';
                    echo '<li><a href="'.osc_base_url().$home.'" target="_blank">'.osc_base_url().$home.'</a></li>';
                    foreach($arr_file as $file){
			if($file != 'sitemap-index.xml'){
                        echo '<li><a href="'.osc_base_url().$file.'" target="_blank">'.osc_base_url().$file.'</a></li>';}
                    }
                    echo '</ul>';
                    ?>
            <h4 style="color:"><?php _e('Last Generate - ', 'seo_plugin');  echo $lastmod; ?></h4>
        <?php } else {?>
            <h4 style="color:red"><?php _e('No Sitemap Generated', 'seo_plugin'); ?></h4>
        <?php } ?>
    </div>
	<form action="<?php echo osc_admin_base_url(true); ?>" method="post">
            <input type="hidden" name="page" value="plugins" />
            <input type="hidden" name="action" value="renderplugin" />
            <input type="hidden" name="route" value="seoplugin-admin-sitemap" />
            <input type="hidden" name="plugin_action" value="generate_sitemap" />
        <div class="seo_button">
        <input type="submit" value="<?php _e('Generate Sitemap', 'seo_plugin'); ?>" class="btn btn-submit" />
        </div>
    </form>
        <div class="clear"></div>
        <div class="seo_copyright">
            <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
        </div>
        <div class="clear"></div>
    </div></div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>
