<?php
 /*
 * Copyright 2016 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
if(Params::getParam('plugin_action')=='done') {
osc_set_preference('simple_region_enable', Params::getParam("simple_region_enable") ? Params::getParam("simple_region_enable") : '0', 'seo_plugin','INTEGER');
osc_set_preference('region_number', Params::getParam("region_number") ? Params::getParam("region_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('region_text_number', Params::getParam("region_text_number") ? Params::getParam("region_text_number") : '0', 'seo_plugin', 'INTEGER');
osc_reset_preferences();
    $detail2 = ModelSeoplugin::newInstance()->getSEOregion_simple();
    if(!empty($detail2['id'])) {
        ModelSeoplugin::newInstance()->updateSEOregion_simple(1, Params::getParam('seo_title'), Params::getParam('seo_description'), Params::getParam('seo_keywords') );
    } else {
        ModelSeoplugin::newInstance()->insertSEOregion_simple(1, Params::getParam('seo_title'), Params::getParam('seo_description'), Params::getParam('seo_keywords') );
    }
    seo_message_ok(__('Settings saved','seo_plugin'));
}

$num_pages = Region::newInstance()->count();
$num_row = 100;
if(Params::getParam('iPage')) {
    $select_page = Params::getParam('iPage') - 1;
    $i = $select_page * $num_row;
    $to = $i + $num_row;
}else{
    $select_page = 0;
    $i = 0;
    $to = 100;
}

$total_page = ceil($num_pages / $num_row);
$params=[
    'total'=>$total_page,
    'selected' =>$select_page,
    'url' => 'index.php?page=plugins&action=renderplugin&route=seoplugin-admin-regions&iPage={PAGE}'
];
?>
<?php require_once 'top_menu.php'; ?>
<div id="regions">
<h2><i class="fa fa-cog"></i> <?php _e('Regions Settings','seo_plugin'); ?></h2>
<div class="well" >
    <div>
	<form name="upayments_form" id="upayments_form" action="<?php echo osc_admin_base_url(true); ?>?page=plugins&action=renderplugin&route=seoplugin-admin-regions<?php if(Params::getParam('iPage')){ echo '&iPage='.Params::getParam('iPage');}?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="plugin_action" value="done" />
        <input type="hidden" name="region_number" id="region_number" value="<?php echo osc_get_preference('region_number', 'seo_plugin'); ?>" />
        <input type="hidden" name="region_text_number" id="region_text_number" value="<?php echo osc_get_preference('region_text_number', 'seo_plugin'); ?>" />
<?php $detail2 = ModelSeoplugin::newInstance()->getSEOregion_simple(); ?>
<div class="div_seo_meta"></div><br>
							<h3 class="h3-bottom-line"><i class="fa fa-cog"></i> <?php _e('Simple','seo_plugin'); ?></h3><br>
							       <table>
							   <tr><td class="seo_info"><span>
  <?php _e('Use simple function', 'seo_plugin'); ?></span>
  <td>
        <label class="switch"><input type="checkbox" <?php echo (osc_get_preference('simple_region_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="simple_region_enable" id="simple_region_enable" value="1" >
    <span class="slider round"></span>
</label></td></tr></table>
	  <?php _e('Enable this option if you want use one Title, Description and Keywords with Region Name.', 'seo_plugin'); ?><br>
	   <div id="bx"><strong><?php _e('Current Order:','seo_plugin'); ?></strong><br><br>
  <div id="box">
    <?php
    foreach(ActulatR() as $name_n){
      echo '<div id="'.$name_n['id'].'" class="seo_title_el">
                <div class="seo_title_el_name">'.$name_n['name'].'</div>
                <div id="left" class="seo_title_button"> < </div>
                <div id="right" class="seo_title_button"> > </div>
                </div>';
    }
    ?>
  </div>
  </div><br>
               <br><div class="div_seo_meta"><?php _e('Title', 'seo_plugin'); ?><br>
			   <input type="text" name="seo_title" id="seo_title" value="<?php if(isset($detail2['title']) != ''){echo $detail2['title']; } ?>" size="40" /></div>
               <div class="div_seo_meta"><?php _e('Meta Description', 'seo_plugin'); ?><br>
			   <textarea name="seo_description" id="seo_description" /><?php if(isset($detail2['description']) != ''){echo $detail2['description']; } ?></textarea></div>
                <div class="div_seo_meta"><?php _e('Meta Keywords', 'seo_plugin'); ?><br>
				<input type="text" name="seo_keywords" id="seo_keywords" value="<?php if(isset($detail2['keywords']) != ''){echo $detail2['keywords']; } ?>" size="20" /></div>
<br><br><br><h3 class="h2-bottom-line"><i class="fa fa-cog"></i> <?php _e('Advanced','seo_plugin'); ?></h3><br>
	  <?php _e('You cand disable Simple option and use Anvanced option for each Region.', 'seo_plugin'); ?><br><br><br>
        <?php
		$detail['title'] = "" ;
        $detail['description'] = "" ;
        $detail['keywords'] = "" ;
        $arrRegions = Region::newInstance()->listAll();
        $num_pages = Region::newInstance()->count();
        foreach($arrRegions as $regions){

        if(Params::getParam('plugin_action')=='done') {
                $detail = ModelSeoplugin::newInstance()->getSEORegionId($regions['pk_i_id']);
                if (isset($detail['id'])) {
                    ModelSeoplugin::newInstance()->updateSEORegion($regions['pk_i_id'], Params::getParam('title' . $regions['pk_i_id']), Params::getParam('description' . $regions['pk_i_id']), Params::getParam('keywords' . $regions['pk_i_id']));
                } else {
                    ModelSeoplugin::newInstance()->insertSEORegion($regions['pk_i_id'], Params::getParam('title' . $regions['pk_i_id']), Params::getParam('description' . $regions['pk_i_id']), Params::getParam('keywords' . $regions['pk_i_id']));
                }
            }
            $detail = ModelSeoplugin::newInstance()->getSEORegionId($regions['pk_i_id']);
            ?>
        <div id="<?php echo $regions['pk_i_id']; ?>" class="seo_category" >
            <div class="seo_category_id"><?php echo $regions['pk_i_id']; ?></div>
            <?php echo $regions['s_name']; ?>
            <div class="seo_detail_box">
                <div id="<?php echo $regions['pk_i_id']; ?>button" class="seoMetaItem" style="width: 50px;float:left;color: #1295bb"><?php if($num_pages > 1){echo 'open';}else{echo 'close';}?></div>
                <div class="seo_detail_box_element">
                    <div><?php if(!empty($detail['title'])){echo "title"; } ?></div>
                    <div><?php if(!empty($detail['description'])){echo "desc"; } ?></div>
                    <div><?php if(!empty($detail['keywords'])){echo "key"; } ?></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div style="padding: 5px; margin-left: 30px; <?php if($num_pages > 1){echo 'display: none';}?>" id="<?php echo $regions['pk_i_id']; ?>box">
            <div class="div_seo_meta">
                <?php _e('Meta Title', 'seo_plugin'); ?><br>
                <input type="text" name="title<?php echo $regions['pk_i_id']; ?>" id="title" value="<?php if(!empty($detail['title'])){echo $detail['title']; } ?>" />
            </div>
            <div class="div_seo_meta">
                <?php _e('Meta Description', 'seo_plugin'); ?><br>
                <textarea name="description<?php echo $regions['pk_i_id']; ?>" id="description" ><?php if(!empty($detail['description'])){echo $detail['description']; } ?></textarea>
            </div>
            <div class="div_seo_meta">
                <?php _e('Meta Keywords', 'seo_plugin'); ?><br>
                <input type="text" name="keywords<?php echo $regions['pk_i_id']; ?>" id="keywords" value="<?php if(!empty($detail['keywords'])){echo $detail['keywords']; } ?>" />
            </div>
        </div>
        <?php
        }
        ?>
        </div>
        <div class="seo_button">
<button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
        </div>
    </form>
<div class="clear"></div>
        <div class="seo_pagination"><?php if($total_page > 1){echo osc_pagination($params);} ?></div>
        <div class="clear"></div>
<div class="seo_copyright">
    <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
</div>
<div class="clear"></div>
</div></div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>
<script type="text/javascript">
  $('#promo_form').ready(function(){

    function addTitleElement(id_el, name_el){
      $('<div id="'+id_el+'" class="seo_title_el">' +
          '<div class="seo_title_el_name">'+name_el+'</div>' +
          '<div id="left" class="seo_title_button"> < </div>' +
          '<div id="right" class="seo_title_button"> > </div>' +
          '</div>').appendTo('#box');
    }

    $(document).on("click", "input[name='simple_region_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('region', '<?php echo __('Region', 'seo_plugin')?>');
      }else{
        $('[id="region"]').remove();
      }
    });
    $(document).on("click", "input[name='simple_region_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('region_text', '<?php echo __('Text', 'seo_plugin')?>');
      }else{
        $('[id="region_text"]').remove();
      }
    });

    $(document).on("click", ".seo_title_button", function(){
      var rout = $(this).attr('id');
      var el_out = $(this).parent();
      var id_out = $(this).parent().attr('id');
      if(rout == 'left'){
        var el_to = $(el_out).prev();
        var id_to = $(this).parent().prev().attr('id');
      }
      if(rout == 'right'){
        var el_to = $(el_out).next();
        var id_to = $(this).parent().next().attr('id');
      }
      var htm_out = $(el_out).html();
      var htm_to = $(el_to).html();
      $(el_out).html(htm_to);
      $(el_to).html(htm_out);
      $(el_out).attr('id', id_to);
      $(el_to).attr('id', id_out);
    });

    $(document).on("click", "#theButton", function(){
      var elems = $('.seo_title_el');
      for (var i = 0; i < elems.length; i++) {
        name_el = $(elems[i]).attr('id');
        num_id = $(elems[i]).index();
        if(name_el == 'region'){
          $("input[name='region_number']").val(num_id);
        }
        if(name_el == 'region_text'){
          $("input[name='region_text_number']").val(num_id);
        }
      }

    })
  });
</script>