<?php 
 /*
 * Copyright 2016 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
if(Params::getParam('plugin_action')=='done') {
  osc_set_preference('help_pages', Params::getParam("help_pages") ? Params::getParam("help_pages") : '1', 'seo_plugin', 'INTEGER');
  osc_add_flash_ok_message(__('Settings saved', 'seo_plugin'), 'admin');
  osc_reset_static_pages();
  
  while(pages_exist()) {
    $pages = ModelSeoplugin::newInstance()->getSEOpage( osc_static_page_id() );
    if(isset($pages['id'])) {
      ModelSeoplugin::newInstance()->updateSEOpage( osc_static_page_id(), Params::getParam('title' . osc_static_page_id()), Params::getParam('description' . osc_static_page_id()), Params::getParam('keywords' . osc_static_page_id()) );
    } else {
      ModelSeoplugin::newInstance()->inserSEOpage( osc_static_page_id(), Params::getParam('title' . osc_static_page_id()), Params::getParam('description' . osc_static_page_id()), Params::getParam('keywords' . osc_static_page_id()) );
    } 
  }
	ob_get_clean();
    osc_redirect_to(osc_route_admin_url('seoplugin-admin-pages'));
} 
?>
<?php require_once 'top_menu.php'; ?>
<div id="pages">
<h2><i class="fa fa-cog"></i> <?php _e('Static Pages Settings','seo_plugin'); ?></h2>
<div class="well" >
  <div class="seo_help_button"><?php if(osc_get_preference('help_pages', 'seo_plugin') == 0){echo 'open help';}else{echo 'close help';} ?></div>
  <div class="seo_help" <?php if(osc_get_preference('help_pages', 'seo_plugin') == 0){echo 'style="display:none"';} ?>>
    <div class="seo_help_text">
      <?php _e('In this section you can configure the title, description and keywords for each static page', 'seo_plugin'); ?>
    </div>
  </div>
  <div style="border-top: 1px #B0B0B0 solid;">
<form name="upayments_form" id="upayments_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
<input type="hidden" name="page" value="plugins" />
<input type="hidden" name="action" value="renderplugin" />
<input type="hidden" name="route" value="seoplugin-admin-pages" />
<input type="hidden" name="plugin_action" value="done" />
<input type="hidden" name="help_pages" value="<?php echo osc_get_preference('help_pages', 'seo_plugin'); ?>" />

  <?php
  osc_reset_static_pages();
  $num_pages = Page::newInstance()->count() - 22;

  while(pages_exist()) {
    $pages = ModelSeoplugin::newInstance()->getSEOpage( osc_static_page_id() ); 
    ?>

        <div id="<?php echo osc_static_page_id(); ?>" class="seo_category" >
          <div class="seo_category_id"><?php echo osc_static_page_id(); ?></div>
          <?php echo osc_static_page_title(); ?>
          <div class="seo_detail_box">
            <div id="<?php echo osc_static_page_id(); ?>button" class="seoMetaItem" style="width: 50px;float:left;color: #1295bb"><?php if($num_pages > 1){echo 'open';}else{echo 'close';}?></div>
            <div class="seo_detail_box_element">
              <div><?php if(isset($pages['title']) != ''){echo "title"; } ?></div>
              <div><?php if(isset($pages['description']) != ''){echo "desc"; } ?></div>
              <div><?php if(isset($pages['keywords']) != ''){echo "key"; } ?></div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
        <div style="padding: 5px; margin-left: 30px; <?php if($num_pages > 1){echo 'display: none';}?>" id="<?php echo osc_static_page_id(); ?>box">
          <div class="div_seo_meta">
            <?php _e('Meta Title', 'seo_plugin'); ?><br>
            <input type="text" name="title<?php echo osc_static_page_id(); ?>" id="title" value="<?php if(isset($pages['title']) != ''){echo $pages['title']; } ?>" />
          </div>
          <div class="div_seo_meta">
            <?php _e('Meta Description', 'seo_plugin'); ?><br>
            <textarea name="description<?php echo osc_static_page_id(); ?>" id="description" ><?php if(isset($pages['description']) != ''){echo $pages['description']; } ?></textarea>
          </div>
          <div class="div_seo_meta">
            <?php _e('Meta Keywords', 'seo_plugin'); ?><br>
            <input type="text" name="keywords<?php echo osc_static_page_id(); ?>" id="keywords" value="<?php if(isset($pages['keywords']) != ''){echo $pages['keywords']; } ?>" />
          </div>
        </div>


  <?php }?>

</div>
  <div class="seo_button">
    <button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
</div>
</form>
  <div class="clear"></div>
  <div class="seo_copyright">
    <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
  </div>
  <div class="clear"></div>
</div>
</div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>