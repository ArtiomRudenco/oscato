<?php
 /*
 * Copyright 2016 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
if(Params::getParam('plugin_action')=='done') {
osc_set_preference('canonical_enable', Params::getParam("canonical_enable") ? Params::getParam("canonical_enable") : '0', 'seo_plugin','INTEGER');
osc_set_preference('canonical_add_to_theme', Params::getParam("canonical_add_to_theme") ? Params::getParam("canonical_add_to_theme") : '0', 'seo_plugin', 'INTEGER');
osc_reset_preferences();
    osc_add_flash_ok_message(__('Settings saved','seo_plugin'), 'admin');
	ob_get_clean();
osc_redirect_to(osc_route_admin_url('seoplugin-admin-canonical'));
}

?>
<?php require_once 'top_menu.php'; ?>
<div id="regions">
<h2><i class="fa fa-link"></i> <?php _e('Canonical Settings','seo_plugin'); ?></h2>
<div class="well" >
    <div>
    <form name="seo_plugin" id="seo_plugin" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="route" value="seoplugin-admin-canonical" />
        <input type="hidden" name="plugin_action" value="done" />
        <input type="hidden" name="help_home" value="<?php echo osc_get_preference('help_home', 'seo_plugin'); ?>" />
<div class="div_seo_meta"></div><br>
<h3 class="h3-bottom-line"><?php _e('Use Canonical function', 'seo_plugin'); ?></h3><br>
<?php _e('Enable this option if you want use canonical urls for site pages and improve SEO. For disable duplicate pages like &sOrder, &iOrderType , sShowAs in search and other.', 'seo_plugin'); ?><br><br>
<label class="switch"><input type="checkbox" <?php echo (osc_get_preference('canonical_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="canonical_enable" id="canonical_enable" value="1" >
             <span class="slider round"></span>
</label><br><br><br>
<h3 class="h3-bottom-line"> <?php _e('Add canonical to theme','seo_plugin'); ?></h3><br>
	  <?php _e('Add canonical urls in your theme, if theme does not do not generate canonical.', 'seo_plugin'); ?><br><br>
	  <label class="switch"><input type="checkbox" <?php echo (osc_get_preference('canonical_add_to_theme', 'seo_plugin') ? 'checked="true"' : ''); ?> name="canonical_add_to_theme" id="canonical_add_to_theme" value="1" >
             <span class="slider round"></span>
</label>
	   </div>
        <div class="seo_button">
<button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
        </div>
    </form>
        <div class="clear"></div>
<div class="seo_copyright">
    <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
</div>
<div class="clear"></div>
</div></div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>