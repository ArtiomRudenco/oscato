<?php
  /*
 * Copyright 2016 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
 $robotstxt = osc_base_path() . 'robots.txt' ;
 if (file_exists($robotstxt)){
 $robots = file_get_contents($robotstxt);}
 else { $robots ='';} 
if(Params::getParam('plugin_action')=='done') {
	$edit_robots  = Params::getParam('edit_robots',false,false);
 osc_set_preference('help_robots', Params::getParam("help_robots") ? Params::getParam("help_robots") : '1', 'seo_plugin', 'INTEGER');
if( file_exists($robotstxt) ) {
if( is_writable($robotstxt) && file_put_contents($robotstxt, $edit_robots) ) {
 $status = 1 ;
    }
      } 
else { 
  if( is_writable(osc_base_path()) && file_put_contents($robotstxt, $edit_robots) ) {
 $status = 1 ;
  }
   }
if ($status = 1 ){ 
   osc_add_flash_ok_message(__('Robots.txt updated successfully', 'seo_plugin'), 'admin');
   } 
else {
   osc_add_flash_error_message(__('It is impossible to write to robots.txt file, please change CHMOD settings on this file', 'seo_plugin'), 'admin');
      }    
  ob_get_clean();
    osc_redirect_to(osc_route_admin_url('seoplugin-admin-robots'));
}
unset($dao_preference) ;
?>
<?php require_once 'top_menu.php'; ?>
<div id="robots">
    <h2><i class="fa fa-cog"></i> <?php _e('Settings robots.txt','seo_plugin'); ?></h2>
<div class="well" >
    <div class="seo_help_button"><?php if(osc_get_preference('help_robots', 'seo_plugin') == 0){echo 'open help';}else{echo 'close help';} ?></div>
    <div class="seo_help" <?php if(osc_get_preference('help_robots', 'seo_plugin') == 0){echo 'style="display:none"';} ?>>
        <div class="seo_help_text">
            <?php _e('There can be formed robots.txt file. You can create a default or its variants.', 'seo_plugin'); ?><br>
            <?php _e('You should always disable admin folder (oc-admin) to be indexed by search engines:', 'seo_plugin'); ?><br>
            <?php echo osc_base_url() . 'oc-admin';?><br>
            <?php _e('It is also desirable to close the registration page, login and password reminder:', 'seo_plugin'); ?><br>
            <?php echo osc_base_url() . 'user/login';?><br>
            <?php echo osc_base_url() . 'user/register';?><br>
            <?php echo osc_base_url() . 'user/recover';?>
        </div>
    </div>
	<div class="div_seo_meta">
      <form name="upayments_form" id="upayments_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
      <input type="hidden" name="page" value="plugins" />
      <input type="hidden" name="action" value="renderplugin" />
      <input type="hidden" name="route" value="seoplugin-admin-robots" />
      <input type="hidden" name="plugin_action" value="done" />
      <input type="hidden" name="help_robots" value="<?php echo osc_get_preference('help_robots', 'seo_plugin'); ?>" />

        <div class="form-controls"  ><textarea name="edit_robots" rows="10"><?php echo $robots; ?></textarea></div>



          <div class="seo_button">
      <button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
     </div>
      </form></div>
    <div class="clear"></div>
    <div class="seo_copyright">
        <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
    </div>
    <div class="clear"></div>
</div></div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>