<?php if ( (!defined('ABS_PATH')) ) exit('ABS_PATH is not loaded. Direct access is not allowed.'); ?>
<?php if ( !OC_ADMIN ) exit('User access is not allowed.'); ?>
<?php 
 /*
 * Copyright 2017 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
if(Params::getParam('plugin_action')=='done') {
osc_set_preference('simple_category_enable', Params::getParam("simple_category_enable") ? Params::getParam("simple_category_enable") : '0', 'seo_plugin','INTEGER');
osc_set_preference('category_number', Params::getParam("category_number") ? Params::getParam("category_number") : '0', 'seo_plugin', 'INTEGER');
osc_set_preference('category_text_number', Params::getParam("category_text_number") ? Params::getParam("category_text_number") : '0', 'seo_plugin', 'INTEGER');
osc_reset_preferences();
    $detail2 = ModelSeoplugin::newInstance()->getSEOCategory_simple();
    if(!empty($detail2['id'])) {
        ModelSeoplugin::newInstance()->updateSEOCategory_simple(1, Params::getParam('seo_title'), Params::getParam('seo_description'), Params::getParam('seo_keywords') );
    } else {
        ModelSeoplugin::newInstance()->insertSEOCategory_simple(1, Params::getParam('seo_title'), Params::getParam('seo_description'), Params::getParam('seo_keywords') );
    }
    seo_message_ok(__('Settings saved','seo_plugin'));
}


$categories = Category::newInstance()->toTreeAll();
				$detail['title'] = "" ;
$detail['description'] = "" ;
$detail['keywords'] = "" ;
    function drawCategories($categories, $depth = 0, $detail) {

        foreach($categories as $c) { ?>
		  <?php
	if(Params::getParam('plugin_action')=='done'){
    $detail = ModelSeoplugin::newInstance()->getSEOCategory( $c['pk_i_id'] );
	        if(isset($detail['id'])) {
          ModelSeoplugin::newInstance()->updateSEOCategory( $c['pk_i_id'], Params::getParam('title'.$c['pk_i_id']), Params::getParam('description'.$c['pk_i_id']), Params::getParam('keywords'.$c['pk_i_id']) );
        }else{
          ModelSeoplugin::newInstance()->insertSEOCategory( $c['pk_i_id'], Params::getParam('title'.$c['pk_i_id']), Params::getParam('description'.$c['pk_i_id']), Params::getParam('keywords'.$c['pk_i_id']) );
        }

  } ?>
		 <?php $detail = ModelSeoplugin::newInstance()->getSEOCategory( $c['pk_i_id'] ); ?>
		          <div id="<?php echo $c['pk_i_id'] ?>" class="seo_category" >
            <div class="seo_category_id"><strong><?php echo $c['pk_i_id'] ?></strong></div>
            <strong><?php for($d=0;$d<$depth;$d++) { echo "&nbsp;&nbsp;"; }; echo $c['s_name']; ?></strong>
            <div class="seo_detail_box">
              <div id="<?php echo $c['pk_i_id'] ?>button" class="seo_detail_box_button"><?php _e('Open', 'seo_plugin');?></div>
              <div class="seo_detail_box_element">
                <div><?php if(!empty($detail['title'])){echo "title"; } ?></div>
                <div><?php if(!empty($detail['description'])){echo "desc"; } ?></div>
                <div><?php if(!empty($detail['keywords'])){echo "key"; } ?></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>

			        <div style="padding: 5px; margin-left: 30px; display: none" id="<?php echo $c['pk_i_id'] ?>box">
          <div class="div_seo_meta">
            <?php _e('Meta Title', 'seo_plugin'); ?><br>
            <input type="text" name="title<?php echo $c['pk_i_id'] ?>" id="title" value="<?php if(!empty($detail['title'])){echo $detail['title']; } ?>" />
          </div>
          <div class="div_seo_meta">
            <?php _e('Meta Description', 'seo_plugin'); ?><br>
            <textarea name="description<?php echo $c['pk_i_id'] ?>" id="description" ><?php if(!empty($detail['description'])){echo $detail['description']; } ?></textarea>
          </div>
          <div class="div_seo_meta">
            <?php _e('Meta Keywords', 'seo_plugin'); ?><br>
            <input type="text" name="keywords<?php echo $c['pk_i_id'] ?>" id="keywords" value="<?php if(!empty($detail['keywords'])){echo $detail['keywords']; } ?>" />
          </div>
        </div>
        <?php drawCategories($c['categories'], $depth+1, $detail);
        }
    };
?>
<?php require_once 'top_menu.php'; ?>
<div id="categories">
<h2><i class="fa fa-cog"></i> <?php _e('Settings categories','seo_plugin'); ?></h2>
<div class="well" >
<form name="upayments_form" id="upayments_form" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
<input type="hidden" name="page" value="plugins" />
<input type="hidden" name="action" value="renderplugin" />
<input type="hidden" name="route" value="seoplugin-admin-categories" />
<input type="hidden" name="plugin_action" value="done" />
<input type="hidden" name="category_number" id="category_number" value="<?php echo osc_get_preference('category_number', 'seo_plugin'); ?>" />
<input type="hidden" name="category_text_number" id="category_text_number" value="<?php echo osc_get_preference('category_text_number', 'seo_plugin'); ?>" />
         <?php $detail2 = ModelSeoplugin::newInstance()->getSEOCategory_simple(); ?>
<div class="div_seo_meta"></div><br>
							<h3 class="h3-bottom-line"><i class="fa fa-cog"></i> <?php _e('Simple','seo_plugin'); ?></h3><br>
							       <table>
							   <tr><td class="seo_info"><span>
  <?php _e('Use simple function', 'seo_plugin'); ?></span>
  <td><label class="switch">
        <input type="checkbox" <?php echo (osc_get_preference('simple_category_enable', 'seo_plugin') ? 'checked="true"' : ''); ?> name="simple_category_enable" id="simple_category_enable" value="1" >
      <span class="slider round"></span>
</label>
	</td></tr></table>
	  <?php _e('Enable this option if you want use one Title, Description and Keywords with Category Name.', 'seo_plugin'); ?><br>
	   <div id="bx"><strong><?php _e('Current Order:','seo_plugin'); ?></strong><br><br>
  <div id="box">
    <?php
    foreach(ActulatCategory() as $name_n){
      echo '<div id="'.$name_n['id'].'" class="seo_title_el">
                <div class="seo_title_el_name">'.$name_n['name'].'</div>
                <div id="left" class="seo_title_button"> < </div>
                <div id="right" class="seo_title_button"> > </div>
                </div>';
    }
    ?>
  </div>
  </div><br>
               <br><div class="div_seo_meta"><?php _e('Title', 'seo_plugin'); ?><br>
			   <input type="text" name="seo_title" id="seo_title" value="<?php if(isset($detail2['title']) != ''){echo $detail2['title']; } ?>" size="40" /></div>
               <div class="div_seo_meta"><?php _e('Meta Description', 'seo_plugin'); ?><br>
			   <textarea name="seo_description" id="seo_description" /><?php if(isset($detail2['description']) != ''){echo $detail2['description']; } ?></textarea></div>
                <div class="div_seo_meta"><?php _e('Meta Keywords', 'seo_plugin'); ?><br>
				<input type="text" name="seo_keywords" id="seo_keywords" value="<?php if(isset($detail2['keywords']) != ''){echo $detail2['keywords']; } ?>" size="20" /></div>
<br><br><br><h3 class="h2-bottom-line"><i class="fa fa-cog"></i> <?php _e('Advanced','seo_plugin'); ?></h3><br>   


  <div>

<?php drawCategories($categories, 0, $detail ); ?>


</div>

  <div class="seo_button">
<button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
</div>
</form>
  <div class="clear"></div>
  <div class="seo_copyright">
    <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
  </div>
  <div class="clear"></div>
</div>
</div>
<script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>
<script type="text/javascript">
  $('#promo_form').ready(function(){

    function addTitleElement(id_el, name_el){
      $('<div id="'+id_el+'" class="seo_title_el">' +
          '<div class="seo_title_el_name">'+name_el+'</div>' +
          '<div id="left" class="seo_title_button"> < </div>' +
          '<div id="right" class="seo_title_button"> > </div>' +
          '</div>').appendTo('#box');
    }

    $(document).on("click", "input[name='simple_category_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('category', '<?php echo __('Category', 'seo_plugin')?>');
      }else{
        $('[id="category"]').remove();
      }
    });
    $(document).on("click", "input[name='simple_category_enable']", function(){
      if($(this).attr('checked') == 'checked'){
        addTitleElement('category_text', '<?php echo __('Text', 'seo_plugin')?>');
      }else{
        $('[id="category_text"]').remove();
      }
    });

    $(document).on("click", ".seo_title_button", function(){
      var rout = $(this).attr('id');
      var el_out = $(this).parent();
      var id_out = $(this).parent().attr('id');
      if(rout == 'left'){
        var el_to = $(el_out).prev();
        var id_to = $(this).parent().prev().attr('id');
      }
      if(rout == 'right'){
        var el_to = $(el_out).next();
        var id_to = $(this).parent().next().attr('id');
      }
      var htm_out = $(el_out).html();
      var htm_to = $(el_to).html();
      $(el_out).html(htm_to);
      $(el_to).html(htm_out);
      $(el_out).attr('id', id_to);
      $(el_to).attr('id', id_out);
    });

    $(document).on("click", "#theButton", function(){
      var elems = $('.seo_title_el');
      for (var i = 0; i < elems.length; i++) {
        name_el = $(elems[i]).attr('id');
        num_id = $(elems[i]).index();
        if(name_el == 'category'){
          $("input[name='category_number']").val(num_id);
        }
        if(name_el == 'category_text'){
          $("input[name='category_text_number']").val(num_id);
        }
      }

    })
  });
</script>