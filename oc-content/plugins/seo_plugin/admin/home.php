<?php
 /*
 * Copyright 2016 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this plugin and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */

if(Params::getParam('plugin_action')=='done') {
    osc_add_flash_ok_message(__('Settings saved', 'seo_plugin'), 'admin');
    $home = ModelSeoplugin::newInstance()->getSEOhome();
    if(isset($home['id'])) {
        ModelSeoplugin::newInstance()->updateSEOhome( 1, Params::getParam('title'), Params::getParam('description'), Params::getParam('keywords') );
    } else {
        ModelSeoplugin::newInstance()->insertSEOhome( 1, Params::getParam('title'), Params::getParam('description'), Params::getParam('keywords') );
    }
    osc_set_preference('help_home', Params::getParam("help_home") ? Params::getParam("help_home") : '1', 'seo_plugin', 'INTEGER');
	ob_get_clean();
    osc_redirect_to(osc_route_admin_url('seoplugin-admin-home'));
}
?>
<?php require_once 'top_menu.php'; ?>
<div id="home">
<h2><i class="fa fa-cog"></i> <?php _e('Settings for Main Page','seo_plugin'); ?></h2>
<div class="well" >
    <form name="seo_plugin" id="seo_plugin" action="<?php echo osc_admin_base_url(true); ?>" method="POST" enctype="multipart/form-data" >
        <input type="hidden" name="page" value="plugins" />
        <input type="hidden" name="action" value="renderplugin" />
        <input type="hidden" name="route" value="seoplugin-admin-home" />
        <input type="hidden" name="plugin_action" value="done" />
        <input type="hidden" name="help_home" value="<?php echo osc_get_preference('help_home', 'seo_plugin'); ?>" />

<?php  $home = ModelSeoplugin::newInstance()->getSEOhome();?>

<div class="div_seo_meta">
    <div class="form-label"><?php _e('Meta Title', 'seo_plugin'); ?></div>
    <input type="text" name="title" id="title" value="<?php if(isset($home['title']) != ''){echo $home['title']; } ?>"/>
</div>
<div class="div_seo_meta">
    <div class="form-label"><?php _e('Meta Description', 'seo_plugin'); ?></div>
    <textarea name="description" id="description"><?php if(isset($home['description']) != ''){echo $home['description']; } ?></textarea>
</div>
<div class="div_seo_meta">
    <div class="form-label"><?php _e('Meta Keywords', 'seo_plugin'); ?></div>
    <input type="text" name="keywords" id="keywords" value="<?php if(isset($home['keywords']) != ''){echo $home['keywords']; } ?>"/>
</div>

<div class="seo_button">
<button name="theButton" id="theButton" type="submit" style="float: left;" class="btn btn-submit"><?php _e('Save', 'seo_plugin');?></button>
</div>
    </form>
    <div class="clear"></div>
<div class="seo_copyright">
  <span>&copy; <?php echo date('Y') ?> <a target="_blank" title="osclass-pro.com" href="https://<?php _e('osclass-pro.com', 'seo_plugin'); ?>/"><?php _e('osclass-pro.com', 'seo_plugin'); ?></a>. All rights reserved.</span>
</div>
    <div class="clear"></div>
</div>
</div>
 <script type="text/javascript" src="<?php echo osc_base_url();?>oc-content/plugins/seo_plugin/admin/js/seo_script.js"></script>