CREATE TABLE IF NOT EXISTS /*TABLE_PREFIX*/t_reviews (
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `fk_i_item_id` INT(10) UNSIGNED NOT NULL,
	`fk_i_user_id` TINYINT(10) UNSIGNED NOT NULL,
	`rating` TINYINT(10),
    `review_title` VARCHAR(150),
	`review_text` VARCHAR(255),
	`hash` varchar(255) DEFAULT NULL,
	`status` enum('1', '0') NOT NULL,
	`date` datetime NOT NULL,
	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';

CREATE TABLE IF NOT EXISTS /*TABLE_PREFIX*/t_user_reviews (
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `fk_i_user_id` INT(10) UNSIGNED NOT NULL,
	`comment_user_id` TINYINT(10) UNSIGNED NOT NULL,
	`rating` TINYINT(10),
    `review_title` VARCHAR(150),
	`review_text` VARCHAR(255),
	`hash` varchar(255) DEFAULT NULL,
	`status` enum('1', '0') NOT NULL,
	`date` datetime NOT NULL,
	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB DEFAULT CHARACTER SET 'UTF8' COLLATE 'UTF8_GENERAL_CI';