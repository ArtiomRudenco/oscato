<?php 
if(Params::getParam("edit")) {
	$edit = ModelReviews::newInstance()->getReviewById(Params::getParam("edit"));
		foreach($edit as $edit) { 
		$title = $edit['title'];
		$description = $edit['description'];
		
	}
}


?>

<div class="plugin-header clearfix">
 
        <h1 class="float_left"><?php _e('Edit Review', 'blog'); ?></h1>
    
    <a class="float_right" href="http://www.drizzlethemes.com/" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>



<div class="plugin-content">
	<form name="review_edit"  action="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/reviews.php');?>" method="post" >
	<input type="hidden" name="plugin_action" value="post_edit" />
    <input type="hidden" name="edit" value="<?php echo Params::getParam('edit');?>" />
    <div class="form-horizontal">
    <div class="leftSide">
        <div class="form-row">
            <div class="form-label"><?php _e('Title', 'blog'); ?></div>
            <div class="form-controls">
                <input type="text" placeholder="<?php echo osc_esc_html(__('Enter title here', 'blog'));?>" value="<?php echo osc_esc_html($title) ;?>" name="title" id="title">
            </div>
         </div>
  
        <div class="form-row">
            <div class="form-label"><?php _e('Description', 'blog'); ?></div>
            <div class="form-controls">
                <input type="text"  value="<?php echo osc_esc_html($description) ;?>" name="description" id="description">
               
            </div>
        </div>
    
    
       <div class="form-row">
            <div class="form-controls">
                <button class="btn btn-blue" type="submit"><?php _e('Publish', 'blog'); ?></button>
            </div>
        </div>
     </div>
     </div>
    </form>
</div>

<div class="plugin-footer">
	&copy <?php echo date('Y'); ?> Blog Osclass Plugin. Developed by <a href="http://www.drizzlethemes.com/" target="_blank">DrizzleThemes</a>.
</div>