<?php
if(Params::getParam("review_id")) {
	ModelReviews::newInstance()->updateReview(Params::getParam('review_id'), Params::getParam('review_title'), Params::getParam('review_text') );
	osc_add_flash_ok_message(__('Review has been updated', 'review_plugin'), 'admin');
	header('Location: '.osc_admin_render_plugin_url('reviews_plugin/admin/reviews.php'));
}

if(Params::getParam("user_review_id")) {
	ModelReviews::newInstance()->updateUserReview(Params::getParam('user_review_id'), Params::getParam('user_review_title'), Params::getParam('user_review_text') );
	osc_add_flash_ok_message(__('Review has been updated', 'review_plugin'), 'admin');
	header('Location: '.osc_admin_render_plugin_url('reviews_plugin/admin/user-reviews.php'));
}
?>
