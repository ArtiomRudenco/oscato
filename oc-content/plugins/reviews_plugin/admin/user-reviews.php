<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>admin.css" type="text/css" rel="stylesheet" />

<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('Reviews', 'reviews_plugin'); ?></h1>
    <a class="float_right" href="http://www.drizzlethemes.com/" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/reviews.php');?>"><?php _e('Manage Reviews', 'reviews_plugin'); ?></a></li>
        <li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>"><?php _e('Manage User Reviews', 'reviews_plugin'); ?></a></li>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'reviews_plugin'); ?></a></li>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'reviews_plugin'); ?></a></li>
    </ul>
</div>

<?php
	$page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
	$author = Params::getParam('authorId');
	$user = Params::getParam('userId');
	
	$reviews = ModelReviews::newInstance()->getUserCommentsByArg($page, '', $author, $user);
	
	$del = Params::getParam("deleteReview");
	if(Params::getParam("deleteReview") != "") {
		ModelReviews::newInstance()->deleteUserReview($del);
		osc_add_flash_ok_message(__('Review has been deleted', 'reviews_plugin'), 'admin');
		header('Location: '.osc_admin_render_plugin_url('reviews_plugin/admin/user-reviews.php'));
	 
	}
	if(Params::getParam("plugin_action") == "post_edit") {
		ModelReviews::newInstance()->updateUserReview(Params::getParam("edit"), Params::getParam("title"), Params::getParam("description"));
		osc_add_flash_ok_message(__('Review has been updated', 'reviews_plugin'), 'admin');
		header('Location: '.osc_admin_render_plugin_url('reviews_plugin/admin/user-reviews.php'));
	 
	}
	if(Params::getParam("active")) {
		ModelReviews::newInstance()->activeUserReview(Params::getParam("active"), "1");
		osc_add_flash_ok_message(__('Review has been enabled', 'reviews_plugin'), 'admin');
		header('Location: '.osc_admin_render_plugin_url('reviews_plugin/admin/user-reviews.php'));
	}
	if(Params::getParam("deactive")) {
		ModelReviews::newInstance()->activeUserReview(Params::getParam("deactive"), "0");
		osc_add_flash_ok_message(__('Review has been disabled', 'reviews_plugin'), 'admin');
		header('Location: '.osc_admin_render_plugin_url('reviews_plugin/admin/user-reviews.php'));
	}

?>

<div class="plugin-content">
	<h2><?php _e('Manage User Reviews','reviews_plugin'); ?></h2>
	<div id="listing-toolbar">
		<div class="float-left">
			<form action="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>" method="post" class="inline nocsrf">
	            <input name="authorId" type="text" class="input-actions input-has-select " placeholder="<?php echo osc_esc_html(__('Author ID', 'reviews_plugin')); ?>"  value="<?php echo osc_esc_html($author);?>"/>
	            <input name="userId" type="text" class="input-actions input-has-select " placeholder="<?php echo osc_esc_html(__('User ID', 'reviews_plugin')); ?>"  value="<?php echo osc_esc_html($user);?>"/>
	            <button type="submit" class="btn"><?php _e('Find', 'reviews_plugin'); ?></button>
	        </form>
		</div>
    </div>

	<table class="table" cellspacing="0" cellpadding="0">
		<thead>
		<tr>
			<th class="col-status"><?php _e('Status', 'reviews_plugin'); ?></th>
			<th class="col-title"><?php _e('Review', 'reviews_plugin'); ?></th>
            <th class="col-item"><?php _e('Author', 'reviews_plugin'); ?></th>
            <th class="col-user"><?php _e('User', 'reviews_plugin'); ?></th>
            <th class="col-date"><?php _e('Date', 'reviews_plugin'); ?></th>
            <th></th>   
		</tr>
		</thead>
		<tbody>
		<?php
		if( count($reviews) > 0 ) { 
		foreach($reviews as $review) { ?>
		<tr class="status-<?php if($review['status'] == 1) { echo 'active'; } else { echo 'inactive';} ?>">
			<td class="col-status">
				<?php if($review['status'] == 1) { 
					_e('Active','reviews_plugin'); 
				} else { 
					_e('Inactive','reviews_plugin');
				} ?>
			</td>
            <td>
            <?php if(Params::getParam('edit') != $review['id']){ ?>
			<?php echo $review['review_title'];?>
            <?php } ?>
            <?php if( Params::getParam('edit') == $review['id']){ ?>
            <form name="review_edit" action="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/index.php');?>" method="post" class="form-horizontal">
            	<input name="user_review_id" type="hidden" value="<?php echo $review['id']; ?>" />
            	<div class="form-row">
            		<input style="width: 75%;" type="text" name="user_review_title" value="<?php echo osc_esc_html($review['review_title']);?>" />
            	</div>
            	<div class="form-row">
            		<textarea style="width: 96%;" name="user_review_text"><?php echo osc_esc_html($review['review_text']);?></textarea>
            	</div>
            	<button class="btn btn-submit" type="submit"><?php _e('Update','reviews_plugin'); ?></button>
            	<a href="javascript:history.go(-1)" class="btn" role="button"><?php _e('Cancel','reviews_plugin'); ?></a>
            </form>
            <?php } ?>
            </td>
			<td>
				<?php  $author = User::newInstance()->findByPrimaryKey($review['fk_i_user_id']);?>
                
				<a href="<?php echo osc_user_public_profile_url($author['pk_i_id']);?>"><?php echo $author['s_name']; ?><span class="icon-new-window"></span></a><br>
				(<?php echo $review['rating'].' '.__('ratings','reviews_plugin'); ?>)<br>
				
			</td>
			<td>
				<?php $user = User::newInstance()->findByPrimaryKey($review['comment_user_id']);?>
				<a href="<?php echo osc_admin_base_url(true). "?page=users&action=edit&id=".$review['comment_user_id'] ?>"><?php echo $user['s_name']; ?></a>
			</td>
			<td> <?php echo date(osc_date_format() . " H:m", strtotime($review['date'])); ?> </td>
			<td>
			<?php if($review['status'] != 1) {?>
				<a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>&active=<?php echo $review['id'];?>"><?php _e('Active','reviews_plugin'); ?></a> | 
			<?php } else { ?>
				<a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>&deactive=<?php echo $review['id'];?>"><?php _e('Deactive','reviews_plugin'); ?></a> |
			<?php } ?>
			<a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>&edit=<?php echo $review['id'];?>"><?php _e('Edit', 'reviews_plugin'); ?></a> | <a onclick="return confirm('<?php echo osc_esc_js(__('Are you sure you want to continue?', 'reviews_plugin')); ?>');" href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>&deleteReview=<?php echo $review['id']?>"><?php _e('Delete', 'reviews_plugin'); ?></a>
			</td>
		</tr>
		<?php }
		} ?>
		</tbody>
  	</table>
  	
  	<div class="has-pagination">
  		<ul>
	    <?php 
		    $tbl_name = ModelReviews::newInstance()->getTable_User_Reviews();
		    $count 	= new DAO();
		    $count->dao->select('COUNT(*) AS count');
		    $count->dao->from($tbl_name);

		
			if(Params::getParam('authorId')){
				$count->dao->where('fk_i_user_id', Params::getParam('authorId'));
			}

			if(Params::getParam('userId')){
				$count->dao->where('comment_user_id', Params::getParam('userId'));
			}
		    
		    $result = $count->dao->get()->row();
		    $total_records = $result['count'];
		    $total_pages = ceil($total_records / 10); 
		          
		    for ($i=1; $i<=$total_pages; $i++) { 
		        echo "<li><a href='". osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php')."&iPage=".$i."'>".$i."</a></li>"; 
		    }; 
	    ?>
    	</ul>
    </div>
</div>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Reviews Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>