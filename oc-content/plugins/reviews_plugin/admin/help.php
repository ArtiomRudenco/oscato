<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>admin.css" type="text/css" rel="stylesheet" />

<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('Reviews', 'reviews_plugin'); ?></h1>
    <a class="float_right" href="http://www.drizzlethemes.com/" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/reviews.php');?>"><?php _e('Manage Reviews', 'reviews_plugin'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>"><?php _e('Manage User Reviews', 'reviews_plugin'); ?></a></li>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'reviews_plugin'); ?></a></li>
    	<li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'reviews_plugin'); ?></a></li>
    </ul>
</div> 

<div class="plugin-content">
	<h2><?php _e('Help','reviews_plugin'); ?></h2>
	<div class="helpsection">
		<h3><?php _e('Review Form with Reviews','reviews_plugin'); ?></h3>
		<p><?php _e('To display review form with Reviews on single listing page, copy and paste following code to <u>item.php</u> file.','reviews_plugin'); ?>
		<br><br>
		<code>&lt;?php osc_run_hook('show_review_form'); ?&gt;</code></p>
	</div>
    
    <div class="helpsection">
		<h3><?php _e('User Review Form with Reviews','reviews_plugin'); ?></h3>
		<p><?php _e('To display user review form with Reviews on public profile page, copy and paste following code to <u>user_public_profile.php</u> file.','reviews_plugin'); ?>
		<br><br>
		<code>&lt;?php osc_run_hook('show_user_review_form'); ?&gt;</code></p>
	</div>
    <div class="helpsection">
		<h3>Show user rating by user id</h3>
		<p>You can display user ratings on anywhere by include user id function. Helpers are: <b>osc_logged_user_id(), osc_item_user_id(), osc_comment_user_id(), osc_user_id()</b><br><br>
		<code>&lt;?php if (function_exists('show_user_ratings_by_id')) { show_user_ratings_by_id(<b>osc_item_user_id()</b>); } ?&gt;</code></p>
	</div>
	<div class="clearfix"></div>
	<hr>

	<div class="helpsection">
		<h3><?php _e('Show rating','reviews_plugin'); ?></h3>
		<p><?php _e('To display rating result on single listing page, copy and past following code to <u>item.php</u> file.','reviews_plugin'); ?>
		<br><br>
		<code>&lt;?php if (function_exists('show_rating')) { show_rating(); } ?&gt;</code></p>
	</div>
    
    <div class="helpsection">
		<h3><?php _e('Show user rating','reviews_plugin'); ?></h3>
		<p><?php _e('To display rating result on user public profile page, copy and past following code to <u>user_public_profile.php</u> file.','reviews_plugin'); ?>
		<br><br>
		<code>&lt;?php if (function_exists('show_user_rating')) { show_user_rating(); } ?&gt;</code></p>
	</div>

	<div class="clearfix"></div>
	<hr>

	<div class="helpsection">
		<h3><?php _e('Show rating on Loop listings','reviews_plugin'); ?></h3>
		<p><?php _e('To display rating on Home or Search page, copy and past following code to <u>loop-single.php</u> file.','reviews_plugin'); ?>
		<br><br>
		<code>&lt;?php if (function_exists('show_rating_loop')) { show_rating_loop(); } ?&gt;</code></p>

		<p><?php _e('And for <u>loop-single-premium.php</u> file use following code:','reviews_plugin'); ?>
		<br><br>
		<code>&lt;?php if (function_exists('show_rating_premiumloop')) { show_rating_premiumloop(); } ?&gt;</code></p>
		<br><br>
		<p>
			<b><?php _e('Plugin Name','reviews_plugin'); ?></b>: Reviews Plugin<br />
			<b><?php _e('Version','reviews_plugin'); ?></b>: 1.0.5<br />
			<b><?php _e('Author','reviews_plugin'); ?></b>: <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a><br />
			<b><?php _e('Support','reviews_plugin'); ?></b>: <a href="mailto:support@drizzlethemes.com">support@drizzlethemes.com</a><br />
		</p>
	</div>
</div>