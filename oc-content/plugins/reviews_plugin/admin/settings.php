<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>admin.css" type="text/css" rel="stylesheet" />
<?php $pluginInfo = osc_plugin_get_info('reviews_plugin/index.php');  ?>

<div class="plugin-header clearfix">
	<h1 class="float_left"><?php _e('Reviews', 'reviews_plugin'); ?></h1>
    <a class="float_right" href="http://www.drizzlethemes.com/" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>
<div class="sub-header clearfix">
    <ul>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/reviews.php');?>"><?php _e('Manage Reviews', 'reviews_plugin'); ?></a></li>
        <li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/user-reviews.php');?>"><?php _e('Manage User Reviews', 'reviews_plugin'); ?></a></li>
    	<li class="active"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'reviews_plugin'); ?></a></li>
    	<li><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'reviews_plugin'); ?></a></li>
    </ul>
</div> 

<div class="plugin-content">
	<h2><?php _e('Settings', 'reviews_plugin'); ?></h2>
	<form action="<?php echo osc_admin_render_plugin_url('reviews_plugin/admin/settings.php'); ?>" class="form-horizontal" method="post">
		<input type="hidden" name="reviewoption" value="reviewsettings" />

		<div class="form-row">
			<div class="form-label"><?php _e('Rating','reviews_plugin'); ?></div>
			
			<div class="form-controls">
				<select name="rp_enabled">
					<option <?php if(rp_enabled() == "1"){ echo "selected='selected'"; }?> value="1"><?php _e('Enable','reviews_plugin'); ?></option>
					<option <?php if(rp_enabled() == "2"){ echo "selected='selected'"; }?> value="2"><?php _e('Disable','reviews_plugin'); ?></option>
				</select>
			</div>
		</div>

		<div class="form-row">
			<div class="form-label"><?php _e('Review Moderation','reviews_plugin'); ?></div>
			<div class="form-controls">
				<select name="rp_modration">
					<option <?php if(rp_modration() == "1"){ echo "selected='selected'"; }?> value="1"><?php _e('Disable','reviews_plugin'); ?></option>
					<option <?php if(rp_modration() == "0"){ echo "selected='selected'"; }?> value="0"><?php _e('Enable','reviews_plugin'); ?></option>
				</select>
			</div>
		</div>

		<div class="form-row">
			<div class="form-label"><?php _e('Rating Stats','reviews_plugin'); ?></div>
			<div class="form-controls">
				<select name="rp_showStats">
					<option <?php if(rp_showStats() == "1"){ echo "selected='selected'"; }?> value="1"><?php _e('Enable','reviews_plugin'); ?></option>
					<option  <?php if(rp_showStats() == "2"){ echo "selected='selected'"; }?> value="2"><?php _e('Disable','reviews_plugin'); ?></option>
				</select>
			</div>
		</div>
			
         <div class="form-row">
			<div class="form-label"><?php _e('Multiple review per user','reviews_plugin'); ?></div>
			<div class="form-controls">
				<select name="rp_multiple">
					<option <?php if(rp_multiple() == "1"){ echo "selected='selected'"; }?> value="1"><?php _e('Allow','reviews_plugin'); ?></option>
					<option  <?php if(rp_multiple() == "2"){ echo "selected='selected'"; }?> value="2"><?php _e("Don't allow","reviews_plugin"); ?></option>
				</select>
			</div>
		</div>
		<div class="clear"></div>
		<div class="form-actions">
	        <button class="btn btn-submit" type="submit"><?php _e('Save','reviews_plugin'); ?></button>
	    </div>
	</form>