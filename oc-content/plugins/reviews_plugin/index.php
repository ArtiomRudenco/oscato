<?php
/*
Plugin Name: Reviews Plugin
Plugin URI: http://www.osclass.org
Description: Reviews plugin allows user to write reivew and rate the listings.
Version: 1.0.5
Author: Drizzle
Author URI: http://www.drizzlethemes.com/
Short name: reviews_plugin
Plugin update URI: reviews-plugin
*/

include "ModelReviews.php";

/* Install Plugin */
function review_install() {
    ModelReviews::newInstance()->import('reviews_plugin/struct.sql');
    osc_set_preference('rp_enabled', '1', 'reviews_plugin', 'STRING');
    osc_set_preference('rp_modration', '2', 'reviews_plugin', 'STRING');
    osc_set_preference('rp_showStats', '1', 'reviews_plugin', 'STRING');
    osc_set_preference('rp_multiple', '2', 'reviews_plugin', 'STRING');

}

function reviews_plugin_actions() {
    $dao_preference = new Preference();
    $option = Params::getParam('reviewoption');
    if ($option == 'reviewsettings') {
        osc_set_preference('rp_enabled', Params::getParam("rp_enabled"), 'reviews_plugin', 'STRING');
        osc_set_preference('rp_modration', Params::getParam("rp_modration"), 'reviews_plugin', 'STRING');
        osc_set_preference('rp_showStats', Params::getParam("rp_showStats"), 'reviews_plugin', 'STRING');
        osc_set_preference('rp_multiple', Params::getParam("rp_multiple") ? Params::getParam("rp_multiple") : '1', 'reviews_plugin', 'STRING');
        osc_add_flash_ok_message(__('Plugin has been updated', 'reviews_plugin'), 'admin');
        osc_redirect_to(osc_admin_render_plugin_url('reviews_plugin/admin/settings.php'));
    }
}

function rp_enabled() {
    return(osc_get_preference('rp_enabled', 'reviews_plugin'));
}

function rp_modration() {
    return(osc_get_preference('rp_modration', 'reviews_plugin'));
}
function rp_showStats() {
    return(osc_get_preference('rp_showStats', 'reviews_plugin'));
}
function rp_multiple() {
    return(osc_get_preference('rp_multiple', 'reviews_plugin'));
}

// Basic oscato class
//function rating_stars($count) {
//	switch ($count) {
//			case "1":
//				echo '<span><i class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i></span>';
//			break;
//			case "2":
//				echo '<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i></span>';
//			break;
//			case "3":
//				echo '<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i></span>';
//			break;
//			case "4":
//				echo '<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i></span>';
//			break;
//			case "5":
//				echo '<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
//			break;
//			default:
//			echo '<span><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i></span>';
//	}
//
//}

function rating_stars($count)
{
    switch ($count) {
        case "1":
            echo '
                    <span class="fa fa-star checked"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    ';
            break;
        case "2":
            echo '
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    ';
            break;
        case "3":
            echo '
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    ';
            break;
        case "4":
            echo '
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    ';
            break;
        case "5":
            echo '
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                ';
            break;
        default:
            echo '
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                    <span style="color:#d6d6d6" class="fa fa-star"></span>
                 ';
    }

}

function starCounts($star){
    $count = ModelReviews::newInstance()->starCounts($star);
    return $count;
}
function userStarCounts($star){
    $count = ModelReviews::newInstance()->userStarCounts($star);
    return $count;
}

function rating_average() {
    $total = ModelReviews::newInstance()->getComments();
    $array = ModelReviews::newInstance()->getComments();
    foreach ($array as $array){
        $rating[] = $array['rating'];
    }
    //Calculate the average and round it up.
    $average = ceil( array_sum($rating) / count($rating) );
    echo rating_stars($average)."<br>";


    // Basic Oscato rating logic
//	if(!empty($rating)){
//		$average = ceil( array_sum($rating) / count($rating) );
//		echo rating_stars($average)."<br>"; ?>
    <!--		<b class="rating_average_total">--><?php //printf(__('(%1s ratings from %2s reviews)','reviews_plugin'), $average, count($total)); ?><!--</b>-->
    <!--         --><?php //if(rp_showStats()=='1'){ ?>
    <!--        <div class="rating_average">-->
    <!--        	<div class="--><?php //if(starCounts('5')=="0"){echo 'dull';}?><!--">-->
    <!--        		<b class="rating_label">--><?php //_e('Excellent', 'reviews_plugin'); ?><!--</b>-->
    <!--	        	--><?php //echo rating_stars('5').' <b>('.starCounts('5').')</b>'; ?>
    <!--	        </div>-->
    <!--	        <div class="--><?php //if(starCounts('4')=="0"){echo 'dull';}?><!--">-->
    <!--        		<b class="rating_label">--><?php //_e('Very Good', 'reviews_plugin'); ?><!--</b>-->
    <!--	        	--><?php //echo rating_stars('4').' <b>('.starCounts('4').')</b>'; ?>
    <!--	        </div>-->
    <!--	        <div class="--><?php //if(starCounts('3')=="0"){echo 'dull';}?><!--">-->
    <!--        		<b class="rating_label">--><?php //_e('Good', 'reviews_plugin'); ?><!--</b>-->
    <!--	        	--><?php //echo rating_stars('3').' <b>('.starCounts('3').')</b>'; ?>
    <!--	        </div>-->
    <!--	        <div class="--><?php //if(starCounts('2')=="0"){echo 'dull';}?><!--">-->
    <!--        		<b class="rating_label">--><?php //_e('Average', 'reviews_plugin'); ?><!--</b>-->
    <!--	        	--><?php //echo rating_stars('2').' <b>('.starCounts('2').')</b>'; ?>
    <!--	        </div>-->
    <!--	        <div class="--><?php //if(starCounts('1')=="0"){echo 'dull';}?><!--">-->
    <!--        		<b class="rating_label">--><?php //_e('Poor', 'reviews_plugin'); ?><!--</b>-->
    <!--	        	--><?php //echo rating_stars('1').' <b>('.starCounts('1').')</b>'; ?>
    <!--	        </div>-->
    <!--        </div>-->
    <!--        --><?php //} ?>
    <!--	--><?php //} else {
//		echo '
//            <div class="field_control stars" style="display: inline-block;">
//                <input type="radio" name="rate" class="star-1" value="1" id="star-1" />
//                <input type="radio" name="rate" value="2" class="star-2" id="star-2" />
//                <input type="radio" name="rate" value="3" class="star-3" id="star-3" />
//                <input type="radio" name="rate" value="4" class="star-4" id="star-4" />
//                <input type="radio" name="rate" value="5" class="star-5" id="star-5" />
//                <span></span>
//            </div>';
//	}
}
//user rate
function user_rating_average() {
    $total = ModelReviews::newInstance()->getUserComments();
    $array = ModelReviews::newInstance()->getUserComments();
    foreach ($array as $array){
        $rating[] = $array['rating'];
    }
    //Calculate the average and round it up.
    if(!empty($rating)){
        $average = ceil( array_sum($rating) / count($rating) );
        echo rating_stars($average)."<br>"; ?>
        <b class="rating_average_total"><?php printf(__('(%1s ratings from %2s reviews)','reviews_plugin'), $average, count($total)); ?></b>
        <?php if(rp_showStats()=='1'){ ?>
            <div class="rating_average">
                <div class="<?php if(userStarCounts('5')=="0"){echo 'dull';}?>">
                    <b class="rating_label"><?php _e('Excellent', 'reviews_plugin'); ?></b>
                    <?php echo rating_stars('5').' <b>('.userStarCounts('5').')</b>'; ?>
                </div>
                <div class="<?php if(userStarCounts('4')=="0"){echo 'dull';}?>">
                    <b class="rating_label"><?php _e('Very Good', 'reviews_plugin'); ?></b>
                    <?php echo rating_stars('4').' <b>('.userStarCounts('4').')</b>'; ?>
                </div>
                <div class="<?php if(userStarCounts('3')=="0"){echo 'dull';}?>">
                    <b class="rating_label"><?php _e('Good', 'reviews_plugin'); ?></b>
                    <?php echo rating_stars('3').' <b>('.userStarCounts('3').')</b>'; ?>
                </div>
                <div class="<?php if(userStarCounts('2')=="0"){echo 'dull';}?>">
                    <b class="rating_label"><?php _e('Average', 'reviews_plugin'); ?></b>
                    <?php echo rating_stars('2').' <b>('.userStarCounts('2').')</b>'; ?>
                </div>
                <div class="<?php if(userStarCounts('1')=="0"){echo 'dull';}?>">
                    <b class="rating_label"><?php _e('Poor', 'reviews_plugin'); ?></b>
                    <?php echo rating_stars('1').' <b>('.userStarCounts('1').')</b>'; ?>
                </div>
            </div>
        <?php } ?>
    <?php } else {
        echo '<i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i>'.'<br><b class="rating_average_total">('.__('No reviews yet','reviews_plugin').')</b>';
    }
}

function rating_loop() {
    $total = ModelReviews::newInstance()->getComments();
    $array = ModelReviews::newInstance()->getComments();
    foreach ($array as $array){
        $rating[] = $array['rating'];
    }
    //Calculate the average and round it up.
    if(!empty($rating)){
        $average = ceil( array_sum($rating) / count($rating) );
        echo rating_stars($average).' ('.count($total).' '.__('Reviews','reviews_plugin').')';
    } else {
        echo '<i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i>';
    }

}

function show_user_ratings_by_id($user) { ?>
    <div class="review_ratings">
        <?php	$total = ModelReviews::newInstance()->getUserCommentsByID($user);
        $array = ModelReviews::newInstance()->getUserCommentsByID($user);
        foreach ($array as $array){
            $rating[] = $array['rating'];
        }
        //Calculate the average and round it up.
        if(!empty($rating)){
            $average = ceil( array_sum($rating) / count($rating) );
            echo rating_stars($average).' ('.count($total).' '.__('Reviews','reviews_plugin').')';
        } else {
            echo '<i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i>';
        } ?>
    </div>

<?php }

function rating_premium_loop() {
    $total = ModelReviews::newInstance()->getpremiumComments();
    $array = ModelReviews::newInstance()->getpremiumComments();
    foreach ($array as $array){
        $rating[] = $array['rating'];
    }
    //Calculate the average and round it up.
    if(!empty($rating)){
        $average = ceil( array_sum($rating) / count($rating) );
        echo rating_stars($average).' ('.count($total).' '.__('Reviews','reviews_plugin').')';
    } else {
        echo '<i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i><i style="color:#d6d6d6" class="fa fa-star"></i>';
    }

}

/* Uninstall Plugin */
function review_uninstall() {
    ModelReviews::newInstance()->uninstall();
}

if(Params::getParam('rate') && Params::getParam('review_title') && Params::getParam('review_text')){
    ModelReviews::newInstance()->insertReview( Params::getParam('rate'), Params::getParam('review_title'), Params::getParam('review_text'), Params::getParam('item'), Params::getParam('user') );
    if(rp_modration() == "1"){
        osc_add_flash_ok_message(__('Your review has been published','reviews_plugin'));
    } else {
        osc_add_flash_info_message(__('Your review is awaiting moderation','reviews_plugin'));
    }
    osc_redirect_to(osc_item_url().Params::getParam('item'));

}





function modarate_menu() {
    echo '<h3><a href="#">Reviews</a></h3>
	<ul>
		<li><a href="'.osc_admin_render_plugin_url("reviews_plugin/admin/reviews.php").'">&raquo; '.__('Reviews', 'reviews_plugin').'</a></li>
		<li><a href="'.osc_admin_render_plugin_url("reviews_plugin/admin/settings.php").'">&raquo; '.__('Settings', 'reviews_plugin').'</a></li>
		<li><a href="'.osc_admin_render_plugin_url("reviews_plugin/admin/help.php").'">&raquo; '.__('Help', 'reviews_plugin').'</a></li>
	</ul>';

}


function review_form() {
    $total = ModelReviews::newInstance()->getComments();
    $check = ModelReviews::newInstance()->checkUser();

    $page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
    $comments = ModelReviews::newInstance()->getCommentsByArg($page, '1'); ?>

    <div id="review_main">
        <?php if(count($total) >= 1) { ?>
            <h2><?php printf(__('%s Reviews', 'reviews_plugin'),count($total)); ?></h2>
        <?php } else { ?>
            <h2><?php _e('Reviews', 'reviews_plugin'); ?></h2>
        <?php } ?>
        <?php
        if (!osc_is_web_user_logged_in()){ ?>
            <div class="rw_notice">
                <?php printf(__('Please <a href="%s">Login</a> to rate and review this listing.', 'reviews_plugin'), osc_user_login_url()); ?>
            </div>
        <?php } else if( $check == '1' and rp_multiple() != '1') { ?>
            <div class="rw_info"><?php _e('You have already rated and reviewed this listing.', 'reviews_plugin'); ?></div>
        <?php } else {?>
            <div class="review_form">
                <form name="review_form" action="<?php echo osc_base_url(true);?>" method="post">
                    <input type="hidden" name="item" value="<?php echo osc_item_id();?>" />
                    <input type="hidden" name="user" value="<?php echo osc_logged_user_id();?>" />
                    <div class="field_group">
                        <label class="field_label"><?php _e('Rating','reviews_plugin'); ?></label>
                        <div class="field_control stars">
                            <input type="radio" name="rate" class="star-1" value="1" id="star-1" />
                            <label class="star-1" for="star-1"><?php _e('1 Star','reviews_plugin'); ?></label>
                            <input type="radio" name="rate" value="2" class="star-2" id="star-2" />
                            <label class="star-2" for="star-2"><?php _e('2 Stars','reviews_plugin'); ?></label>
                            <input type="radio" name="rate" value="3" class="star-3" id="star-3" />
                            <label class="star-3" for="star-3"><?php _e('3 Stars','reviews_plugin'); ?></label>
                            <input type="radio" name="rate" value="4" class="star-4" id="star-4" />
                            <label class="star-4" for="star-4"><?php _e('4 Stars','reviews_plugin'); ?></label>
                            <input type="radio" name="rate" value="5" class="star-5" id="star-5" />
                            <label class="star-5" for="star-5"><?php _e('5 Stars','reviews_plugin'); ?></label>
                            <span></span>
                        </div>
                    </div>
                    <div class="field_group">
                        <label class="field_label"><?php _e('Title','reviews_plugin'); ?></label>
                        <div class="field_control">
                            <input type="text" name="review_title" id="review_title" value="" />
                        </div>
                    </div>
                    <div class="field_group">
                        <label class="field_label"><?php _e('Review','reviews_plugin'); ?></label>
                        <div class="field_control">
                            <textarea name="review_text" id="review_text"></textarea>
                        </div>
                    </div>
                    <button class="btn btn_submit" type="submit"><?php _e('Submit', 'reviews_plugin'); ?></button>
                </form>
            </div>
        <?php  } ?>

        <?php if(count($comments) != 0) { ?>
            <div class="reviews_list">
                <?php foreach($comments as $comment){?>
                    <div class="review_single">
                        <?php $user = User::newInstance()->findByPrimaryKey($comment['fk_i_user_id']);?>
                        <h4><?php echo $user['s_name']; ?></h4>
                        <span class="review_date"><?php echo date(osc_date_format() . " H:m", strtotime($comment['date'])); ?></span>
                        <div class="review_text">
                            <strong><?php echo $comment['review_title'];?></strong>
                            <?php echo rating_stars($comment['rating']);?>
                            <p><?php echo $comment['review_text'];?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="has-pagination">
                <?php
                $tbl_name = ModelReviews::newInstance()->getTable_Reviews();
                $count = new DAO();
                $count->dao->select('COUNT(*) AS count');
                $count->dao->from($tbl_name);
                $count->dao->where('fk_i_item_id', osc_item_id());
                $count->dao->where('status', '1');
                $result = $count->dao->get()->row();
                $total_records = $result['count'];
                $total_pages = ceil($total_records / 10);

                for ($i=1; $i<=$total_pages; $i++) {
                    echo "<a href='". osc_item_url()."&iPage=".$i."'>".$i."</a> ";
                };
                ?>

            </div>
        <?php } else { ?>
            <div class="rw_info">
                <?php _e('No reviews yet, Be the first to write a review this listing.', 'reviews_plugin'); ?>
            </div>
        <?php } ?>
    </div>
<?php }
//user review form
function user_review_form() {

    if(Params::getParam('user_rate') && Params::getParam('user_review_title')){
        ModelReviews::newInstance()->insertUserReview( Params::getParam('user_rate'), Params::getParam('user_review_title'), Params::getParam('user_review_text'), Params::getParam('user'), Params::getParam('commenter') );
        if(rp_modration() == "1"){
            osc_add_flash_ok_message(__('Your review has been published','reviews_plugin'));
        } else {
            osc_add_flash_info_message(__('Your review is awaiting moderation','reviews_plugin'));
        }
        header('Location:'.osc_user_public_profile_url(Params::getParam('user')));
    }


    $total = ModelReviews::newInstance()->getUserComments();
    $check = ModelReviews::newInstance()->checkUserReview();

    $page = (Params::getParam('iPage') != '') ? Params::getParam('iPage') : 1;
    $comments = ModelReviews::newInstance()->getUserCommentsByArg($page, '1'); ?>

    <div id="review_main">
        <?php if(count($total) >= 1) { ?>
            <h2><?php printf(__('%s Reviews', 'reviews_plugin'),count($total)); ?></h2>
        <?php } else { ?>
            <h2><?php _e('Reviews', 'reviews_plugin'); ?></h2>
        <?php } ?>
        <?php
        if (!osc_is_web_user_logged_in()){ ?>
            <div class="rw_notice">
                <?php printf(__('Please <a href="%s">Login</a> to rate and review this listing.', 'reviews_plugin'), osc_user_login_url()); ?>
            </div>
        <?php } else if( $check == '1' and rp_multiple() != '1') { ?>
            <div class="rw_info"><?php _e('You have already rated and reviewed this listing.', 'reviews_plugin'); ?></div>
        <?php } else if(osc_logged_user_id() == osc_user_id()) {  ?>
            <div class="rw_info"><?php _e('You cant review your self.', 'reviews_plugin'); ?></div>
        <?php } else { ?>
            <div class="review_form">
                <form name="review_form" action="<?php echo osc_user_public_profile_url(osc_user_id());?>" method="post">
                    <input type="hidden" name="user" value="<?php echo osc_user_id();?>" />
                    <input type="hidden" name="user_reviews" value="1" />
                    <input type="hidden" name="commenter" value="<?php echo osc_logged_user_id();?>" />
                    <div class="field_group">
                        <label class="field_label"><?php _e('Rating','reviews_plugin'); ?></label>
                        <div class="field_control stars">
                            <input type="radio" name="user_rate" class="star-1" value="1" id="star-1" />
                            <label class="star-1" for="star-1"><?php _e('1 Star','reviews_plugin'); ?></label>
                            <input type="radio" name="user_rate" value="2" class="star-2" id="star-2" />
                            <label class="star-2" for="star-2"><?php _e('2 Stars','reviews_plugin'); ?></label>
                            <input type="radio" name="user_rate" value="3" class="star-3" id="star-3" />
                            <label class="star-3" for="star-3"><?php _e('3 Stars','reviews_plugin'); ?></label>
                            <input type="radio" name="user_rate" value="4" class="star-4" id="star-4" />
                            <label class="star-4" for="star-4"><?php _e('4 Stars','reviews_plugin'); ?></label>
                            <input type="radio" name="user_rate" value="5" class="star-5" id="star-5" />
                            <label class="star-5" for="star-5"><?php _e('5 Stars','reviews_plugin'); ?></label>
                            <span></span>
                        </div>
                    </div>
                    <div class="field_group">
                        <label class="field_label"><?php _e('Title','reviews_plugin'); ?></label>
                        <div class="field_control">
                            <input type="text" name="user_review_title" id="review_title" value="" />
                        </div>
                    </div>
                    <div class="field_group">
                        <label class="field_label"><?php _e('Review','reviews_plugin'); ?></label>
                        <div class="field_control">
                            <textarea name="user_review_text" id="review_text"></textarea>
                        </div>
                    </div>
                    <button class="btn btn_submit" type="submit"><?php _e('Submit', 'reviews_plugin'); ?></button>
                </form>
            </div>
        <?php  } ?>

        <?php if(count($comments) != 0) { ?>
            <div class="reviews_list">
                <?php foreach($comments as $comment){?>
                    <div class="review_single">
                        <?php $user = User::newInstance()->findByPrimaryKey($comment['fk_i_user_id']);?>
                        <h4><?php echo $user['s_name']; ?></h4>
                        <span class="review_date"><?php echo date(osc_date_format() . " H:m", strtotime($comment['date'])); ?></span>
                        <div class="review_text">
                            <strong><?php echo $comment['review_title'];?></strong>
                            <?php echo rating_stars($comment['rating']);?>
                            <p><?php echo $comment['review_text'];?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="has-pagination">
                <?php
                $tbl_name = ModelReviews::newInstance()->getTable_User_Reviews();
                $count = new DAO();
                $count->dao->select('COUNT(*) AS count');
                $count->dao->from($tbl_name);
                $count->dao->where('fk_i_user_id', osc_user_id());
                $count->dao->where('status', '1');
                $result = $count->dao->get()->row();
                $total_records = $result['count'];
                $total_pages = ceil($total_records / 10);

                for ($i=1; $i<=$total_pages; $i++) {
                    echo "<a href='". osc_user_public_profile_url(osc_user_id())."&iPage=".$i."'>".$i."</a> ";
                };
                ?>

            </div>
        <?php } else { ?>
            <div class="rw_info">
                <?php _e('No reviews yet, Be the first to write a review this listing.', 'reviews_plugin'); ?>
            </div>
        <?php } ?>
    </div>
<?php }

// Show Start and Rating (Single Item)
function show_rating_loop() {
    if(rp_enabled()== '1'){ ?>
        <div class="review_ratings">
            <?php echo rating_loop(); ?>
        </div>
    <?php }
}

function show_rating_premiumloop() {
    if(rp_enabled()== '1'){?>
        <div class="review_ratings">
            <?php echo rating_premium_loop(); ?>
        </div>
    <?php }
}

function show_rating() {
    if(rp_enabled()== '1'){ ?>
        <div class="review_ratings">
            <?php echo rating_average(); ?>
        </div>
    <?php }
}

function show_user_rating() {
    if(rp_enabled()== '1'){ ?>
        <div class="review_ratings">
            <?php echo user_rating_average(); ?>
        </div>
    <?php }
}

function reviewcss() {
    echo '<link rel="stylesheet" type="text/css" href="'.osc_base_url().'oc-content/plugins/reviews_plugin/css/reviews.css">';
    echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
}



osc_add_hook('header', 'reviewcss');
osc_add_hook('admin_menu', 'modarate_menu');
if(rp_enabled()== '1'){
    osc_add_hook('show_review_form', 'review_form');
    osc_add_hook('show_user_review_form', 'user_review_form');
}
osc_add_hook('init_admin', 'reviews_plugin_actions');
osc_register_plugin(osc_plugin_path(__FILE__), 'review_install') ;
osc_add_hook(osc_plugin_path(__FILE__) . '_uninstall', 'review_uninstall') ;

?>
