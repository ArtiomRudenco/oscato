��    C      4  Y   L      �  
   �     �     �     �     �     �     �          	  ?     "   O     r     y     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �  
   �     �            <   )  >   f     �     �     �     �     �     �     �               ,     E     ]     u     }     �     �     �     �     �     �     �  f   �  f   7	  r   �	     
     
     
     %
  	   -
  1   7
     i
  "   �
     �
  �  �
     P     X  	   w  
   �  
   �  
   �  
   �     �  	   �  H   �  &        -     4     <     D     I  	   U     _     k     |     �  	   �     �     �     �     �     �     �     �     �     �  K     M   O     �     �     �     �     �     �     �          %     <     U     j     �     �     �     �  +   �     �  	   �     �     �  x      p   y     �     j     y     �     �  
   �  7   �     �  )   �     $             )       2   @   :   4          9   <   #       &   .   >   -   (   0         $   +      ,       %   '           *      8          B          ;   5   3         	   =                  /   A   ?      6                
                             7                             "   !                   1          C       %s Reviews (%1s ratings from %2s reviews) 1 Star 2 Stars 3 Stars 4 Stars 5 Stars Active Allow And for <u>loop-single-premium.php</u> file use following code: Are you sure you want to continue? Author Average Cancel Date Deactive Delete Disable Don't allow Edit Enable Excellent Find Good Help Inactive Listing Listing ID Manage Reviews Multiple review per user No reviews yet No reviews yet, Be the first to write a review this listing. Please <a href="%s">Login</a> to rate and review this listing. Plugin Name Plugin has been updated Poor Rating Rating Stats Review Review Form with Reviews Review Moderation Review has been deleted Review has been disabled Review has been enabled Review has been updated Reviews Save Settings Show rating Show rating on Loop listings Status Submit Support Title To display rating on Home or Search page, copy and past following code to <u>loop-single.php</u> file. To display rating result on single listing page, copy and past following code to <u>item.php</u> file. To display review form with Reviews on single listing page, copy and paste following code to <u>item.php</u> file. Update User User ID Version Very Good You have already rated and reviewed this listing. Your review has been published Your review is awaiting moderation ratings Project-Id-Version: Reviews Osclass Plugin
POT-Creation-Date: 2017-03-22 21:05+0100
PO-Revision-Date: 2017-03-22 21:38+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 %s Avis (%1s évaluations de %2s avis) 1 Étoile 2 Étoiles 3 Étoiles 4 Étoiles 5 Étoiles Actif Autoriser Et pour <u>loop-single-premium.php</u> fichier utiliser le code suivant: Êtes-vous sûr de vouloir continuer ? Auteur Moyenne Annuler Date Désactiver Supprimer Désactiver Ne pas autoriser Modifier Activer Excellent Chercher Bien Aide Inactif Annonce Announce ID Gérer Avis Plusieurs avis par utilisateur Pas encore d'avis Pas encore d'avis, Soyez le premier à donner votre avis sur cette annonce. Veuillez vous <a href="%s">connecter</a> pour évaluer et avis cette annonce. Nom du Plugin Plugin a été mis à jour Pauvre Évaluation Évaluations Statistiques Avis Formulaire d'avis avec avis Avis Modération Avis a été supprimé Avis a été désactivé Avis a été activé Avis a été mis à jour Avis Sauvegarder Paramètres Afficher évaluation Afficher évaluation sur les listes de Loop Statut Soumettre Soutien Titre Pour afficher évolution sur la page d'accueil ou de recherche, copiez et collez le code suivant <u>loop-single.php</u>. Pour afficher résultat d'évolution une seule page d'annonce, copiez et collez le code suivant <u>item.php</u>. Pour Afficher le formulaire d'avis avec les avis en une seule page d'annonce, copiez et collez le code suivant <u>item.php</u>. Mettre à jour Utilisateur Utilisateur ID Version Très bien Vous avez déjà mis un avis et évalué cette annonce. Votre avis a été publié Votre avis est en attente de modération. évaluations 