<?php
class ModelReviews extends DAO
    {
      	private static $instance ;

      
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

       
        function __construct()
        {
            parent::__construct();
        }
		
		
		public function import($file)
        {
            $path = osc_plugin_resource($file) ;
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( "Error importSQL::ModelReviews<br>".$file ) ;
            }
        }
		
      
        public function getTable_Reviews()
        {
            return DB_TABLE_PREFIX.'t_reviews' ;
        }
		 public function getTable_User_Reviews()
        {
            return DB_TABLE_PREFIX.'t_user_reviews' ;
        }
		
		
		
		
		public function uninstall()
        {
			$this->dao->query(sprintf('DROP TABLE %s', $this->getTable_Reviews()) ) ;
			$this->dao->query(sprintf('DROP TABLE %s', $this->getTable_User_Reviews()) ) ;
		}
		
		
		public function getComments()
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_Reviews() ) ;
			$this->dao->where('fk_i_item_id', osc_item_id());
			$this->dao->where('status', '1');
			$this->dao->orderBy('date', 'DESC');
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		public function getUserComments()
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_User_Reviews() ) ;
			$this->dao->where('fk_i_user_id', osc_user_id());
			$this->dao->where('status', '1');
			$this->dao->orderBy('date', 'DESC');
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		public function getUserCommentsByID($user)
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_User_Reviews() ) ;
			$this->dao->where('fk_i_user_id', $user);
			$this->dao->where('status', '1');
			$this->dao->orderBy('date', 'DESC');
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}

		public function getpremiumComments()
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_Reviews() ) ;
			$this->dao->where('fk_i_item_id', osc_premium_id());
			$this->dao->where('status', '1');
			$this->dao->orderBy('date', 'DESC');
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		
		public function starCounts($star)
        {
			$this->dao->select('count(*) as total');
            $this->dao->from( $this->getTable_Reviews() ) ;
			$this->dao->where('fk_i_item_id', osc_item_id());
			$this->dao->where('rating', $star);
			$this->dao->where('status', '1');
			$results = $this->dao->get();
           if($results == false) {
                return 0;
            }
			$total_ads = $results->row();
            return $total_ads['total'];
        }
		
		public function userStarCounts($star)
        {
			$this->dao->select('count(*) as total');
            $this->dao->from( $this->getTable_User_Reviews() ) ;
			$this->dao->where('fk_i_user_id', osc_user_id());
			$this->dao->where('rating', $star);
			$this->dao->where('status', '1');
			$results = $this->dao->get();
           if($results == false) {
                return 0;
            }
			$total_ads = $results->row();
            return $total_ads['total'];
        }
		
		public function getCommentsByArg($page, $status="", $item="", $user="")
        {
			$items_per_page = '10';
			$offset = ($page - 1) * $items_per_page;
			$this->dao->select();
            $this->dao->from( $this->getTable_Reviews() ) ;
			if(Params::getParam('itemId')){
				$this->dao->where('fk_i_item_id', $item);
			}
			if(Params::getParam('userId')){
				$this->dao->where('fk_i_user_id', $user);
			}
			if(Params::getParam('id')){
				$this->dao->where('fk_i_item_id', Params::getParam('id'));
			}
			if($status) {
				$this->dao->where('status', '1');
			}
			$this->dao->limit($offset, $items_per_page );
			$this->dao->orderBy('date', 'DESC');
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		public function getUserCommentsByArg($page, $status="", $user="", $commenter="")
        {
			$items_per_page = '10';
			$offset = ($page - 1) * $items_per_page;
			$this->dao->select();
            $this->dao->from( $this->getTable_User_Reviews() ) ;
			if(Params::getParam('authorId')){
				$this->dao->where('fk_i_user_id', $user);
			}
			if(Params::getParam('userId')){
				$this->dao->where('comment_user_id', $commenter);
			}
			
			if($status) {
				$this->dao->where('status', '1');
			}
			$this->dao->limit($offset, $items_per_page );
			$this->dao->orderBy('date', 'DESC');
			$results = $this->dao->get();
            if( !$results ) {
                return array() ;
            }

            return $results->result();
		}
		
		public function getReviewById( $id )
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_Reviews());
            $this->dao->where('id', $id );
            
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }

            return $result->result();
        }
		
		public function getUserReviewById( $id )
        {
            $this->dao->select();
            $this->dao->from( $this->getTable_User_Reviews());
            $this->dao->where('id', $id );
            
            $result = $this->dao->get();
            if( !$result ) {
                return array() ;
            }

            return $result->result();
        }
		
		 public function deleteReview( $id )
        {
            return $this->dao->delete( $this->getTable_Reviews(), array('id' => $id) ) ;
        }
		public function deleteUserReview( $id )
        {
            return $this->dao->delete( $this->getTable_User_Reviews(), array('id' => $id) ) ;
        }
		
		public function checkUser()
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_Reviews() ) ;
			$this->dao->where('fk_i_item_id', osc_item_id());
			$this->dao->where('fk_i_user_id', osc_logged_user_id());
			$result = $this->dao->get();
			$result = $result->row();
            if($result == true) {
                return 1;
            }
        }
		
		public function checkUserReview()
        {
			$this->dao->select();
            $this->dao->from( $this->getTable_User_Reviews() ) ;
			$this->dao->where('fk_i_user_id', osc_user_id());
			$this->dao->where('comment_user_id', osc_logged_user_id());
			$result = $this->dao->get();
			$result = $result->row();
            if($result == true) {
                return 1;
            }
        }
		
		
	
		
		public function insertReview( $rate, $review_title, $review_text, $item, $user )
        {
			if(rp_modration()=='1'){ $status = '1'; } else { $status = '0'; }
            return $this->dao->insert($this->getTable_Reviews(), array('status' => $status, 'rating' => $rate, 'review_title' => $review_title, 'review_text' => $review_text, 'fk_i_item_id' => $item, 'fk_i_user_id' => $user, 'date' => date('Y-m-d H:i:s'))) ;
        }
		
		public function insertUserReview( $rate, $review_title, $review_text, $user, $commenter )
        {
			if(rp_modration()=='1'){ $status = '1'; } else { $status = '0'; }
            return $this->dao->insert($this->getTable_User_Reviews(), array('status' => $status, 'rating' => $rate, 'review_title' => $review_title, 'review_text' => $review_text, 'fk_i_user_id' => $user, 'comment_user_id' => $commenter, 'date' => date('Y-m-d H:i:s'))) ;
        }
		
		
		public function updateReview( $id, $review_title, $review_text  ) 
		{
			$aSet = array('review_title' => $review_title, 'review_text' => $review_text);
			$aWhere = array('id' => $id);
			return $this->_update($this->getTable_Reviews(), $aSet, $aWhere);
		}
		
		public function updateUserReview( $id, $review_title, $review_text  ) 
		{
			$aSet = array('review_title' => $review_title, 'review_text' => $review_text);
			$aWhere = array('id' => $id);
			return $this->_update($this->getTable_User_Reviews(), $aSet, $aWhere);
		}
		
		public function activeReview( $id, $status) 
		{
			$aSet = array('status' => $status);
			$aWhere = array('id' => $id);
			
			return $this->_update($this->getTable_Reviews(), $aSet, $aWhere);
		}
		public function activeUserReview( $id, $status) 
		{
			$aSet = array('status' => $status);
			$aWhere = array('id' => $id);
			
			return $this->_update($this->getTable_User_Reviews(), $aSet, $aWhere);
		}		
		 
       // update
        function _update($table, $values, $where)
        {
            $this->dao->from($table) ;
            $this->dao->set($values) ;
            $this->dao->where($where) ;
            return $this->dao->update() ;
        }
    }
?>