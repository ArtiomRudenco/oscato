<?php
/*
Plugin Name: Category Listings
Plugin URI: https://www.drizzlethemes.com/
Description: This plugin helps you to show the Latest / Premium listings from selected Categories.
Version: 1.0.1
Author: Drizzle
Author URI: https://www.drizzlethemes.com/
Short name: category_listings
Plugin update URI: category-listings-plugin
*/


/* Install Plugin */
function category_listings_install() {
	osc_set_preference('txt_align', 'left', 'category_listings');
	osc_set_preference('btn_color', '#FFFFFF', 'category_listings');
	osc_set_preference('btn_bg', '#35C3D9', 'category_listings');
	osc_set_preference('owl_slider', '1', 'category_listings');
	osc_set_preference('owl_settings', trim('margin:20,
	loop:false,
	nav: true,
	responsive:{
	0:{items:1,nav: true},
	480:{items:3,nav: true},
	600:{items:3},
	1000:{items:3}
	}'), 'category_listings');
	osc_goto_first_category();
	$i = 0; while ( osc_has_categories() ) {
	osc_set_preference('show_only_'.osc_category_id(), '2', 'category_listings');
	osc_set_preference('total_item_'.osc_category_id(), '12', 'category_listings');
	$i++; }
}

/* Uninstall Plugin */
function category_listings_uninstall() {
	osc_delete_preference('txt_align', 'category_listings');
	osc_delete_preference('btn_color', 'category_listings');
	osc_delete_preference('btn_bg', 'category_listings');
	osc_delete_preference('owl_settings', 'category_listings');
	osc_delete_preference('owl_slider', 'category_listings');
	osc_goto_first_category();
	$i = 0; while ( osc_has_categories() ) {
	osc_delete_preference('show_only_'.osc_category_id(), 'category_listings');
	osc_delete_preference('total_item_'.osc_category_id(), 'category_listings');
	$i++; }
}

function category_listings_actions() {
  $dao_preference = new Preference();
  $option = Params::getParam('option');

  if (Params::getParam('file') != 'category_listings/admin/settings.php') {
    return '';
  }

  if ($option == 'settings') {
  	osc_set_preference('show_only', Params::getParam("show_only") ? Params::getParam("show_only") : '1', 'category_listings', 'STRING');
	osc_set_preference('owl_slider', Params::getParam("owl_slider") ? Params::getParam("owl_slider") : '1', 'category_listings', 'STRING');
    osc_set_preference('txt_align', Params::getParam("txt_align") ? Params::getParam("txt_align") : 'left', 'category_listings', 'STRING');
	osc_set_preference('btn_color', Params::getParam("btn_color") ? Params::getParam("btn_color") : '#ffffff', 'category_listings', 'STRING');
    osc_set_preference('btn_bg', Params::getParam("btn_bg") ? Params::getParam("btn_bg") : '#000000', 'category_listings', 'STRING');
	osc_set_preference('owl_settings', trim(Params::getParam('owl_settings', false, false, false)), 'category_listings');

		osc_goto_first_category();
	$i = 0; while ( osc_has_categories() ) {
	osc_set_preference('cat_item_'.osc_category_id(), '1', 'category_listings');
	osc_set_preference('cat_item_'.osc_category_id(), Params::getParam('cat_item_'.osc_category_id()), 'category_listings');
	osc_set_preference('show_only_'.osc_category_id(), Params::getParam('show_only_'.osc_category_id()), 'category_listings');
	osc_set_preference('total_item_'.osc_category_id(), Params::getParam('total_item_'.osc_category_id()), 'category_listings');
	$i++; }

    osc_add_flash_ok_message(__('Settings has been updated', 'category_listings'), 'admin');
    osc_redirect_to(osc_admin_render_plugin_url('category_listings/admin/settings.php'));
  }
}
function show_only() {
  return(osc_get_preference('show_only', 'category_listings'));
}

function owl_slider() {
  return(osc_get_preference('owl_slider', 'category_listings'));
}
function txt_align() {
  return(osc_get_preference('txt_align', 'category_listings'));
}
function owl_settings() {
  return(osc_get_preference('owl_settings', 'category_listings'));
}
function btn_color() {
  return(osc_get_preference('btn_color', 'category_listings'));
}
function btn_bg() {
  return(osc_get_preference('btn_bg', 'category_listings'));
}

osc_add_hook('init_admin', 'category_listings_actions');
/* Custom items from category */
function category_listings_list_categories() {
		osc_goto_first_category();
	  	$i = 0; while ( osc_has_categories() ) { ?>


        <dt><input id="cat_item_<?php echo osc_category_id(); ?>" <?php if( osc_get_preference('cat_item_'.osc_category_id(), 'category_listings') == '1') { echo"checked='checked'";} ?> type="checkbox" name="cat_item_<?php echo osc_category_id();?>" value="<?php echo osc_esc_html( osc_get_preference('cat_item_'.osc_category_id(), 'category_listings') ); ?>"> <?php echo osc_category_name();?> <a href="">Edit</a></dt>
        <dd>
        	<div class="form-row">
            <?php _e('Show', 'category_listings'); ?>

                    <input type="radio" name="show_only_<?php echo osc_category_id();?>" <?php if(osc_get_preference('show_only_'.osc_category_id(), 'category_listings')=='2'){echo "checked='checked'";} ?> value="2" /><?php _e('Latest ads', 'category_listings'); ?> <input type="radio" name="show_only_<?php echo osc_category_id();?>" <?php if(osc_get_preference('show_only_'.osc_category_id(), 'category_listings')=='1'){echo "checked='checked'";} ?> value="1" /><?php _e('Premium ads', 'category_listings'); ?>
             </div>
             <div class="form-row">
             <?php _e('Total Items', 'category_listings'); ?>
             <input type="text"  name="total_item_<?php echo osc_category_id();?>" value="<?php echo osc_get_preference('total_item_'.osc_category_id(), 'category_listings') ?>"/>
             </div>
        </dd>

   <script type="text/javascript">
   $('#cat_item_<?php echo osc_category_id(); ?>').click(function(){

        if($(this).prop('checked')){
              $(this).val('1');
         }else{
              $(this).val('2');
         }
    });
   </script>

   <?php $i++; } }

function category_listings_header(){
	osc_enqueue_style('owlcss', osc_base_url().'oc-content/plugins/category_listings/assets/owl/css/owl.carousel.min.css');
	osc_register_script('owljs', osc_base_url().'oc-content/plugins/category_listings/assets/owl/js/owl.carousel.min.js');
	osc_enqueue_script('owljs'); ?>
<?php }
if(owl_slider() == 1) {
	osc_add_hook('header', 'category_listings_header'); ?>
<?php }

//All categorises in loop
function show_category_listings(){ ?>
    <section class="carousel-section">
        <div class="container">
            <div class="carousel-section__ins">
                <div class="carousel-wrp">
                    <div class="carousel owl-carousel two-ca">
   <?php osc_goto_first_category(); ?>
    <?php if ( osc_count_categories() >= 1 ) { ?>
            <?php while ( osc_has_categories() ) { ?>
            <?php if( osc_get_preference('cat_item_'.osc_category_id(), 'category_listings') == '1') {?>
                <?php osc_query_item(array('category'=>osc_category_id(), 'premium' => osc_get_preference('show_only_'.osc_category_id(), 'category_listings'), 'results_per_page' => osc_get_preference('total_item_'.osc_category_id(), 'category_listings')));
                if( osc_count_custom_items() >= 1) { ?>

                            <?php while ( osc_has_custom_items() ) { ?>
                        <div class="item">
                            <?php if( osc_images_enabled_at_items() ) { ?>
                            <?php if( osc_count_premium_resources() ) { ?>
                            <a href="<?php echo osc_premium_url() ; ?>" class="item__photo">
                                <img src="<?php echo osc_resource_thumbnail_url(); ?>" alt="<?php echo osc_highlight(osc_rus2url(osc_premium_title())); ?>">
                                <?php } else { ?>
                                <a href="<?php echo osc_premium_url() ; ?>" class="item__photo">
                                    <img src="<?php echo osc_current_web_theme_url('img/no_photo.gif') ; ?>">
                                    <?php } ?>
                                    <?php } ?>
                                    <div class="premium_label">
                                        <span class="item__favourites"><i class="mdi mdi-star-outline"></i><?php _e('Premium', 'eva'); ?></span>
                                    </div>
                                    <span class="purchased"><?php echo osc_format_date(osc_premium_pub_date()); ?></span>
                                    <div class="overlay"></div>
                                </a>
                                <?php if( osc_get_preference('item-icon', 'eva') == 'enable') {?>
                                    <div class="item__cat">
                                        <img src="<?php echo osc_current_web_theme_url('img/').eva_category_root(osc_premium_category_id()).'.png'; ?>">
                                    </div>
                                <?php } ?>
                                <div class="item__ins" id="<?php if(function_exists('upayments_premium_get_class_color')){echo upayments_premium_get_class_color(osc_premium_id());}?>">
                                    <div class="item__middle-desc">
                                        <a href="<?php echo osc_premium_url() ; ?>" class="item__title"><?php echo osc_highlight(osc_premium_title()); ?></a>
                                        <div class="item__text">
                                            <div><?php echo osc_highlight(osc_premium_description()); ?></div>
                                        </div>
                                        <span class="item__date"><i class="mdi mdi-map-marker"></i> <?php echo osc_premium_city(); ?></span>
                                        <strong class="item__price"><?php if( osc_price_enabled_at_items() && osc_item_category_price_enabled(osc_premium_category_id()) ) {?>
                                                <i class="mdi mdi-tag"></i> <?php echo osc_premium_formated_price() ; } ?></strong>
                                    </div>
                                </div>
                        </div>
                            <?php } ?>
                    <?php } ?>
                 <?php } ?>
            <?php } ?>
        <?php } ?>
            </div>
        </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
		$(".owl-carousel").owlCarousel({
		 	<?php echo owl_settings(); ?>
		});
	</script>
    <style>
	.category-listings h2{text-align: <?php echo txt_align(); ?>;}
	.owl-carousel .owl-nav .owl-next, .owl-carousel .owl-nav .owl-prev {
	background: <?php echo btn_bg(); ?> !important;
	color: <?php echo btn_color(); ?> !important;
	font-size:35px !important;
	top: 43%;
	width: 50px;
	padding: 17px 0 !important;
	position: absolute;
	text-align: center;
	}
	.owl-nav .owl-next  {
		right: 0;
	}
	.owl-nav .owl-prev {
		left: 0;
	}
	</style>
<?php }

function category_listings_user_menu() {
	echo '<li class="submenu-divide">' . __('Category Listings', 'category_listings') . '</li>';
	echo '<li><a href="' . osc_admin_render_plugin_url(osc_plugin_folder(__FILE__) . 'admin/settings.php') . '" >' . __('Settings', 'category_listings') . '</a></li>';
	echo '<li><a href="' . osc_admin_render_plugin_url(osc_plugin_folder(__FILE__) . 'admin/settings.php') . '" >' . __('Help', 'category_listings') . '</a></li>';
}
function cust_query_premium($mSearch, $key, $value) {
    if ($key == 'premium') {
		$mSearch->addConditions(sprintf("%st_item.b_premium = 1", DB_TABLE_PREFIX) );
    }
}
osc_add_hook('custom_query', 'cust_query_premium');
osc_add_hook('admin_menu', 'category_listings_user_menu');
osc_add_hook(osc_plugin_path(__FILE__) . '_uninstall', 'category_listings_uninstall') ;
osc_register_plugin(osc_plugin_path(__FILE__), 'category_listings_install') ;
?>
