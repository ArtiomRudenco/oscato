<?php include('header.php'); ?>

<h2><?php _e('How to show the Category listings?', 'category_listings'); ?></h2>
<p><?php _e('Place a following code into your theme Main(main.php) file Or Where you want to display the Category Listings.', 'category_listings'); ?></p>

<pre>
    &lt;?php if(function_exists('show_category_listings')){ echo show_category_listings(); } ?&gt;
</pre>

<div class="related_plugins">
            <h2 style="border-bottom:1px solid #ddd; padding-bottom:5px;"><?php _e('Other plugins you may like', 'category_listings'); ?></h2>
            <ul style="padding: 0;margin: 0;">
            	<li><a href="https://market.osclass.org/plugins/ad-management/advanced-delete_751"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/advdelete.jpg" width="150" /></a></li>
                <li><a href="https://market.osclass.org/plugins/reviews-ratings/reviews-plugin_826"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/review.jpg" width="150"/></a></li>
                <li><a href="https://market.osclass.org/plugins/e-mail/newsletter-plugin_715"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/newsletter.jpg" width="150"/></a></li>
                <li><a href="https://market.osclass.org/plugins/appearance/menu-manager_717"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/menumanager.jpg" width="150"/></a></li>
                <li><a href="https://market.osclass.org/plugins/site-recommendation/blog-for-osclass_820"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/blog.jpg" width="150"/></a></li>
                <li><a href="https://market.osclass.org/plugins/appearance/easy-translator_713"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/easytranslator.jpg" width="150"/></a></li>
            </ul>
            </div>

<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Category Listings Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>