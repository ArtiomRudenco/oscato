<?php
include('header.php');
 $pluginInfo = osc_plugin_get_info('category_listings/index.php');  ?>

<div class="plugin-content">

<div class="form-horizontal">
    <form action="<?php echo osc_admin_render_plugin_url('category_listings/admin/settings.php'); ?>" method="post">
      <input type="hidden" name="option" value="settings" />
      <div class="left">
          <h4><?php _e('Genaral', 'category_listings'); ?></h4>
         
          
         <div class="form-row">
            <div class="form-label"><?php _e('Title Placement', 'category_listings'); ?></div>
            <div class="form-controls">
                <select name="txt_align">
                    <option <?php if (txt_align() == "left"){echo "selected='selected'";} ?> value="left"><?php _e('Left', 'category_listings'); ?></option>
                    <option <?php if (txt_align() == "center"){echo "selected='selected'";} ?> value="center"><?php _e('Center', 'category_listings'); ?></option>
                </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label"><?php _e('Carousel', 'category_listings'); ?></div>
                 <div class="form-controls">
                    <input type="radio" name="owl_slider" <?php if(owl_slider()=='1'){echo "checked='checked'";} ?> value="1" /><?php _e('Enable', 'category_listings'); ?> <input type="radio" name="owl_slider" <?php if(owl_slider()=='2'){echo "checked='checked'";} ?> value="2" /><?php _e('Disable', 'category_listings'); ?>
                </div>
          </div>
          <div class="form-row">
            <div class="form-label"><?php _e('Navigation Button Color', 'category_listings'); ?></div>
            <div class="form-controls">
                <input type="text" name="btn_color" value="<?php echo osc_esc_html(btn_color()); ?>">
                <br /><small>Default: #FFFFFF</small>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label"><?php _e('Navigation Background Color', 'category_listings'); ?></div>
            <div class="form-controls">
                <input type="text" name="btn_bg" value="<?php echo osc_esc_html(btn_bg()); ?>">
                <br /><small>Default: #35C3D9</small>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label"><?php _e('Carousel Settings', 'category_listings'); ?></div>
            <div class="form-controls">
                <textarea name="owl_settings"><?php echo owl_settings(); ?></textarea>
                <br /><small>Owl settings: <a href="https://owlcarousel2.github.io/OwlCarousel2/demos/basic.html">Documentation</a></small>
            </div>
          </div>
       </div>
       <div class="right">
            <h4><?php _e('Choose Categories', 'category_listings'); ?></h4>
            <div class="list-categories">
                <dl class="accordion">
                    <?php echo category_listings_list_categories(); ?>
                </dl>
            </div>
       </div>
       <div class="clear"></div>
      <div class="form-actions">
        <input type="submit" value="<?php _e('Save Changes', 'category_listings'); ?>" class="btn btn-submit">
      </div>
      
    </form>
    </div>
</div>
<div class="plugin-footer">
	&copy; <?php echo date('Y'); ?> Category Listings Plugin. Developed by <a href="https://market.osclass.org/user/profile/15" target="_blank">DrizzleThemes</a>.
</div>
<script type="text/javascript">
(function($) {
    
  var allPanels = $('.accordion > dd').hide();
    
  $('.accordion > dt > a').click(function() {
    allPanels.slideUp();
    $(this).parent().next().slideDown();
    return false;
  });

})(jQuery);
</script>
<style>
.list-categories dt {
	background: #ddd;
	margin: 5px 0;
	padding: 10px;
	font-size: 16px;
}
.list-categories dt a {
	float: right;
}
.right {
	width: 500px;
	float: right;
}
.left {
	width: 500px;
	float: left;
}

</style>