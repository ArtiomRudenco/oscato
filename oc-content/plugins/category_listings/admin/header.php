<link href="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>css/plugin.css" type="text/css" rel="stylesheet" />
<?php
$param = Params::getParam('file');
switch($param) {
	case "category_listings/admin/settings.php":
	$title = "Settings";
	$setActive = "active";
	break;
	case "category_listings/admin/help.php":
	$title = "Help";
	$helpActive = "active";
	break;
}

?>
<div class="plugin-header clearfix">

	<h1 class="float_left">Category Lisitings - <small><?php _e($title, 'category_listings'); ?></small></h1>
    <a class="float_right" href="https://market.osclass.org/user/profile/15" target="_blank"><img src="<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>images/dt_white_web_logo.png" alt="DrizzleThemes - Osclass Themes and Osclass Plugins" /></a>
</div>

<div class="sub-header clearfix">
    <ul>
        <li class="<?php echo @$setActive;?>"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/settings.php');?>"><?php _e('Settings', 'category_listings'); ?></a></li>
        <li class="<?php echo @$helpActive;?>"><a href="<?php echo osc_admin_render_plugin_url(osc_plugin_path(dirname(__FILE__)) . '/help.php');?>"><?php _e('Help', 'category_listings'); ?></a></li>
    </ul>
</div>   