<?php
		   /*
 * Copyright 2018 osclass-pro.com
 *
 * You shall not distribute this theme and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
 osc_goto_first_category();
?>
    <footer class="footer-page">
        <div class="footer-main">
            <div class="container">
                <div class="footer-main__logo">
				<?php if( osc_get_preference('footer-logo', 'eva') == 'enable') {?>
                    <a href="<?php echo osc_base_url(); ?>"><?php echo logo_header(); ?></a>
					<?php } ?>
                </div>
                <div class="footer__contacts">
				<?php if( osc_get_preference('footer-categories', 'eva') == 'enable') {?>
				<ul class="footer_list">
						<?php while(osc_has_categories()){ ?>
						<li class="list__item"><a href="<?php echo osc_search_category_url(); ?>" class="item__link"><?php if(strlen(osc_category_name()) > 25){ echo mb_substr(osc_category_name(), 0, 23,'UTF-8').'...'; } else { echo osc_category_name(); } ?></a></li>
						<?php } ?>
					</ul>
					<?php } ?>
                </div>
            </div>
        </div>
		        <div class="footer-widgets">
            <div class="container">
                <div class="footer-widgets__ins">
                    <article class="footer-widget">
<?php osc_show_widgets('footer1') ; ?>
                    </article>
                    <article class="footer-widget">
<?php osc_show_widgets('footer2') ; ?>
                    </article>
                    <article class="footer-widget">
<?php osc_show_widgets('footer3') ; ?>
                    </article>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
			    <div class="footer-bottom__copyright">
                <span>&copy;<?php echo date('Y'); ?>. <?php if(osc_get_preference('contact-copy', 'eva') != ''){?>
				<?php echo osc_get_preference('contact-copy', 'eva'); ?>
				<?php } ?></span>
                </div>
				<div class="footer-bottom__center">
				                    <ul class="social-list">
						<?php if( osc_get_preference('facebook-evarevo', 'eva') != '') {?>
								    <li><a href="<?php echo osc_get_preference('facebook-evarevo', 'eva'); ?>" target="_blank" class="fc-link"><i class="mdi mdi-facebook"></i></a></li>
									<?php } if( osc_get_preference('twitter-evarevo', 'eva') != '') {?>
									<li><a href="<?php echo osc_get_preference('twitter-evarevo', 'eva'); ?>" target="_blank" class="tw-ico"><i class="mdi mdi-twitter"></i></a></li>
									<?php } if( osc_get_preference('link-evarevo', 'eva') != '') {?>
									<li><a href="<?php echo osc_get_preference('link-evarevo', 'eva'); ?>" target="_blank" class="g-link"><i class="mdi mdi-linkedin"></i></a></li>
									<?php } if( osc_get_preference('vk-evarevo', 'eva') != '') {?>
									<li><a href="<?php echo osc_get_preference('vk-evarevo', 'eva'); ?>" target="_blank" class="vk-link"><i class="mdi mdi-vk"></i></a></li>
									<?php } if( osc_get_preference('pinterest-evarevo', 'eva') != '') {?>
									<li><a href="<?php echo osc_get_preference('pinterest-evarevo', 'eva'); ?>" target="_blank" class="p-ico"><i class="mdi mdi-pinterest"></i></a></li>
									<?php } if( osc_get_preference('in-evarevo', 'eva') != '') {?>
									<li><a href="<?php echo osc_get_preference('in-evarevo', 'eva'); ?>" target="_blank" class="in-ico"><i class="mdi mdi-instagram"></i></a></li>
									<?php } if( osc_get_preference('odnoklassniki-evarevo', 'eva') != '') {?>
									<li><a href="<?php echo osc_get_preference('odnoklassniki-evarevo', 'eva'); ?>" target="_blank" class="ok-link"><i class="mdi mdi-odnoklassniki"></i></a></li>
								    <?php } ?>
                    </ul>
					</div>
                <div class="footer-bottom__text">
								<?php if(osc_count_static_pages() > 0){?>
								<?php osc_reset_static_pages() ; ?>
				<?php while(osc_has_static_pages()){?>
							<span><a href="<?php echo osc_static_page_url(); ?>" title="<?php echo osc_esc_html(osc_static_page_title()); ?>"><?php echo osc_esc_html(osc_static_page_title()); ?></a> |  </span>
						<?php } ?>
						<?php } ?>
					<span><a href="<?php echo osc_contact_url(); ?>" ><?php _e('Contact', 'eva'); ?></a></span>
                </div>
            </div>
        </div>
<?php osc_run_hook('footer'); ?>
	</footer>

<div class="widget-form modalcontact" id="modal-select-city">
    <div class="modal-content">
        <span class="closemodal">&times;</span>
        <span class="widget-form__title"><?php _e("Выбрать город", 'eva'); ?></span>
        <label>
            <input type="text" class="input" name="city" id="select-city-input" value="" placeholder="<?= osc_esc_html(__('Введите назание', 'eva')) ?>">
        </label>
        <ul class="select-cities">
            <?php
                $citiesPopular = osc_cities_popular();
            ?>
            <?php foreach ($citiesPopular as $item): ?>
                <li>
                    <a href="<?= osc_city_url_domain($item) ?>" title="<?= $item['label'] ?>">
                        <?= $item['label'] ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php if (true): ?>
    <div class="widget-form modalcontact" id="modal-categories">
        <div class="modal-content-adaptive">
            <?php if(osc_users_enabled()) { ?>
                                <?php if( osc_is_web_user_logged_in() ) { ?>
                                    <li class="login_btn"><a href="<?php echo osc_user_dashboard_url(); ?>" <?php if(Params::getParam('page') == 'user'){echo 'class="active"';} ?>><strong><i class="mdi mdi-account"></i><?php _e('My account', 'eva'); ?></strong></a></li>
                                    <!-- <li class="login_btn"><a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'eva'); ?></a></li> -->
                                <?php } else { ?>
                                    <li class="login_btn"><a href="<?php echo osc_user_login_url(); ?>" <?php if(Params::getParam('page') == 'login'){echo 'class="active"';} ?> data-fancybox="modal2" data-src="#insign"><strong><i class="mdi mdi-account"></i><?php _e('Sign in', 'eva'); ?></strong></a></li>
                                    <!-- <?php if(osc_user_registration_enabled()) { ?>
                                        <li><a href="<?php echo osc_register_account_url(); ?>" <?php if(Params::getParam('page') == 'register'){echo 'class="active"';} ?>><strong><?php _e('Sign up', 'eva'); ?></strong></a></li>
                                    <?php } ?> -->
                                <?php } ?>
                            <?php } ?>
            <span class="closemodal">&times;</span>
            <span class="widget-form__title"><h2 class="h2-bottom-line">КАТАЛОГ ОРГАНИЗАЦИЙ</h2></span>
            <?php
            osc_goto_first_category();
            ?>
            <div class="row categories">
                <?php while ( osc_has_categories() ) { ?>
                    <div class="col-sm-3 main-cat">
                        <div class="category">
                            <div class="head">
                                <!-- <div class="icon">
                                    <a href="?php echo osc_search_category_url(); ?>"><img src="?php echo osc_current_web_theme_url('img/') . osc_category_id() .'.png' ?>" alt=""></a>
                                </div> -->
                                <div class="title">
                                    <a href="<?php echo osc_search_category_url(); ?>"><?php if(strlen(osc_category_name()) > 25){ echo mb_substr(osc_category_name(), 0, 23,'UTF-8').'...'; } else { echo osc_category_name(); } ?></a>
                                </div>
                            </div>
                            <?php if ( osc_count_subcategories() > 0 ) { ?>
                                <div class="items">
                                    <?php $num = 0; while ( osc_has_subcategories()) { ?>
                                        <a class="subcategory-item" href="<?php echo osc_search_category_url() ; ?>">
                                            <?= osc_category_name() ?>
                                        </a>
                                        <?php $num++; ?>
                                    <?php } ?>
                                </div>
                                <?php View::newInstance()->_erase('subcategories') ; ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <style>
    .categories .h2-bottom-line {
        margin-top: 53px;
    }
    .categories .items {
            max-width: 100%;
            box-shadow: 0 0 16px rgba(109, 109, 109, 0.25);
            background-color: #fff;
            padding: 10px 20px 6px;
    }
    .categories .category .subcategory-item {
        font-size: 16px;
    }
    .categories .main-cat {
            line-height: 30px;
        }
    #login {
        margin: 30px 0px;
        margin-left: 10px;
        font-size: 20px;
    }
    .content_page {
        background: #fff;
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.16);
        padding: 27px 20px 40px;
        margin-bottom: 53px;
    }
    .item-inline__title .date {
        font-size: 15px;
        color: #bdbcbc;
    }
    .review_ratings {
        font-size: 16px;
        width: 100%;
    }
    .row .categories {
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.25);
        background-color: #fff;
        padding: 10px 20px 6px;
    }
    .categories .items {
        box-shadow: none;
        background-color: inherit;
        padding: 0px;
    }
    ul.filter-category-listings li a {
        font-weight: normal;
    }
    .um-page-header {
        color: #959494;
    }
    .um-top-menu li a {
        color: #1531AE!important;
        transition: all .2s ease;
        opacity: 1;
    }
    .um-top-menu li a:hover {
        color: #6BE400!important;
    }
    .alert-warning {
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.25);
        background-color: #fff;
        color: #404040;
        margin-bottom: 20px;
    }
    .login_btn {
        font-size: 19px;
        margin-top: 20px;
    }
    .modal-content-adaptive li {
        list-style: none;
    }
    .categories .h2-bottom-line {
        margin-top: 53px;
    }
    .widget-form.modalcontact .h2-bottom-line {
        font-size: 20px;
    }
    .login_btn {
        display: none;
    }
    ul.filter-category-listings li a {
        text-decoration: none;
    }
    .box {
        
    }
    .widget-form__title .h2-bottom-line {
            font-size: 32px;
        }
    .modal-content-adaptive {
            margin-left: 0px;
        }
    .widget-form.modalcontact {
        padding-top: 0px;
    }
    .modal-content-adaptive {
            max-width: 50vw;
        }
    .d-block .short-search-trigger.open {
        display: none!important;
    }
    .page-title, .board-list h2, .user-watchlist h1, .um-page-header {
        border-left: 3px solid #1531AE!important;
        display: inline-block;
        padding: 5px 0;
        z-index: 1;
        padding-left: 10px;
        font-size: 32px;
    }
    .user-watchlist h1, .um-page-header {
        text-transform: uppercase;
        font-weight: 400;
        margin: 0 0 25px;
        letter-spacing: 1px;
        color: #444343;
    }
    .user-watchlist h1 strong{
        font-weight: 400;
    }
    @media screen and (max-width: 1500px) {
        .modal-content-adaptive {
            max-width: 65vw;
        }
    }
    @media screen and (max-width: 1200px) {
        .modal-content-adaptive {
            max-width: 80vw;
        }
    }
    @media screen and (max-width: 1000px) {
        .modalcontact {
            padding-top: 0px;
            padding-left: 0px;
            margin-left: 0px;
        }
        .modal-content-adaptive {
            margin-left: 0px;
            max-width: 100%;
            margin-right: 0%;
            width: 100%;
        }
        .widget-form__title .h2-bottom-line {
            font-size: 20px;
            margin-top: 30px;
        }
        .container.notmain {
            margin: 0px 30px;
        }
        .closemodal {
            position: absolute;
            top: 10px;
            right: 5%;
        }
        .login_btn {
            display: block;
        }
    }
    @media screen and (max-width: 700px) {
        .categories .main-cat {
            line-height: 35px;
            width: 100%;
        }
        .top-bar__logo-wrp {
            text-align: center;
            margin-left: 7%;
        }
        .top-bar__left {
            margin: 0px auto;
            padding-top: 20px;
            padding-bottom: 8px;
            margin-left: 33.5%;
        }
    }
    @media screen and (max-width: 500px) {
        /*.categories .title {
            padding-left: 5px;
        }*/
        .footer-widgets__ins article {
            width: 100%;
        }
        .footer-widgets {
            display: block;
        }
        .top-bar__logo-wrp {
            margin-left: 4%;
        }
        .top-bar .container {

        }
    
    }
</style>

    <script>
        $(function() {
            var modal = $('#modal-categories');
            var openButton = $('#button-categories, .mobile-menu-trigger');
            var closeButton = modal.find('.closemodal');

            closeButton.on('click', function () {
                modal.hide();
            });
            openButton.on('click', function (e) {
                e.preventDefault();
                modal.show();
            });
            $(window).on('click', function (e) {
                if (e.target === modal[0]) {
                    modal.hide();
                }
            });
        });
    </script>
<?php endif; ?>

<script>
    $(function() {
        var modal = $('#modal-select-city');
        var openButton = $('#button-select-city');
        var closeButton = modal.find('.closemodal');

        closeButton.on('click', function () {
            modal.hide();
        });
        openButton.on('click', function (e) {
            e.preventDefault();
            modal.show();
        });
        $(window).on('click', function (e) {
            if (e.target === modal[0]) {
                modal.hide();
            }
        });

        $( "#select-city-input" ).autocomplete({
            source: "<?php echo osc_base_url(true); ?>?page=ajax&action=location",
            appendTo: '#modal-select-city .modal-content',
            minLength: 2,
            select: function( event, ui ) {
                document.location.href = ui.item.url;
            }
        });
    });
</script>
<script>
    $('.d-block .short-search-form').addClass('open');
    $('body').click(function(){
        $('.d-block .short-search-form').addClass('open');
    });
    $('body').contextmenu(function(){
        $('.d-block .short-search-form').addClass('open');
    });
</script>
