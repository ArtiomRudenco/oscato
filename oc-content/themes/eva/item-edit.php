<?php
/*
* Copyright 2019 osclass-pro.com and osclass-pro.ru
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/
osc_enqueue_script('jquery-ui');
osc_enqueue_script('fineuploader');
osc_enqueue_script('owl');
osc_enqueue_script('main');
osc_enqueue_script('select');
osc_enqueue_script('date');
osc_enqueue_script('jquery-validate');
osc_enqueue_script('rotate-js');
?>
<!DOCTYPE html>
<html lang="<?php echo str_replace('_', '-', osc_current_user_locale()); ?>">
<head>
    <?php osc_current_web_theme_path('head.php'); ?>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="googlebot" content="noindex, nofollow" />

    <!-- only item-post.php -->
    <?php ItemForm::location_javascript(); ?>
    <?php
    if(osc_images_enabled_at_items() && !eva_is_fineuploader()) {
        ItemForm::photos_javascript();
    }
    ?>
    <script type="text/javascript">
        function uniform_input_file(){
            photos_div = $('div.photos');
            $('div',photos_div).each(
                function(){
                    if( $(this).find('div.uploader').length == 0  ){
                        divid = $(this).attr('id');
                        if(divid != 'photos'){
                            divclass = $(this).hasClass('box');
                            if( !$(this).hasClass('box') & !$(this).hasClass('uploader') & !$(this).hasClass('row')){
                                $("div#"+$(this).attr('id')+" input:file").uniform({fileDefaultText: fileDefaultText,fileBtnText: fileBtnText});
                            }
                        }
                    }
                }
            );
        }

        <?php if(osc_locale_thousands_sep()!='' || osc_locale_dec_point() != '') { ?>
        $().ready(function(){
            $("#price").blur(function(event) {
                var price = $("#price").prop("value");
                <?php if(osc_locale_thousands_sep()!='') { ?>
                while(price.indexOf('<?php echo osc_esc_js(osc_locale_thousands_sep());  ?>')!=-1) {
                    price = price.replace('<?php echo osc_esc_js(osc_locale_thousands_sep());  ?>', '');
                }
                <?php } ?>
                <?php if(osc_locale_dec_point()!='') { ?>
                var tmp = price.split('<?php echo osc_esc_js(osc_locale_dec_point())?>');
                if(tmp.length>2) {
                    price = tmp[0]+'<?php echo osc_esc_js(osc_locale_dec_point())?>'+tmp[1];
                }
                <?php }; ?>
                $("#price").prop("value", price);
            });
        });
        <?php } ?>
    </script>
    <!-- end only item-post.php -->
</head>
<body>
<?php osc_current_web_theme_path('header.php'); ?>

<div class="container">
    <div class="forcemessages-inline">
        <?php osc_show_flash_message(); ?>
    </div>
    <!-- content -->
<div class="publish">
                <form name="item" action="<?php echo osc_base_url(true);?>" class="form-publish" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="item_edit_post" />
                    <input type="hidden" name="page" value="item" />
                    <input type="hidden" name="id" value="<?php echo osc_item_id();?>" />
                    <input type="hidden" name="secret" value="<?php echo osc_item_secret();?>" />
					                    <h2 class="center"><?php _e('Update your listing', 'eva'); ?></h2>
                    <div class="inp-group">
                        <h4 class="inp-group__title"><?php _e('Category', 'eva')?> <span>*</span></h4>
						<div class="input-row catpub">
                                    <?php ItemForm::category_multiple_selects(null, null, __('Select a category', 'eva')); ?>
</div></div>
                    <?php if(osc_get_preference('custom-fileds', 'eva') == 'top'){ ?>
					<div class="inp-group">
                        <div class="box">
                            <?php ItemForm::plugin_edit_item(); ?>
                        </div> </div>
                    <?php } ?>
					<div class="inp-group">
<h4 class="inp-group__title"><?php _e('Title', 'eva'); ?> <span>*</span></h4>
                            <div class="inp-counter titledis">
                                <?php ItemForm::title_input('title', osc_current_user_locale(), osc_esc_html( eva_item_title() )); ?>
								<span class="inp-counter__count" data-val="<?php echo osc_max_characters_per_title(); ?>"><?php echo osc_max_characters_per_title(); ?></span>
                        </div></div>
                        <div class="inp-group">
						<h4 class="inp-group__title"><?php _e('Description', 'eva'); ?> <span>*</span></h4>
<div class="inp-counter titledis">
                                <?php ItemForm::description_textarea('description',osc_current_user_locale(), osc_esc_html( eva_item_description() )); ?>
								 <span class="inp-counter__count bottom-count" data-val="<?php echo osc_max_characters_per_description(); ?>"><?php echo osc_max_characters_per_description(); ?></span>
                        </div> </div>
                    <?php if( osc_images_enabled_at_items() ) { ?>
                    <div class="inp-group">
                        <h4 class="inp-group__title plus__image"><?php _e('Add images','eva')?></h4>
						<span class="inp-group__sub-title"><?php _e('You can upload up to', 'eva'); ?> <?php echo osc_max_images_per_item(); ?> <?php _e('pictures per listing', 'eva'); ?></span>
                        <span class="inp-group__sub-title"><?php _e('Max size', 'eva'); ?> <?php echo osc_max_size_kb(); ?> <?php _e('Kb for each image', 'eva'); ?></span>
				  <div class="load-img">
                        <?php if(osc_images_enabled_at_items()) {
                            if(eva_is_fineuploader()) {
                                ItemForm::ajax_photos();
                            }
                        } else { ?>
                            <div id="photos" class="load-img">
                                <div class="row">
                                    <input type="file" name="photos[]" />
                                </div>
                            </div>
                            <a href="#" onclick="addNewPhoto(); uniform_input_file(); return false;"><?php _e('Add new photo', 'eva'); ?></a>

                        <?php } ?>
                    </div>
					                    </div>
                    <?php } ?>
                    <!--  -->
                    <?php if( osc_price_enabled_at_items() ) { ?>
                    <div class="inp-group">
                            <h4 class="inp-group__title"><?php _e('Price', 'eva'); ?></h4>
                        <div class="inp-select">
                        <div class="form-group-sm">
                        <?php ItemForm::price_input_text(); ?>
                        </div>
                        <div class="select_currency">
                        <?php ItemForm::currency_select(); ?>
                        </div>
                    </div></div>
                    <?php } ?>
                    
                    <?php if(osc_get_preference('item-post', 'eva') == 'countries'){ ?>
                    <div class="inp-group">
                        <h4 class="inp-group__title"><?php _e('Country', 'eva'); ?></h4>
                            <?php ItemForm::country_select(); ?>
                    </div>
                    <?php } ?>
					<?php if(osc_get_preference('item-post-loc', 'eva') == 'enable'){ ?>
                    <div class="inp-group">
					<div class="input-row">
					<div class="input-col">
                        <h4 class="inp-group__title"><?php _e('Region', 'eva'); ?></h4>
                            <?php ItemForm::region_select(osc_get_regions(osc_item_country_code()), osc_item()); ?>
							</div>
						<div class="input-col">
						<h4 class="inp-group__title"><?php _e('City', 'eva'); ?></h4>
                            <?php ItemForm::city_select(null, osc_item()); ?>
							</div>
                    </div></div>
                    <div class="inp-group">
                        <h4 class="inp-group__title"><?php _e('City Area', 'eva'); ?></h4>
                        <?php ItemForm::city_area_text(); ?>
                    </div>
                    <div class="inp-group">
                        <h4 class="inp-group__title"><?php _e('Address', 'eva'); ?></h4>
                        <?php ItemForm::address_text(); ?>
                    </div>
					<?php } ?>
                    <?php if(!osc_is_web_user_logged_in() ) { ?>
                    <div class="inp-group">
                        <h4 class="inp-group__title"><?php _e('Name', 'eva'); ?></h4>
                        <?php ItemForm::contact_name_text(); ?>
                    </div>
                    <div class="inp-group">
                        <h4 class="inp-group__title"><?php _e('E-mail', 'eva'); ?> <span>*</span></h4>
						<div class="input-row">
						<div class="input-col">
                        <?php ItemForm::contact_email_text(); ?>
                    </div>
                        <div class="input-col">
						<div class="checkbox-wrp">
                            <input type="checkbox" name="showEmail" id="showEmail">
                            <label for="showEmail"><span><?php _e('Show email on listing page', 'eva'); ?></span></label>
                        </div>
                    </div></div></div>
                    <?php } ?>
					
                    <?php if(osc_get_preference('custom-fileds', 'eva') == 'bottom'){ ?>
					<div class="inp-group">
                            <?php ItemForm::plugin_edit_item(); ?>
						</div>
                    <?php } ?>
                    <?php if( osc_recaptcha_items_enabled() ) {?>
						<script type="text/javascript">
                                                            var RecaptchaOptions = {
                                                                theme : 'clean'
                                                            };
                                                        </script>
					<div class="inp-group">
                            <div class="captch" style="transform:scale(0.8);transform-origin:0 0">
                                <?php osc_show_recaptcha(); ?>
                        </div></div>
                    <?php }?>
			<?php if( function_exists( "MyHoneyPot" )) { ?>		
			<?php MyHoneyPot(); ?>		
		<?php } ?>  			
                    <div class="inp-group">
                        <button type="submit" class="btn btn-center upcase">
                            <?php _e('Update', 'eva'); ?>
                        </button>
                    </div>
					<div class="inp-group">
                        <button type="submit" class="btn btn-center upcase" onclick="javascript:history.back(-1)">
                            <?php _e('Cancel', 'eva'); ?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- content -->
                    </div>
</div>
<?php osc_current_web_theme_path('footer.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
       
    $('body').on('click', 'a.qq-upload-delete', function() {
        var imgMain = $('#img-main').val(),
            img = $(this).parents('.qq-upload-success').find('img').attr('alt');
            
        if(imgMain == img) {
            $('#img-main').val('');
        }
    });
    
    $('body').on('click', 'i#rotate-edited', function(){
        var img = $(this).parents('.qq-upload-success').find('img'),
            imgName = img.attr('alt'),
            imgPath = img.attr('src'),
            angle = parseInt(img.attr('img-angle')),
            url = '<?php echo osc_base_url();?>/oc-content/themes/eva/ajax-img.php';
            angle = parseInt(img.attr('img-angle'));
			if(isNaN(angle)){angle = 0};
        angle += 90;
        
        setTimeout(function(){
            img.rotate({ 
                animateTo: angle,
                duration: 1000,
                callback: function() {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {"do" : "img-rotate", "itemType" : "edit", "imgName" : imgName, "imgPath" : imgPath},
                        error: function(){},
                        success: function(data){}
                    });
                }
            });
        }, 500);
        
        img.attr('img-angle', angle);
    });
    
    $('body').on('click', 'i#rotate-uploaded', function(){
        var img = $(this).parents('.qq-upload-success').find('img'),
            imgName = img.attr('alt'),
            url = '<?php echo osc_base_url();?>/oc-content/themes/eva/ajax-img.php',
            angle = parseInt(img.attr('img-angle'));
			if(isNaN(angle)){angle = 0};
        
        angle += 90;
        
        setTimeout(function(){
            img.rotate({ 
                animateTo: angle,
                duration: 1000,
                callback: function() {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {"do" : "img-rotate", "imgName" : imgName},
                        error: function(){},
                        success: function(data){}
                    });
                }
            });
        }, 500);
        
        img.attr('img-angle', angle);
    });
});
</script>
</body>
</html>