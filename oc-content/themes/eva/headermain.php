<?php
/*
* Copyright 2018 osclass-pro.com and osclass-pro.ru
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/

$image_dir = osc_base_path().'oc-content/themes/eva/img/main/*';
$images = glob($image_dir);
foreach($images as $image){$img = basename($image);}

$currentCity = GeoIP::newInstance()->getCurrentCity();
$currentCityName = !empty($currentCity['s_name']) ? $currentCity['s_name'] : 'Выбрать город';

?>
<!-- container -->
<div class="wrapper">
    <?php if ( OSC_DEBUG || OSC_DEBUG_DB ) { ?>
        <div id="maintenance"><?php _e('You have enabled DEBUG MODE, autocomplete for locations will not work! Disable it in your config.php.', 'eva'); ?></div>
    <?php } ?>
    <div class="wrapper-in">
        <header class="header-page header-page--paralax-bg" style="<?php if( $img != '') {?>background-image:url(<?php echo osc_base_url(); ?>oc-content/themes/eva/img/main/main.jpg) ;<?php } ?> center top;">
            <div class="top-bar">
                <div class="container">
                    <div class="mobile-menu-trigger">
                            <i></i>
                            <i></i>
                            <i></i>
                    </div>
                    <div class="top-bar__logo-wrp">
                        <a href="<?php echo osc_base_url();?>"><?php echo logo_header(); ?></a>
                        <!-- <a href="/" class="short-search-trigger"><i class="search-black-ico"></i></a>
                        <form class="nocsrf short-search-form" action="<?php echo osc_base_url(true); ?>" method="post" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                            <input type="hidden" name="page" value="search"/>
                            <input type="text" name="sPattern" placeholder="<?php echo osc_esc_html(osc_get_preference('keyword_placeholder', 'eva')); ?>" class="input-search" id="search-example">
                            <input type="submit" value="" class="submit-search">
                        </form> -->
                    </div>
                    <!-- Menu -->
                    <div class="top-bar__left disleft">
                        <!-- <a id="button-select-city" href="javascript:void()" class="upcase">?= $currentCityName ?></a> -->
                        <!-- <a href="/" class="short-search-trigger"><i class="search-ico"></i></a> -->
                        <form class="nocsrf short-search-form" action="<?php echo osc_base_url(true); ?>" method="post" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                            <input type="hidden" name="page" value="search"/>
                            <input type="text" name="sPattern" placeholder="<?php echo osc_esc_html(osc_get_preference('keyword_placeholder', 'eva')); ?>" class="input-search" id="search-example">
                            <input type="submit" value="" class="submit-search">
                        </form>
                    </div>
                    <div class="top-bar__action">
                        <?php if ( osc_count_web_enabled_locales() > 1) { ?>
                            <?php osc_goto_first_locale() ; ?>
                            <div class="lang-list select-country select_font" id="select-country__wrap" >
                                <form name="select_language" action="<?php echo osc_base_url(true);?>" method="post">
                                    <input type="hidden" name="page" value="language" />
                                    <select name="locale" id="select-country" >
                                        <?php while ( osc_has_web_enabled_locales() ) { ?>
                                            <option value="<?php echo osc_locale_code() ; ?>" <?php if(osc_locale_code() == osc_current_user_locale()){echo 'selected';} ?>><?php echo osc_locale_name() ; ?></option>
                                        <?php } ?>
                                    </select>
                                </form>
                            </div>
                        <?php } ?>
                        <?php if( osc_users_enabled() || ( !osc_users_enabled() && !osc_reg_user_post() )) { ?>
                            <a href="<?php echo osc_item_post_url_in_category(); ?>" class="btn-publish upcase"><strong><?php _e('Publish your ad', 'eva'); ?></strong></a>
                        <?php } ?>
                    </div>
                    <nav>
                        <ul class="upcase">
                            <!-- <li><a href="<?php echo osc_base_url(); ?>" <?php if(!Params::getParam('page')){echo 'class="active"';} ?>><strong><?php _e('Home', 'eva'); ?></strong></a></li> -->
                            <?php if(osc_users_enabled()) { ?>
                                <?php if( osc_is_web_user_logged_in() ) { ?>
                                    <li><a class="user-account__btn" href="<?php echo osc_user_dashboard_url(); ?>"><strong><i class="mdi mdi-account"></i><?php _e('My account', 'eva'); ?></strong></a></li>
                                    <!-- <li><a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'eva'); ?></a></li> -->
                                <?php } else { ?>
                                    <li><a class="user-account__btn" href="<?php echo osc_user_login_url(); ?>" data-fancybox="modal2" data-src="#insign"><strong><i class="mdi mdi-account"></i><?php _e('Sign in', 'eva'); ?></strong></a></li>
                                    <!-- <?php if(osc_user_registration_enabled()) { ?>
                                        <li><a href="<?php echo osc_register_account_url(); ?>"><strong><?php _e('Sign up', 'eva'); ?></strong></a></li>
                                    <?php } ?> -->
                                <?php } ?>
                            <?php } ?>
                        </ul>
                        
                    </nav>
                </div></div>
            <!--/.  Menu -->
            <div class="forcemessages-inline">
                <?php osc_show_flash_message(); ?>
            </div>
            <div class="header__ins">
                <div class="container">
                    <!--Header Text -->
                    <h1 class="upcase"><?php if( osc_get_preference('mainh1-evarevo', 'eva') != '') {
                            echo osc_get_preference('mainh1-evarevo', 'eva');
                        } ?></h1>
                    <p class="sub-h1-text"><?php if( osc_get_preference('maintext-evarevo', 'eva') != '') {?><?php echo osc_get_preference('maintext-evarevo', 'eva'); ?>
                        <?php } ?></p>
                    <!---/. Header Text -->
                    <?php osc_current_web_theme_path('templates/mainsearch/'.osc_get_preference('main-search', 'eva').'.php') ; ?>
                </div>
            </div>
            <style>
                .mobile-menu-trigger {
                        float: left;
                        display: inline-block;
                        margin-top: 25px;
                }
                .user-account__btn {
                    padding-top: 12px;
                    margin-top: 12px;
                    border-top: 2px solid transparent;
                    margin-right: 20px;
                }
                .user-account__btn:hover {
                    border-top: 2px solid #1531AE!important;
                    background-color: inherit!important;
                    color: #1531AE!important;
                }   
                @media only screen and (max-width: 1000px){
                    .nav-left {
                        display: none;
                    }
                    .mobile-menu-trigger {
                        float: left;
                    }
                    .widget-form__title .h2-bottom-line {
                        font-size: 20px;
                        margin-top: 30px;
                    }
                    /*.top-bar__action {
                        margin-right: 70px;
                    }*/
                    .top-bar .short-search-trigger {
                        display: block !important; 
                        position: absolute;
                        top: 25px;
                        right: 2%;
                        left: auto;
                    }
                }
                @media screen and (max-width: 700px) {
                    /*.top-bar .short-search-trigger {
                        top: 35px;
                    }
                    }
                    .d-block .short-search-trigger {
                        right: 20px;
                    }*/
                }
                @media screen and (max-width: 767px){
                    .top-bar__action {
                        margin-right: 0px;
                    }
                    .top-bar__left.disleft {
                        display: none;
                    }
                }
                @media screen and (max-width: 600px){ 
                    .top-bar__logo-wrp {
                        margin-left: 4%;
                    }
                }
                

                
            </style>
        </header>
