<?php
/*
* Copyright 2018 osclass-pro.com and osclass-pro.ru
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/
$user_keep = osc_user();
osc_enqueue_script('jquery-ui');
osc_enqueue_script('select');
osc_enqueue_script('owl');
if (osc_get_preference('gallery', 'eva') == 'swiper') {
    osc_enqueue_script('swiper');
} else {
    osc_enqueue_script('bxslider');
}
osc_enqueue_script('photoswipe');
osc_enqueue_script('photoswipe-ui');
osc_enqueue_script('main');
osc_enqueue_script('jssocials');
osc_enqueue_script('date');
osc_enqueue_script('jquery-validate');


// Get additional item data (meta fields)
$itemMetaData = osc_get_item_meta();
foreach ($itemMetaData as $meta) {
    switch ($meta['s_slug']) {
        case('date_event'):
            $metaEventDate = $meta['s_value'];
            break;
        case('telephone'):
            $metaPhone = $meta['s_value'];
            break;
        case('email'):
            $metaEmail = $meta['s_value'];
            break;
        case('metro'):
            $metaMetro = $meta['s_value'];
            break;
        case('workhours'):
            $metaWorkhours = $meta['s_value'];
            break;
    }
}

?>
<!DOCTYPE html>
<html lang="<?php echo str_replace('_', '-', osc_current_user_locale()); ?>">
<head>
    <?php osc_current_web_theme_path('head.php'); ?>
    <meta name="robots" content="index, follow"/>
    <meta name="googlebot" content="index, follow"/>
</head>
<body>
<?php osc_current_web_theme_path('header.php'); ?>
<div class="forcemessages-inline">
    <?php osc_show_flash_message(); ?>
</div>
<!-- content -->
<!-- new -->
<div class="col-wrp">
    <div class="col-main item-main">
        <div class="item-main-info">
            <div class="item-main-title">
                <div class="item-main-sutitle">
                    <h1 class="item-ins-name"><?php echo osc_esc_html(osc_item_title()); ?></h1><strong
                            class="item-ins__price"><?php if (osc_price_enabled_at_items() && osc_item_category_price_enabled()) {
                            echo osc_item_formated_price();
                        } ?> </strong>
                </div>
            </div>
            <div class="item-under-sutitle">
                <?php if (osc_get_preference('item-icon', 'eva') == 'enable') { ?>
                    <a href="<?php echo eva_item_category_url(osc_item_category_id()); ?>"
                       class="small_icon">
                                    <span class="item-ins__cat"><i
                                                class="mdi mdi-bookmark mdi-24px"></i><?php echo osc_item_category(); ?></span>
                    </a>
                <?php } ?>

                <?php if (!empty($metaEventDate)) { ?>
                    <span class="item-ins__date">
                        <i class="mdi mdi-calendar-text mdi-24px"></i>
                            <?php echo htmlentities(date(osc_date_format(), $metaEventDate), ENT_COMPAT, "UTF-8"); ?>
                    </span>
                <?php } ?>

                <span class="item-ins__view"><i class="mdi mdi-eye mdi-24px"></i><?php echo osc_item_views(); ?></span>
            </div>
            <?php if (osc_images_enabled_at_items()) { ?>
                <?php if (osc_count_item_resources() > 0) {
                    if (osc_get_preference('gallery', 'eva') == 'swiper') { ?>
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper picture" itemscope itemtype="http://schema.org/ImageGallery">
                                <?php for ($i = 1; osc_has_item_resources(); $i++) { ?>
                                    <figure class="swipdis swiper-slide" itemprop="associatedMedia" itemscope
                                            itemtype="http://schema.org/ImageObject">
                                        <a href="<?php echo osc_resource_url(); ?>" class="imgswipurl"
                                           itemprop="contentUrl"
                                           data-size="<?php echo osc_esc_html(osc_normal_dimensions()); ?>"
                                           data-index="<?php echo $i; ?>">
                                            <img src="<?php echo osc_resource_preview_url(); ?>" class="imgswipdis"
                                                 alt="<?php echo osc_esc_html(osc_item_title()); ?>"
                                                 itemprop="thumbnail">
                                        </a>
                                    </figure>
                                <?php } ?> </div>
                            <?php if (osc_count_item_resources() > 1) { ?>
                                <div class="swiper-button-next swiper-button-dis"></div>
                                <div class="swiper-button-prev swiper-button-dis"></div>
                            <?php } ?>
                        </div>
                        <?php osc_reset_resources(); ?>
                        <?php if (osc_count_item_resources() > 1) { ?>
                            <div class="swiper-container gallery-thumbs">
                                <div class="swiper-wrapper">
                                    <?php for ($i = 0; osc_has_item_resources(); $i++) { ?>
                                        <div class="swiper-slide slide_no"
                                             style="background-image:url(<?php echo osc_resource_thumbnail_url(); ?>)"></div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="slider bxslider picture" style="display:none" itemscope
                             itemtype="http://schema.org/ImageGallery">
                            <?php for ($i = 0; osc_has_item_resources(); $i++) { ?>
                                <div class="slider-item" class="photos">
                                    <figure itemprop="associatedMedia" itemscope
                                            itemtype="http://schema.org/ImageObject">

                                        <a href="<?php echo osc_resource_url(); ?>" itemprop="contentUrl"
                                           data-size="<?php echo osc_esc_html(osc_normal_dimensions()); ?>"
                                           data-index="<?php echo $i; ?>">
                                            <img src="<?php echo osc_resource_preview_url(); ?>"
                                                 alt="<?php echo osc_esc_html(osc_item_title()); ?>"
                                                 itemprop="thumbnail">
                                        </a>
                                    </figure>
                                </div>
                            <?php } ?> </div>
                        <div class="slider-thumbnails"></div>
                    <?php }
                }
            } ?></div>

        <div class="item-tabs">
            <a href="" id="to-reviews"></a>
            <div class="item-tab-control">
                <a href="" data-tab="1" class="tab-btn tab-btn-1 active"><i
                            class="mdi mdi-tooltip-text mdi-24px"></i><?php _e('Description', 'eva'); ?></a>
                <a href="" data-tab="2" id="dismap" class="tab-btn tab-btn-2"><i
                            class="mdi mdi-map-marker-radius mdi-24px"></i><?php _e('Map', 'eva'); ?></a>
                <a href="" data-tab="3" class="tab-btn tab-btn-3"><i
                            class="mdi mdi-comment-multiple-outline mdi-24px"></i><?php _e('Отзывы', 'eva'); ?></a>
            </div>
            <div class="tab active" data-tab="1">
                <div class="text">
                    <h3><?php _e('Description', 'eva'); ?></h3>
                    <?php echo external_links_make_clickable(osc_item_description()); ?>
                    <?php if (osc_count_item_meta() >= 1) { ?>

                        <div class="meta_list">
                            <?php $class = "even"; ?>
                            <?php while (osc_has_item_meta()) { ?>
                                <?php if (osc_item_meta_value() != '') {
                                    if (osc_item_meta_name() === 'Телефон' && !empty($metaMetro)) {
                                        continue;
                                    }
                                    if (osc_item_meta_name() === 'E-mail' && !empty($metaMetro)) {
                                        continue;
                                    }
                                    if (osc_item_meta_name() === 'Метро' && !empty($metaMetro)) {
                                        continue;
                                    }
                                    if (osc_item_meta_name() === 'Режим работы' && !empty($metaMetro)) {
                                        continue;
                                    }
                                    if (osc_item_meta_name() == 'Соцсети') {
                                        echo("
                                                <div class='meta " . $class . "'>
                                                    <i class='mdi'></i>
                                                    <strong> " . osc_item_meta_name() . " </strong>
                                                        "  . external_links_make_clickable(str_replace(',', '<br>', osc_item_meta_value())) . "
                                                    </div>
                                        ");
                                        continue;
                                    }
                                    ?>
                                    <div class="meta <?php echo $class; ?>">
                                        <i class="mdi"></i>
                                        <?php

                                        ?>
                                        <strong><?php echo osc_item_meta_name(); ?>:</strong> <?php echo external_links_make_clickable(osc_item_meta_value()); ?>
                                    </div>
                                    <?php $class = ($class == 'even') ? 'odd' : 'even'; ?>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php osc_run_hook('item_detail', osc_item()); ?>
                    <?php if (osc_get_preference('item-evarevo_desc', 'eva') != '') { ?>
                        <div class="ads">
                            <div class="itemtextad">
                                <?php echo osc_get_preference('item-evarevo_desc', 'eva'); ?>
                            </div>
                        </div>
                    <?php } ?><?php osc_run_hook('show_review_form'); ?>
                </div>
            </div>

            <?php if (osc_comments_enabled()) { ?>
                <?php if (osc_reg_user_post_comments() && osc_is_web_user_logged_in() || !osc_reg_user_post_comments()) { ?>

                    <div class="tab" data-tab="3">
                        <h3><?php _e('Leave comments', 'eva'); ?></h3>
                        <ul id="comment_error_list"></ul>
                        <?php CommentForm::js_validation(); ?>
                        <form action="<?php echo osc_base_url(true); ?>" method="post" name="comment_form"
                              id="comment_form" class="comments-form">
                            <input type="hidden" name="action" value="add_comment"/>
                            <input type="hidden" name="page" value="item"/>
                            <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>"/>
                            <div class="input-row">
                                <?php if (osc_is_web_user_logged_in()) { ?>
                                    <input type="hidden" name="authorName"
                                           value="<?php echo osc_logged_user_name(); ?>"/>
                                    <input type="hidden" name="authorEmail"
                                           value="<?php echo osc_logged_user_email(); ?>"/>
                                <?php } else { ?>
                                    <div class="input-col">
                                        <input class="input" type="text" name="authorName" id="authorName"
                                               placeholder="<?php echo osc_esc_html(__('Your name', 'eva')); ?>">
                                    </div>
                                    <div class="input-col">
                                        <input type="text" class="input" name="authorEmail" id="authorEmail"
                                               placeholder="<?php echo osc_esc_html(__('Your e-mail*', 'eva')); ?>"
                                               required>
                                    </div>
                                <?php } ?>
                            </div>
                            <textarea class="textarea" id="body" name="body" rows="10"
                                      placeholder="<?php echo osc_esc_html(__('Comment*', 'eva')); ?>"
                                      required></textarea>
                            <input type="submit" value="<?php echo osc_esc_html(__('Send', 'eva')); ?>"
                                   class="btn-right submit upcase" id="comment_send">
                        </form>

                        <div class="list-comments">
                            <h3><?php _e('Comments', 'eva'); ?></h3>
                            <?php if (osc_count_item_comments() >= 1) { ?>
                                <?php while (osc_has_item_comments()) { ?>
                                    <div class="comment">
                                        <div class="comment__photo">
                                            <img src="<?php echo osc_current_web_theme_url('img/author.png'); ?>"
                                                 alt="comment">
                                        </div>
                                        <div class="comment__desc">
                                            <strong class="comment__name"><?php echo osc_comment_author_name(); ?></strong>
                                            <p><?php echo nl2br(osc_comment_body()); ?></p>
                                            <span class="comment__date"><?php echo osc_format_date(osc_comment_pub_date()); ?></span>
                                            <?php if (osc_comment_user_id() && (osc_comment_user_id() == osc_logged_user_id())) { ?>
                                                <p>
                                                    <a rel="nofollow" href="<?php echo osc_delete_comment_url(); ?>"
                                                       title="<?php echo osc_esc_html(__('Delete your comment', 'eva')); ?>"><?php _e('Delete', 'eva'); ?></a>
                                                </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="paginate" style="text-align: right;">
                                <?php echo osc_comments_pagination(); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="tab" data-tab="2">
                <?php if (osc_get_preference('item-map', 'eva') == 'enable') { ?>
                    <?php osc_current_web_theme_path('templates/map/item_map.php'); ?>
                <?php } else {
                    osc_run_hook('location');
                } ?>
                <!--								    <script>-->
                <!--$(document).ready(function() {-->
                <!--    $('#dismap').click(function(e) {-->
                <!--var center=map.getCenter();-->
                <!--  google.maps.event.trigger(map, "resize");-->
                <!--  map.setCenter(center);-->
                <!--    });-->
                <!--});-->
                <!--    </script>-->
                <?php if (osc_get_preference('item-map', 'eva') == 'enable') { ?>
                    <div id="map"></div>
                <?php } ?>
            </div>
            <div class="tab" data-tab="3" id="tab-3">

            </div>
        </div>

    </div>
    <aside class="col-right">
        <?php if (osc_get_preference('item-evarevo_image', 'eva') != '') { ?>
            <div class="sidead">
                <?php echo osc_get_preference('item-evarevo_image', 'eva'); ?>
            </div>
        <?php } ?>
        <div class="item-author">
            <div class="item-author__user">
                <?php if (function_exists('show_rating')) {
                    show_rating();
                } ?>
                <!-- <a href="<?php if (osc_item_user_id() != null) { ?><?php echo osc_user_public_profile_url(osc_item_user_id()); ?><?php } ?>" class="item-author__photo"><img src="<?php echo osc_current_web_theme_url('img/profile.jpg'); ?>" alt="profile"></a>
                            <a href="<?php if (osc_item_user_id() != null) { ?><?php echo osc_user_public_profile_url(osc_item_user_id()); ?><?php } ?>" class="item-author__name"><?php echo osc_item_contact_name(); ?></a> -->
            </div>
            <div class="item-author__details">
                <?php if (osc_item_user_id() <> 0) { ?>
                    <?php $user = User::newInstance()->findByPrimaryKey(osc_item_user_id()); ?>
                    <?php if ($user['s_phone_mobile'] != '') { ?>
                        <span class="item-author__phone"><i
                                    class="mdi mdi-phone-in-talk mdi-24px mdipad"></i><?php if (function_exists('eva_mobile_number')) {
                                eva_mobile_number();
                            } ?></span>
                    <?php }
                    if ($user['s_phone_land'] != '') { ?>
                        <span class="item-author__phone"><i
                                    class="mdi mdi-deskphone mdi-24px mdipad"></i><?php if (function_exists('eva_phone_number')) {
                                eva_phone_number();
                            } ?></span>
                    <?php } ?>
                    <?php if ($user['b_company'] == 1) { ?>
                        <span class="item-author__phone"><i
                                    class="mdi mdi-comment-account-outline mdi-24px mdipad"></i> <?php _e('Company', 'eva'); ?></span>
                    <?php } else { ?>
                        <span class="item-author__phone"><i
                                    class="mdi mdi-comment-account-outline mdi-24px mdipad"></i> <?php _e('User', 'eva'); ?></span>
                    <?php } ?>
                <?php } ?>
                <?php if (osc_item_show_email()) { ?>
                    <span class="item-author__phone"><i
                                class="mdi mdi-email mdi-24px mdipad"></i><?php echo osc_item_contact_email(); ?></span>
                <?php } ?>
                <?php
                $location_array = array(osc_item_region(), osc_item_city(), osc_item_city_area(), osc_item_address());
                $location_array = array_filter($location_array);
                $item_loc = implode(', ', $location_array);
                ?>

                <?php if (!empty($metaPhone)) { ?>
                    <span class="item-inline__phone_number ">
                        <i class="mdi mdi-phone-in-talk mdi-24px mdipad"></i>
                        <?php echo($metaPhone); ?>
                    </span>
                <?php } ?>

                <?php if (!empty($metaEmail)) { ?>
                    <span class="item-inline__phone_number">
                        <a href="mailto:<?php echo($metaEmail); ?>">
                            <i class="mdi mdi-email mdi-24px mdipad"></i>
                            <?php echo($metaEmail); ?>
                        </a>>
                    </span>
                <?php } ?>

                <?php if(!empty($metaWorkhours)) { ?>
                <span class="item-inline__phone_number item-inline__working_time">
                    <i class="mdi-alarm mdi mdi-24px mdipad"></i>
                        <span class="working-time">Режим работы: <br>
                            <span class="time">
                                <?php echo($metaWorkhours); ?>
                            </span>
                        </span>
                </span>
                <?php } ?>

                <?php if ($item_loc <> '') { ?>
                    <?php if (!empty(osc_item_city())) { ?>
                        <p class="item-author__adress">
                            <i class="mdi mdi-city mdi-24px mdipad"></i>
                            <span class="working-time">
                                <?php echo osc_item_city() ?>
                            </span>
                        </p>
                    <?php } ?>

                    <?php if (!empty(osc_item_address())) { ?>
                        <p class="item-author__adress adress">
                            <i class="mdi mdi-map-marker mdi-24px mdipad"></i>
                            <span class="working-time">
                                    <?php echo osc_item_address() ?>
                            </span>
                        </p>
                    <?php } ?>
                <?php } ?>

                <?php if (!empty($metaMetro)) { ?>
                    <span class="item-inline__metro">
                            <i class="mdi mdi-train mdi-24px mdipad"></i>
                                <?php echo($metaMetro); ?>
                        </span>
                <?php } ?>


            </div>
        </div>

        <div class="widget-form modalcontact" id="myModal">
            <div class="modal-content">
                <span class="closemodal">&times;</span>
                <?php if (osc_logged_user_id() != osc_user_id() && osc_logged_user_id() != 0) { ?>
                    <p>
                        <?php _e("It's your own listing, you can't contact the publisher.", 'eva'); ?>
                    </p>
                <?php } else if (osc_reg_user_can_contact() && !osc_is_web_user_logged_in()) { ?>
                    <p>
                        <?php _e("You must log in or register a new account in order to contact the publisher", 'eva'); ?>
                    </p>
                    <div class="person__name">
                        <a href="<?php echo osc_user_login_url(); ?>"
                           class="btn btn-primary"><?php _e('Login', 'eva'); ?></a>
                        <a href="<?php echo osc_register_account_url(); ?>"
                           class="btn btn-primary"><?php _e('Register for a free account', 'eva'); ?></a>
                    </div>
                <?php } else { ?>
                    <ul id="error_list"></ul>
                    <?php ContactForm::js_validation(); ?>
                    <form class="form" action="<?php echo osc_base_url(true); ?>" method="post" name="contact_form"
                          id="contact_form">
                        <span class="widget-form__title"><?php _e("Contact publisher", 'eva'); ?></span>
                        <?php osc_prepare_user_info(); ?>
                        <label>
                            <input type="text" class="input" name="yourName" id="yourName"
                                   placeholder="<?php echo osc_esc_html(__('Your name', 'eva')); ?>">
                        </label><label>
                            <input type="email" class="input" id="yourEmail" name="yourEmail"
                                   placeholder="<?php echo osc_esc_html(__('Your e-mail*', 'eva')); ?>" required>
                        </label> <label>
                            <input type="tel" class="input" id="phoneNumber" name="phoneNumber"
                                   placeholder="<?php echo osc_esc_html(__('Phone number', 'eva')); ?>">
                            <?php osc_run_hook('item_contact_form', osc_item_id()); ?>
                        </label><label>
                            <textarea class="textarea" id="message" name="message"
                                      placeholder="<?php echo osc_esc_html(__('Message*', 'eva')); ?>"
                                      required></textarea>
                        </label>
                        <div class="attach">
                            <?php if (osc_item_attachment()) { ?>
                                <label for="contact-attachment"><?php _e('Attachment', 'eva'); ?>
                                    (<?php _e('optional', 'eva'); ?>)</label>
                                <?php ContactForm::your_attachment(); ?>

                            <?php } ?></div>
                        <input type="hidden" name="action" value="contact_post"/>
                        <input type="hidden" name="page" value="item"/>
                        <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>"/>
                        <div class="center2 dismitem">
                            <?php osc_show_recaptcha(); ?>
                        </div>
                        <div class="button-post text-center">
                            <input type="submit" class="submit btn-center upcase"
                                   value="<?php echo osc_esc_html(__('Send', 'eva')); ?>">
                        </div>
                    </form>
                <?php } ?>
            </div>
        </div>
        <div class="item-info">

            <?php if (osc_is_web_user_logged_in() && osc_logged_user_id() == osc_item_user_id()) { ?>
                <div class="sidebar__form">
                    <div class="form-group">
                        <div class="row center sidebar_move">
                            <a href="<?php echo osc_item_edit_url(); ?>" rel="nofollow"
                               class="btn-publish upcase"><?php _e('Edit item', 'eva'); ?></a>

                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <?php if (osc_get_preference('mark-as', 'eva') == 'enable') { ?>
                    <div class="markform">
                        <form action="<?php echo osc_base_url(true); ?>" method="post" name="mask_as_form"
                              id="mask_as_form">
                            <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>"/>
                            <input type="hidden" name="as" value="spam"/>
                            <input type="hidden" name="action" value="mark"/>
                            <input type="hidden" name="page" value="item"/>

                            <select name="as" id="as" class="form-control" style="width: 100%">
                                <option> <?php _e("Mark as...", 'eva'); ?></option>
                                <option value="spam"><?php _e("Mark as spam", 'eva'); ?></option>
                                <option value="badcat"><?php _e("Mark as misclassified", 'eva'); ?></option>
                                <option value="repeated"><?php _e("Mark as duplicated", 'eva'); ?></option>
                                <option value="expired"><?php _e("Mark as expired", 'eva'); ?></option>
                                <option value="offensive"><?php _e("Mark as offensive", 'eva'); ?></option>
                            </select>
                        </form>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

        <div class="item-info social">
            <!-- <h5>Поделиться</h5> -->
            <a id="myBtn" class="btn-pink btn-full-width upcase"><strong><?php _e('Задать вопрос ', 'eva'); ?></strong></a>
            <?php watchlist(); ?>
            <!-- <a id="myBtn" class="btn-pink btn-full-width upcase"><strong><?php _e('Добавить в избранное ', 'eva'); ?></strong></a> -->
            <a id="myBtn" class="btn-pink btn-full-width upcase reviews-link" href="#to-reviews"
               data-tab="3"><strong><?php _e('Оставить отзыв', 'eva'); ?></strong></a>
            <a id="myBtn"
               class="btn-pink btn-full-width upcase"><strong><?php _e('Сообщить об ошибке', 'eva'); ?></strong></a>
        </div>
        <div class="item-info social social-links">
            <h5>Поделиться</h5>
            <!-- <div id="nicedis"></div>
            <script>
                $("#nicedis").jsSocials({
                    showLabel: false,
                    showCount: false,
                    shareIn: "popup",
                            shares: [{share: "facebook",
                        logo: "shared-fc"
                    },  {share:"twitter",
                      logo: "shared-tw"
                    },
                    {share:"pinterest",
                     logo: "shared-p"
                    },
                    {share:"linkedin",
                     logo: "shared-in"
                    },
                    {share:"email",
                     logo: "shared-g"
                    }]
                });
            </script> -->
        </div>


        <?php if (osc_get_preference('useful', 'eva') == 'enable') { ?>
            <div class="info-b">
                <h5><?php _e('Useful information', 'eva'); ?></h5>
                <ul>
                    <li><?php _e('Avoid scams by acting locally or paying with PayPal', 'eva'); ?></li>
                    <li><?php _e('Never pay with Western Union, Moneygram or other anonymous payment services', 'eva'); ?></li>
                    <li><?php _e('Don\'t buy or sell outside of your country. Don\'t accept cashier cheques from outside your country', 'eva'); ?></li>
                    <li><?php _e('This site is never involved in any transaction, and does not handle payments, shipping, guarantee transactions, provide escrow services, or offer "buyer protection" or "seller certification"', 'eva'); ?></li>
                </ul>
            </div>
        <?php } ?>
    </aside>
</div>
<!-- Related items -->
<?php if (function_exists('related_eva_start')) {
    related_eva_start();
} ?>
<!-- Related items -->
<?php if (osc_get_preference('item-evarevo_desc2', 'eva') != '') { ?>
    <div class="ads">
        <div class="container">
            <?php echo osc_get_preference('item-evarevo_desc2', 'eva'); ?>
        </div>
    </div>
<?php } ?>
</div>
<!-- content -->

</div>

<?php osc_current_web_theme_path('footer.php'); ?>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
    <style>
        /*.tab {
            display: block;
        }*/
        /*.review_main {
            display: none;
        }
        .review_main .active {
            display: block;*/
        }
        .review_ratings {
            font-size: 16px;
            width: 100%;
        }

        .item-author__details .item-inline__phone_number i {
            color: #1531AE !important;
        }

        .item-author__details .item-author__adress {
            margin-bottom: 10px;
            margin-top: 10px;
        }

        .item-author__details .item-inline__metro {
            color: #bdbcbc;
        }

        .item-author__details .item-inline__metro i {
            color: #1531AE !important;
            margin-top: 0px;
        }

        .item-author__details .item-inline__user_email {
            margin-top: 10px;
        }

        .item-tabs .even {
            background-color: inherit;
        }

        .item-info {
            padding-top: 10px;
        }

        .item-info .social {
            padding-top: 0px;
        }

        .social h5 {
            text-align: center;
            margin: 0 0 19px;
            margin-bottom: 0px;
            margin-top: 10px;
            font-size: 20px;
            color: #555;
        }

        .item-author {
            padding-bottom: 0px;
        }

        .btn-full-width {
            width: 89%;
            margin-top: 15px;
        }

        .watchlist {
            width: 89%;
            margin-top: 15px;
            background-color: #1531AE !important;
            font-size: 14px;
            border-radius: 2px;
            color: #fff;
            padding: 14px 10px;
            min-width: 200px;
            text-align: center;
            cursor: pointer;
            text-transform: uppercase;
            font-weight: 700;
            transition: all .2s;
        }

        .watchlist:hover {
            color: #fff !important;
            background-color: #6BE400 !important;
        }

        #tab-3 {
            padding-top: 0px;
        }

        #review_main {
            margin-top: 0px;
            border-top: 0px;
        }

        .item-inline__phone_number-second {
            margin-left: 30px;
        }

        /*.working-time {
            max-width: 85%;
        }*/
        .item-inline__working_time {
            display: flex;
            margin-top: 10px;
        }

        .ya-share2 {
            margin-top: 20px;
        }

        .item-author__details {
            padding-bottom: 18px;
        }

        .um-item-btn {
            display: inline-flex;
            align-items: center;
            width: 89%;
            font-weight: 700;
            font-size: 14px;
            border-radius: 0px;
            color: #fff;
            padding: 14px 10px;
            text-align: center;
            cursor: pointer;
            background-color: #1531AE !important;
            line-height: 1.3;
            transition: all .2s ease;
            margin-left: 10px;
        }

        .um-item-btn:hover {
            background-color: #6BE400 !important;
        }

        .item-author__adress.adress {
            display: flex;
        }

        .item__date {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            max-width: 100%;
        }

        .details {
            position: absolute;
            bottom: 15px;
        }

        .item__middle-desc {
            min-height: 208px;
        }

        .item__text {
            display: none;
        }
        .details {
            left: 0px;
            right: 0px;
        }
        .meta i {
            color: #1531AE !important;
        }
        .rating-hide {
            display: none;
        }
        .item-tabs .meta_list {
            display: flex;
            flex-direction: column;
        }
        .item-tabs .meta_list .meta:nth-child(1) {
            order: 5;
        }
        .item-tabs .meta_list .meta:nth-child(7) {
            order: 6;
        }
        .meta_list .meta i {
            margin-right: 5px;
        }
        @media screen and (max-width: 600px) {
            .details {
                bottom: 0px;
                left: 0px;
                right: 0px;
            }
        }
    </style>
    <script text="text/javascript">
        <?php if( osc_get_preference('gallery', 'eva') == 'swiper') { ?>
        $(document).ready(function () {
            var galleryTop = new Swiper('.gallery-top', {
                spaceBetween: 10,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
            $(".gallery-thumbs .swiper-wrapper div:first-child").addClass("active");
            <?php if( osc_count_item_resources() > 1 ) { ?>
            var galleryThumbs = new Swiper('.gallery-thumbs', {
                spaceBetween: 10,
                centeredSlides: false,
                slidesPerView: 5,
                touchRatio: 0.2,
                slideToClickedSlide: true,
            });
            $('.slide_no').each(function (i) {
                $(this).click(function (e) {
                    e.preventDefault();
                    var thumb = i;
                    galleryTop.slideTo(thumb, 1000, false);
                    $('.slide_no').removeClass('active');
                    $(this).addClass('active');

                });
            });
            galleryTop.controller.control = galleryThumbs;
            galleryThumbs.controller.control = galleryTop;

            <?php } ?>
        });

        <?php } else { ?>
        $(document).ready(function () {
            $('.slider').show().bxSlider({
                preloadImages: 'all'
            });
        });
        <?php } ?>
    </script>
    <script text="text/javascript">
        (function ($) {
            var $pswp = $('.pswp')[0];
            var image = [];

            $('.picture').each(function () {
                var $pic = $(this),
                    getItems = function () {
                        var items = [];
                        $pic.find('a').each(function () {
                            var $href = $(this).attr('href'),
                                $size = $(this).data('size').split('x'),
                                $width = $size[0],
                                $height = $size[1];

                            var item = {
                                src: $href,
                                w: $width,
                                h: $height
                            }

                            items.push(item);
                        });
                        return items;
                    }

                var items = getItems();

                $.each(items, function (index, value) {
                    image[index] = new Image();
                    image[index].src = value['src'];
                });

                $pic.on('click', 'figure', function (event) {
                    event.preventDefault();

                    var $index = $(this).index();
                    var options = {
                        index: $index,
                        bgOpacity: 0.7,
                        showHideOpacity: true
                    }

                    var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
                    lightBox.listen('gettingData', function (index, item) {

                        var img = new Image();
                        img.onload = function () {
                            item.w = this.width;
                            item.h = this.height;
                            lightBox.updateSize(true);
                        };
                        img.src = item.src;

                    });
                    lightBox.init();
                });
            });
        })(jQuery);
    </script>
    <script text="text/javascript">
        var modal = document.getElementById('myModal');

        var modalbtn = document.getElementById("myBtn");

        var modalspan = document.getElementsByClassName("closemodal")[0];

        modalbtn.onclick = function () {
            modal.style.display = "block";
        }

        modalspan.onclick = function () {
            modal.style.display = "none";
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        $("#review_main").detach().appendTo('#tab-3');
        $('.um-item-btn').detach().appendTo('.item-author__details').addClass('btn-pink btn-full-width upcase');
        $('.ya-share2').detach().appendTo('.social-links');

        $('.reviews-link').click(function (e) {
            e.preventDefault();
            $('.item-tab-control a').removeClass('active');
            $('.tab-btn.tab-btn-3').addClass('active');
            var tab_id = $(this).attr('data-tab');

            $('.tab').removeClass('active');
            $('.tab').each(function () {
                if ($(this).attr('data-tab') == tab_id) {
                    $(this).addClass('active');
                }
            });
            $('html,body').stop().animate({scrollTop: $('#to-reviews').offset().top}, 1000);
            e.preventDefault();


        });
        $('.meta_list .meta:nth-child(1)').children().first().addClass('mdi-tag');
        $('.meta_list .meta:nth-child(2)').children().first().addClass('mdi-calendar-text');
        $('.meta_list .meta:nth-child(3)').addClass('rating-hide');
        $('.meta_list .meta:nth-child(4)').children().first().addClass('mdi-web');
        $('.meta_list .meta:nth-child(5)').children().first().addClass('mdi-share');
        $('.meta_list .meta:nth-child(6)').children().first().addClass('mdi-view-list');
        $('.meta_list .meta:nth-child(7)').children().first().addClass('mdi-format-align-left');

    </script>

</body>
</html>
