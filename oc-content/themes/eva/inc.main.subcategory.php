<?php
/*
* Copyright 2016 osclass-pro.com
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/
?>
<?php
function drawSubcategory($category) {
    if ( osc_count_subcategories2() > 0 ) {
        osc_category_move_to_children();

        ?>
        <ul>
            <?php while ( osc_has_categories() ) { ?>
                <li><a class="category cat_<?php echo osc_category_id(); ?>" href="<?php echo osc_search_category_url(); ?>"><?php echo osc_category_name(); ?></a> <span>(<?php echo osc_category_total_items(); ?>)</span><?php drawSubcategory(osc_category()); ?></li>
            <?php } ?>
        </ul>

        <?php
        osc_category_move_to_parent();
    }

}
$total_categories   = osc_count_categories();
?>
<h2 class="h2-bottom-line">КАТАЛОГ ОРГАНИЗАЦИЙ</h2>
<div class="row categories categories-bg">
    <?php while ( osc_has_categories() ) { ?>
        <div class="col-sm-3 main-cat">
            <div class="category">
                <div class="head">
                    <!-- <div class="icon">
                        <a href="<?php echo osc_search_category_url(); ?>"><img src="<?php echo osc_current_web_theme_url('img/') . osc_category_id() .'.png' ?>" alt=""></a>
                    </div> -->
                    <div class="title">
                        <a href="<?php echo osc_search_category_url(); ?>"><?php if(strlen(osc_category_name()) > 25){ echo mb_substr(osc_category_name(), 0, 23,'UTF-8').'...'; } else { echo osc_category_name(); } ?></a>
                    </div>
                </div>
                <?php if ( osc_count_subcategories() > 0 ) { ?>
                    <div class="items">
                        <?php $num = 0; while ( osc_has_subcategories()) { ?>
                            <a class="subcategory-item" href="<?php echo osc_search_category_url() ; ?>">
                                <?= osc_category_name() ?>
                            </a>
                            <?php $num++; ?>
                        <?php } ?>
                    </div>
                    <?php View::newInstance()->_erase('subcategories') ; ?>
                <?php } ?>
            </div>
        </div>

    <?php } ?>
</div>
<style>
    .categories .h2-bottom-line {
        margin-top: 53px;
    }
    .categories .categories-bg{
        box-shadow: 0 0 16px rgba(109, 109, 109, 0.25);
        background-color: #fff;
        padding: 10px 20px 6px;
    }
    .categories .category .subcategory-item {
        font-style: 16px;
    }
    .categories .main-cat {
            line-height: 30px;
            padding-left: 10px;
        }
    @media screen and (max-width: 700px) {
        .categories .main-cat {
            line-height: 35px;
            width: 100%;
        }
    }
    @media screen and (max-width: 500px) {
        .categories .title {
            padding-left: 0px;
        }
    }
</style>
