<?php if( osc_count_items() == 0) { ?>
		<?php } else { ?>
	                <section class="carousel-section related-sec">
                    <div class="container">
                        <div class="carousel-section__ins">
		<h2 class="h2-bottom-line"><?php _e('Related Ads', 'eva'); ?></h2>
 <div class="carousel-wrp">
 <div class="carousel owl-carousel one-ca">
		<?php while ( osc_has_items() ) { ?>
<div class="item">
								<?php if( osc_images_enabled_at_items() ) { ?>
													<?php if( osc_count_item_resources() ) { ?>
									<a href="<?php echo osc_item_url() ; ?>" class="item__photo">
									<img src="<?php echo osc_resource_thumbnail_url(); ?>" alt="<?php echo osc_highlight(osc_item_title()); ?>">
										<?php } else { ?>
									<a href="<?php echo osc_item_url() ; ?>" class="item__photo">
									<img src="<?php echo osc_current_web_theme_url('img/no_photo.gif') ; ?>">
													<?php } ?>
												<?php } ?>
												<?php if( osc_item_is_premium() ) { ?>
									<div class="premium_label">
                                        <span class="item__favourites"><i class="mdi mdi-star-outline"></i><?php _e('Premium', 'eva'); ?></span>
                                    </div><?php } ?>
									<span class="purchased"><?php echo osc_format_date(osc_item_pub_date()); ?></span>
									<div class="overlay"></div>
									</a>
									 <?php if( osc_get_preference('item-icon', 'eva') == 'enable') {?>
										<div class="item__cat">
                                          <img src="<?php echo osc_current_web_theme_url('img/').eva_category_root(osc_item_category_id()).'.png'; ?>">
										   </div>
											<?php } ?>
									<div class="item__ins" id="<?php if(function_exists('upayments_get_class_color')){echo upayments_get_class_color(osc_item_id());}?>">
                                        <div class="item__middle-desc">
                                            <a href="<?php echo osc_item_url() ; ?>" class="item__title"><?php echo osc_highlight(osc_item_title()); ?></a>
                                            <!-- <div class="item__text">
                                                <div>?php echo osc_highlight(external_links_make_clickable(osc_item_description())); ?></div>
                                            </div> -->

                                            <span class="item__date"><?php if( osc_item_city()!= '' ) {?><i class="mdi mdi-city"></i><div class="adaptive-adress"><?php echo osc_item_city(); ?></div> <?php } ?></span>

                                            <span class="item__date"><?php if( osc_item_address()!= '' ) {?><i class="mdi mdi-map-marker"></i><div class="adaptive-adress"><?php echo osc_item_address() ; ?></div><?php } ?></span>

                                            <span class="item__date item-inline__metro"><?php if (osc_has_item_meta() != '') { ?>
                                                         <?php while (osc_has_item_meta()) { ?>
                                                             <?php if (osc_item_meta_name() == 'Метро' && !empty(osc_item_meta_value())) { ?>
                                                                 <i class="mdi mdi-train"></i>
                                                                 <?php
                                                                 echo osc_item_meta_value();
                                                             }
                                                         }
                                                     } ?>
                                                </span>
                                            <strong class="item__price"><?php if( osc_price_enabled_at_items() && osc_item_category_price_enabled(osc_item_category_id()) ) {?>
											<i class="mdi mdi-tag"></i> <?php echo osc_item_formated_price() ; } ?></strong>
											<div class="details">

                                        		<a href="<?php echo osc_item_url() ; ?>" class="btn-full-width searchbutton upcase">ПОДРОБНЕЕ</a>
                                        	</div>
                                        	<!-- <div class="details details-2">

                                        		<a href="<?php echo osc_item_url() ; ?>" class="btn-full-width searchbutton upcase"><i class="mdi-more mdi"></i></a>
                                        	</div> -->
                                        </div>
                                    </div>
                                </div>
		<?php } ?>
		</div>
                    </div>
		         </div>
                    </div>
                    <style>
                    	.purchased {
                    		display: none;
                    	}
                    	.adaptive-adress {
							display: inline;
						}
						.item-author__details .item-inline__user_email {
							display: none;
						}
						/*.details.details-2 {
							display: none;
						}
						.details {
							display: block;
						}*/
						.details a {
							padding-top: 6%;
							text-align: center;
						}
						.item-inline__metro i {
							color: #1531AE!important;
							margin-top: 10px;
						}
						@media screen and (max-width: 600px) {
							/*.details.details-2 {
								display: block;
							}
							.details {
								display: none;
							}*/
							.adaptive-adress {
								display: block;
							}
							/*.item {
								display: flex;
								flex-direction: row;
								width: 150%;
							}*/
							/*.item__photo {
								min-width: 30vw;
					    		max-width: 30vw;
					    		max-height: 160px;
							}*/
							/*.item__ins {
								width: 60vw;
								padding: 10px;
							}*/
							.item {
								width: 100%;
							}
							.item__middle-desc {
								text-align: left;
							}
							.item__cat {
								display: none;
							}
							.item__title {
								margin-bottom: 10px;
								margin-top: 10px;
							}
							.item__text {
								display: none;
							}
							.item__date {
								text-align: left;
								display: flex;
								margin-bottom: 10px;
							}
							.item__date i {
								margin-right: 5px;
							}
							.item-inline__metro i {
								color: #1531AE!important;
								padding-bottom: 0px;
								margin-top: 0px;
							}
							.details a {
								text-align: center;
							    margin-top: 20px;
							    margin-bottom: 20px;
							    margin-left: 10px;
							    padding-top: 5.5%;
							}
							.item__price {
								padding: 0px;
							}
						}
                    </style>
                </section>
		<?php } ?>
