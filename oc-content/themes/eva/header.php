<?php
/*
* Copyright 2018 osclass-pro.com and osclass-pro.ru
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/

$image_dir = osc_base_path().'oc-content/themes/eva/img/search/*';
$images = glob($image_dir);
foreach($images as $image){$img = basename($image);}

$currentCity = GeoIP::newInstance()->getCurrentCity();
$currentCityName = !empty($currentCity['s_name']) ? $currentCity['s_name'] : 'Выбрать город';

?>
<!-- container -->
<div class="wrapper">
    <div class="wrapper-in">
        <header class="header-page header-page--ins">
            <div class="top-bar">
                <div class="container">
                    <div class="mobile-menu-trigger">
                            <i></i>
                            <i></i>
                            <i></i>
                        </div>
                    <div class="top-bar__logo-wrp">
                        <a href="<?php echo osc_base_url();?>"><?php echo logo_header(); ?></a>
                    </div>

               
                       <!--  <a id="button-select-city" href="javascript:void()" class="upcase">?= $currentCityName ?></a> -->
                        <a href="/" class="short-search-trigger"><i class="search-black-ico"></i></a>
                        <form class="nocsrf short-search-form" action="<?php echo osc_base_url(true); ?>" method="post" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                            <input type="hidden" name="page" value="search"/>
                            <input type="text" name="sPattern" placeholder="<?php echo osc_esc_html(osc_get_preference('keyword_placeholder', 'eva')); ?>" class="input-search" id="search-example">
                            <input type="submit" value="" class="submit-search">
                        </form>

                    

                    <!-- Menu -->
                    <div class="top-bar__left disleft">
                        <!-- <a id="button-select-city" href="javascript:void()" class="upcase"><?= $currentCityName ?></a> -->
                        <!-- <a href="/" class="short-search-trigger"><i class="search-black-ico"></i></a>
                        <form class="nocsrf short-search-form" action="<?php echo osc_base_url(true); ?>" method="post" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                            <input type="hidden" name="page" value="search"/>
                            <input type="text" name="sPattern" placeholder="<?php echo osc_esc_html(osc_get_preference('keyword_placeholder', 'eva')); ?>" class="input-search" id="search-example">
                            <input type="submit" value="" class="submit-search">
                        </form> -->
                    </div>
                    <div class="top-bar__action">
                        <?php if ( osc_count_web_enabled_locales() > 1) { ?>
                            <?php osc_goto_first_locale() ; ?>
                            <div class="lang-list select-country select_font" id="select-country__wrap" >
                                <form name="select_language" action="<?php echo osc_base_url(true);?>" method="post">
                                    <input type="hidden" name="page" value="language" />
                                    <select name="locale" id="select-country" >
                                        <?php while ( osc_has_web_enabled_locales() ) { ?>
                                            <option value="<?php echo osc_locale_code() ; ?>" <?php if(osc_locale_code() == osc_current_user_locale()){echo 'selected';} ?>><?php echo osc_locale_name() ; ?></option>
                                        <?php } ?>
                                    </select>
                                </form>
                            </div>
                        <?php } ?>
                        <?php if( osc_users_enabled() || ( !osc_users_enabled() && !osc_reg_user_post() )) { ?>
                            <a href="<?php echo osc_item_post_url_in_category(); ?>" class="btn-publish upcase"><strong><?php _e('Publish your ad', 'eva'); ?></strong></a>
                        <?php } ?>
                    </div>
                    <?php if (!osc_is_home_page()): ?>
                        <nav class="nav-left">
                            <ul class="upcase">
                                <li>
                                    <a href="<?php echo osc_search_url(); ?>" id="button-categories">
                                        <strong class="mdi mdi-view-grid"></strong>
                                        <strong><?php _e('Каталог', 'eva'); ?></strong>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    <?php endif; ?>
                    <nav>
                        <ul class="upcase">
                            <!-- <li><a href="<?php echo osc_base_url(); ?>"><strong><?php _e('Home', 'eva'); ?></strong></a></li> -->
                            <?php if(osc_users_enabled()) { ?>
                                <?php if( osc_is_web_user_logged_in() ) { ?>
                                    <li><a class="user-btn" href="<?php echo osc_user_dashboard_url(); ?>" <?php if(Params::getParam('page') == 'user'){echo 'class="active"';} ?>><strong><i class="mdi mdi-account"></i><?php _e('My account', 'eva'); ?></strong></a></li>
                                    <!-- <li><a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'eva'); ?></a></li> -->
                                <?php } else { ?>
                                    <li><a class="user-btn" href="<?php echo osc_user_login_url(); ?>" <?php if(Params::getParam('page') == 'login'){echo 'class="active"';} ?> data-fancybox="modal2" data-src="#insign"><strong><i class="mdi mdi-account"></i><?php _e('Sign in', 'eva'); ?></strong></a></li>
                                    <!-- <?php if(osc_user_registration_enabled()) { ?>
                                        <li><a href="<?php echo osc_register_account_url(); ?>" <?php if(Params::getParam('page') == 'register'){echo 'class="active"';} ?>><strong><?php _e('Sign up', 'eva'); ?></strong></a></li>
                                    <?php } ?> -->
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </nav>
                </div></div>
            <!--/.  Menu -->
            <?php
            if (osc_is_search_page() && osc_get_preference('search-image', 'eva') == 'enable') { ?>
                <div class="h-block h-block-paralax" style="<?php if( $img != '') {?>background:url(<?php echo osc_base_url(); ?>oc-content/themes/eva/img/search/<?php echo $img; ?>) ;<?php } ?> center top;">
                    <div class="container">
                        <h1><?php echo search_title(); ?></h1>
                    </div>
                </div>
            <?php } ?>
            <?php if( osc_is_search_page() && osc_get_preference('search-map', 'eva') == 'enable') {?>
                <?php osc_current_web_theme_path('templates/map/search_map.php') ; ?>
            <?php } ?>
            <?php
            if (osc_is_search_page() && osc_get_preference('search-image', 'eva') == 'disable') { ?>
                <div class="d-block" >
                    <div class="container">
                        <h1>
                            <?php echo search_title(); ?>
                        </h1>
                        <div class="top-bar__left disleft">
                       <!--  <a id="button-select-city" href="javascript:void()" class="upcase">?= $currentCityName ?></a> -->
                        <!-- <a href="/" class="short-search-trigger open"><i class="search-black-ico"></i></a>
                        <form class="nocsrf short-search-form open" action="?php echo osc_base_url(true); ?>" method="post" ?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                            <input type="hidden" name="page" value="search"/>
                            <input type="text" name="sPattern" placeholder="?php echo osc_esc_html(osc_get_preference('keyword_placeholder', 'eva')); ?>" class="input-search" id="search-example">
                            <input type="submit" value="" class="submit-search">
                        </form> -->

                    </div>
                </div>
            <?php } ?>
            <style>
                .header-page .container {
                  position: relative;
                  z-index: 2;
                  /*max-width: 100%;*/
                  max-width: 1200px;
                  margin: 0px auto;
                  padding: 0px;
                }
                .d-block {
                    padding-top: 20px; 
                }
                .d-block h1 {
                  color: #444343;
                  font-size: 32px;
                  margin-top: 20px;
                  text-transform:uppercase;
                  /*display: inline-block;*/
                }
                .d-block .container .top-bar__left{
                    /*margin-left: 20px;*/
                    display: inline-block;
                    float: none;
                    padding-top: 0px;
                    /*margin-bottom: 20px;*/
                }
                .short-search-form {
                    right: 0px;
                }
                .d-block .container {
                        max-width: 1500px;
                }
                    }
                .d-block .short-search-trigger {
                    position: absolute;
                    right: 0px;
                    top: 30px;
                    display: none !important;
                }
                .mobile-menu-trigger {
                        float: left;
                        display: inline-block;
                        margin-top: 25px;
                    }
                .nav-left {
                    display: none;
                }
                .user-btn {
                    margin-right: 20px;
                }
                .top-bar .short-search-trigger {
                    display: block; 
                    position: absolute;
                    top: 25px;
                    right: 0px;
                    left: auto;
                }
                .top-bar__action {
                    margin-right: 40px;
                }
                .short-search-trigger.open {
                    display: none!important;
                }
                .nocsrf.short-search-form {
                    right: -10px;
                    top: 78px;
                }
                @media screen and (max-width: 1700px) {
                    .d-block .container {
                        max-width: 1200px;
                    }
                }
                @media screen and (max-width: 1200px) {
                    .d-block .container {
                        max-width: 1000px;
                    }
                }
                @media screen and (max-width: 1000px) {
                    .modalcontact {
                        padding-top: 0px;
                        padding-left: 0px;
                        margin-left: 0px;
                    }
                    .modal-content-adaptive {
                        margin-left: 0px;
                    }
                    .widget-form__title .h2-bottom-line {
                        font-size: 20px;
                        margin-top: 30px;
                    }
                    .top-bar .short-search-trigger {
                        display: block; 
                        position: absolute;
                        top: 25px;
                        right: 2%;
                        left: auto;
                    }
                    .d-block .container {
                        max-width: 768px;
                        margin: 0px 30px;
                    }
                    .d-block h1 {
                      padding-right: 35%;
                    }
                    .top-bar__action {
                        margin-right: 70px;
                    }
                    .top-bar__left.disleft {
                        display: none;
                    }

                }
                @media screen and (max-width: 767px) {
                    .d-block h1 {
                      padding-right: 0%;
                      margin-bottom: 60px;
                    }
                    .short-search-form.open {
                        top: 70px;
                        right: 0%;
                        left: 0%;
                    }
                    .top-bar__action {
                        margin-right: 0px;
                    }

                }
                @media screen and (max-width: 700px) {
                    .top-bar .short-search-trigger {
                        top: 35px;
                    }
                    .d-block .container {
                        max-width: 480px;
                    }
                    .d-block .short-search-trigger {
                        right: 20px;
                    }
                    .mobile-menu-trigger {
                        float: left;
                    }
                    .d-block .container {
                        max-width: 600px;
                    }
                }
                
                @media screen and (max-width: 500px) {
                    .short-search-form.open {
                        top: 20px;
                    }
                }
                /*@media only screen and (max-width: 1000px){
                    .upcase {
                        display: none;
                    }
                }*/
            </style>
            <script>

                $(document).mouseup(function (e){ // событие клика по веб-документу
                    var div = $(".short-search-form"); // тут указываем class элемента
                    if (!div.is(e.target) // если клик был не по нашему блоку
                        && div.has(e.target).length === 0) { // и не по его дочерним элементам
                        $('.short-search-form').removeClass('open'); // убираем класс "open"
                    }
                });

            </script>
        </header>
        <div class="container notmain <?php if(Params::getParam('page') == 'item' or Params::getParam('action') == 'pub_profile'){echo 'item-container';} ?>">
            <?php $breadcrumb = osc_breadcrumb('&raquo;', false); if( $breadcrumb != '') { ?>
                <?php echo $breadcrumb; ?>
            <?php } ?>
