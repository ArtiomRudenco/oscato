<?php
/*
* Copyright 2018 osclass-pro.com
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/

?>
<form id="custom_search" class="nocsrf form" action="<?php echo osc_base_url(true); ?>"
      method="post" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
    <input type="hidden" name="page" value="search"/>
    <div class="input-row">
        <div class="input-3-col">
            <select name="sCategory" id="sCategory" class="form-select-3" onchange="getSubCategories(this.value)"
                    data-placeholder="<?php echo osc_esc_html(__('Select a category...', 'eva')); ?>">
                <option value=""><?php _e('None', 'eva'); ?></option>
                <?php foreach (Category::newInstance()->toTree() as $category) { ?>
                    <option value="<?php echo $category['pk_i_id'] ?>"><?php echo $category['s_name'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="input-3-col">
            <select
                    id="sSubCategory"
                    name="sSubCategory"
                    data-placeholder="Выбрать подкатегорию...">
            </select>
        </div>
        <div>
            <div hidden>
                <select name="subcategories" id="subcategories" class="form-select-3"
                        data-placeholder="Выбрать подкатегорию...">
                    <option value=""><?php _e('None', 'eva'); ?></option>
                    <?php foreach (Category::newInstance()->toTree() as $category) { ?>
                        <option value="<?php echo $category['pk_i_id'] ?>"><?php echo $category['s_name'] ?></option>
                        <?php
                        if (isset($category['categories']) && is_array($category['categories']) && osc_get_preference('subcategories', 'eva') == 'enable')
                            CategoryForm::subcategory_select($category['categories'], null, __('Select a category...', 'eva'), 0);
                        ?>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="input-3-col">
            <div class="form-search-action">
                <input type="text" name="sPattern"
                       placeholder="<?php echo osc_esc_html(__(osc_get_preference('keyword_placeholder', 'eva'), 'eva')); ?>"
                       class="input-search">
                <input type="submit" value="" class="submit-search">
            </div>
        </div>
    </div>
</form>

<script>
    function getSubCategories(category) {
        let subCategories = document.getElementById('subcategories');
        var str = "<option value=\"\"><?php _e('None', 'eva'); ?></option>";
        for (let i = 0; i < subCategories.length; i++) {
            let parentId = null;
            parentId = subCategories.options[i].getAttribute('parent_id');
            if (parentId === category) {
                str += "<option value=" + subCategories.options[i].getAttribute('value') + ">" + subCategories.options[i].text + "</option>"
            }
        }

        document.getElementById("sSubCategory").innerHTML = str;
        $('#sSubCategory').attr('disabled', false);
    }

    $(document).ready(function () {
        $('#sSubCategory').attr('disabled', 'disabled');
    });

</script>

<style>
    .select2-container--default .select2-selection--single .select2-selection__placeholder {
        color: #000000;
    }
</style>