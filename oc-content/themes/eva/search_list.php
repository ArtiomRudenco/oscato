<?php
/*
* Copyright 2018 osclass-pro.com
*
* You shall not distribute this theme and any its files (except third-party libraries) to third parties.
* Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
*/
?>

<div class="list-item__inline">
    <div class="list">
        <?php osc_get_premiums(10);
        if (osc_count_premiums() > 0) {
            ?>
            <h2 class="h2-bottom-linesearch"><?php _e("Premium items", 'eva'); ?></h2>
            <?php while (osc_has_premiums()) { ?>
                <?php $index = 0;

                ?>
                <div class="item-list-main">
                    <div class="item-inline item-inline-main">
                        <?php if (osc_images_enabled_at_items()) { ?>
                        <?php if (osc_count_premium_resources()) { ?>
                        <a href="<?php echo osc_premium_url(); ?>" class="item-inline__img-wrp">
                            <img src="<?php echo osc_resource_thumbnail_url(); ?>"
                                 alt="<?php echo osc_highlight(osc_rus2url(osc_premium_title())); ?>">
                            <?php } else { ?>
                            <a href="<?php echo osc_premium_url(); ?>" class="item-inline__img-wrp">
                                <img src="<?php echo osc_current_web_theme_url('img/no_photo.gif'); ?>">
                                <?php } ?>
                                <div class="premium_label">
                                    <span class="item__favourites"><i
                                                class="mdi mdi-star-outline"></i><?php _e('Premium', 'eva'); ?></span>
                                </div>
                                <!-- <span class="purchased">?php echo osc_format_date(osc_premium_pub_date()); ?></span> -->
                                <div class="overlay"></div>
                            </a>
                            <?php } ?>

                            <div class="item-inline__ins"
                                 id="<?php if (function_exists('upayments_premium_get_class_color')) {
                                     echo upayments_premium_get_class_color(osc_premium_id());
                                 } ?>">
                                <div class="item-inline__ins__in">
                                    <div class="item-inline__desc">
                                        <?php if (osc_get_preference('item-icon', 'eva') == 'enable') { ?>
                                            <a href="<?php echo osc_premium_url(); ?>" class="item-inline__cat">
                                                <img src="<?php echo osc_current_web_theme_url('img/') . eva_category_root(osc_premium_category_id()) . '.png'; ?>">
                                            </a>
                                        <?php } ?>
                                        <a href="<?php echo osc_premium_url(); ?>"
                                           class="item-inline__title"><?php echo osc_highlight(osc_premium_title()); ?></a>
                                        <div class="item-inline__text">
                                            <p><?php echo osc_highlight(osc_premium_description()); ?></p>
                                        </div>
                                        <div class="item-inline__action">
                                            <span class="item-inline__place"><?php if (osc_premium_city() != '') { ?><i
                                                        class="mdi mdi-city"></i><?php } ?> <?php echo osc_premium_city(); ?></span>

                                            <span class="item-inline__price"><?php if (osc_price_enabled_at_items() && osc_item_category_price_enabled(osc_premium_category_id())) {
                                                    echo osc_premium_formated_price();
                                                } ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <?php
                $index++;
                if ($index == 10) {
                    break;
                }
            }
            ?>
        <?php } ?>
        <?php $i = 0; ?>
        <?php if (osc_count_items() > 0) { ?>
            <!-- <h2 class="h2-bottom-linesearch">?php _e("Latest items", 'eva'); ?></h2> -->
            <?php while (osc_has_items()) {
                $i++; ?>
                <div class="item-list-main">
                    <div class="item-inline    item-inline-main">
                        <?php if (osc_images_enabled_at_items()) { ?>
                        <?php if (osc_count_item_resources()) { ?>
                        <a href="<?php echo osc_item_url(); ?>" class="item-inline__img-wrp">
                            <img src="<?php echo osc_resource_thumbnail_url(); ?>"
                                 alt="<?php echo osc_highlight(osc_rus2url(osc_premium_title())); ?>">
                            <?php } else { ?>
                            <a href="<?php echo osc_item_url(); ?>" class="item-inline__img-wrp">
                                <img src="<?php echo osc_current_web_theme_url('img/no_photo.gif'); ?>">
                                <?php } ?>
                                <!-- <span class="purchased">?php echo osc_format_date(osc_item_pub_date()); ?></span> -->
                                <div class="overlay"></div>
                            </a>
                            <?php } ?>
                            <div class="item-inline__ins" id="<?php if (function_exists('upayments_get_class_color')) {
                                echo upayments_get_class_color(osc_item_id());
                            } ?>">
                                <div class="item-inline__ins__in">
                                    <div class="item-inline__desc">
                                        <?php if (osc_get_preference('item-icon', 'eva') == 'enable') { ?>
                                            <a href="<?php echo osc_item_url(); ?>" class="item-inline__cat">
                                                <img src="<?php echo osc_current_web_theme_url('img/') . eva_category_root(osc_item_category_id()) . '.png'; ?>">
                                            </a>
                                        <?php } ?>
                                        <a href="<?php echo osc_item_url(); ?>"
                                           class="item-inline__title"><?php echo osc_highlight(osc_item_title()); ?></a>


                                        <a href="<?php echo osc_item_url(); ?>" class="item-inline__title date"
                                           style="font-size: 15px; color: #bdbcbc;">
                                            <?php
                                            rating_average();

                                            ?>
                                            <?php if (!empty(osc_get_item_meta())) { ?>
                                                <?php
                                                $itemMetaData = osc_get_item_meta();
                                                foreach ($itemMetaData as $meta) {
                                                    if ($meta['s_slug'] == 'date_event') {
                                                        $eventDate = $meta['s_value'];
                                                    }
                                                }
                                                if (!empty($eventDate)) { ?>
                                                    <i class="mdi mdi-calendar-text"></i>
                                                    <?php echo htmlentities(date(osc_date_format(), $eventDate), ENT_COMPAT, "UTF-8");
                                                }
                                            } ?>
                                        </a>

                                        <div class="item-inline__text">
                                            <p><?php echo osc_highlight(external_links_make_clickable(osc_item_description())); ?></p>
                                        </div>
                                        <div class="item-inline__place-wrapper">
                                            <span class="item-inline__place"><?php if (osc_item_city() != '') { ?><i
                                                        class="mdi mdi-city"></i><?php } ?><?php echo osc_item_city(); ?>, <?php echo osc_item_address(); ?> </span>
                                            <span class="item-inline__metro"><?php if (osc_has_item_meta() != '') { ?>
                                                    <?php while (osc_has_item_meta()) { ?>
                                                        <?php if (osc_item_meta_name() == 'Метро' && !empty(osc_item_meta_value())) { ?>
                                                            <i class="mdi mdi-train"></i>
                                                            <?php
                                                            echo osc_item_meta_value();
                                                        }
                                                    }
                                                } ?>
                                                </span>
                                            <br>
                                            <span class="item-inline__phone_number"><?php if (osc_item_city() != '') { ?>
                                                    <a href=""><!-- <i class="mdi mdi-phone-in-talk"></i> -->
                                                        <!-- ?php echo osc_item_phone_number()() ; ?> --></a><?php } ?> <?php if (osc_item_city() != '') { ?>
                                                    <a href="tel:"><!-- ?php echo osc_item_phone_number()() ; ?> --></a><?php } ?></span>
                                        </div>
                                        <div class="item-inline__action">
                                            <strong class="item-inline__price"><?php if (osc_price_enabled_at_items() && osc_item_category_price_enabled(osc_item_category_id())) {
                                                    echo osc_item_formated_price();
                                                } ?></strong>
                                        </div>
                                        <div class="details">
                                            <a href="<?php echo osc_item_url(); ?>"
                                               class="btn-full-width searchbutton upcase">ПОДРОБНЕЕ</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>
                <?php if ($i == osc_get_preference('search-middle', 'eva') && osc_get_preference('search-middle', 'eva') !== '') { ?>
                    <div class="ads">
                        <div class="container">
                            <?php osc_run_hook('search_middle'); ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<style>
    .item-inline__place-wrapper {
        display: inline-flex;
        flex-direction: column;
    }

    .item-inline__action {
        display: inline-block;
    }

    .item-inline__place {
        padding-top: 5px;
    }

    /*.item-inline__text {
        height: 40px
    }*/
    .item-inline__img-wrp {
        /*width: 364px;
       height: 300px;
*/
        width: 270px;
        height: 300px;
    }

    .item-inline__phone_number, .item-inline__metro {
        font-size: 15px;
        display: block;
        letter-spacing: 0.9px;
        padding-top: 10px;
        color: #bdbcbc;
        float: left;
    }

    .item-inline__phone_number i, .item-inline__metro i {
        color: #1531AE !important;
    }

    .item-inline__ins {
        padding-bottom: 20px;
    }

    .item-inline__img-wrp img {
        width: 170% !important;
    }

    .details {
        display: inline-block;
        position: absolute;
        bottom: 0px;
        right: 38px;
    }

    .item-inline__desc .btn-full-width {
        width: auto;
        margin: 0px;
        text-align: center;
        vertical-align: center;
        padding-top: 7.5%;
    }

    .item-inline__title {
        margin: 0 0 10px;
    }

    .item-inline__title span.fa.fa-star{
        margin: 0 0 15px;
        font-size: 21px;
        color: #d6d6d6;
    }

    .item-inline__title span.fa.fa-star.checked{
        margin: 0 0 15px;
        font-size: 21px;
        color: #FFCC00;
    }


    i {
        padding-right: 5px;
    }

    .item-inline__title .date {
        font-size: 15px;
        color: #bdbcbc;
    }

    .adaptive-adress {
        display: inline;
    }

    @media screen and (max-width: 600px) {
        .item-inline__place-wrapper {
            display: flex;
        }

        .adaptive-adress {
            display: block;
        }

        .item-inline-main {
            display: flex;
            flex-direction: row;
            width: 100%;
        }

        .item-inline__img-wrp {
            min-width: 30vw;
            max-width: 30vw;
            max-height: 160px;
        }

        .container.notmain {
            margin: 10px !important;
        }

        .item-inline__ins {
            padding-bottom: 0px;
            width: 80vw;
        }

        .item-inline__ins__in {
            padding: 10px;
        }

        .item-inline__ins__in .item-inline__desc {
            margin-bottom: 0px;
            text-align: left;
        }

        .item-inline__cat {
            display: none;
        }

        .item-inline__title {
            margin-bottom: 10px;
            font-size: 18px;
        }

        .item-inline__title.date {
            font-size: 13px;
        }

        .item-inline__text {
            display: none;
        }

        .item-inline__desc .details {
            display: none;
        }

        .details {
            display: none !important;
        }

        .item-inline__desc .item-inline__action {
            display: none;
        }

        .item-inline__desc .item-inline__phone_number {
            display: none;
        }

        .item-inline__desc .item-inline__metro {
            display: block;
        }

        .item-inline__place {
            font-size: 13px;
            text-align: left;
            display: flex;
        }

        .item-inline__metro {
            font-size: 13px;
        }

        .item-inline__img-wrp img {
            width: 210% !important;
        }
    }


</style>
