<?php
 /*
 * Copyright 2019 osclass-pro.com and osclass-pro.ru
 *
 * You shall not distribute this theme and any its files (except third-party libraries) to third parties.
 * Rental, leasing, sale and any other form of distribution are not allowed and are strictly forbidden.
 */
?>
<?php
/*
Theme Name: eva
Theme URI: https://osclass-pro.com/
Description: Eva - Full Responsive Premium Theme.
Version: 1.2.0
Author: OSCLASS-PRO.COM
Author URI: https://osclass-pro.com
Widgets: footer1, footer2, footer3
Theme update URI: eva
*/

    function eva_theme_info() {
        return array(
            'name'        => 'eva',
            'version'     => '1.2.0',
            'description' => 'Osclass Eva Premium Theme',
            'author_name' => 'osclass-pro.com',
            'author_url'  => 'https://osclass-pro.com/',
            'locations'   => array('header', 'footer')
        );
    }

?>