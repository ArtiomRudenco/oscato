<?php

define('OSC_DEBUG', false);

/**
 * The base MySQL settings of Osclass
 */
define('MULTISITE', 0);

/** MySQL database name for Osclass */
define('DB_NAME', 'oscato');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'qwe12');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Table prefix */
define('DB_TABLE_PREFIX', 'oc_');

define('REL_WEB_URL', '/');

define('WEB_MAIN_DOMAIN', 'oscato');

define('WEB_PROTOCOL', 'https://');

define('WEB_PATH', WEB_PROTOCOL. WEB_MAIN_DOMAIN .'/');

define('WEB_PATH_CURRENT', WEB_PROTOCOL . $_SERVER['HTTP_HOST'] .'/');

define('WEB_PATH_CITY', $_SERVER['HTTP_HOST'] === WEB_MAIN_DOMAIN ? null : str_replace('.'.WEB_MAIN_DOMAIN, '', $_SERVER['HTTP_HOST']));
?>
