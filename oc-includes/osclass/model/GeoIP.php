<?php if ( !defined('ABS_PATH') ) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/**
 * Model database for GeoIP tables
 *
 * @package Osclass
 * @subpackage Model
 * @since unknown
 */
class GeoIP extends DAO
{
    private static $instance;

    /** @var array|null */
    private static $cityCached = null;

    /** @var array|null */
    private static $currentCity = null;

    public static function newInstance()
    {
        if (!self::$instance instanceof self ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @return array|null
     */
    public function findCity()
    {
        if (self::$cityCached) {
            return self::$cityCached;
        }

        $ip = self::getClientIp();
        if (!$ip) {
            return null;
        }

        $result = $this->dao->query("SELECT city_id FROM net_city_ip WHERE begin_ip >= INET_ATON('$ip') AND INET_ATON('$ip') <= end_ip LIMIT 1");
        if($result->numRows <= 0) {
            return null;
        }
        $row = $result->row();
        if (empty($row) || empty($row['city_id'])) {
            return null;
        }
        $cityId = $row['city_id'];

        $result = $this->dao->query("SELECT name_ru FROM net_city WHERE id = $cityId");
        if($result->numRows <= 0) {
            return null;
        }
        $row = $result->row();
        if (empty($row) || empty($row['name_ru'])) {
            return null;
        }
        $cityName = $row['name_ru'];

        $city = City::newInstance()->findByName($cityName);
        if (empty($city) || empty($city['pk_i_id'])) {
            return null;
        }

        self::$cityCached = $city;

        return $city;
    }

    public function getCurrentCity()
    {
        if (self::$currentCity) {
            return self::$currentCity;
        }
        if (!WEB_PATH_CITY) {
            return array();
        }
        self::$currentCity = City::newInstance()->findBySlug(WEB_PATH_CITY);
        return self::$currentCity;
    }

    /**
     * @return array|null
     */
    public function getCityToRedirect()
    {
        $currentCity = $this->getCurrentCity();
        if (!empty($currentCity['pk_i_id'])) {
            return null;
        }
        $city = osc_user_geo_city();
        return !empty($city) && !empty($city['pk_i_id']) ? $city : null;
    }

    /**
     * @return string|null
     */
    public static function getClientIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if (!empty($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return null;
    }
}
